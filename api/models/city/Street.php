<?php

namespace api\models\city;

use Yii;


class Street extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%street}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }


}
