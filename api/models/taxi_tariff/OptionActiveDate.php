<?php

namespace api\models\taxi_tariff;

use Yii;

/**
 * This is the model class for table "tbl_option_active_date".
 *
 * @property integer $date_id
 * @property integer $tariff_id
 * @property string $active_date
 * @property string $tariff_type
 *
 * @property TaxiTariff $tariff
 */
class OptionActiveDate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_option_active_date';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }
}
