<?php

namespace api\modules\v1\services;

use api\modules\v1\repositories\ClientRepository;
use api\modules\v1\repositories\OrderRepository;

class ClientService
{
    public $clientRepository;
    public $orderRepository;

    public function __construct(ClientRepository $clientRepository, OrderRepository $orderRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->orderRepository  = $orderRepository;
    }

    /**
     * @param $tenantId
     * @param $orderId
     * @param $like
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function setWorkerLikeByOrder($tenantId, $orderId, $like)
    {
        $order = $this->orderRepository->getOrderData($tenantId, $orderId);
        $this->setWorkerLike($order->client->id, $order->worker->id, $order->worker->positionId, $like);
    }

    private function setWorkerLike($clientId, $workerId, $positionId, $like)
    {
        if ($like) {
            $this->clientRepository->addWorkerLike($clientId, $workerId, $positionId);
        } else {
            $this->clientRepository->addWorkerDislike($clientId, $workerId, $positionId);
        }
    }
}
