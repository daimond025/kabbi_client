<?php

namespace api\modules\v1\repositories\dto;

class OrderAddressData
{
    public $city;
    public $street;
    public $house;
    public $lat;
    public $lon;
    public $phone;
    public $comment;
    public $confirmation_code;


    public function __construct($city, $street, $house, $lat, $lon, $phone, $comment, $confirmationCode)
    {
        $this->city              = $city;
        $this->street            = $street;
        $this->house             = $house;
        $this->lat               = $lat;
        $this->lon               = $lon;
        $this->phone             = $phone;
        $this->comment           = $comment;
        $this->confirmation_code = $confirmationCode;
    }
}
