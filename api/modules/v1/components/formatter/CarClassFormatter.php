<?php

namespace api\modules\v1\components\formatter;

use api\models\worker\Worker;
use yii\helpers\ArrayHelper;

class CarClassFormatter implements FormatterInterface
{
    public static function formatter($data, $options = [])
    {
        $lang     = ArrayHelper::getValue($options, 'lang', 'en');
        $distance = ArrayHelper::getValue($options, 'distance');
        $carInfo  = ArrayHelper::getValue($data, 'car.brand' . '') . ' '
            . ArrayHelper::getValue($data, 'car.model') . ' '
            . t('car', ArrayHelper::getValue($data, 'car.color', ''), [], $lang);
        $result   = [
            'driver_callsign' => ArrayHelper::getValue($data, 'worker.callsign', ''),
            'car_id'          => ArrayHelper::getValue($data, 'car.car_id', ''),
            'car_class'       => ArrayHelper::getValue($data, 'car.class_id', ''),
            'car_info'        => trim($carInfo),
            'car_number'      => ArrayHelper::getValue($data, 'car.gos_number'),
            'car_lat'         => ArrayHelper::getValue($data, 'geo.lat'),
            'car_lon'         => ArrayHelper::getValue($data, 'geo.lon'),
            'car_is_free'     => (integer)(ArrayHelper::getValue($data, 'worker.status') === Worker::STATUS_FREE),
            'degree'         => ArrayHelper::getValue($data, 'geo.degree'),
        ];


        if (!is_null($distance)) {
            $result['distance'] = (float)$distance;
        }

        return $result;
    }
}
