<?php

namespace api\modules\v1\actions;

use api\exceptions\NeedReauthException;
use api\models\bankCard\Card;
use api\models\client\Client;
use api\models\client\ClientHasCompany;
use api\models\order\Order;
use api\modules\v1\exceptions\BadParamsException;
use api\modules\v1\exceptions\InvalidPanException;
use api\modules\v1\exceptions\ResultNotFoundException;
use api\modules\v1\requests\UpdateOrderRequest;
use paymentGate\ProfileService;
use yii\rest\Controller;

/**
 * Редактирование заказа
 *
 * Class UpdateOrderAction
 *
 * @package api\modules\v1\actions
 * @property UpdateOrderRequest $request
 */
class UpdateOrderAction extends BaseApiAction
{
    protected $_clientId;
    protected $profileService;

    public function __construct($id, Controller $controller, ProfileService $profileService, array $config = [])
    {
        parent::__construct($id, $controller, $config);

        $this->profileService = $profileService;
    }

    protected function getRequest()
    {
        return new UpdateOrderRequest();
    }

    /**
     * @return bool
     * @throws BadParamsException
     * @throws InvalidPanException
     * @throws NeedReauthException
     * @throws \api\modules\v1\exceptions\EmptyDeviceIdException
     * @throws \api\modules\v1\exceptions\EmptyTenantIdException
     * @throws \api\modules\v1\exceptions\EmptyTypeClientException
     * @throws \api\modules\v1\exceptions\IncorrectlyAppIdException
     * @throws \api\modules\v1\exceptions\IncorrectlySignatureException
     * @throws \api\modules\v1\exceptions\MissingInputParamsException
     * @throws \api\modules\v1\exceptions\NeedUpdateAppVersionException
     * @throws \api\modules\v1\exceptions\UnsupportedTypeClientException
     * @throws \api\modules\v1\exceptions\UnsupportedVersionClientException
     */
    protected function beforeExecute()
    {
        if (parent::beforeExecute()) {
            // Получить информацию о клиенте
            if ($this->request->phone) {
                $client = Client::findByPhone($this->request->tenantId, $this->request->phone);
                if (!$client || $client->getIsBlackList()) {
                    throw new NeedReauthException();
                }
                $this->_clientId = $client->client_id;
            }

            // Проверка привязки клиента к заказу
            $existOrder = Order::find()
                ->where([
                    'order_id'  => $this->request->orderId,
                    'tenant_id' => $this->request->tenantId,
                    'client_id' => $this->_clientId,
                ])
                ->exists();

            if (!$existOrder) {
                throw new BadParamsException();
            }

            // Проверка наличия клиента в компании
            $existCompany = ClientHasCompany::find()
                ->where([
                    'client_id'  => $this->_clientId,
                    'company_id' => $this->request->companyId,
                ])
                ->exists();

            if ($this->request->payment === Order::PAYMENT_CORP_BALANCE && !$existCompany) {
                throw new BadParamsException();
            }
            // Проверка банковской карты
            if ($this->request->payment === Order::PAYMENT_CARD) {
                $card = new Card();
                try {
                    $profile = $this->profileService->getProfile($this->request->tenantId, $client->city_id);
                    if (!$card->isValid($profile, $this->_clientId, $this->request->pan)) {
                        throw new InvalidPanException();
                    }
                } catch (\Exception $ignore) {
                    throw new InvalidPanException();
                }
            }

            return true;
        }

        return false;
    }

    protected function execute()
    {
        $params = $this->getParams();

        $updatedOrder = app()->orderService->updateOrder($this->_clientId, $this->request->tenantId,
            $this->request->orderId, $this->request->requestId, $this->request->lang, $params);

        if (!$updatedOrder) {
            throw new ResultNotFoundException();
        }

        $this->response->setContent($this->request->requestId);
    }

    protected function getParams()
    {
        $params = [
            'payment' => $this->request->payment,
            'address' => $this->request->address,
        ];

        switch ($this->request->payment) {
            case Order::PAYMENT_CORP_BALANCE:
                $params['company_id'] = $this->request->companyId;
                break;

            case Order::PAYMENT_CARD:
                $params['pan'] = $this->request->pan;
        }

        return $params;
    }
}