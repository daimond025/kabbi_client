<?php

namespace api\modules\v1\requests;

use api\models\order\Order;

/**
 * Class GetOrderRouteRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property int $orderId - Ид филиала*
 * @property int $date - Дата
 */
class GetOrderRouteRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'orderId' => [
                'name' => 'order_id',
                'type' => self::TYPE_GET,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['orderId', 'required'],
            [
                'orderId',
                'exist',
                'targetClass'     => Order::className(),
                'targetAttribute' => ['orderId' => 'order_id', 'tenantId' => 'tenant_id'],
            ],
        ]);
    }


}