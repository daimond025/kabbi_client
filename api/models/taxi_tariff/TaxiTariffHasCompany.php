<?php

namespace api\models\taxi_tariff;



use api\models\client\Company;

class TaxiTariffHasCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxi_tariff_has_company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'company_id']);
    }

}
