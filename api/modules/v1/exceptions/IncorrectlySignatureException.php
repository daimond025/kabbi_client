<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class IncorrectlySignatureException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::INCORRECTLY_SIGNATURE;
        parent::__construct('', $code, null);
    }

}