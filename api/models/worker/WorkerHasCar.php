<?php

namespace api\models\worker;

use api\models\transport\car\Car;
use Yii;

/**
 * This is the model class for table "{{%worker_has_car}}".
 *
 * @property integer $has_position_id
 * @property integer $car_id
 *
 * @property Car $car
 * @property WorkerHasPosition $hasPosition
 */
class WorkerHasCar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_has_car}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['has_position_id', 'car_id'], 'required'],
            [['has_position_id', 'car_id'], 'integer'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasPosition()
    {
        return $this->hasOne(WorkerHasPosition::className(), ['id' => 'has_position_id']);
    }
}