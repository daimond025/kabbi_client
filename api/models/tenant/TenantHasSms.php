<?php

namespace api\models\tenant;

use Yii;
use api\models\city\City;


/**
 * Class TenantHasSms
 * @package api\models\tenant
 *
 * @property integer $id
 * @property integer $server_id
 * @property integer $tenant_id
 * @property string $login
 * @property string $password
 * @property integer $active
 * @property string $sign
 * @property integer $city_id
 * @property integer $default
 *
 */
class TenantHasSms extends \yii\db\ActiveRecord
{

    const IS_ACTIVE = 1;
    const IS_DEFAULT = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_has_sms}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }
}
