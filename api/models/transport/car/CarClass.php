<?php

namespace api\models\transport\car;

use api\models\worker\Position;
use Yii;
use api\models\transport\TransportType;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "{{%tbl_car_class}}".
 *
 * @property integer $id
 * @property string $name
 */
class CarClass extends \yii\db\ActiveRecord
{

    const SOURCE_CATEGORY = 'car';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_class}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'class_id' => 'ID',
            'class' => 'Class',
            'type_id' => 'Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransportType::className(), ['type_id' => 'type_id']);
    }

    public function getClass($lang)
    {
        return t(self::SOURCE_CATEGORY, $this->class, $lang);
    }

    public static function getList($type, $lang = 'ru')
    {
        $model = self::find()
            ->where([
                'type_id' => $type
            ])
            ->asArray()
            ->all();
        $result = [];

        foreach ($model as $item) {
            $result[$item['class_id']] = t(self::SOURCE_CATEGORY, $item['class'], [], $lang);
        }
        return $result;
    }

}
