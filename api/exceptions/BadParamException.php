<?php

namespace api\exceptions;

use api\components\taxiApiRequest\TaxiErrorCode;

class BadParamException extends \Exception
{

    protected $code = TaxiErrorCode::BAD_PARAM;
    protected $message = 'BAD_PARAM';

}
