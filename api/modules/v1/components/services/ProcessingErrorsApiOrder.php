<?php

namespace api\modules\v1\components\services;

use api\components\taxiApiRequest\TaxiErrorCode;

class ProcessingErrorsApiOrder
{

    protected $statusCode;
    protected $message;
    protected $errors;

    /** @var TaxiErrorCode */
    public $taxiErrorCodes;

    public $errors422 = [
        OrderError::MISSING_PARAMETER => TaxiErrorCode::MISSING_INPUT_PARAMETER,
        OrderError::BAD_PARAM         => TaxiErrorCode::BAD_PARAM,
    ];

    public $errors400 = [
        OrderError::BLACK_LIST                    => TaxiErrorCode::BLACK_LIST,
        OrderError::BAD_PAY_TYPE                  => TaxiErrorCode::BAD_PAY_TYPE,
        OrderError::NO_MONEY                      => TaxiErrorCode::NO_MONEY,
        OrderError::INVALID_PAN                   => TaxiErrorCode::INVALID_PAN,
        OrderError::INVALID_PROMO_CODE            => TaxiErrorCode::INVALID_PROMO_CODE,
        OrderError::BONUS_SYSTEM_IS_NOT_ACTIVATED => TaxiErrorCode::BONUS_SYSTEM_IS_NOT_ACTIVATED,
    ];

    public function setResponse($response)
    {

        $this->statusCode = $response['status'];
        $this->message    = $response['message'];
        $this->errors     = $response['errors'];

        $this->taxiErrorCodes = new TaxiErrorCode();
    }

    public function getCodeError()
    {

        switch ($this->statusCode) {
            case 422:
                return $this->parseError422();

            case 400:
                return $this->parseError400();

            default:
                return TaxiErrorCode::INTERNAL_ERROR;
        }
    }

    protected function parseError422()
    {
        if (is_array($this->errors) && array_key_exists(current($this->errors)[0], $this->errors422)) {
            $errorCode = $this->errors422[current($this->errors)[0]];

            return $errorCode;
        }

        return TaxiErrorCode::INTERNAL_ERROR;
    }

    protected function parseError400()
    {
        if (array_key_exists((int)$this->message, $this->errors400)) {
            $errorCode = $this->errors400[$this->message];

            return $errorCode;
        }

        return TaxiErrorCode::INTERNAL_ERROR;
    }
}
