<?php

namespace api\models\taxi_tariff;

use yii\db\ActiveRecord;

/**
 * Class TaxiTariffActiveDate
 * @package api\models\taxi_tariff
 *
 * @property integer        $date_id
 * @property integer        $type_id
 * @property string         $active_date
 *
 * @property TaxiTariffType $tariffType
 */
class TaxiTariffActiveDate extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_option_active_date}}';
    }

    public function getTariffType()
    {
        return $this->hasOne(TaxiTariffType::className(), ['type_id' => 'type_id']);
    }
}