<?php

return array_map(function ($variable) {    return "!_SERVER.{$variable}";
}, array_keys(array_filter($_ENV, function ($value, $key) {
    return mb_stripos($key, 'password') !== false
        || mb_stripos($key, 'username') !== false;
}, ARRAY_FILTER_USE_BOTH)));


