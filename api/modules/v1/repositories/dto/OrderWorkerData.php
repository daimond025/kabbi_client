<?php

namespace api\modules\v1\repositories\dto;

class OrderWorkerData
{
    public $id;
    public $lastName;
    public $name;
    public $secondName;
    public $callsign;
    public $phone;
    public $photo;
    public $rating;
    public $positionId;

    public $lat;
    public $lon;
    public $degree;
    public $speed;

    public $car;


    public function __construct(
        $id,
        $lastName,
        $name,
        $secondName,
        $callsign,
        $phone,
        $photo,
        $rating,
        $positionId,
        $lat,
        $lon,
        $degree,
        $speed,
        $car,
        $company
    ) {
        $this->id         = (integer)$id;
        $this->lastName   = $lastName;
        $this->name       = $name;
        $this->secondName = $secondName;
        $this->callsign   = $callsign;
        $this->phone      = $phone;
        $this->photo      = $photo;
        $this->rating     = (float)$rating;
        $this->positionId = (integer)$positionId;
        $this->lat        = (float)$lat;
        $this->lon        = (float)$lon;
        $this->degree     = (float)$degree;
        $this->speed      = (float)$speed;
        $this->car        = $car;
        $this->company        = $company;
    }
}
