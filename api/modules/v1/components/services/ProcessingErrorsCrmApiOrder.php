<?php

namespace api\modules\v1\components\services;

use api\components\taxiApiRequest\TaxiErrorCode;
use api\modules\v1\exceptions\BlackListException;
use api\modules\v1\exceptions\InternalErrorException;

class ProcessingErrorsCrmApiOrder
{
    protected $statusCode;
    protected $message;
    protected $errors;

    /** @var TaxiErrorCode */
    public $taxiErrorCodes;

    public $errors422 = [];

    public $errors400 = [
        OrderError::BLACK_LIST => TaxiErrorCode::BLACK_LIST,
    ];

    public function setResponse($response)
    {

        $this->statusCode = $response['status'];
        $this->message    = $response['message'];
        $this->errors     = $response['errors'];

        $this->taxiErrorCodes = new TaxiErrorCode();
    }

    public function testCodeError()
    {

        switch ($this->statusCode) {
            case 422:
                $this->parseError422();
                break;

            case 400:
                $this->parseError400();
                break;

            default:
                throw new InternalErrorException();
        }
    }

    protected function parseError422()
    {
        if (is_array($this->errors) && array_key_exists(current($this->errors)[0], $this->errors422)) {
            throw new InternalErrorException();
        }

        throw new InternalErrorException();
    }

    protected function parseError400()
    {
        if (array_key_exists((int)$this->message, $this->errors400)) {
            throw new BlackListException();
        }

        throw new InternalErrorException();
    }
}
