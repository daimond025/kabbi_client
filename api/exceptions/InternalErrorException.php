<?php

namespace api\exceptions;

use api\components\taxiApiRequest\TaxiErrorCode;

class InternalErrorException extends \Exception
{

    protected $code = TaxiErrorCode::INTERNAL_ERROR;
    protected $message = 'INTERNAL_ERROR';

}
