<?php

namespace api\components\serviceEngine;

use api\components\consul\exceptions\ConsulServiceException;
use yii\base\Object;
use Yii;

/**
 * Class ServiceEngine
 * @package app\components\serviceEngine
 */
class ServiceEngine extends Object
{
    const CONSUL_SERVICE_NAME = 'service_engine';

    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * Send order to service_engine
     *
     * @param $orderId
     * @param $tenantId
     *
     * @return bool
     */
    public function neworderAuto($orderId, $tenantId)
    {

        $params = [
            'order_id'  => (string)$orderId,
            'tenant_id' => (string)$tenantId,
        ];

        $response = $this->sendRequest('neworder_auto', $params);

        return isset($response['result']) && $response['result'] == 1;
    }


    /**
     * Get relevant workers to service_engine
     *
     * @param $tenantId
     * @param $orderId
     *
     * @return array
     */
    public function getRelevantWorkers($tenantId, $orderId)
    {
        $params = [
            'order_id'  => (string)$orderId,
            'tenant_id' => (string)$tenantId,
        ];

        $response = $this->sendRequest('get_relevant_workers', $params);

        if (!$response) {
            Yii::error('Connect Error');

            return [];
        }

        if ($response['error']) {
            Yii::error($response['error']);

            return [];
        }

        return $response['result']['workers'];
    }

    /**
     * Send request
     *
     * @param $method
     * @param $params
     *
     * @return bool|mixed
     */
    private function sendRequest($method, $params)
    {
        if (is_array($params)) {
            $params = http_build_query($params);
        }
        $url = $this->getUrl($method) . "?" . $params;
        if (!$url) {
            return false;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencode',
        ]);
        $result    = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }

    /**
     * Get url
     *
     * @param $method
     *
     * @return bool|string
     */
    private function getUrl($method)
    {
        try {
            return Yii::$app->get('consulService')->createMethodUrl(self::CONSUL_SERVICE_NAME, $method);
        } catch (ConsulServiceException $ex) {
            Yii::error('Ошибка получения location из consul  для:' . self::CONSUL_SERVICE_NAME . ". Ошибка: " . $ex->getMessage());

            return false;
        }
    }


}
