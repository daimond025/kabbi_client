<?php

namespace api\modules\v1\services;

use api\components\gearman\Gearman;
use api\models\order\OrderStatus;
use api\modules\v1\components\OrderStatusService;
use api\modules\v1\exceptions\OrderIsNotCompletedYetException;
use api\modules\v1\filters\OrderInfoStatusFilter;
use api\modules\v1\repositories\ClientRepository;
use api\modules\v1\repositories\dto\OrderCarData;
use api\modules\v1\repositories\OrderRepository;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class OrderService extends Object
{
    public $orderRepository;
    public $clientRepository;

    public function __construct(
        OrderRepository $orderRepository,
        ClientRepository $clientRepository,
        array $config = []
    ) {
        parent::__construct($config);

        $this->orderRepository  = $orderRepository;
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param $tenantId
     * @param $orderId
     * @param $positionId
     * @param $lang
     * @param $needCarPhoto
     * @param $needWorkerPhoto
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function getOrderInfo($tenantId, $orderId, $positionId, $lang, $needCarPhoto, $needWorkerPhoto)
    {
        $ordersId   = [$orderId];
        $ordersInfo = $this->getOrdersInfo($tenantId, $ordersId, $positionId, $lang, $needCarPhoto, $needWorkerPhoto);

        return ArrayHelper::getValue($ordersInfo, 0, []);
    }

    /**
     * @param       $tenantId
     * @param array $orderIds
     * @param       $positionId
     * @param       $lang
     * @param       $needCarPhoto
     * @param       $needWorkerPhoto
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getOrdersInfo($tenantId, array $orderIds, $positionId, $lang, $needCarPhoto, $needWorkerPhoto)
    {
        $ordersData = $this->getOrdersData($tenantId, $orderIds);

        $responseOrdersData = [];
        foreach ($ordersData as $orderData) {
            $responseWorkerData = [];

            if ($worker = $orderData->worker) {
                $responseWorkerData = [
                    'car_description' => '',
                    'car_lat'         => $worker->lat,
                    'car_lon'         => $worker->lon,
                    'car_class_id'    => '',
                    'speed'           => $worker->speed,
                    'car_time'        => ceil($orderData->timeToClient / 60),
                    'degree'          => $worker->degree,
                    'driver_fio'      => trim("{$worker->lastName} {$worker->name} {$worker->secondName}"),
                    'driver_phone'    => $worker->phone,
                    'raiting'         => $worker->rating,
                    'car_photo'       => '',
                    'driver_photo'    => $worker->photo,
                    'company'    => $worker->company,
                ];

                if ($car = $worker->car) {
                    $responseWorkerData = ArrayHelper::merge($responseWorkerData, [
                        'car_description' => $this->getCarDescription($car, $lang),
                        'car_photo'       => $car->photo,
                    ]);
                }

                if($worker->car->class_id){
                    $responseWorkerData = ArrayHelper::merge($responseWorkerData, [
                        'car_class_id' => $worker->car->class_id,
                    ]);
                }



                if (!$needCarPhoto) {
                    unset($responseWorkerData['car_photo']);
                }

                if (!$needWorkerPhoto) {
                    unset($responseWorkerData['driver_photo']);
                }
            }

            $currentResponseOrdersData = [
                'order_id'       => $orderData->id,
                'order_number'       => $orderData->number,
                'status_id'          => $orderData->status->id,
                'status_group'       => $orderData->status->group,
                'status_name'        => $this->getStatusName(
                    $orderData->status->id,
                    $orderData->status->group,
                    $positionId,
                    $lang
                ),
                'status_description' => $this->getStatusDescription($orderData->status->id, $positionId, $lang),
                'payment'            => [
                    'type' => $orderData->payment->type,
                ],
                'tariff'             => $orderData->client_tariff,
                'predv_price'        => $orderData->prePrice,
                'order_time'         => $orderData->time,
                'time_offset'        => $orderData->timeOffset,
                'car_data'           => $responseWorkerData,
                'detail_cost_info'   => $orderData->detailCostInfo,
                'address'            => $orderData->address,
            ];

            (new OrderInfoStatusFilter())->execute($currentResponseOrdersData);

            $responseOrdersData[] = $currentResponseOrdersData;
        }

        return $responseOrdersData;
    }

    /**
     * @param       $tenantId
     * @param array $orderIds
     *
     * @return \api\modules\v1\repositories\dto\OrderInfoData[]
     * @throws \yii\base\InvalidConfigException
     */
    private function getOrdersData($tenantId, array $orderIds)
    {
        return $this->orderRepository->getOrdersData($tenantId, $orderIds);
    }

    private function getStatusName($statusId, $statusGroup, $positionId, $lang)
    {
        return OrderStatus::getFilterStatusLabel($statusId, $statusGroup, $positionId, true, $lang);
    }

    private function getStatusDescription($statusId, $positionId, $lang)
    {
        return OrderStatusService::translate($statusId, $positionId, $lang);
    }

    private function getCarDescription(OrderCarData $carData, $lang)
    {
        $color = t('car', $carData->color, [], $lang);
        return trim("$carData->name $color $carData->gosNumber");
    }

    /**
     * @param        $tenantId
     * @param        $orderId
     * @param        $grade
     * @param string $text
     *
     * @throws OrderIsNotCompletedYetException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function sendResponse($tenantId, $orderId, $grade, $text = '')
    {
        $order = $this->orderRepository->getOrderData($tenantId, $orderId);

        if (!ArrayHelper::isIn($order->status->group, [OrderStatus::STATUS_GROUP_4, OrderStatus::STATUS_GROUP_5])) {
            throw new OrderIsNotCompletedYetException();
        }

        $transaction = \Yii::$app->db->beginTransaction();

        $this->clientRepository->addClientReview($order, $grade, $text);
        $this->clientRepository->addClientReviewRating($order, $grade);

        \Yii::$app->get('gearman')->doBackground(Gearman::ORDER_STATISTIC, [
            'order_id' => $order->id,
            'type'     => 'feedback',
        ]);

        $transaction->commit();
    }
}
