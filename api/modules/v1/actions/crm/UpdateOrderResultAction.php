<?php

namespace api\modules\v1\actions\crm;

use api\modules\v1\actions\BaseApiAction;
use api\modules\v1\exceptions\ResultNotFoundException;
use api\modules\v1\requests\crm\UpdateOrderResultRequest;

/**
 * Проверка статуса редактирования заказа
 *
 * Class UpdateOrderResultAction
 *
 * @package api\modules\v1\actions\crm
 * @property UpdateOrderResultRequest $request
 */
class UpdateOrderResultAction extends BaseApiAction
{
    protected $_clientId;

    protected function getRequest()
    {
        return new UpdateOrderResultRequest();
    }


    protected function execute()
    {
        $response = app()->orderService->updateOrderStatus($this->request->updateId);
        $result = [];
        if (empty($response)) {
            throw new ResultNotFoundException();
        } else {

            if($response['code'] !== 100) {
               return $this->response->setContent(0);
            }

            $result['code']    = isset($response['code']) ? $response['code'] : null;
            $result['info']    = isset($response['info']) ? $response['info'] : null;
            $result['updated'] = isset($response['result']) ? $response['result'] : null;
        }

        return $this->response->setContent(1);
    }
}