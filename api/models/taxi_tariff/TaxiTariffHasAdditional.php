<?php

namespace api\models\taxi_tariff;

use api\models\transport\car\CarOption;
use yii\db\ActiveRecord;

/**
 * Class TaxiTariffHasAdditional
 * @package api\models\taxi_tariff
 *
 * @property integer        $id
 * @property integer        $type_id
 * @property integer        $additional_option_id
 * @property float          $price
 *
 * @property TaxiTariffType $tariffType
 * @property CarOption      $option
 */
class TaxiTariffHasAdditional extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_has_additional}}';
    }

    public function getTariffType()
    {
        return $this->hasOne(TaxiTariffType::className(), ['type_id' => 'type_id']);
    }

    public function getOption()
    {
        return $this->hasOne(CarOption::className(), ['option_id' => 'additional_option_id']);
    }
}