<?php

namespace api\models\bankCard;

use api\components\curl\RestCurl;
use Yii;
use api\components\curl\exceptions\RestException;

/**
 * Model of bank card
 */
class Card extends \yii\base\Model
{
    const BASE_URL = 'v0/cards';

    /**
     * @var string
     */
    public $lang;

    private $url;

    public function init()
    {
        $this->url = rtrim(\Yii::$app->params['paymentGateApi.url'], '/');

        parent::init();
    }

    /**
     * Getting a list of bank cards
     *
     * @param string $profile
     * @param int    $clientId
     *
     * @return array
     */
    public function getCards($profile, $clientId)
    {
        $response = Yii::$app->restCurl->get(implode("/",
            [
                $this->url,
                self::BASE_URL,
                $profile,
                "client_{$clientId}",
            ]));

        return isset($response['result']) ? $response['result'] : [];
    }

    public function doAction($type, $tenantId, $profile, $clientId, $url, $postParams)
    {
        try {
            /* @var $httpClient RestCurl */
            $httpClient = Yii::$app->restCurl;
            $httpClient->setHeaders([
                'Accept-Language' => $this->lang,
            ]);

            switch ($type) {
                case CardPaymentLog::TYPE_ADD_CARD:
                    $response = $httpClient->post($url, $postParams);
                    break;
                case CardPaymentLog::TYPE_CHECK_CARD:
                    $response = $httpClient->put($url, $postParams);
                    break;
                case CardPaymentLog::TYPE_REMOVE_CARD:
                    $response = $httpClient->delete($url, $postParams);
                    break;
                default:
                    throw new \yii\base\NotSupportedException("Type of action is not supported by log");
            }
            $result = CardPaymentLog::RESULT_OK;
        } catch (RestException $ex) {
            $response  = [
                'message'  => $ex->getMessage(),
                'code'     => $ex->getCode(),
                'response' => $ex->getResponse(),
            ];
            $exception = $ex;
            $result    = CardPaymentLog::RESULT_ERROR;
        } catch (\Exception $ex) {
            $response  = [
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode(),
            ];
            $exception = $ex;
            $result    = CardPaymentLog::RESULT_ERROR;
        }

        if($type != CardPaymentLog::TYPE_CHECK_CARD )
        {
            CardPaymentLog::log($tenantId, $profile,null, $clientId, $type, $postParams, $response, $result);
        }


        if ($result == CardPaymentLog::RESULT_OK) {
            return $response;
        } else {
            Yii::error(implode("\n",
                [
                    "An error has occurred in step '{$type}'",
                    "Url: {$url}",
                    print_r($postParams, true),
                    print_r($response, true),
                ]), 'bank-card');
            throw $exception;
        }
    }

    /**
     * Binding bank card
     *
     * @param integer $tenantId
     * @param string  $profile
     * @param integer $clientId
     *
     * @throws RestException
     * @return array
     */
    public function create($tenantId, $profile, $clientId)
    {
        $url = implode("/",
            [
                $this->url,
                self::BASE_URL,
                $profile,
                "client_{$clientId}",
            ]);

        // empty array is bugfix for common\components\curl\Curl
        $postParams = ['empty' => 'empty'];

        return $this->doAction(CardPaymentLog::TYPE_ADD_CARD, $tenantId, $profile, $clientId, $url, $postParams);
    }

    /**
     * Check whether the bank card is binded
     *
     * @param integer $tenantId
     * @param string  $profile
     * @param integer $clientId
     * @param string  $orderId
     *
     * @throws RestException
     * @return array
     */
    public function check($tenantId, $profile, $clientId, $pan)
    {
        $url = implode("/",
            [
                $this->url,
                self::BASE_URL,
                $profile,
                "client_{$clientId}",
            ]);

        return $this->doAction(CardPaymentLog::TYPE_CHECK_CARD, $tenantId, $profile, $clientId, $url,
            compact('pan'));
    }


    public function check_review($tenantId, $profile, $clientId, $pan)
    {
        $url = implode("/",
            [
                $this->url,
                self::BASE_URL,
                $profile,
                "client_{$clientId}",
            ]);

        return $this->doAction(CardPaymentLog::TYPE_CHECK_CARD, $tenantId, $profile, $clientId, $url,
            compact('pan'));
    }

    /**
     * Delete bank card
     *
     * @param integer $tenantId
     * @param string  $profile
     * @param integer $clientId
     * @param string  $pan
     *
     * @throws RestException
     * @return boolean
     */
    public function delete($tenantId, $profile, $clientId, $pan)
    {
        $url = implode("/",
            [
                $this->url,
                self::BASE_URL,
                $profile,
                "client_{$clientId}",
                $pan,
            ]);

        return $this->doAction(CardPaymentLog::TYPE_REMOVE_CARD, $tenantId, $profile, $clientId, $url, []);
    }

    /**
     * Check whether bank card is valid
     *
     * @param string  $profile
     * @param integer $clientId
     * @param string  $pan
     *
     * @return boolean
     */
    public function isValid($profile, $clientId, $pan)
    {
        $cards = $this->getCards($profile, $clientId);

        return in_array($pan, $cards);
    }
}
