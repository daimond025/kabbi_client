<?php

namespace api\modules\v1\components;

use api\models\city\City;
use api\models\parking\Parking;
use api\models\taxi_tariff\TaxiTariff;
use api\models\taxi_tariff\TaxiTariffHasCity;
use api\models\tenant\mobileApp\MobileApp;
use api\models\tenant\mobileApp\MobileAppHasCity;
use api\models\tenant\TenantHasCity;
use api\models\tenant\TenantSetting;
use api\modules\v1\requests\BaseVersionedApiRequest;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class TenantService extends Object
{
    protected $_supportedCitiesByAppId = [];

    protected $_supportedCurrencyByAppId = [];

    protected $_supportedPhoneLineByAppId = [];

    protected $_supportedPolygonByAppId = [];

    protected $_tenantIdByAppId = [];
    protected $_tenantApiKey = [];

    protected $_tenantCurrencyId = [];


    public function getTenantCityListByAppId($appId, $typeClient, $prefix = '')
    {
        $supportedCities    = $this->getCitiesByAppId($appId);
        $supportedPolygons  = $this->getPolygonByAppId($appId);
        $supportedCurrency  = $this->getCurrencyByAppId($appId);
        $supportedPhoneLine = $this->getPhoneLineByAppId($appId);
        $tenantId           = $this->getTenantIdByAppId($appId);
        $citiesHasTariff    = $this->getCitiesByExistTaxiTariff($tenantId, $typeClient);
        $supportedCities    = array_intersect($supportedCities, $citiesHasTariff);

        $models = City::find()
            ->alias('city')
            ->joinWith([
                'hasTenant thc',
            ])
            ->where([
                'city.city_id'  => $supportedCities,
                'thc.tenant_id' => $tenantId,
                'thc.block'     => TenantHasCity::NOT_BLOCK,

            ])
            ->orderBy([
                'thc.sort'            => SORT_ASC,
                'city.name' . $prefix => SORT_ASC,
            ])
            ->all();


        $result = array_map(function ($model) use (
            $prefix,
            $supportedCurrency,
            $supportedPhoneLine,
            $supportedPolygons
        ) {

            /** @var $model City */

            $currency = ArrayHelper::getValue($supportedCurrency, $model->city_id);
            $phone    = ArrayHelper::getValue($supportedPhoneLine, $model->city_id);
            $polygon  = ArrayHelper::getValue($supportedPolygons, $model->city_id, '');

            return [
                'city_id'             => (string)$model->city_id,
                'city_name'           => (string)$model->{'name' . $prefix},
                'city_lat'            => (string)$model->lat,
                'city_lon'            => (string)$model->lon,
                'currency'            => (string)$currency,
                'phone'               => (string)$phone,
                'city_reseption_area' => $polygon,

            ];
        }, $models);

        return $result;
    }

    /**
     * Получить список поддерживаемых филиалов по ID мобильного приложения
     *
     * @param int $appId
     *
     * @return array
     */
    public function getCitiesByAppId($appId)
    {
        if (!array_key_exists($appId, $this->_supportedCitiesByAppId)) {
            $cities = MobileAppHasCity::find()
                ->alias('mhc')
                ->select(['city_id'])
                ->where([
                    'app_id' => $appId,
                ])
                ->indexBy('city_id')
                ->all();
            $cities = array_keys($cities);

            $this->_supportedCitiesByAppId[$appId] = (array)$cities;
        }

        return $this->_supportedCitiesByAppId[$appId];
    }

    public function getCitiesByExistTaxiTariff($tenantId, $typeClient)
    {
        $cities = TaxiTariffHasCity::find()
            //            ->alias('thc')
            ->joinWith('tariff t')
            ->where([
                't.tenant_id' => $tenantId,
                't.block'     => TaxiTariff::NOT_BLOCK,
            ]);

        if ($typeClient === BaseVersionedApiRequest::TYPE_CLIENT_WEB) {
            $cities->andWhere([
                't.enabled_site' => TaxiTariff::ENABLED,
            ]);
        } else {

            $cities->andWhere([
                't.enabled_app' => TaxiTariff::ENABLED,
            ]);
        }


        $cities = $cities->indexBy('city_id')->all();

        return array_keys($cities);
    }

    /**
     * Получить список поддерживаемых областей примема заказа филиалов по ID мобильного приложения
     *
     * @param int $appId
     *
     * @return array
     */
    public function getPolygonByAppId($appId)
    {
        if (!array_key_exists($appId, $this->_supportedPolygonByAppId)) {
            $cities    = $this->getCitiesByAppId($appId);
            $tenantId  = $this->getTenantIdByAppId($appId);
            $districts = Parking::find()
                ->where([
                    'type'      => Parking::TYPE_RESEPTION_AREA,
                    'tenant_id' => $tenantId,
                    'city_id'   => $cities,
                ])
                ->all();


            $polygons                               = ArrayHelper::map($districts, 'city_id', function ($district) {
                /** @var $district Parking */
                $polygon = json_decode($district->polygon);
                $polygon = !empty($polygon) ? $polygon->geometry->coordinates[0] : [];

                return $polygon;
            });
            $this->_supportedPolygonByAppId[$appId] = $polygons;
        }

        return $this->_supportedPolygonByAppId[$appId];
    }

    /**
     * Получить список поддерживаемых валют по ID мобильного приложения
     *
     * @param int $appId
     *
     * @return array
     */
    public function getCurrencyByAppId($appId)
    {
        if (!array_key_exists($appId, $this->_supportedCurrencyByAppId)) {
            $cities   = $this->getCitiesByAppId($appId);
            $tenantId = $this->getTenantIdByAppId($appId);

            $currencyList = TenantSetting::find()
                ->alias('setting')
                ->joinWith('currency currency')
                ->where([
                    'setting.tenant_id' => $tenantId,
                    'setting.city_id'   => $cities,
                    'setting.name'      => TenantSetting::SETTING_CURRENCY,
                ])
                ->indexBy('city_id')
                ->all();

            $this->_supportedCurrencyByAppId[$appId] = ArrayHelper::getColumn($currencyList, 'currency.code');
        }

        return $this->_supportedCurrencyByAppId[$appId];
    }

    /**
     * Получить список поддерживаемых телефонных линий по ID мобильного приложения
     *
     * @param int $appId
     *
     * @return array
     */
    public function getPhoneLineByAppId($appId)
    {
        if (!array_key_exists($appId, $this->_supportedPhoneLineByAppId)) {
            $cities   = $this->getCitiesByAppId($appId);
            $tenantId = $this->getTenantIdByAppId($appId);

            $phoneLineList = TenantSetting::find()
                ->alias('setting')
                ->where([
                    'setting.tenant_id' => $tenantId,
                    'setting.city_id'   => $cities,
                    'setting.name'      => TenantSetting::SETTING_PHONE,
                ])
                ->indexBy('city_id')
                ->all();

            $this->_supportedPhoneLineByAppId[$appId] = ArrayHelper::getColumn($phoneLineList, 'value');
        }

        return $this->_supportedPhoneLineByAppId[$appId];
    }

    /**
     * Получить ID арендатора по ID мобильного приложения
     *
     * @param int $appId
     *
     * @return int|null
     */
    public function getTenantIdByAppId($appId)
    {
        if (!array_key_exists($appId, $this->_tenantIdByAppId)) {
            $tenantId                       = MobileApp::find()
                ->select('tenant_id')
                ->where(['app_id' => $appId])
                ->scalar();
            $this->_tenantIdByAppId[$appId] = $tenantId ? (int)$tenantId : null;
        }

        return $this->_tenantIdByAppId[$appId];
    }

    /**
     * Получить apiKey по ID мобильного приложения
     *
     * @param int $appId
     *
     * @return int|null
     */
    public function getTenantApiKey($appId)
    {
        if (!array_key_exists($appId, $this->_tenantApiKey)) {
            $this->_tenantApiKey[$appId] = MobileApp::find()
                ->select('api_key')
                ->where([
                    'app_id' => $appId,
                    'active' => MobileApp::ACTIVE,
                ])
                ->scalar();
        }

        return $this->_tenantApiKey[$appId];
    }

    public function getActiveCityListByTenantId($tenantId, $prefix = '')
    {
        $models = City::find()
            ->alias('city')
            ->joinWith([
                'hasTenant thc',
            ])
            ->where([
                'thc.tenant_id' => $tenantId,
                'thc.block'     => TenantHasCity::NOT_BLOCK,

            ])
            ->orderBy([
                'city.sort'           => SORT_ASC,
                'city.name' . $prefix => SORT_ASC,
            ])
            ->all();

        $result = array_map(function ($model) use ($prefix) {
            /** @var $model City */
            return [
                'city_id'   => (string)$model->city_id,
                'city_name' => $model->{'name' . $prefix},
            ];
        }, $models);

        return $result;
    }


    public function getCurrency($tenantId, $cityId)
    {
        $key = $this->getKey($tenantId, $cityId);
        if (!ArrayHelper::keyExists($key, $this->_tenantCurrencyId)) {
            $this->_tenantCurrencyId[$key] = TenantSetting::find()
                ->select('value')
                ->where([
                    'tenant_id' => $tenantId,
                    'city_id'   => $cityId,
                    'name'      => TenantSetting::SETTING_CURRENCY,
                ])
                ->scalar();
        }

        return $this->_tenantCurrencyId[$key];
    }

    protected function getKey($tenantId, $cityId)
    {
        return $tenantId . '_' . $cityId;
    }

}