<?php

namespace api\modules\v1\components\services;


use api\components\taxiApiRequest\TaxiErrorCode;

class PromoCodeService
{
    public function findSuitablePromoCode($tenant_id, $clientId, $orderTime, $cityId, $position_id, $carClassId)
    {
        $promoCode = \Yii::$app->get('orderApi')->findSuitablePromoCode(
            $tenant_id, $clientId, $orderTime, $cityId, $position_id, $carClassId
        );

        if (($promoCode['code'] == TaxiErrorCode::OK) && array_key_exists('code_id', $promoCode['result'])) {
            $promoCodeId = $promoCode['result']['code_id'];
        } else {
            $promoCodeId = null;
        }

        return $promoCodeId;
    }

}