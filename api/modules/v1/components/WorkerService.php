<?php

namespace api\modules\v1\components;

use api\modules\v1\components\formatter\CarClassFormatter;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class WorkerService extends Component
{
    /**
     * @param       $tenantId
     * @param       $carIds
     * @param       $cityId
     * @param       $lang
     * @param       $lat
     * @param       $lon
     * @param       $radius
     * @param array $exceptCarModels
     * @param array $additionalOption
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getCars(
        $tenantId,
        $carIds,
        $cityId,
        $lang,
        $lat,
        $lon,
        $radius,
        $exceptCarModels = [],
        $additionalOption = []
    ) {
        $workersOnline = app()->get('redis_workers')->executeCommand('HVALS', [$tenantId]);

        if (!$workersOnline) {
            return [];
        }

        $result = [];



        foreach ($workersOnline as $workerOnline) {
            $data = unserialize($workerOnline);


            $currentClassId       = ArrayHelper::getValue($data, 'car.class_id');
            $currentLat           = ArrayHelper::getValue($data, 'geo.lat');
            $currentLon           = ArrayHelper::getValue($data, 'geo.lon');
            $carModelId           = ArrayHelper::getValue($data, 'car.model_id');
            $carAdditionalOptions = ArrayHelper::getColumn(
                (array)ArrayHelper::getValue($data, 'car.carHasOptions', []),
                'option_id'
            );



            if (!empty($carIds) && !ArrayHelper::isIn($currentClassId, $carIds)) {
                continue;
            }


            if ((ArrayHelper::getValue($data, 'worker.city_id') != $cityId) && ((int)$cityId !== 0))  {
                continue;
            }

            $distance = null;
            if ($lat && $lon && $radius) {
                $distance = $this->getDistanceToClient($lat, $lon, $currentLat, $currentLon, 'M');

                if ($distance > $radius) {
                    continue;
                }
            }

            if (ArrayHelper::isIn($carModelId, $exceptCarModels)) {
                continue;
            }

            if (array_diff($additionalOption, $carAdditionalOptions)) {
                continue;
            }

            $result[] = CarClassFormatter::formatter($data, ['lang' => $lang, 'distance' => $distance]);
        }

        return $result;
    }

    protected function getDistanceToClient($lat1, $lon1, $lat2, $lon2, $unit)
    {
        $theta = $lon1 - $lon2;
        $dist  = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2))
            * cos(deg2rad($theta));
        $dist  = acos($dist);
        $dist  = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit  = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else {
            if ($unit == "M") {
                $k = $miles * 1.609344;

                return (int)($k * 1000);
            } else {
                if ($unit == "N") {
                    return ($miles * 0.8684);
                } else {
                    return $miles;
                }
            }
        }
    }

    public function findAllInRedis($tenantId, $callsigns = null)
    {
        /** @var \yii\redis\Connection $redisWorkers */
        $redisWorkers = app()->get('redis_workers');

        $workers = $redisWorkers->executeCommand('HGETALL', [$tenantId]);

        $result = [];
        for ($i = 1; $i < count($workers); $i = $i + 2) {
            if ($callsigns !== null && !in_array($workers[$i - 1], $callsigns)) {
                continue;
            }
            $result[] = unserialize($workers[$i]);
        }

        return $result;
    }
}