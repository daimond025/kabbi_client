<?php

namespace api\modules\v1\requests;

use api\models\order\Order;
use api\models\worker\Worker;

/**
 * Class LikeWorkerRequest
 * Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property integer $workerCallsign- Идентификатор водителя *
 * @property integer $password  -- пароль водителя *
 */
class CheckWorkerPasswordRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'workerCallsign' => [
                'name' => 'workerCallsign',
                'type' => self::TYPE_POST,
            ],
            'password' => [
                'name' => 'password',
                'type' => self::TYPE_POST,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['workerCallsign', 'required'],
            ['password', 'required'],
            ['password', 'string'],
          /*  [
                'workerCallsign',
                'exist',
                'targetClass'     => Worker::class,
                'targetAttribute' => ['workerCallsign' => 'callsign', 'tenantId' => 'tenant_id'],
            ],*/
        ]);
    }
}
