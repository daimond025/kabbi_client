<?php

namespace api\modules\v1\requests;

use api\models\order\Order;

/**
 * Class SendResponseRequest
 * Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property integer $orderId - Идентификатор заказа *
 * @property integer $grade - Флаг. Получить фото автомобиля *
 * @property string  $text - Отзыв
 */
class SendResponseRequest extends BaseVersionedApiRequest
{
    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'orderId' => [
                'name' => 'order_id',
                'type' => self::TYPE_POST,
            ],
            'grade'   => [
                'name' => 'grade',
                'type' => self::TYPE_POST,
            ],
            'text'    => [
                'name' => 'text',
                'type' => self::TYPE_POST,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['orderId', 'required'],
            [
                'orderId',
                'exist',
                'targetClass'     => Order::className(),
                'targetAttribute' => ['orderId' => 'order_id', 'tenantId' => 'tenant_id'],
            ],

            ['grade', 'required'],
            ['grade', 'in', 'range' => range(1, 5)],

            ['text', 'string'],
        ]);
    }
}
