<?php

namespace api\modules\v1\filters;

use api\models\order\OrderStatus;
use yii\helpers\ArrayHelper;

class OrderInfoStatusFilter
{
    public function execute(array &$orderData)
    {
        $statusId    = ArrayHelper::getValue($orderData, 'status_id');
        $statusGroup = ArrayHelper::getValue($orderData, 'status_group');

        if ($statusGroup === OrderStatus::STATUS_GROUP_0
            && !ArrayHelper::isIn($statusId, [OrderStatus::WORKER_ASSIGNED_SOFT, OrderStatus::WORKER_ASSIGNED_HARD])) {
            $orderData['car_data'] = [];
        }
    }
}
