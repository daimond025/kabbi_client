<?php

namespace api\modules\v1\components\logger\target;

use yii\base\Component;

class FakeTarget extends Component implements LogTargetInterface
{
    /**
     * @var string
     */
    public $identity;

    public function export($message)
    {

    }
}