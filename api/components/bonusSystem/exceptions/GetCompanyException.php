<?php

namespace bonusSystem\exceptions;

use yii\base\Exception;

/**
 * Class GetCompanyException
 * @package bonusSystem\exceptions
 */
class GetCompanyException extends Exception
{

}