<?php

namespace api\modules\v1\requests;

use api\models\tenant\mobileApp\MobileApp;
use api\models\tenant\Tenant;
use api\modules\v1\components\ErrorCode;
use Ramsey\Uuid\Uuid;

/**
 * Class BaseApiRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property string $signature
 * @property int    $appId - Id мобильного приложения.
 * @property int    $tenantId - Id арендатора *
 * @property string $lang - Поддерживаемый язык клиента
 * @property string $typeClient - Тип устройства клиента *
 * @property string $versionClient - Версия приложения клиента
 * @property string $deviceId - Уникальный код устройства. (для андроид - udid, для ios - device_token) * - в
 *     зависимости от typeClient
 * @property string $requestId - Id запроса
 * @property int    $currentTime - Время запроса в UNIX time *
 */
class BaseApiRequest extends BaseRequest
{
    const TYPE_CLIENT_ANDROID = 'ANDROID';
    const TYPE_CLIENT_IOS = 'IOS';
    const TYPE_CLIENT_WEB = 'WEB';
    const TYPE_CLIENT_HOSPITAL = 'HOSPITAL';

    const DEFAULT_APP_VERSION = '1.26.12.1.1';

    public function attributes()
    {
        return [
            'signature'     => [
                'type' => self::TYPE_HEADER,
                'name' => 'signature',
            ],
            'appId'         => [
                'type' => self::TYPE_HEADER,
                'name' => 'appid',
            ],
            'tenantId'      => [
                'type' => self::TYPE_HEADER,
                'name' => 'tenantid',
            ],
            'lang'          => [
                'type'    => self::TYPE_HEADER,
                'name'    => 'lang',
                'default' => 'en',
            ],
            'typeClient'    => [
                'type' => self::TYPE_HEADER,
                'name' => 'typeclient',
            ],
            'versionClient' => [
                'type'    => self::TYPE_HEADER,
                'name'    => 'versionclient',
                'default' => '1.0.0',
            ],
            'deviceId'      => [
                'type' => self::TYPE_HEADER,
                'name' => 'deviceid',
            ],
            'requestId'     => [
                'type'    => self::TYPE_HEADER,
                'name'    => 'request-id',
                'default' => Uuid::uuid4()->toString(),
            ],
            'currentTime'   => [
                'type' => app()->request->isPost ? self::TYPE_POST : self::TYPE_GET,
                'name' => 'current_time',
            ],
        ];
    }

    public function rules()
    {
        return [

            ['tenantId', 'required', 'message' => ErrorCode::EMPTY_TENANT_ID],
            [
                'tenantId',
                'exist',
                'targetClass'     => Tenant::class,
                'targetAttribute' => ['tenantId' => 'tenant_id'],
                'message'         => ErrorCode::EMPTY_TENANT_ID,
            ],


            [
                'appId',
                'default',
                'value' => function ($model) {
                    $appId = MobileApp::find()
                        ->select('app_id')
                        ->where([
                            'tenant_id' => $model->tenantId,
                            'active'    => MobileApp::ACTIVE,
                        ])
                        ->orderBy(['sort' => SORT_ASC])
                        ->scalar();

                    return $appId;
                },
            ],
            ['appId', 'required', 'message' => ErrorCode::INCORRECTLY_APP_ID],
            [
                'appId',
                'exist',
                'targetClass'     => MobileApp::class,
                'targetAttribute' => ['appId' => 'app_id', 'tenantId' => 'tenant_id'],
                'filter'          => ['active' => MobileApp::ACTIVE],
                'message'         => ErrorCode::INCORRECTLY_APP_ID,
            ],

            [
                'typeClient',
                'required',
                'message' => ErrorCode::EMPTY_TYPE_CLIENT,
                'when'    => function () {
                    return gtoet('1.20.0');
                },
            ],

            [
                'typeClient',
                'filter',
                'filter' => function ($value) {
                    return mb_strtoupper($value);
                },
            ],
            ['typeClient', 'in', 'range' => self::getTypeClientList(), 'message' => ErrorCode::UNSUPPORTED_TYPE_CLIENT],

            ['versionClient', 'default', 'value' => '1.0.0'],
            //            ['versionClient', 'validatorVersionClient'],

            ['currentTime', 'required', 'message' => ErrorCode::MISSING_INPUT_PARAMETER],
            ['currentTime', 'integer', 'min' => 1],

            [
                'lang',
                'filter',
                'filter' => function ($value) {
                    $supportedLanguages = (array)app()->params['supportedLanguages'];
                    if (!in_array($value, $supportedLanguages)) {
                        return reset($supportedLanguages);
                    }

                    return $value;
                },
            ],

            [['deviceId', 'requestId'], 'safe'],

            //            [
            //                'deviceId',
            //                'required',
            //                'when'    => function ($model) {
            //                    return in_array($model->typeClient, self::getTypeClientListAsMobile());
            //                },
            //                'message' => ErrorCode::EMPTY_DEVICE_ID,
            //            ],MISSING_INPUT_PARAMETER

            [
                'signature',
                'validatorSignature',
                'skipOnEmpty' => false,
            ],
        ];
    }

    public function validatorSignature($attribute)
    {
        if ($this->$attribute !== $this->getHash() && false ) {
            $this->addError($attribute, (string)ErrorCode::INCORRECTLY_SIGNATURE);
        }
    }

    public function getHash()
    {
        $params = $this->getStringParams();

        $apiKey = app()->tenantService->getTenantApiKey($this->appId);

        return hash('md5', $params . $apiKey);
    }

    protected function getStringParams()
    {
        $params = app()->request->isPost ? post() : get();

        ksort($params);
        $params = $this->filterParams($params);
        return http_build_query($params, '', '&', PHP_QUERY_RFC3986);
    }

    /**
     * Список доступных устройств
     * @return array
     */
    public static function getTypeClientList()
    {
        return array_merge(self::getTypeClientListAsMobile(), [self::TYPE_CLIENT_WEB, self::TYPE_CLIENT_HOSPITAL]);
    }

    /**
     * Список доступных мобильных устройств
     * @return array
     */
    public static function getTypeClientListAsMobile()
    {
        return [
            self::TYPE_CLIENT_ANDROID,
            self::TYPE_CLIENT_IOS,
        ];
    }

}