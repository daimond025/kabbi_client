<?php

namespace api\modules\v1\actions;

use api\modules\v1\requests\SendResponseRequest;
use api\modules\v1\services\OrderService;
use yii\rest\Controller;

/**
 * Оставить отзыв о поездке.
 *
 * Class GetOrderInfoAction
 *
 * @package api\modules\v1\actions
 * @property SendResponseRequest $request
 */
class SendResponseAction extends BaseApiAction
{
    public $orderService;

    public function __construct($id, Controller $controller, OrderService $orderService, array $config = [])
    {
        parent::__construct($id, $controller, $config);

        $this->orderService = $orderService;
    }

    protected function getRequest()
    {
        return new SendResponseRequest();
    }

    /**
     * @throws \api\modules\v1\exceptions\OrderIsNotCompletedYetException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    protected function execute()
    {
        $this->orderService->sendResponse(
            $this->request->tenantId,
            $this->request->orderId,
            $this->request->grade,
            $this->request->text
        );

        if (gtoet('1.2.0')) {
            $this->response->setContent([
                'response_result' => 1,
            ]);
        } else {
            $this->response->setContent([
                'respone_result' => 1,
            ]);
        }

    }
}
