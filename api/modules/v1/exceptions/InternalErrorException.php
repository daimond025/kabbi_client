<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class InternalErrorException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::INTERNAL_ERROR;
        parent::__construct('', $code, null);
    }

}