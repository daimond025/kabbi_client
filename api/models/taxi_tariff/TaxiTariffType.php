<?php

namespace api\models\taxi_tariff;

use yii\db\ActiveRecord;

/**
 * Class TaxiTariffType
 * @package api\models\taxi_tariff
 *
 * @property integer                   $type_id
 * @property integer                   $tariff_id
 * @property string                    $type
 * @property integer                   $sort
 *
 * @property TaxiTariff                $tariff
 * @property TaxiTariffOption[]        $tariffOption
 * @property TaxiTariffActiveDate[]    $tariffActiveDate
 * @property TaxiTariffHasAdditional[] $tariffHasAdditional
 */
class TaxiTariffType extends ActiveRecord
{

    const TYPE_CURRENT = 'CURRENT';
    const TYPE_EXCEPTION = 'EXCEPTION';

    public static function tableName()
    {
        return '{{%taxi_tariff_option_type}}';
    }

    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    public function getTariffOption()
    {
        return $this->hasMany(TaxiTariffOption::className(), ['type_id' => 'type_id']);
    }

    public function getTariffActiveDate()
    {
        return $this->hasMany(TaxiTariffActiveDate::className(), ['type_id' => 'type_id']);
    }

    public function getHasAdditional()
    {
        return $this->hasMany(TaxiTariffHasAdditional::className(), ['type_id' => 'type_id']);
    }

}