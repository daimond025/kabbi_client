<?php

namespace api\models\referralSystem\formatter;


class ReferrerBonusReplace extends BaseReplace
{
    protected $bonus = '';

    public function __construct(BaseFormatter $object, $bonus = '')
    {
        $this->object =& $object;
        $this->bonus = $bonus;
    }

    public function formatter($value)
    {
        if (strpos($value, "#REFERRER_BONUS#") !== false) {
            return str_replace("#REFERRER_BONUS#", (float)$this->bonus, $this->object->formatter($value));
        }

        return $this->object->formatter($value);
    }
}