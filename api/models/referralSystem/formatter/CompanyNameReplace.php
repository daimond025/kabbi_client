<?php

namespace api\models\referralSystem\formatter;


class CompanyNameReplace extends BaseReplace
{
    protected $companyName = '';

    public function __construct(BaseFormatter $object, $companyName = '')
    {
        $this->object =& $object;
        $this->companyName = $companyName;
    }

    public function formatter($value)
    {
        if (strpos($value, "#NAME#") !== false) {
            return str_replace("#NAME#", $this->companyName, $this->object->formatter($value));
        }

        return $this->object->formatter($value);
    }
}