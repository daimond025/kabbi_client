<?php

namespace api\modules\v1\requests;

use api\models\client\ClientPhone;
use api\models\tenant\TenantHasCity;

/**
 * Class GetClientHistoryAddressRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property string $cityId - ID филиала
 * @property string $phone - Телефонный номер клиента
 */
class GetClientHistoryAddressRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'cityId' => [
                'name' => 'city_id',
                'type' => self::TYPE_GET,
            ],
            'phone'  => [
                'name' => 'phone',
                'type' => self::TYPE_GET,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['cityId', 'phone'], 'required'],
            [
                'cityId',
                'exist',
                'targetClass'     => TenantHasCity::class,
                'targetAttribute' => ['cityId' => 'city_id', 'tenantId' => 'tenant_id'],
                'filter'          => ['block' => TenantHasCity::NOT_BLOCK],
            ],
            ['phone', 'exist', 'targetClass' => ClientPhone::class, 'targetAttribute' => ['phone' => 'value']],
        ]);
    }
}