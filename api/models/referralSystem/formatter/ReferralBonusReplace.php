<?php

namespace api\models\referralSystem\formatter;


class ReferralBonusReplace extends BaseReplace
{
    protected $bonus = '';

    public function __construct(BaseFormatter $object, $bonus = '')
    {
        $this->object =& $object;
        $this->bonus = $bonus;
    }

    public function formatter($value)
    {
        if (strpos($value, "#REFERRAL_BONUS#") !== false) {
            return str_replace("#REFERRAL_BONUS#", (float)$this->bonus, $this->object->formatter($value));
        }

        return $this->object->formatter($value);
    }
}