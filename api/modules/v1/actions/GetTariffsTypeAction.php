<?php

namespace api\modules\v1\actions;

use api\exceptions\NeedReauthException;
use api\models\city\City;
use api\models\client\Client;
use api\modules\v1\components\TaxiTariffService;
use api\modules\v1\requests\GetTariffsTypeRequest;

/**
 * Получение списка типов тарифов службы такси.
 *
 * Class GetTariffsTypeAction
 *
 * @package api\modules\v1\actions
 * @property GetTariffsTypeRequest $request
 * @property int                   $_client
 */
class GetTariffsTypeAction extends BaseApiAction
{
    protected $_clientId;

    protected function getRequest()
    {
        return new GetTariffsTypeRequest();
    }

    protected function beforeExecute()
    {
        if (parent::beforeExecute()) {

            if ($this->request->phone) {
                $client = Client::findByPhone($this->request->tenantId, $this->request->phone);
                if (!$client || $client->getIsBlackList()) {
                    throw new NeedReauthException();
                }
                $this->_clientId = $client->client_id;
            }

            return true;
        }

        return false;
    }

    protected function execute()
    {
        $datetime = $this->getDatetime();

        /** @var TaxiTariffService $taxiTariffService */
        $taxiTariffService = app()->get('taxiTariffService');
        $result            = $taxiTariffService->getTaxiTariffList($this->_clientId, $datetime, $this->request->lang,
            $this->request->tenantId, $this->request->cityId, $this->request->typeClient, null, true);

        $this->response->setContent($result);
    }

    protected function getDatetime()
    {
        if (!$this->request->date) {
            return app()->formatter->asDatetime(time() + City::getTimeOffset($this->request->cityId), 'd.MM.Y H:mm');
        }

        return $this->request->date;
    }

}