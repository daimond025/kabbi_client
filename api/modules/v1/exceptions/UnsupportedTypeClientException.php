<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class UnsupportedTypeClientException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::UNSUPPORTED_TYPE_CLIENT;
        parent::__construct('', $code, null);
    }

}