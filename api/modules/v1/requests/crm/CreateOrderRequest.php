<?php

namespace api\modules\v1\requests\crm;

use api\models\client\Company;
use api\models\order\Order;
use api\models\taxi_tariff\TaxiTariff;
use api\models\tenant\mobileApp\MobileAppHasCity;
use api\models\tenant\TenantCityHasPosition;
use api\models\tenant\TenantHasCity;
use api\models\tenant\TenantSetting;
use api\models\transport\car\CarOption;
use api\models\worker\Position;
use api\modules\v1\components\OrderService;
use api\modules\v1\requests\BaseVersionedApiRequest;

/**
 * Class CreateOrderRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests\crm
 *
 * @property string  $additionalOptions
 * @property string  $address *
 * @property integer $bonusPayment
 * @property integer $cityId *
 * @property string  $comment
 * @property integer $companyId *
 * @property string  $cost
 * @property string  $orderTime
 * @property integer $tariffId *
 * @property string  $payment *
 * @property string  $phone *
 * @property integer $positionId
 * @property string  $state *
 *
 */
class CreateOrderRequest extends BaseVersionedApiRequest
{
    const DEFAULT_GEOCODER_TYPE = 'ru';
    const DEFAULT_BONUS_PAYMENT = 0;

    const STATE_NEW = 'NEW_ORDER';       // Начать распределение
    const STATE_FREE = 'FREE_ORDER';     // Свободный заказ
    const STATE_FREEZE = 'FREEZE_ORDER'; // Заморозить заказ


    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'additionalOptions' => [
                'name'    => 'additional_options',
                'type'    => self::TYPE_POST,
                'default' => [],
            ],
            'address'           => [
                'name' => 'address',
                'type' => self::TYPE_POST,
            ],
            'bonusPayment'      => [
                'name'    => 'bonus_payment',
                'type'    => self::TYPE_POST,
                'default' => self::DEFAULT_BONUS_PAYMENT,
            ],
            'cityId'            => [
                'name' => 'city_id',
                'type' => self::TYPE_POST,
            ],
            'comment'           => [
                'name' => 'comment',
                'type' => self::TYPE_POST,
            ],
            'companyId'         => [
                'name' => 'company_id',
                'type' => self::TYPE_POST,
            ],
            'cost'              => [
                'name' => 'cost',
                'type' => self::TYPE_POST,
            ],
            'orderTime'         => [
                'name' => 'order_time',
                'type' => self::TYPE_POST,
            ],
            'tariffId'          => [
                'name' => 'tariff_id',
                'type' => self::TYPE_POST,
            ],
            'payment'           => [
                'name' => 'pay_type',
                'type' => self::TYPE_POST,
            ],
            'phone'             => [
                'name' => 'phone',
                'type' => self::TYPE_POST,
            ],
            'positionId'        => [
                'name'    => 'position_id',
                'type'    => self::TYPE_POST,
                'default' => Position::TAXI_DRIVER_ID,
            ],
            'state'             => [
                'name' => 'state',
                'type' => self::TYPE_POST,
            ],

        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [

            ['additionalOptions', 'filter', 'filter' => [$this, 'filterAdditionalOptions']],
            [
                'additionalOptions',
                'each',
                'rule' => ['exist', 'targetClass' => CarOption::className(), 'targetAttribute' => 'option_id'],
            ],

            ['address', 'required'],
            ['address', 'filter', 'filter' => [$this, 'filterAddress']],

            ['bonusPayment', 'in', 'range' => self::getBonusPaymentList()],

            ['cityId', 'required'],
            [
                'cityId',
                'exist',
                'targetClass'     => TenantHasCity::className(),
                'targetAttribute' => ['cityId' => 'city_id', 'tenantId' => 'tenant_id'],
                'filter'          => ['block' => TenantHasCity::NOT_BLOCK],
            ],
            [
                'cityId',
                'exist',
                'targetClass'     => MobileAppHasCity::className(),
                'targetAttribute' => ['cityId' => 'city_id', 'appId' => 'app_id'],
            ],

            ['comment', 'string', 'max' => 255],

            ['companyId', 'required', 'when' => [$this, 'whenRequiredCompanyByPayment']],
            [
                'companyId',
                'exist',
                'targetClass'     => Company::className(),
                'targetAttribute' => ['companyId' => 'company_id', 'tenantId' => 'tenant_id'],
                'filter'          => ['block' => TenantHasCity::NOT_BLOCK],
                'when'            => [$this, 'whenRequiredCompanyByPayment'],
            ],

            ['cost', 'double'],

            ['orderTime', 'safe'],
            ['orderTime', 'filter', 'filter' => [$this, 'filterOrderTime']],

            ['tariffId', 'required'],
            [
                'tariffId',
                'exist',
                'targetClass'     => TaxiTariff::className(),
                'targetAttribute' => ['tariffId' => 'tariff_id', 'tenantId' => 'tenant_id'],
                'filter'          => ['block' => TaxiTariff::NOT_BLOCK],
            ],

            ['payment', 'required'],
            ['payment', 'filter', 'filter' => [$this, 'filterStringToUpper']],
            ['payment', 'in', 'range' => self::getPaymentList()],

            ['phone', 'required'],
            ['phone', 'match', 'pattern' => '#^[1-9][0-9]{0,11}$#'],
            //            ['phone', 'exist', 'targetClass' => ClientPhone::className(), 'targetAttribute' => ['phone' => 'value']],

            ['positionId', 'required'],
            [
                'positionId',
                'exist',
                'targetClass'     => TenantCityHasPosition::className(),
                'targetAttribute' => [
                    'tenantId'   => 'tenant_id',
                    'cityId'     => 'city_id',
                    'positionId' => 'position_id',
                ],
            ],

            ['state', 'required'],
            ['state', 'filter', 'filter' => [$this, 'filterStringToUpper']],
            ['state', 'in', 'range' => self::getStateList()],
        ]);
    }

    public function filterAdditionalOptions($value)
    {
        if (empty($value) || !is_array($value)) {
            return [];
        }

        $value = array_filter($value, function ($item) {
            $itemToInt = (int)$item;
            if ($itemToInt < 1) {
                return false;
            }

            return $itemToInt == $item;
        });

        $value = array_unique($value, SORT_NUMERIC);

        return $value;
    }

    public function filterAddress($value)
    {

        $geoCoderType = TenantSetting::getSettingValue($this->tenantId, TenantSetting::SETTING_GEOCODER_TYPE,
            $this->cityId, $this->positionId);
        $geoCoderType = !empty($geoCoderType) ? $geoCoderType : self::DEFAULT_GEOCODER_TYPE;
        $address      = Order::filterAddress($value, $this->lang, $geoCoderType, $this->tenantId, $this->cityId);

        return $address['address'];
    }

    public function filterOrderTime($value)
    {
        if ($value === null) {
            return null;
        }

        return (int)app()->formatter->asTimestamp($value);
    }


    public function whenRequiredCompanyByPayment($model)
    {
        /** @var OrderService $orderService */
        $orderService = app()->get('orderService');

        return $orderService->isCompanyByPayment($model->payment);
    }


    public function filterStringToUpper($value)
    {
        return strtoupper($value);
    }

    public function getOrderTimeAsDate()
    {
        return date("d-m-Y H:i:s", $this->orderTime);
    }

    public static function getPaymentList()
    {
        /** @var OrderService $orderService */
        $orderService = app()->get('orderService');

        return $orderService->getPaymentList();
    }

    public static function getBonusPaymentList()
    {
        /** @var OrderService $orderService */
        $orderService = app()->get('orderService');

        return $orderService->getBonusPaymentList();
    }

    public static function getStateList()
    {
        return [
            self::STATE_NEW,
            self::STATE_FREE,
            self::STATE_FREEZE,
        ];
    }

}