<?php


namespace api\modules\v1\controllers;

use api\modules\v1\actions\GetCityListAction;

class NewApiSiteController extends NewApiController
{
    public function actions()
    {
        return array_merge(parent::actions(), [
            'get_active_city_list' => GetCityListAction::className(),
        ]);
    }

    
}