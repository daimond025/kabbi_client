<?php

namespace api\modules\v1\actions;

use api\exceptions\NeedReauthException;
use api\models\client\Client;
use api\models\tenant\TenantSetting;
use api\modules\v1\components\CityService;
use api\modules\v1\components\OrderService;
use api\modules\v1\helpers\RouteAnalyzerHelper;
use api\modules\v1\requests\CostingOrderRequest;

/**
 * Получение предварительной стоимости заказа
 *
 * Class CostingOrderAction
 *
 * @package api\modules\v1\actions\crm
 * @property CostingOrderRequest $request
 */
class CostingOrderAction extends BaseApiAction
{
    protected $clientId;

    protected function getRequest()
    {
        return new CostingOrderRequest();
    }

    /**
     * @return bool
     * @throws NeedReauthException
     * @throws \api\modules\v1\exceptions\BadParamsException
     * @throws \api\modules\v1\exceptions\EmptyDeviceIdException
     * @throws \api\modules\v1\exceptions\EmptyTenantIdException
     * @throws \api\modules\v1\exceptions\EmptyTypeClientException
     * @throws \api\modules\v1\exceptions\IncorrectlyAppIdException
     * @throws \api\modules\v1\exceptions\IncorrectlySignatureException
     * @throws \api\modules\v1\exceptions\MissingInputParamsException
     * @throws \api\modules\v1\exceptions\NeedUpdateAppVersionException
     * @throws \api\modules\v1\exceptions\UnsupportedTypeClientException
     * @throws \api\modules\v1\exceptions\UnsupportedVersionClientException
     * @throws \yii\base\InvalidConfigException
     */
    protected function beforeExecute()
    {
        if (parent::beforeExecute()) {
            // Получить информацию о клиенте
            if ($this->request->phone) {
                $client = Client::findByPhone($this->request->tenantId, $this->request->phone);
                if (!$client || $client->getIsBlackList()) {
                    throw new NeedReauthException();
                }
                $this->clientId = $client->client_id;
            }

            // Если время заказа не задано, то указываем текущее время
            if ($this->request->orderTime === null) {
                $this->request->orderTime = time();

                /** @var CityService $cityService */
                $cityService = app()->get('cityService');

                $offsetTime               = $cityService->getTimeOffset($this->request->cityId);
                $this->request->orderTime += $offsetTime;
            }

            return true;
        }

        return false;
    }


    protected function execute()
    {
        /** @var OrderService $orderService */
        $orderService = app()->get('orderService');

        $geoCoderType = TenantSetting::getSettingValue($this->request->tenantId, TenantSetting::SETTING_GEOCODER_TYPE,
            $this->request->cityId, $this->request->positionId);



        $geoCoderType = !empty($geoCoderType) ? $geoCoderType : CostingOrderRequest::DEFAULT_GEOCODER_TYPE;

        $this->request->orderTime = $orderService->formattedOrderTime($this->request->orderTime,
            $this->request->tenantId, $this->request->cityId, $this->request->positionId);


        $params                   = [
            'additionalOptions' => $this->request->additionalOptions,
            'address'           => $this->request->address,
            'cityId'            => $this->request->cityId,
            'geoCoderType'      => $geoCoderType,
            'orderTime'         => $this->request->getOrderTimeAsDate(),
            'clientId'          => $this->clientId,
            'tariffId'          => $this->request->tariffId,
            'tenantId'          => $this->request->tenantId,
            'lang'              => $this->request->lang,
        ];


        $result = $orderService->costingOrderManyTariffs($params);

        $this->response->setContent(RouteAnalyzerHelper::formattedManyTariff($result, $this->request->debug));
    }

}