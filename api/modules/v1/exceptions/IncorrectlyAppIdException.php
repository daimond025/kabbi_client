<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class IncorrectlyAppIdException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::INCORRECTLY_APP_ID;
        parent::__construct('', $code, null);
    }

}