<?php

namespace api\models\worker;

/**
 * This is the model class for table "{{%worker_review_rating}}".
 *
 * @property integer $worker_id
 * @property integer $position_id
 * @property integer $one
 * @property integer $two
 * @property integer $three
 * @property integer $four
 * @property integer $five
 * @property integer $count
 */
class WorkerReviewRating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_review_rating}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['worker_id', 'required'],
            [['rating_id', 'worker_id', 'one', 'two', 'three', 'four', 'five', 'count', 'position_id'], 'integer']
        ];
    }

    public static function getWorkerRating($worker_id, $position_id)
    {
        $model = self::find()
            ->select(['one', 'two', 'three', 'four', 'five' => 'five', 'count'])
            ->where([
                'worker_id' => $worker_id,
                'position_id' => $position_id
            ])
            ->asArray()
            ->one();
        if ($model) {
            $rating = $model['count'] == 0 ? 0 : ($model['one'] + $model['two'] * 2 + $model['three'] * 3
                    + $model['four'] * 4 + $model['five'] * 5) / $model['count'];
            return round($rating, 1);
        }
        return 0;
    }

    public static function make($workerId, $positionId)
    {
        return new WorkerReviewRating([
            'worker_id'   => $workerId,
            'position_id' => $positionId,
            'one'         => 0,
            'two'         => 0,
            'three'       => 0,
            'four'        => 0,
            'five'        => 0,
            'count'       => 0,
        ]);
    }
}
