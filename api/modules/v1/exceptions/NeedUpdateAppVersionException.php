<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class NeedUpdateAppVersionException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::NEED_UPDATE_APP_VERSION;
        parent::__construct('', $code, null);
    }

}