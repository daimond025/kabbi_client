<?php

namespace api\modules\v1\requests;

use api\models\taxi_tariff\TaxiTariff;

/**
 * Class GetAccessibleCarModelsRequest
 * Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property integer $tariffId - Идентификатор тарифа *
 */
class GetAccessibleCarModelsRequest extends BaseVersionedApiRequest
{
    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'tariffId' => [
                'name' => 'tariff_id',
                'type' => self::TYPE_GET,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['tariffId', 'required'],
            [
                'tariffId',
                'exist',
                'targetClass'     => TaxiTariff::className(),
                'targetAttribute' => ['tariffId' => 'tariff_id', 'tenantId' => 'tenant_id'],
            ],
        ]);
    }
}
