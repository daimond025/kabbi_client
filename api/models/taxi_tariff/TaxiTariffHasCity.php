<?php

namespace api\models\taxi_tariff;

use Yii;
use api\models\city\City;

/**
 * Class TaxiTariffHasCity
 * @package api\models\taxi_tariff
 *
 * @property int        $tariff_id
 * @property int        $city_id
 *
 * @property TaxiTariff $tariff
 * @property City       $city
 */
class TaxiTariffHasCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxi_tariff_has_city}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

}
