<?php

namespace api\models\city;

use Yii;

/**
 * This is the model class for table "{{%tbl_country}}".
 *
 * @property integer $country_id
 * @property string $name
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => t('city', 'Country ID'),
            'name' => t('city', 'Name'),
        ];
    }



    public static function getCountryList()
    {
        return  self::find()->asArray()->orderBy('name' . getLanguagePrefix())->all();
    }

}
