<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class EmptyDeviceIdException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::EMPTY_DEVICE_ID;
        parent::__construct('', $code, null);
    }

}