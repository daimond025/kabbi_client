<?php

namespace api\models\transport\car;

use api\exceptions\EmptyInDbException;
use yii\helpers\ArrayHelper;

/**
 * Class CarColor
 * @package api\models\transport\car
 *
 * @property integer $color_id
 * @property string  $name
 */
class CarColor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_color}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'color_id' => 'ID',
            'name' => 'Name',
        ];
    }

    public static function getColorList($lang)
    {
        $model = self::find()->all();

        if(!$model) {
            throw new EmptyInDbException();
        }

        $result = ArrayHelper::map($model, 'color_id', 'name');

        $result = array_map(function ($item) use ($lang) {
            $item = t('car', $item, [], $lang);
            return $item;
        },$result);

        asort($result);
        return $result;
    }

}
