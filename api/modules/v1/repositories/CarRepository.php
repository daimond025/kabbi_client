<?php

namespace api\modules\v1\repositories;

use api\models\transport\car\Car;

class CarRepository
{
    public function getCarsByCarClass($tenantId, $carClassId)
    {
        $cars = Car::find()
            ->alias('car')
            ->joinWith([
                'carModel model',
                'carModel.carBrand brand',
            ])
            ->where([
                'car.class_id'  => $carClassId,
                'car.tenant_id' => $tenantId,
            ])
            ->all();

        return $cars;
    }
}
