<?php

namespace api\models\tenant;

use Yii;
use api\models\balance\Currency;

/**
 * This is the model class for table "tbl_tenant_setting".
 *
 * @property integer $setting_id
 * @property integer $tenant_id
 * @property string  $name
 * @property string  $value
 *
 * @property Tenant  $tenant
 */
class TenantSetting extends \yii\db\ActiveRecord
{
    const CACHE_TIME = 3600;
    const CACHE_ON = true;

    const DEFAULT_CURRENCY_CODE = 'RUB';

    const SETTING_PICK_UP = 'PICK_UP';
    const SETTING_PRE_ORDER = 'PRE_ORDER';
    const SETTING_GEOCODER_TYPE = 'GEOCODER_TYPE';
    const SETTING_PHONE = 'PHONE';
    const SETTING_CURRENCY = 'CURRENCY';
    const SETTING_CLIENT_API_KEY = 'CLIENT_API_KEY';
    const SETTING_TYPE_OF_DISTRIBUTION_ORDER = 'TYPE_OF_DISTRIBUTION_ORDER';
    const SETTING_ORDER_OFFER_SEC = 'ORDER_OFFER_SEC';
    const SETTING_DISTANCE_FOR_SEARCH = 'DISTANCE_FOR_SEARCH';
    const SETTING_CHECK_WORKER_RATING_TO_OFFER_ORDER = 'CHECK_WORKER_RATING_TO_OFFER_ORDER';
    const SETTING_CIRCLES_DISTRIBUTION_ORDER = 'CIRCLES_DISTRIBUTION_ORDER';
    const SETTING_DELAY_CIRCLES_DISTRIBUTION_ORDER = 'DELAY_CIRCLES_DISTRIBUTION_ORDER';
    const SETTING_FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES = 'FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES';
    const SETTING_ANDROID_CLIENT_APP_VERSION = 'ANDROID_CLIENT_APP_VERSION';
    const SETTING_IOS_CLIENT_APP_VERSION = 'IOS_CLIENT_APP_VERSION';
    const SETTING_WORKER_REVIEW_COUNT = 'WORKER_REVIEW_COUNT';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tenant_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_id' => 'Setting ID',
            'tenant_id'  => 'Tenant ID',
            'name'       => 'Name',
            'value'      => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['currency_id' => 'value']);
    }

    /**
     * Getting setting value.
     *
     * @param int $tenantId
     * @param int $setting
     * @param int $cityId
     *
     * @return string
     */
    public static function getSettingValue($tenantId, $setting, $cityId = null, $positionId = 1)
    {
        return self::find()
            ->select('value')
            ->where([
                'tenant_id' => $tenantId,
                'name'      => $setting,
                'city_id'   => $cityId,
            ])
            ->scalar();
    }

    public static function getTenantCurrencyId($tenantId, $cityId)
    {
        return self::getSettingValue($tenantId, self::SETTING_CURRENCY, $cityId);
    }

    public static function getTenantCurrencyCode($tenantId, $cityId)
    {
        $currencyId = self::getSettingValue($tenantId, self::SETTING_CURRENCY, $cityId);
        $code       = Currency::find()
            ->select('code')
            ->where(['currency_id' => $currencyId])
            ->scalar();

        return empty($code) ? self::DEFAULT_CURRENCY_CODE : $code;
    }

}