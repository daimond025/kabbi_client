<?php

namespace api\modules\v1\requests;

use api\models\order\Order;

/**
 * Class LikeWorkerRequest
 * Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property integer $orderId - Идентификатор заказа *
 * @property integer $like *
 */
class LikeWorkerRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'orderId' => [
                'name' => 'order_id',
                'type' => self::TYPE_POST,
            ],
            'like'    => [
                'name' => 'like',
                'type' => self::TYPE_POST,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['orderId', 'required'],
            [
                'orderId',
                'exist',
                'targetClass'     => Order::class,
                'targetAttribute' => ['orderId' => 'order_id', 'tenantId' => 'tenant_id'],
            ],

            ['like', 'required'],
            ['like', 'in', 'range' => [0, 1]],
        ]);
    }
}
