<?php

/**
 * This class is wrapper for Gearman.
 * Gearman is a generic application framework for farming out work to multiple machines or processes.
 * It allows applications to complete tasks in parallel, to load balance processing, and to call functions between
 * languages. The framework can be used in a variety of applications, from high-availability web sites to the transport
 * of database replication events.
 *
 * Пример вызова воркера
 * Yii::$app->gearman->doBackground('Email', $params);
 */

namespace api\components\gearman;

class Gearman extends \yii\base\Object
{
    const SMS_TASK = 'Sms';
    const PUSH_TASK = 'Push';
    const EMAIL_TASK = 'Email';
    const PARKING_TASK_ADD = 'Parking_Add';
    const ORDER_STATISTIC = 'OrderStatistic';
    const BILLING = 'Billing';
    public $host;
    public $port;
    private $_client;

    public function init()
    {
        $this->_client = new \GearmanClient();
        $this->_client->addServer($this->host, $this->port);
        parent::init();
    }

    /**
     * Run a task in the background
     *
     * @param string $task If job class named like EmailJob, the $task will be 'Email'
     * @param array  $params Params for your worker function
     */
    public function doBackground($task, array $params)
    {
        $this->_client->doBackground($task, json_encode($params));
    }

    /**
     * Run a single high priority task
     *
     * @param string $task If job class named like EmailJob, the $task will be 'Email'
     * @param array  $params Params for your worker function
     *
     * @return string A string representing the results of running a task.
     */
    public function doHigh($task, array $params)
    {
        return $this->_client->doHigh($task, json_encode($params));
    }

}
