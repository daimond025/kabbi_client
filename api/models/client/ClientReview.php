<?php

namespace api\models\client;

/**
 * Class ClientReview
 * @package api\models\client
 *
 * @property integer $review_id
 * @property integer $client_id
 * @property integer $order_id
 * @property integer $worker_id
 * @property integer $position_id
 * @property string  $text
 * @property integer $rating
 * @property string  $create_time
 * @property integer $active
 */
class ClientReview extends \yii\db\ActiveRecord
{
    const ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_review}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['client_id', 'exist', 'targetClass' => '\api\models\client\Client'],
            ['order_id', 'exist', 'targetClass' => '\api\models\order\Order'],
            ['text', 'safe'],
            ['rating', 'in', 'range' => [1, 2, 3, 4, 5]],
        ];
    }
}
