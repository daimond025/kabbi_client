<?php

namespace api\models\order;

use api\models\order\exceptions\QueueIsNotExistsException;
use PhpAmqpLib\Exception\AMQPProtocolChannelException;
use Ramsey\Uuid\Uuid;
use yii\base\Object;

/**
 * Class OrderUpdateEvent
 * @package api\models\order
 */
class OrderUpdateEvent extends Object
{
    public $requestId;
    public $tenantId;
    public $orderId;
    public $senderId;
    public $lastUpdateTime;
    public $lang;
    public $sender;

    const SENDER_CLIENT = 'client_service';
    const SENDER_OPERATOR = 'operator_service';


    /**
     * @return string
     */
    private function generateUuid()
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * Add order event for update.
     *
     * @param array $data
     */
    public function addEvent(array $data)
    {
        $this->addEventToQueue($data);
    }

    /**
     * Add event to rabbit queue
     *
     * @param array $data
     *
     * @return mixed
     * @throws QueueIsNotExistsException
     */
    private function addEventToQueue(array $data)
    {
        $data = $this->prepareData($data);

        try {
            return \Yii::$app->amqp->send($data, 'order_' . $this->orderId, '', false);
        } catch (AMQPProtocolChannelException $ex) {
            throw new QueueIsNotExistsException('Queue is not exists', 0, $ex);
        }
    }

    /**
     * Prepare data
     *
     * @param array $data
     *
     * @return array
     */
    private function prepareData(array $data)
    {
        return [
            'uuid'             => $this->requestId === null ? $this->generateUuid() : $this->requestId,
            'type'             => 'order_event',
            'timestamp'        => time(),
            'sender_service'   => $this->sender,
            'command'          => 'update_order_data',
            'order_id'         => $this->orderId,
            'tenant_id'        => $this->tenantId,
            'change_sender_id' => $this->senderId,
            'last_update_time' => $this->lastUpdateTime,
            'lang'             => $this->lang,
            'params'           => $data,
        ];
    }

    /**
     * Getting result of request
     *
     * @param string $requestId
     *
     * @return mixed
     */
    public function getResultResponse($requestId)
    {
        $db = \Yii::$app->redis_order_event;

        return $db->executeCommand('GET', [$requestId]);
    }

}