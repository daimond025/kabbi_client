<?php

namespace api\models\transport\car;

use api\exceptions\EmptyInDbException;
use yii\helpers\ArrayHelper;

/**
 * Class CarModel
 * @package api\models\transport\car
 *
 * @property CarBrand $carBrand
 */
class CarModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_model}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }


    public static function getModelList($brand_id)
    {
        $model = self::find()
            ->where(['brand_id' => $brand_id])
            ->all();

        if(!$model) {
            throw new EmptyInDbException();
        }

        $result = ArrayHelper::map($model, 'model_id', 'name');

        asort($result);
        return $result;
    }

    /**
     * Возвращает назавние модели по его Ид
     *
     * @param integer $modelId
     * @return string
     */
    public static function getModelName($modelId)
    {
        return (string) self::find()
            ->select('name')
            ->where([
                'model_id' => $modelId
            ])
            ->scalar();
    }

    public function getCarBrand()
    {
        return $this->hasOne(CarBrand::class, ['brand_id' => 'brand_id']);
    }
}
