<?php

namespace api\models\transport\car;

use api\exceptions\EmptyInDbException;
use yii\helpers\ArrayHelper;
use Yii;


class CarBrand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_brand}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }


    public static function getBrandList()
    {
        $model = self::find()->all();

        if(!$model) {
            throw new EmptyInDbException();
        }

        $result = ArrayHelper::map($model, 'brand_id', 'name');

        asort($result);
        return $result;
    }

    /**
     * Возвращает название бренда по его Ид
     *
     * @param integer $brandId
     * @return string
     */
    public static function getBrandName($brandId)
    {
        return (string) self::find()
            ->select('name')
            ->where([
                'brand_id' => $brandId
            ])
            ->scalar();
    }


}
