<?php

namespace api\modules\v1\actions\crm;

use api\models\worker\Worker;
use api\modules\v1\actions\BaseApiAction;
use api\modules\v1\components\OrderService;
use api\modules\v1\exceptions\ResultNotFoundException;
use api\modules\v1\requests\crm\UpdateOrderRequest;

/**
 * Редактирование заказа
 *
 * Class UpdateOrderAction
 *
 * @package api\modules\v1\actions\crm
 * @property UpdateOrderRequest $request
 */
class UpdateOrderAction extends BaseApiAction
{

    protected function getRequest()
    {
        return new UpdateOrderRequest();
    }

    protected function execute()
    {
        $params = $this->getParams();

        /** @var OrderService $orderService */
        $orderService = app()->get('orderService');

        $updatedOrder = $orderService->updateOrderAsOperator($this->request->tenantId, $this->request->orderId,
            $this->request->requestId, $this->request->lang, $params);

        if (!$updatedOrder) {
            throw new ResultNotFoundException();
        }

        $this->response->setContent($this->request->requestId);
    }

    protected function getParams()
    {
        $params = [
            'order_id'  => $this->request->orderId,
            'status_id' => $this->request->statusId,
        ];

        /** @var OrderService $orderService */
        $orderService = app()->get('orderService');

        if ($orderService->isRequiredWorker($this->request->statusId)) {
            $worker = $this->findWorker($this->request->workerCallsign);

            if ($worker) {
                $params['worker_id'] = $worker->worker_id;
            }
        }

        return $params;
    }

    protected function findWorker($callsign)
    {
        return Worker::find()
            ->where([
                'callsign' => $callsign,
            ])
            ->one();
    }

}