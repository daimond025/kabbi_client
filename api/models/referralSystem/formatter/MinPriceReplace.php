<?php

namespace api\models\referralSystem\formatter;


class MinPriceReplace extends BaseReplace
{
    protected $minPrice = '';

    public function __construct(BaseFormatter $object, $minPrice = '')
    {
        $this->object =& $object;
        $this->minPrice = $minPrice;
    }

    public function formatter($value)
    {
        if (strpos($value, "#MIN_PRICE#") !== false) {
            return str_replace("#MIN_PRICE#", $this->minPrice, $this->object->formatter($value));
        }

        return $this->object->formatter($value);
    }
}