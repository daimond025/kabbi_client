<?php

namespace api\models\worker;

use Yii;
use api\models\transport\TransportType;

/**
 * This is the model class for table "{{%tbl_position}}".
 *
 * @property integer $id
 * @property string  $name
 */
class Position extends \yii\db\ActiveRecord
{
    const TAXI_DRIVER_ID = 1;
    const TRUCK_DRIVER = 2;
    const COURIER_ON_FOOT = 3;
    const MASTER = 4;
    const COURIER_ON_MOTO = 5;
    const COURIER_ON_AUTO = 5;


    const SOURCE_CATEGORY = 'employee';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id'           => 'ID',
            'name'              => 'Name',
            'module_id'         => 'Module ID',
            'code'              => 'code',
            'has_car'           => 'Has car',
            'transport_type_id' => 'Transport type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(WorkerModule::className(), ['id' => 'module_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransportType()
    {
        return $this->hasOne(TransportType::className(), ['type_id' => 'transport_type_id']);
    }

    public static function getPositionsByCity($lang, $cityId = null)
    {
        $positions = self::find()
            ->andFilterWhere(['city_id' => $cityId])
            ->asArray()
            ->all();

        $result = [];
        if (!empty($positions)) {
            foreach ($positions as $item) {
                $result[$item['position_id']] = t(self::SOURCE_CATEGORY, $item['name'], [], $lang);
            }
        }

        return $result;
    }
}
