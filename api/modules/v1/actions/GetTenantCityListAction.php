<?php

namespace api\modules\v1\actions;

use api\modules\v1\requests\BaseVersionedApiRequest;

/**
 * Получение списка городов службы такси.
 * Отсеиваются филиалы без координат lat и lon
 * Отсеиваются филиалы без клиенских тарифов с учетом enabled_app и enabled_site
 *
 * Class GetTenantCityListAction
 *
 * @package api\modules\v1\actions
 * @property BaseVersionedApiRequest $request
 */
class GetTenantCityListAction extends BaseApiAction
{
    protected function getRequest()
    {
        return new BaseVersionedApiRequest();
    }

    protected function execute()
    {
        $prefix = $this->getPrefix();
        $this->response->setContent([
            'city_list' => app()->tenantService->getTenantCityListByAppId($this->request->appId,
                $this->request->typeClient, $prefix),
        ]);
    }

    protected function getPrefix()
    {
        return $this->request->lang === 'ru' ? '' : '_' . $this->request->lang;
    }
}