<?php

namespace api\models\taxi_tariff;

use Yii;


class TaxiOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%option_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }
}
