<?php

namespace api\modules\v1\components;

class ErrorCode
{
    const OK = 0;
    const INTERNAL_ERROR = 1;
    const UNKNOWN_REQUEST = 3;
    const EMPTY_VALUE = 6;
    const EMPTY_DATA_IN_DATABASE = 7;
    const BLACK_LIST = 9;
    const NEED_REAUTH = 10;
    const BAD_PAY_TYPE = 11;
    const NO_MONEY = 12;
    const BAD_BONUS = 13;
    const PAYMENT_GATE_ERROR = 14;
    const INVALID_PAN = 15;
    const BAD_REQUEST = 16;

    const REQUEST_PROCESSING = 20;

    const BONUS_SYSTEM_IS_NOT_ACTIVATED = 41;

    const ORDER_IS_COMPLETED = 200;
    const ORDER_IS_REJECTED = 201;
    const ORDER_IS_NOT_COMPLETED_YET = 202;

    const RESULT_NOT_FOUND = 300;

    const METHOD_NOT_FOUND = 404;

    // Ошибки с входящими параметрами
    const INCORRECTLY_SIGNATURE = 2;
    const BAD_PARAM = 4;
    const MISSING_INPUT_PARAMETER = 5;
    const EMPTY_TENANT_ID = 8;
    const INCORRECTLY_APP_ID = 17;
    const INVALID_PROMO_CODE = 40;
    const EMPTY_DEVICE_ID = 100;
    const EMPTY_TYPE_CLIENT = 101;
    const UNSUPPORTED_TYPE_CLIENT = 102;
    const UNSUPPORTED_VERSION_CLIENT = 104;
    const NEED_UPDATE_APP_VERSION = 105;


    protected static $errorCodeData = [
        self::OK                            => 'OK',
        self::INTERNAL_ERROR                => 'INTERNAL_ERROR',
        self::INCORRECTLY_SIGNATURE         => 'INCORRECTLY_SIGNATURE',
        self::UNKNOWN_REQUEST               => 'UNKNOWN_REQUEST',
        self::BAD_PARAM                     => 'BAD_PARAM',
        self::MISSING_INPUT_PARAMETER       => 'MISSING_INPUT_PARAMETER',
        self::EMPTY_VALUE                   => 'EMPTY_VALUE',
        self::EMPTY_DATA_IN_DATABASE        => 'EMPTY_DATA_IN_DATABASE',
        self::EMPTY_TENANT_ID               => 'EMPTY_TENANT_ID',
        self::BLACK_LIST                    => 'BLACK_LIST',
        self::NEED_REAUTH                   => 'NEED_REAUTH',
        self::BAD_PAY_TYPE                  => 'BAD_PAY_TYPE',
        self::NO_MONEY                      => 'NO_MONEY',
        self::BAD_BONUS                     => 'BAD_BONUS',
        self::PAYMENT_GATE_ERROR            => 'PAYMENT_GATE_ERROR',
        self::INVALID_PAN                   => 'INVALID_PAN',
        self::INVALID_PROMO_CODE            => 'INVALID_PROMO_CODE',
        self::ORDER_IS_REJECTED             => 'ORDER_IS_REJECTED',
        self::ORDER_IS_COMPLETED            => 'ORDER_IS_COMPLETED',
        self::ORDER_IS_NOT_COMPLETED_YET    => 'ORDER_IS_NOT_COMPLETED_YET',
        self::BAD_REQUEST                   => 'BAD_REQUEST',
        self::BONUS_SYSTEM_IS_NOT_ACTIVATED => 'BONUS_SYSTEM_IS_NOT_ACTIVATED',
        self::RESULT_NOT_FOUND              => 'RESULT_NOT_FOUND',
        self::INCORRECTLY_APP_ID            => 'INCORRECTLY_MOBILE_APP',
        self::METHOD_NOT_FOUND              => 'METHOD_NOT_FOUND',
        self::REQUEST_PROCESSING            => 'REQUEST_PROCESSING',
        self::EMPTY_DEVICE_ID               => 'EMPTY_DEVICE_ID',
        self::EMPTY_TYPE_CLIENT             => 'EMPTY_TYPE_CLIENT',
        self::UNSUPPORTED_TYPE_CLIENT       => 'UNSUPPORTED_TYPE_CLIENT',
        self::UNSUPPORTED_VERSION_CLIENT    => 'UNSUPPORTED_VERSION_CLIENT',
        self::NEED_UPDATE_APP_VERSION       => 'NEED_UPDATE_APP_VERSION',
    ];


    public static function getInfo($error)
    {
        return array_key_exists($error, self::$errorCodeData) ? self::$errorCodeData[$error] : null;
    }
}