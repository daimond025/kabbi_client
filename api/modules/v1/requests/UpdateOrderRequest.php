<?php

namespace api\modules\v1\requests;

use api\models\client\ClientPhone;
use api\models\client\Company;
use api\models\order\Order;
use api\modules\v1\components\OrderService;

/**
 * Class UpdateOrderRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property int    $orderId - Ид заказа*
 * @property string $phone - Телефон клиента*
 * @property string $address - Адрес*
 * @property string $payment - Способ оплаты*
 * @property int    $companyId - Ид компании. Обязателен при оплате за счет комппании.
 * @property string $pan - Номер карты. Обязателен при оплате картой.
 */
class UpdateOrderRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'orderId'   => [
                'name' => 'order_id',
                'type' => self::TYPE_POST,
            ],
            'phone'     => [
                'name' => 'phone',
                'type' => self::TYPE_POST,
            ],
            'address'   => [
                'name' => 'address',
                'type' => self::TYPE_POST,
            ],
            'payment'   => [
                'name' => 'payment',
                'type' => self::TYPE_POST,
            ],
            'companyId' => [
                'name' => 'company_id',
                'type' => self::TYPE_POST,
            ],
            'pan'       => [
                'name' => 'pan',
                'type' => self::TYPE_POST,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['orderId', 'address', 'payment'], 'required'],
            [
                'orderId',
                'exist',
                'targetClass'     => Order::className(),
                'targetAttribute' => ['orderId' => 'order_id', 'tenantId' => 'tenant_id'],
            ],

            ['phone', 'exist', 'targetClass' => ClientPhone::class, 'targetAttribute' => ['phone' => 'value']],

            ['address', 'safe'],

            ['payment', 'in', 'range' => UpdateOrderRequest::getPaymentList()],

            ['companyId', 'required', 'when' => [$this, 'whenCorpBalance']],
            [
                'companyId',
                'exist',
                'targetClass'     => Company::className(),
                'targetAttribute' => ['companyId' => 'company_id', 'tenantId' => 'tenant_id'],
                'when'            => [$this, 'whenCorpBalance'],
            ],

            ['pan', 'required', 'when' => [$this, 'whenCard']],
        ]);
    }

    /**
     * Условие для валидатора "Когда оплата из корпоротивного баланса"
     *
     * @param self $model
     *
     * @return bool
     */
    public function whenCorpBalance($model)
    {
        return $model->payment === Order::PAYMENT_CORP_BALANCE;
    }

    /**
     * Условие для валидатора "Когда оплата картой"
     *
     * @param self $model
     *
     * @return bool
     */
    public function whenCard($model)
    {
        return $model->payment === Order::PAYMENT_CARD;
    }

    public static function getPaymentList()
    {
        /** @var OrderService $orderService */
        $orderService = app()->get('orderService');

        return $orderService->getPaymentListByClient();
    }
}