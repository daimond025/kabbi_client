<?php

$identity = getenv('SYSLOG_IDENTITY') . '-' . require __DIR__ . '/version.php';

\Yii::$container->set(\paymentGate\ProfileService::class, function ($container, $params, $config) {
    return new \paymentGate\ProfileService(Yii::$app->getDb());
});

\Yii::$container->set(
    \logger\targets\LogTargetInterface::class,
    \logger\targets\SyslogTarget::class,
    [$identity]
);

\Yii::$container->set(
    \logger\LogHandlerFactory::class
);