<?php

namespace api\models\tenant;

use api\models\referralSystem\ReferralSystem;
use api\models\city\City;

/**
 * Class TenantHasCity
 * @package api\models\tenant
 *
 * @property int    $tenant_id
 * @property int    $city_id
 * @property int    $sort
 * @property int    $block
 *
 * @property Tenant $tenant
 * @property City   $city
 *
 * @property bool   $isActive
 */
class TenantHasCity extends \yii\db\ActiveRecord
{
    const BLOCK = 1;
    const NOT_BLOCK = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_has_city}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    public function getIsActive()
    {
        return (int)$this->block === self::NOT_BLOCK;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferralSystem()
    {
        return $this->hasOne(ReferralSystem::className(), ['city_id' => 'city_id', 'tenant_id' => 'tenant_id'])
            ->onCondition(['active' => 1]);
    }
}
