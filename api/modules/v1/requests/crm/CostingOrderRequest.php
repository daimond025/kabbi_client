<?php

namespace api\modules\v1\requests\crm;

use api\models\order\Order;
use api\models\taxi_tariff\TaxiTariff;
use api\models\tenant\mobileApp\MobileAppHasCity;
use api\models\tenant\TenantCityHasPosition;
use api\models\tenant\TenantHasCity;
use api\models\tenant\TenantSetting;
use api\models\transport\car\CarOption;
use api\models\worker\Position;
use api\modules\v1\requests\BaseVersionedApiRequest;

/**
 * Class CostingOrderRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests\crm
 *
 * @property string  $additionalOptions
 * @property string  $address *
 * @property integer $cityId *
 * @property string  $orderTime
 * @property integer $tariffId *
 * @property string  $phone
 * @property integer $positionId
 *
 */
class CostingOrderRequest extends BaseVersionedApiRequest
{
    const DEFAULT_GEOCODER_TYPE = 'ru';


    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'additionalOptions' => [
                'name'    => 'additional_options',
                'type'    => self::TYPE_POST,
                'default' => '',
            ],
            'address'           => [
                'name' => 'address',
                'type' => self::TYPE_POST,
            ],
            'cityId'            => [
                'name' => 'city_id',
                'type' => self::TYPE_POST,
            ],
            'orderTime'         => [
                'name' => 'order_time',
                'type' => self::TYPE_POST,
            ],
            'tariffId'          => [
                'name' => 'tariff_id',
                'type' => self::TYPE_POST,
            ],
            'phone'             => [
                'name' => 'phone',
                'type' => self::TYPE_POST,
            ],
            'positionId'        => [
                'name'    => 'position_id',
                'type'    => self::TYPE_POST,
                'default' => Position::TAXI_DRIVER_ID,
            ],

        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [

            ['additionalOptions', 'filter', 'filter' => [$this, 'filterAdditionalOptions']],
            [
                'additionalOptions',
                'each',
                'rule' => ['exist', 'targetClass' => CarOption::className(), 'targetAttribute' => 'option_id'],
            ],

            ['address', 'required'],
            ['address', 'filter', 'filter' => [$this, 'filterAddress']],

            ['cityId', 'required'],
            [
                'cityId',
                'exist',
                'targetClass'     => TenantHasCity::className(),
                'targetAttribute' => ['cityId' => 'city_id', 'tenantId' => 'tenant_id'],
                'filter'          => ['block' => TenantHasCity::NOT_BLOCK],
            ],
            [
                'cityId',
                'exist',
                'targetClass'     => MobileAppHasCity::className(),
                'targetAttribute' => ['cityId' => 'city_id', 'appId' => 'app_id'],
            ],

            ['orderTime', 'safe'],
            ['orderTime', 'filter', 'filter' => [$this, 'filterOrderTime']],

            ['tariffId', 'required'],
            [
                'tariffId',
                'exist',
                'targetClass'     => TaxiTariff::className(),
                'targetAttribute' => ['tariffId' => 'tariff_id', 'tenantId' => 'tenant_id'],
                'filter'          => ['block' => TaxiTariff::NOT_BLOCK],
            ],

            ['phone', 'string'],

            ['positionId', 'required'],
            [
                'positionId',
                'exist',
                'targetClass'     => TenantCityHasPosition::className(),
                'targetAttribute' => [
                    'tenantId'   => 'tenant_id',
                    'cityId'     => 'city_id',
                    'positionId' => 'position_id',
                ],
            ],
        ]);
    }

    public function filterAdditionalOptions($value)
    {
        if (empty($value) || !is_array($value)) {
            return [];
        }

        $value = array_filter($value, function ($item) {
            $itemToInt = (int)$item;
            if ($itemToInt < 1) {
                return false;
            }

            return $itemToInt == $item;
        });

        $value = array_unique($value, SORT_NUMERIC);

        return $value;
    }

    public function filterAddress($value)
    {

        $geoCoderType = TenantSetting::getSettingValue($this->tenantId, TenantSetting::SETTING_GEOCODER_TYPE,
            $this->cityId, $this->positionId);
        $geoCoderType = !empty($geoCoderType) ? $geoCoderType : self::DEFAULT_GEOCODER_TYPE;
        $address      = Order::filterAddress($value, $this->lang, $geoCoderType, $this->tenantId, $this->cityId);

        return $address['address'];
    }

    public function filterOrderTime($value)
    {
        if ($value === null) {
            return null;
        }

        return (int)app()->formatter->asTimestamp($value);
    }

    public function getOrderTimeAsDate()
    {
        return date("d-m-Y H:i:s", $this->orderTime);
    }
}