<?php

namespace api\models\worker;

use api\models\city\City;
use api\models\tenant\TenantHasCity;
use app\models\worker\WorkerHasCity;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker}}".
 *
 * @property integer         $worker_id
 * @property integer         $tenant_id
 * @property integer         $callsign
 * @property  string         $password
 * @property  string         $last_name
 * @property  string         $name
 * @property string          $second_name
 * @property string          $phone
 * @property string          $photo
 * @property string          $device
 * @property string          $device_token
 * @property string          $lang
 * @property string          $description
 * @property string          $device_info
 * @property string          $email
 * @property string          $partnership
 * @property string          $birthday
 * @property integer         $block
 * @property integer         $activate
 * @property integer         $create_time
 * @property string          $card_number
 * @property string          $city_id
 *
 * @property City[]          $cities
 * @property WorkerHasCity[] $workerHasCities
 */
class Worker extends \yii\db\ActiveRecord
{

    const PASSWORD_STR_COUNT = 10;

    const SITE = 'SITE';
    const DISPATCHER = 'DISPATCHER';

    const STATUS_FREE = 'FREE';

    private $_password;
    public $city_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['last_name', 'name', 'partnership', 'phone', 'city_id'], 'required'],
            [
                'phone',
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", '', $value);
                },
            ],
            [
                'birthday',
                'filter',
                'filter'  => function ($value) {
                    $date = explode('-', $value);
                    if (count($date) != 3) {
                        return '';
                    }

                    return checkdate($date[1], $date[2], $date[0]) ? $value : '';
                },
            ],
            [['last_name', 'name', 'partnership', 'city_id', 'phone'], 'required', 'message' => 'emptyParam'],
            ['partnership', 'in', 'range' => $this->getPartnershipList(), 'message' => 'incorrectPartnership'],
            ['phone', 'unique', 'targetAttribute' => ['phone', 'tenant_id'], 'message' => 'phoneNumberIsBusy'],
            ['phone', 'string', 'max' => 15, 'message' => 'phoneNumberIsTooLong'],
            ['callsign', 'default', 'value' => $this->getLastCallsign() + 1],
            ['callsign', 'unique', 'targetAttribute' => ['tenant_id', 'callsign'], 'message' => 'callsignIsBusy'],
            ['password', 'default', 'value' => $this->generatePassword()],
            ['email', 'email', 'message' => 'incorrectEmail'],
            ['activate', 'in', 'range' => [0, 1], 'message' => 'incorrectActivate'],
            ['lang', 'in', 'range' => Yii::$app->params['supportedLanguages'], 'message' => 'unsupportedLang'],
            [['created_via'], 'in', 'range' => [self::SITE, self::DISPATCHER]],
            [['second_name', 'card_number', 'description'], 'safe'],
            ['city_id', 'validateCities'],
        ];
    }

    /**
     * Validate worker cities
     * @return bool
     */
    public function validateCities()
    {
        $cityIds = explode(',', $this->city_id);

        $tenantCityIds = TenantHasCity::find()
            ->select('city_id')
            ->where(['tenant_id' => $this->tenant_id])
            ->column();

        if (count(array_intersect($cityIds, $tenantCityIds)) !== count($cityIds)) {
            $this->addError('city_id', t('yii', '{attribute} is invalid.', ['attribute' => 'City Id']));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])->via('workerHasCity');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasCities()
    {
        return $this->hasMany(WorkerHasCity::className(), ['worker_id' => 'worker_id']);
    }

    protected function getPartnershipList()
    {
        return ['PARTNER', 'STAFF'];
    }

    public function generatePassword()
    {
        return Yii::$app->security->generateRandomString(self::PASSWORD_STR_COUNT);
    }

    public static function getFullNameByCallsign($tenant_id, $callsign)
    {
        $model = self::find()
            ->select(['last_name', 'name', 'second_name'])
            ->where([
                'tenant_id' => $tenant_id,
                'callsign'  => $callsign,
            ])
            ->asArray()
            ->one();

        return implode(' ', [$tenant_id, $callsign]);
    }

    /**
     * Возвращает наибольшее значение поля Позывной у арендатора
     * @return int
     */
    public function getLastCallsign()
    {
        return (int)self::find()
            ->select('callsign')
            ->where(['tenant_id' => $this->tenant_id])
            ->orderBy(['callsign' => SORT_DESC])
            ->scalar();
    }

    public function beforeSave($insert)
    {
        if ($insert || isset($this->dirtyAttributes['activate'])) {
            $this->block = ($this->activate + 1) % 2;
        }

        if ($insert) {
            $this->_password   = $this->password;
            $this->password    = app()->security->generatePasswordHash($this->password);
            $this->create_time = time();
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        WorkerHasCity::deleteAll(['worker_id' => $this->worker_id]);
        WorkerHasCity::saveMany($this->worker_id, explode(',', $this->city_id));

        parent::afterSave($insert, $changedAttributes);
    }

    public function getPassword()
    {
        return $this->_password;
    }

}
