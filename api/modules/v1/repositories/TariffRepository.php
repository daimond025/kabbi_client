<?php

namespace api\modules\v1\repositories;

use api\models\taxi_tariff\TaxiTariff;

class TariffRepository
{
    public function getTariff($tariffId)
    {
        if (!$tariff = TaxiTariff::findOne($tariffId)) {
            throw new NotFoundException();
        }

        return $tariff;
    }
}
