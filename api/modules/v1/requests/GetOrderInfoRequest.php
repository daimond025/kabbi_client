<?php

namespace api\modules\v1\requests;

use api\models\order\Order;

/**
 * Class GetOrderInfoRequest
 * Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property integer $orderId - Идентификатор заказа
 * @property boolean $needCarPhoto - Флаг. Получить фото автомобиля.
 * @property boolean $needDriverPhoto - Флаг. Получить фото исполнителя.
 */
class GetOrderInfoRequest extends BaseVersionedApiRequest
{
    public $positionId = '1';

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'orderId'         => [
                'name' => 'order_id',
                'type' => self::TYPE_GET,
            ],
            'needCarPhoto'    => [
                'name' => 'need_car_photo',
                'type' => self::TYPE_GET,
            ],
            'needDriverPhoto' => [
                'name' => 'need_driver_photo',
                'type' => self::TYPE_GET,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['orderId', 'required'],
            [
                'orderId',
                'exist',
                'targetClass'     => Order::className(),
                'targetAttribute' => ['orderId' => 'order_id', 'tenantId' => 'tenant_id'],
            ],

            [['needCarPhoto', 'needDriverPhoto'], 'filter', 'filter' => [$this, 'filterNeedPhoto']],
            [['needCarPhoto', 'needDriverPhoto'], 'boolean'],
        ]);
    }

    public function filterNeedPhoto($value)
    {
        return (boolean)$value;
    }
}
