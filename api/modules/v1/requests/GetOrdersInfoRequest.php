<?php

namespace api\modules\v1\requests;

use api\models\order\Order;

/**
 * Class GetOrdersInfoRequest
 * Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property integer[] $ordersId - Идентификаторы заказов
 * @property boolean   $needCarPhoto - Флаг. Получить фото автомобиля.
 * @property boolean   $needDriverPhoto - Флаг. Получить фото исполнителя.
 */
class GetOrdersInfoRequest extends BaseVersionedApiRequest
{
    public $positionId = '1';

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'ordersId'        => [
                'name' => 'orders_id',
                'type' => self::TYPE_GET,
            ],
            'needCarPhoto'    => [
                'name' => 'need_car_photo',
                'type' => self::TYPE_GET,
            ],
            'needDriverPhoto' => [
                'name' => 'need_driver_photo',
                'type' => self::TYPE_GET,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['ordersId', 'filter', 'filter' => [$this, 'filterOrdersId']],
            ['ordersId', 'required'],

            [['needCarPhoto', 'needDriverPhoto'], 'filter', 'filter' => [$this, 'filterNeedPhoto']],
            [['needCarPhoto', 'needDriverPhoto'], 'boolean'],
        ]);
    }

    public function filterOrdersId($value)
    {
        $ordersId = explode(',', $value);

        return Order::find()
            ->select('order_id')
            ->where([
                'order_id'  => $ordersId,
                'tenant_id' => $this->tenantId,
            ])
            ->column();
    }

    public function filterNeedPhoto($value)
    {
        return (boolean)$value;
    }
}
