<?php

namespace api\modules\v1\services;

use api\models\transport\car\Car;
use api\modules\v1\repositories\CarRepository;
use api\modules\v1\repositories\TariffRepository;
use yii\helpers\ArrayHelper;

class CarService
{
    public $carRepository;
    public $tariffRepository;

    public function __construct(CarRepository $carRepository, TariffRepository $tariffRepository)
    {
        $this->carRepository    = $carRepository;
        $this->tariffRepository = $tariffRepository;
    }

    public function getAccessibleCarModelsByTariff($tenantId, $tariffId)
    {
        $tariff     = $this->tariffRepository->getTariff($tariffId);
        $carClassId = ArrayHelper::getValue($tariff, 'class_id');

        return $this->getAccessibleCarModelsByCarClass($tenantId, $carClassId);
    }

    private function getAccessibleCarModelsByCarClass($tenantId, $carClassId)
    {
        $cars = $this->carRepository->getCarsByCarClass($tenantId, $carClassId);

        $responseCars = [];
        /** @var Car $car */
        foreach ($cars as $car) {
            if (!$car->carModel) {
                continue;
            }

            $modelId = ArrayHelper::getValue($car, 'carModel.model_id');
            $brandId = ArrayHelper::getValue($car, 'carModel.carBrand.brand_id');
            $key     = "{$brandId}_$modelId";

            $responseCars[$key] = [
                'brand' => [
                    'id'   => $brandId,
                    'name' => ArrayHelper::getValue($car, 'carModel.carBrand.name'),
                ],
                'model' => [
                    'id'   => $modelId,
                    'name' => ArrayHelper::getValue($car, 'carModel.name'),
                ],
            ];
        }

        return array_values($responseCars);
    }
}
