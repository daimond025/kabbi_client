<?php

namespace api\models\client;

use Yii;

/**
 * Class ClientHasCompany
 *
 * @property int     client_id
 * @property int     $company_id
 * @property Client  $client
 * @property Company $company
 * @package api\models\client
 */
class ClientHasCompany extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_has_company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'company_id']);
    }
}
