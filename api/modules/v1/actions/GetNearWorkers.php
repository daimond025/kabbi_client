<?php

namespace api\modules\v1\actions;

use api\components\routeAnalyzer\TaxiRouteAnalyzer;
use api\models\city\City;
use api\models\tenant\Tenant;
use api\models\tenant\TenantHasCity;
use api\models\worker\Worker;
use api\modules\v1\requests\CheckWorkerPasswordRequest;
use api\modules\v1\requests\GetWorkerNearRequest;
use api\modules\v1\requests\GetWorkerRequest;
use api\modules\v1\requests\LikeWorkerRequest;
use api\modules\v1\services\ClientService;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;

/**
 * Понравился исполнитель или нет.
 *
 * Class LikeWorkerAction
 *
 * @package api\modules\v1\actions
 * @property GetWorkerNearRequest $request
 */
class GetNearWorkers extends BaseApiAction
{

    public $count_near_worker = 10;


    protected function getRequest()
    {
        return new GetWorkerNearRequest();
    }


    public function generationWorker($worker){
        $worker_temp = unserialize($worker);

        $worker_rezult = [] ;
        for ($y = 0; $y < 2; $y++){
         //   $workerLat           = ArrayHelper::getValue($worker_temp, 'geo.lat');
           // $workerLon           = ArrayHelper::getValue($worker_temp, 'geo.lon');

            $int = mt_rand(54, 56);
            $fractional = mt_rand(1,90) / 100;
            $workerLat = $int +  $fractional;

            $int = mt_rand(36, 38);
            $fractional =  $int +  mt_rand(1,90) / 100;
            $workerLon = $fractional;

            $worker_add = $worker_temp;
            $worker_add['geo']['lat'] = $workerLat;
            $worker_add['geo']['lon'] = $workerLon;

            $worker_rezult[] = serialize( $worker_add);
        }

        return $worker_rezult;
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    protected function execute()
    {
        $latitude = (double) $this->request->latitude;
        $longitude =(double) $this->request->longitude;

        $city = null;
        if($this->request->city_id){
            $city_tenant = TenantHasCity::find()->where(['city_id' => $this->request->city_id])->one();
            $city = $city_tenant->city->city_id;
        }
        $tenantId = $this->request->tenantId;
        $tenantLogin = Tenant::find()->select(['domain'])->where(['tenant_id' => $tenantId])->one();
        $tenantLogin = $tenantLogin->domain;


        $workersOnline = app()->get('redis_workers')->executeCommand('HVALS', [$this->request->tenantId]);

        // генерация случацных водителей
     /*  $worker_etalon = null;
        foreach ($workersOnline as $worker){
            $worker_etalon = $worker; break;
        }
        $workersOnline = $this->generationWorker($worker_etalon);*/

        $worker_output = [];
        foreach ($workersOnline as $worker){
            $worker_current = unserialize($worker);
            $workerLat           = ArrayHelper::getValue($worker_current, 'geo.lat');
            $workerLon           = ArrayHelper::getValue($worker_current, 'geo.lon');
            $currentCity          = ArrayHelper::getValue($worker_current, 'worker.city_id');

            if(!isset($workerLat) || !isset($workerLon) ){
                continue;
            }

            $distance = $this->getDistanceToClient($latitude,$longitude, $workerLat,$workerLon , "M" );
            $worker_current['distance']  =$distance;

            if(!is_null($city) && (int)$currentCity === (int)$city){
                $worker_output[] =$worker_current;
            }elseif (is_null($city)){
                $worker_output[] =$worker_current;
            }
        }

        ArrayHelper::multisort($worker_output, ['distance'], [SORT_ASC]);

        // если города нет - нужнор для гео запросов обязательно
        if(is_null($city)){
            $city_default = TenantHasCity::find()->where(['block' => 0] )->orderBy('city_id DESC')->one();
            $city = $city_default->city_id;
        }

        $near_worker = [];
        if(count($worker_output) > 0){

            $origins[] = [$latitude,  $longitude];

            $tenant['tenant_login'] = $tenantLogin;
            $tenant['tenant_id'] = $tenantId;

            $worker_distance = $this->getTimeWorkerToOrder($worker_output);
            $routeAnalyzerService = app()->get('routeAnalyzer');

            $rezult =  $routeAnalyzerService->getMatrixDistance($city, $tenant,$origins,$worker_distance );
            if(count($rezult) !== 0){
                $near_worker =$this->formatResponse($rezult);
            }
        }

        $worker_outpur['near_worker'] = $near_worker;
        $this->response->setContent($worker_outpur);
    }

    public function getTimeWorkerToOrder($workersOnline){
        //$workersOnline = array_splice($workersOnline, 0, $this->count_near_worker);
        $workers_coordinates = [];
        foreach ($workersOnline as $worker){
            $workerLat           = ArrayHelper::getValue($worker, 'geo.lat');
            $workerLon           = ArrayHelper::getValue($worker, 'geo.lon');
            $callsign         = ArrayHelper::getValue($worker, 'worker.callsign');

            $workers_coordinates[] = [(double) $workerLat,(double) $workerLon];
        }
        return  $workers_coordinates;


    }

    public function getDistanceToClient($lat1, $lon1, $lat2, $lon2, $unit)
    {
        $theta = $lon1 - $lon2;
        $dist  = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2))
            * cos(deg2rad($theta));
        $dist  = acos($dist);
        $dist  = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit  = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else {
            if ($unit == "M") {
                $k = $miles * 1.609344;

                return (int)($k * 1000);
            } else {
                if ($unit == "N") {
                    return ($miles * 0.8684);
                } else {
                    return $miles;
                }
            }
        }
    }

    // вывод ближайшего водителя
    public function formatResponse($date){

        $rezult = [];
        foreach ($date as $item){
            $distance = (array)$item;
            $rezult[] = [
                'distance' =>  isset( $distance['distance']) ? $distance['distance'] : PHP_INT_MAX ,
                'duration' => isset( $distance['duration']) ? $distance['duration'] : PHP_INT_MAX ,
                'place' => isset( $distance['destination']) ? $distance['destination'] : '',
            ];
        }
        ArrayHelper::multisort($rezult, ['duration'], [SORT_ASC]);
        return $rezult[0];

    }
}
