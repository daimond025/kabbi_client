<?php

namespace api\components\versionCompare;

use yii\base\Object;

/**
 * Компонент для проверки версии
 */
class VersionCompareComponent extends Object
{

    public $defaultVersion = '1.0.0';
    public $currentVersion = '1.0.0';
    public $defaultOperator = '=>';

    const GREATER_THAN_OR_EQUALS_TO = '>=';
    const GREATER_THAN_TO = '>';

    public function __construct($config = [])
    {
        $this->currentVersion = \Yii::$app->request->getHeaders()->get('versionclient', $this->defaultVersion);
        parent::__construct($config);
    }



    /**
     * Получить версию клиента по умолчанию
     * @return string
     */
    public function getDefaultVersion ()
    {
        return $this->defaultVersion;
    }
    /**
     * Установить версию клиента по умолчанию
     * @param $version
     */
    public function setDefaultVersion ($version)
    {
        $this->defaultVersion = $version;
    }



    /**
     * Получить текущую версию клиента
     * @return string
     */
    public function getCurrentVersion ()
    {
        return $this->currentVersion;
    }
    /**
     * Установить текущую версию клиента
     * @param $version
     */
    public function setCurrentVersion ($version)
    {
        $this->currentVersion = $version;
    }



    /**
     *
     * @param string $version
     * @return boolean
     */
    public function greaterThanOrEqualTo($version)
    {
        return $this->versionCompare($this->currentVersion, $version, self::GREATER_THAN_OR_EQUALS_TO);
    }

    /**
     * Сравнение версий клиента
     * @param string $v1
     * @param string $v2
     * @param string|null $operator
     * @return boolean
     */
    private function versionCompare($v1, $v2, $operator = null)
    {
        $operator = is_null($operator) ? $this->defaultOperator : $operator;
        return version_compare($v1, $v2, $operator);
    }
}
