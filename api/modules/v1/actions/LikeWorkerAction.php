<?php

namespace api\modules\v1\actions;

use api\modules\v1\requests\LikeWorkerRequest;
use api\modules\v1\services\ClientService;
use yii\rest\Controller;

/**
 * Понравился исполнитель или нет.
 *
 * Class LikeWorkerAction
 *
 * @package api\modules\v1\actions
 * @property LikeWorkerRequest $request
 */
class LikeWorkerAction extends BaseApiAction
{
    public $clientService;

    public function __construct($id, Controller $controller, ClientService $clientService, array $config = [])
    {
        parent::__construct($id, $controller, $config);

        $this->clientService = $clientService;
    }

    protected function getRequest()
    {
        return new LikeWorkerRequest();
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    protected function execute()
    {
        $this->clientService->setWorkerLikeByOrder(
            $this->request->tenantId,
            $this->request->orderId,
            $this->request->like
        );

        $this->response->setContent(1);
    }

}
