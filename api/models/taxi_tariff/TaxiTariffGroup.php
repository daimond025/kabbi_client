<?php

namespace api\models\taxi_tariff;

use Yii;
use api\models\tenant\Tenant;


class TaxiTariffGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxi_tariff_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => t('tariff', 'Tariff ID'),
            'tenant_id' => t('tariff', 'Tenant ID'),
            'name' => 'Name',
            'sort' => t('app', 'Sort'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

}
