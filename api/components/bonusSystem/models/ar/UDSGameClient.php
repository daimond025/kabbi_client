<?php

namespace bonusSystem\models\ar;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%uds_game_client}}".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $profile_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Client  $client
 */
class UDSGameClient extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%uds_game_client}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'profile_id'], 'required'],
            [['client_id', 'profile_id', 'created_at', 'updated_at'], 'integer'],
            [['client_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'client_id'  => 'Client ID',
            'profile_id' => 'Profile ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
