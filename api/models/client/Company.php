<?php

namespace api\models\client;

use app\models\balance\Account;
use Yii;
use api\models\tenant\Tenant;
use api\models\city\City;


class Company extends \yii\db\ActiveRecord
{

    const BLOCK = 1;
    const NOT_BLOCK = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public function getHasClient()
    {
        return $this->hasMany(ClientHasCompany::className(), ['company_id' => 'company_id']);
    }

    public function getAccount()
    {
        return $this->hasMany(Account::className(), ['owner_id' => 'company_id'])
            ->onCondition(['acc_kind_id' => Account::COMPANY_KIND]);
    }

    public function getAccounts()
    {
        return $this->hasMany(Account::className(), ['owner_id' => 'company_id']);
    }
}
