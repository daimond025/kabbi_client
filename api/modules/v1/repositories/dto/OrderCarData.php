<?php

namespace api\modules\v1\repositories\dto;

class OrderCarData
{
    public $name;
    public $gosNumber;
    public $color;
    public $photo;
    public $class_id;


    public function __construct($name, $gosNumber, $color, $photo, $class_id)
    {
        $this->name      = $name;
        $this->gosNumber = $gosNumber;
        $this->color     = $color;
        $this->photo     = $photo;
        $this->class_id     = $class_id;
    }
}