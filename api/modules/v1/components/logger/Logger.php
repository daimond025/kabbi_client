<?php

namespace api\modules\v1\components\logger;

use api\modules\v1\components\logger\target\LogTargetInterface;
use Ramsey\Uuid\Uuid;
use yii\base\Component;

/**
 * Class Logger
 * @package api\modules\v1\components\logger
 *
 * @property string $id
 * @property LogTargetInterface $target
 * @property string $identity
 */
class Logger extends Component
{
    public $id = null;
    public $target;
    public $identity;

    private static $startTime;

    const TYPE_MAIN = 'main';
    const TYPE_STATISTIC = 'statistic';
    const TYPE_ERROR = 'error';


    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->id = Uuid::uuid4()->toString();
        $this->target = app()->get($this->target);
        self::$startTime = self::getSystemTimes();
    }

    public function setId($value)
    {
        $this->id = $value;
    }

    public function getId()
    {
        return $this->id;
    }

    public function statistic($message = '')
    {
        $statistic = $this->getStatistic();
        $this->log($message . $statistic, [], self::TYPE_STATISTIC);
    }

    public function error($message,  $params = [])
    {
        $this->log($message, $params, self::TYPE_ERROR);
    }

    public function log($message,  $params = [], $main = self::TYPE_MAIN)
    {
        $message = ($message === null) ? $message : $this->interpolate($message, $params);

        $this->export($main, $message);
    }

    protected function export($main, $message)
    {
        $this->target->export($this->getContent($main, $message));
    }

    protected function getContent($main, $message)
    {
        return "[{$main}] {$this->id} {$message}";
    }

    /**
     * Interpolate log message
     *
     * @param string $message
     * @param array  $params
     *
     * @return string
     */
    protected function interpolate($message, array $params = [])
    {
        $replace = [];
        foreach ($params as $key => $val) {
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{' . $key . '}'] = $val;
            }
        }

        return strtr($message, $replace);
    }

    private function getStatistic()
    {
        $size = $this->getServerParam('CONTENT_LENGTH', 0);

        list($time, $utime, $stime) = array_map(function ($a, $b) {
            return ($b - $a) / 1000.0;
        }, self::$startTime, self::getSystemTimes());

        $memory = round(memory_get_peak_usage() / 1024, 3);

        return sprintf('n:%db t:%.3fms u:%.3fms s:%.3fms m:%dkb', $size, $time, $utime, $stime, $memory);
    }

    private function getServerParam($param, $defaultValue = '')
    {
        return array_key_exists($param, $_SERVER) ? $_SERVER[$param] : $defaultValue;
    }

    private static function getSystemTimes()
    {
        $data = getrusage();

        return [
            microtime(true) * 1e6,
            $data['ru_utime.tv_sec'] * 1e6 + $data['ru_utime.tv_usec'],
            $data['ru_stime.tv_sec'] * 1e6 + $data['ru_stime.tv_usec'],
        ];
    }
}