<?php

namespace api\modules\v1\actions;

use api\models\worker\Worker;
use api\modules\v1\requests\CheckWorkerPasswordRequest;
use api\modules\v1\requests\LikeWorkerRequest;
use api\modules\v1\services\ClientService;
use yii\rest\Controller;

/**
 * Понравился исполнитель или нет.
 *
 * Class LikeWorkerAction
 *
 * @package api\modules\v1\actions
 * @property LikeWorkerRequest $request
 */
class CheckWorkerPassword extends BaseApiAction
{


    protected function getRequest()
    {
        return new CheckWorkerPasswordRequest();
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    protected function execute()
    {

         $worker_check =  Worker::find()
            ->where([
                'tenant_id' => $this->request->tenantId,
                'callsign'  => $this->request->workerCallsign,
            ])->one();


        $worker['auth'] = false;
        $worker['work'] = [];
        if($worker_check){
            $validate_password = app()->security->validatePassword( $this->request->password, $worker_check->password) ;
            if($validate_password){
                $worker['auth'] = true;
                $worker_add['callsign'] = $worker_check->callsign;
                $worker_add['last_name'] = $worker_check->last_name;
                $worker_add['name'] = $worker_check->name;
                $worker_add['second_name'] = $worker_check->second_name;
                $worker_add['tenant_company_id'] = $worker_check->tenant_company_id;
                $worker_add['email'] = $worker_check->email;
                $worker['work'] = $worker_add;
            }
        }

        $this->response->setContent($worker);
    }

}
