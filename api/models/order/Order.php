<?php

namespace api\models\order;

use api\components\orderApi\interfaces\createOrder\ApiOrderInterface;
use api\models\city\City;
use api\models\client\Client;
use api\models\client\ClientAddressHistory;
use api\models\taxi_tariff\TaxiTariff;
use api\models\taxi_tariff\TaxiTariffHasCompany;
use api\models\tenant\Tenant;
use api\models\tenant\User;
use api\models\transport\car\Car;
use api\models\transport\car\CarOption;
use api\models\worker\Worker;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $order_id
 * @property integer $tenant_id
 * @property integer $worker_id
 * @property integer $city_id
 * @property integer $tariff_id
 * @property integer $client_id
 * @property string  $phone
 * @property string  $except_car_models
 * @property string  $client_passenger_phone
 * @property string  $client_passenger_last_name
 * @property string  $client_passenger_name
 * @property string  $client_passenger_second_name
 * @property integer $user_create
 * @property integer $status_id
 * @property integer $user_modifed
 * @property integer $company_id
 * @property integer $parking_id
 * @property string  $address
 * @property string  $commentc
 * @property string  $predv_price
 * @property string  $predv_distance
 * @property string  $predv_time
 * @property string  $device
 * @property string  $order_number
 * @property string  $payment
 * @property integer $show_phone
 * @property string  $create_time
 * @property string  $updated_time
 * @property string  $order_time
 * @property string  $time_to_client
 * @property integer $time_offset
 * @property integer $position_id
 * @property integer $is_fix
 * @property integer $update_time
 * @property integer $deny_refuse_order
 * @property integer $promo_code_id
 * @property float   $predv_price_no_discount
 * @property integer $processed_exchange_program_id
 *
 * @property Worker  $worker
 * @property Car     $car
 *
 */
class Order extends ActiveRecord implements ApiOrderInterface
{

    use ApiOrderTrait;

    const ORDER_ACTION_CLIENT = 'NEW_ORDER_CLIENT';

    const DEVICE_0 = 'DISPATCHER';
    const DEVICE_1 = 'IOS';
    const DEVICE_2 = 'ANDROID';
    const PRE_ORDER_TIME = 1800;
    const PICK_UP_TIME = 300;

    const PAYMENT_CORP_BALANCE = 'CORP_BALANCE';
    const PAYMENT_PERSONAL_ACCOUNT = 'PERSONAL_ACCOUNT';
    const PAYMENT_CASH = 'CASH';
    const PAYMENT_CARD = 'CARD';

    public $additional_option;
    public $order_date;
    public $order_hours;
    public $order_minutes;
    public $lang;
    public $versionclient;
    public $bonus_promo_code;
    public $orderAction;
    public $pan;
    public $except_car_models;
    public $client_passenger_lastname;
    public $client_passenger_name;
    public $client_passenger_secondname;

    public static function tableName()
    {
        return '{{%order}}';
    }

    public function rules()
    {
        return [
            ['tenant_id', 'exist', 'targetClass' => '\api\models\tenant\Tenant'],
            ['city_id', 'exist', 'targetClass' => '\api\models\city\City'],
            [
                'tariff_id',
                'exist',
                'targetClass'     => '\api\models\taxi_tariff\TaxiTariff',
                'targetAttribute' => ['tariff_id', 'tenant_id'],
            ],
            [['predv_price', 'predv_time', 'predv_distance', 'client_device_token', 'comment'], 'safe'],
            [
                'parking_id',
                'exist',
                'targetClass'     => '\api\models\parking\Parking',
                'targetAttribute' => ['parking_id', 'tenant_id', 'city_id'],
            ],
            ['client_id', 'exist', 'targetClass' => '\api\models\client\Client'],
            [
                ['phone', 'passenger_phone'],
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", '', $value);
                },
            ],
            ['bonus_payment', 'in', 'range' => [0, 1]],
            [['create_time', 'status_time', 'company_id', 'app_id', 'predv_price_no_discount'], 'number'],
            [['device'], 'in', 'range' => Client::getClentTypeList()],
            [
                'company_id',
                'exist',
                'targetAttribute' => ['tariff_id' => 'tariff_id', 'company_id'],
                'targetClass'     => TaxiTariffHasCompany::className(),
                'when'            => function ($model) {
                    return in_array($model->tariff->type,
                        [TaxiTariff::TYPE_COMPANY, TaxiTariff::TYPE_COMPANY_CORP_BALANCE]);
                },
            ],
            [
                ['order_number'],
                function ($attribute) {
                    if (ctype_digit($this->order_number) || $this->order_number instanceof Expression) {
                        return true;
                    }

                    $this->addError(t('order', 'Invalid "{attribute}" format', [
                        'attribute' => $this->getAttributeLabel($attribute)
                    ]));
                }
            ],
            [
                'tariff_id',
                'exist',
                'targetAttribute' => ['tenant_id' => 'tenant_id', 'tariff_id'],
                'targetClass'     => TaxiTariff::className(),
            ],
            ['is_fix', 'in', 'range' => [0, 1]],
            [
                [
                    'predv_price',
                    'predv_time',
                    'predv_distance',
                    'client_device_token',
                    'comment',
                    'create_time',
                    'status_time',
                    'payment',
                    'status_id',
                    'order_time',
                    'address',
                    'currency_id',
                    'position_id',
                    'time_offset',
                    'company_id',
                    'promo_code_id',
                ],
                'safe',
            ],
        ];
    }

    public static function getPaymentList()
    {
        return [
            self::PAYMENT_CASH,
            self::PAYMENT_CORP_BALANCE,
            self::PAYMENT_PERSONAL_ACCOUNT,
            self::PAYMENT_CARD,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(CarOption::className(), ['option_id' => 'option_id'])->viaTable('{{%order_has_option}}',
            ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_create']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public static function filterAddress($address, $lang, $geocoder_type, $tenant_id, $city_id)
    {
        $address = json_decode($address, true);

        if (is_array($address)) {
            $newArray = ['address' => []];
            $chr_i    = 'A';
            foreach ($address['address'] as $addr) {
                $newAddress = [];
                foreach ($addr as $key => $paramVal) {
                    if (!empty($paramVal)) {
                        $paramVal = trim(strip_tags(stripcslashes($paramVal)));

                        if ($key == 'city') {
                            $paramVal = mb_substr($paramVal, 0, 50, 'utf-8');
                        }
                        if ($key == 'street') {
                            $paramVal = mb_substr($paramVal, 0, 100, 'utf-8');
                        }
                        if (in_array($key, ['house', 'housing', 'porch', 'apt'])) {
                            $paramVal = mb_substr($paramVal, 0, 10, 'utf-8');
                        }
                    }
                    $addr[$key] = $paramVal;
                }

                $addressData['city_id'] = ArrayHelper::getValue($addr, 'city_id');
                $addressData['city']    = ArrayHelper::getValue($addr, 'city');
                $addressData['street']  = ArrayHelper::getValue($addr, 'street');
                $addressData['housing'] = ArrayHelper::getValue($addr, 'housing');
                $addressData['house']   = ArrayHelper::getValue($addr, 'house');
                $addressData['porch']   = ArrayHelper::getValue($addr, 'porch');
                $addressData['apt']     = ArrayHelper::getValue($addr, 'apt');
                $addressData['lat']     = ArrayHelper::getValue($addr, 'lat');
                $addressData['lon']     = ArrayHelper::getValue($addr, 'lon');

                $parkingInfo               = \Yii::$app->routeAnalyzer->getParkingByCoords($tenant_id, $city_id,
                    $addressData['lat'],
                    $addressData['lon'], false);
                $addressData['parking']    = (string)$parkingInfo['parking_name'];
                $addressData['parking_id'] = (string)($parkingInfo['inside']);


                $newArray['address'][$chr_i++] = $addressData;
            }

            return $newArray;
        }
    }

    public static function getLastOrderNumber($tenant_id)
    {
        return self::find()
            ->select('order_number')
            ->where(['tenant_id' => $tenant_id])
            ->orderBy(['order_number' => SORT_DESC])
            ->asArray()
            ->limit(1)
            ->scalar();
    }

    public static function getOrderFromRedis($tenant_id, $order_id)
    {
        $order = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenant_id, $order_id]);

        return !empty($order) ? unserialize($order) : null;
    }

    public static function saveOrderToRedis($orderId, $costData, $isNewRecord = true)
    {
        $tariffId = isset($costData["tariffInfo"]["tariffDataCity"]["tariff_id"])
            ? $costData["tariffInfo"]["tariffDataCity"]["tariff_id"] : null;

        $order                 = self::find()
            ->where([
                'order_id' => $orderId,
            ])
            ->with([
                'status',
                'tariff',
                'tariff.class'              => function ($query) {
                    $query->select(['class_id', 'class']);
                },
                'client'                    => function ($query) {
                    $query->select(['client_id', 'last_name', 'name', 'second_name', 'lang', 'send_to_email', 'email']);
                },
                'userCreated'               => function ($query) {
                    $query->select(['user_id', 'last_name', 'name', 'second_name']);
                },
                'options.additionalOptions' => function ($query) use ($tariffId) {
                    $query->select(['id', 'additional_option_id', 'price'])->where([
                        'tariff_id'   => $tariffId,
                        'tariff_type' => "CURRENT",
                    ]);
                },
                'tenant'                    => function ($query) {
                    $query->select(['domain', 'company_name', 'contact_phone', 'contact_email', 'site']);
                },
                'worker',
            ])
            ->asArray()
            ->one();
        $order['tenant_login'] = $order['tenant']['domain'];
        $order['costData']     = $costData;

        $orderSerialized = serialize($order);
        $result          = \Yii::$app->redis_orders_active->executeCommand('hset',
            [$order['tenant_id'], $orderId, $orderSerialized]);

        return $result;
    }

    public static function getActiveOrderList($tenant_id, $phone, $lang)
    {
        $result     = [];
        $redis_list = \Yii::$app->redis_client_active_orders->executeCommand('HGET', [$tenant_id, $phone]);

        if (!empty($redis_list)) {
            $redis_list = unserialize($redis_list);

            if (is_array($redis_list)) {
                foreach ($redis_list as $redis_id) {
                    $order_data = \Yii::$app->redis_orders_active->executeCommand('HGET', [$tenant_id, $redis_id]);

                    $order_data = unserialize($order_data);
                    if (is_array($order_data)) {
                        $address = unserialize($order_data['address']);
                        unset($address['A']['city_id']);
                        unset($address['A']['parking_id']);
                        unset($address['A']['parking']);
                        $order_info = [
                            'order_id'     => $redis_id,
                            'order_number' => (string)$order_data['order_number'],
                            'status_group' => (string)$order_data['status']['status_group'],
                            'status_name'  => t(OrderStatus::SOURCE_CATEGORY, $order_data['status']['name'], [], $lang),
                            'point_from'   => (array)$address['A'],
                        ];

                        $result[] = $order_info;
                    }
                }
            }
        }

        return $result;
    }

    public static function getOrderNumber($orderId)
    {
        return self::find()
            ->select('order_number')
            ->where(['order_id' => $orderId])
            ->scalar();
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->getIsNewRecord()) {
            $retryCount = app()->params['saveOrder.retryCount'];
            for ($i = 0; $i < $retryCount; $i++) {
                $this->order_number = new Expression('(SELECT * FROM (SELECT COALESCE (MAX(order_number), 0) FROM tbl_order WHERE tenant_id = ' . $this->tenant_id . ' FOR UPDATE) AS t) + 1');
                try {
                    return parent::save($runValidation, $attributeNames)
                        && $this->order_number = (int)self::getOrderNumber($this->order_id);
                } catch (\Exception $ex) {
                    if (strpos($ex->getMessage(), 'ui_order__tenant_id__order_number') === false
                        && strpos($ex->getMessage(),'Deadlock found when trying to get lock; try restarting transaction') === false) {
                        throw $ex;
                    } else {
                        $this->delay();
                        continue;
                    }
                }
            }

            return false;
        }

        return parent::save($runValidation, $attributeNames);
    }

    protected function delay()
    {
        usleep(mt_rand(0, 500000));
    }


    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        //Сохранение адресов клиента в истории
        $this->saveAddressToHistory();
    }


    protected function saveAddressToHistory()
    {
        $address_array = unserialize($this->address);

        foreach ($address_array as $address) {
            $this->updateOrCreateClientAddressHistoryModel($address);
        }
    }

    public function updateOrCreateClientAddressHistoryModel($address)
    {
        $model = ClientAddressHistory::findOne([
            'client_id' => $this->client_id,
            'city_id'   => $this->city_id,
            'city'      => $address['city'],
            'street'    => $address['street'],
            'house'     => $address['house'],
            'lat'       => $address['lat'],
            'lon'       => $address['lon'],
        ]);

        if ($model) {
            return $model->updateTime();
        }

        $model             = new ClientAddressHistory();
        $model->attributes = [
            'client_id' => $this->client_id,
            'city_id'   => $this->city_id,
            'city'      => $address['city'],
            'street'    => $address['street'],
            'house'     => $address['house'],
            'lat'       => $address['lat'],
            'lon'       => $address['lon'],
        ];

        return $model->save();
    }


}