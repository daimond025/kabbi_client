<?php


namespace api\modules\v1\controllers;

use api\modules\v1\actions\crm\CostingOrderAction;
use api\modules\v1\actions\crm\CreateOrderAction;
use api\modules\v1\actions\crm\GetRelevantWorkersAction;
use api\modules\v1\actions\crm\UpdateOrderAction;
use api\modules\v1\actions\crm\UpdateOrderResultAction;
use yii\rest\Controller;

class ApiCrmController extends Controller
{
    public function actions()
    {
        return [
            'get_relevant_workers' => GetRelevantWorkersAction::className(),
            'costing_order'        => CostingOrderAction::className(),
            'create_order'         => CreateOrderAction::className(),
            'update_order'         => UpdateOrderAction::className(),
            'update_order_result'  => UpdateOrderResultAction::className(),
        ];
    }

    public function actionError()
    {
        return 'error';
    }

    public function actionTest()
    {
        return 'test';
    }
}