<?php

namespace bonusSystem\models;

/**
 * Class BonusSystemType
 * @package bonusSystem\models
 */
class BonusSystemType
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $apiKey;


    /**
     * BonusSystemType constructor.
     *
     * @param int    $id
     * @param string $name
     * @param string $apiKey
     */
    public function __construct($id, $name, $apiKey)
    {
        $this->id     = $id;
        $this->name   = $name;
        $this->apiKey = $apiKey;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

}