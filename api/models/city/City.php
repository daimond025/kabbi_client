<?php

namespace api\models\city;

use api\models\tenant\TenantHasCity;
use app\models\worker\WorkerHasCity;
use Yii;

/**
 * This is the model class for table "{{%tbl_city}}".
 *
 * @property string              $city_id
 * @property string              $republic_id
 * @property string              $name
 * @property string              $lat
 * @property string              $lon
 * @property Republic            $republic
 * @property PublicPlace[]       $tblPublicPlaces
 * @property TenantHasCity[]     $tblTenantHasCities
 * @property WorkerHasCity[]     $workerHasCities
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id'     => t('city', 'City ID'),
            'republic_id' => t('city', 'Republic ID'),
            'name'        => t('city', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepublic()
    {
        return $this->hasOne(Republic::className(), ['republic_id' => 'republic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasTenant()
    {
        return $this->hasOne(TenantHasCity::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasCities()
    {
        return $this->hasMany(WorkerHasCity::className(), ['city_id' => 'city_id']);
    }


    /**
     * Возвращает разницу в секундах местного времени от по гринвичу
     *
     * @param int $city_id
     * @param int $time
     *
     * @return int | false
     */
    public static function getTimeOffset($city_id, $time = null)
    {
        $time  = is_null($time) ? time() : $time;
        $model = self::find()
            ->where(['city_id' => $city_id])
            ->one();

        if ($model) {
            return $model->republic->getTimeOffset($time);
        }

        return false;
    }

    public function getName($lang = '')
    {

        if (isset($this->{'name_' . $lang})) {
            return $this->{'name_' . $lang};
        }

        return $this->name;
    }

    public static function searchCity($part, $lang)
    {
        $model = self::find()
            ->select([
                'city_id',
                'city_name' => 'name' . $lang,
                'city_lat'  => 'lat',
                'city_lon'  => 'lon',
            ])
            ->where(['LIKE', self::tableName() . ".name" . $lang, $part])
            ->orderBy([
                'sort' => SORT_DESC,
            ])
            ->limit(15)
            ->asArray()
            ->all();

        return $model;
    }
}
