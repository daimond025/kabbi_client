<?php

namespace api\modules\v1\requests;

use api\models\order\Order;
use api\models\tenant\TenantHasCity;
use api\models\worker\Worker;

/**
 * Class LikeWorkerRequest
 * Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property integer $workerCallsign- Идентификатор водителя *
 * @property integer $password  -- пароль водителя *
 */
class GetWorkerNearRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'latitude' => [
                'name' => 'latitude',
                'type' => self::TYPE_POST,
            ],
            'longitude' => [
                'name' => 'longitude',
                'type' => self::TYPE_POST,
            ],
            'city_id' => [
                'name' => 'city_id',
                'type' => self::TYPE_POST,
            ],
        ]);

    }


    public function rules()
    {
        return array_merge(parent::rules(), [
            [['latitude','longitude' ], 'required'],
            [ ['latitude'], 'number', 'min' => 0, 'max' => 90],
            [ ['longitude'], 'number', 'min' => 0, 'max' => 180],
            [
                'city_id',
                'exist',
                'targetClass'     =>  TenantHasCity::className(),
                'targetAttribute' => ['city_id'],
            ],
        ]);
    }
}
