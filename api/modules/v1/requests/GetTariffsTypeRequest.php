<?php

namespace api\modules\v1\requests;

/**
 * Class GetTariffsTypeRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property string $phone - Телефонный номер клиента
 * @property int    $cityId - Ид филиала*
 * @property int    $date - Дата
 */
class GetTariffsTypeRequest extends GetTariffsListRequest
{

}