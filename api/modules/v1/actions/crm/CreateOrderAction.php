<?php

namespace api\modules\v1\actions\crm;

use api\components\orderApi\OrderApi;
use api\models\client\Client;
use api\models\order\Order;
use api\models\tenant\TenantSetting;
use api\modules\v1\actions\BaseApiAction;
use api\modules\v1\components\CityService;
use api\modules\v1\components\ClientService;
use api\modules\v1\components\services\ProcessingErrorsCrmApiOrder;
use api\modules\v1\exceptions\InternalErrorException;
use api\modules\v1\requests\crm\CreateOrderRequest;
use GuzzleHttp\Exception\ClientException;

/**
 * Получение предварительной стоимости заказа
 *
 * Class CostingOrderAction
 *
 * @package api\modules\v1\actions\crm
 * @property CreateOrderRequest $request
 */
class CreateOrderAction extends BaseApiAction
{
    protected $_clientId;

    protected function getRequest()
    {
        return new CreateOrderRequest();
    }

    protected function beforeExecute()
    {
        if (parent::beforeExecute()) {
            // Если время заказа не задано, то указываем текущее время
            if ($this->request->orderTime === null) {
                $this->request->orderTime = time();

                /** @var CityService $cityService */
                $cityService = app()->get('cityService');

                $offsetTime               = $cityService->getTimeOffset($this->request->cityId);
                $this->request->orderTime += $offsetTime;
            }

            return true;
        }

        return false;
    }


    protected function execute()
    {

        /** @var OrderApi $apiOrder */
        $apiOrder = \Yii::$app->orderApi;
        /** @var ProcessingErrorsCrmApiOrder $processingErrors */
        $processingErrors = \Yii::createObject(ProcessingErrorsCrmApiOrder::class);

        $order = new Order([
            'tenant_id' => $this->request->tenantId,
            'city_id'   => $this->request->cityId,
            'tariff_id' => $this->request->tariffId,

            'phone'    => $this->request->phone,
            'comment'  => $this->request->comment,
            'payment'  => $this->request->payment,
            'address'  => $this->request->address,

            'position_id'   => $this->request->positionId,
            'bonus_payment' => $this->request->bonusPayment,

            'orderAction' => $this->request->state,
            'order_time'  => $this->request->orderTime,

            'additionalOption' => $this->request->additionalOptions,
            'lang'             => $this->request->lang == 'en' ? 'en-US' : $this->request->lang,
            'company_id'       => $this->request->companyId
        ]);

        try {
            $res = $apiOrder->createOrder($order);
            $order = new Order($res);

            $this->response->setContent([
                'order_id' => $order->order_id,
            ]);

        } catch (ClientException $ex) {
            $errors = json_decode($ex->getResponse()->getBody(), true);
            $processingErrors->setResponse($errors);
            $processingErrors->testCodeError();

        } catch (\Exception $ex) {
            \Yii::error("Ошибка сохранения в мускул заказа: " . $ex->getMessage());
            throw new InternalErrorException();
        }
    }

    /**
     * Вернуть или создать и вернуть клиента
     * @return Client|null
     */
    protected function findOrCreateClient()
    {
        /** @var ClientService $clientService */
        $clientService = app()->get('clientService');
        $client        = $clientService->findByPhone($this->request->tenantId, $this->request->phone);

        if (!$client) {
            $client = $clientService->createClientByPhone($this->request->tenantId, $this->request->cityId,
                $this->request->phone);
        }

        return $client;
    }

    protected function getGeoCoderType()
    {
        $geoCoderType = TenantSetting::getSettingValue($this->request->tenantId, TenantSetting::SETTING_GEOCODER_TYPE,
            $this->request->cityId, $this->request->positionId);

        return !empty($geoCoderType) ? $geoCoderType : CreateOrderRequest::DEFAULT_GEOCODER_TYPE;
    }

}