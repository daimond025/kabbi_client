<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class InvalidPanException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::INVALID_PAN;
        parent::__construct('', $code, null);
    }

}