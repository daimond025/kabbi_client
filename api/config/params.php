<?php

return [
    'checkSignature' => true,

    'frontend.protocol'  => getenv('FRONTEND_PROTOCOL'),
    'frontend.domain'    => getenv('FRONTEND_DOMAIN'),
    'frontend.uploadUrl' => getenv('FRONTEND_UPLOAD_URL'),
    'frontend.basicAuth' => getenv('FRONTEND_BASIC_AUTH'),

    'bonusSystem.UDSGame.baseUrl'           => 'https://udsgame.com/v1/partner/',
    'bonusSystem.UDSGame.connectionTimeout' => 15,
    'bonusSystem.UDSGame.timeout'           => 15,

    'service.engine.url' => getenv('SERVICE_ENGINE_URL'),
    'paymentGateApi.url' => getenv('API_PAYGATE_URL'),

    'supportedLanguages'   => ['en', 'ru', 'az', 'de', 'se', 'sr', 'ro', 'fa', 'fi', 'ar', 'uz', 'tg'],
    'saveOrder.retryCount' => 5,

    'version' => require __DIR__ . '/version.php',
];
