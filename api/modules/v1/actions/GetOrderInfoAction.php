<?php

namespace api\modules\v1\actions;

use api\modules\v1\requests\GetOrderInfoRequest;
use api\modules\v1\services\OrderService;
use yii\rest\Controller;

/**
 * Получить информацию по заказу.
 *
 * Class GetOrderInfoAction
 *
 * @package api\modules\v1\actions
 * @property GetOrderInfoRequest $request
 */
class GetOrderInfoAction extends BaseApiAction
{
    public $orderService;

    public function __construct($id, Controller $controller, OrderService $orderService, array $config = [])
    {
        parent::__construct($id, $controller, $config);

        $this->orderService = $orderService;
    }

    protected function getRequest()
    {
        return new GetOrderInfoRequest();
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    protected function execute()
    {
        $orderInfo = $this->orderService->getOrderInfo(
            $this->request->tenantId,
            $this->request->orderId,
            $this->request->positionId,
            $this->request->lang,
            $this->request->needCarPhoto,
            $this->request->needDriverPhoto
        );

        $this->response->setContent([
            'order_info' => $orderInfo,
        ]);
    }
}
