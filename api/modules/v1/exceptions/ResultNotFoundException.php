<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class ResultNotFoundException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::RESULT_NOT_FOUND;
        parent::__construct('', $code, null);
    }

}