<?php

namespace api\modules\v1\components;

use api\modules\v1\exceptions\InvalidErrorCode;
use yii\base\Object;
use yii\web\ErrorAction;

/**
 * Class Response
 *
 * @property integer $code
 * @property mixed   $content
 *
 * @package api\modules\v1\components
 */
class Response extends Object
{
    protected $code;
    protected $content;
    protected $currentTime;


    public function __construct($config = [])
    {
        parent::__construct($config);

        $this->code    = ErrorCode::OK;
        $this->content = [];
    }

    /**
     * Получить $code
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    public function getCodeInfo()
    {
        return ErrorCode::getInfo($this->code);
    }


    /**
     * Получить $content
     *
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Получить $currentTime
     *
     * @return mixed
     */
    public function getCurrentTime()
    {
        return $this->currentTime ? (int)$this->currentTime : time();
    }

    /**
     * Установить $code
     *
     * @param integer $code
     */
    public function setCode($code)
    {
        if ($code !== ErrorCode::OK) {
            $this->setContent(null);
        }
        $this->code = $code;
    }

    /**
     * Установить $content
     *
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Установить $currentTime
     *
     * @param mixed $currentTime
     */
    public function setCurrentTime($currentTime)
    {
        $this->currentTime = $currentTime;
    }

    public function getResponse()
    {
        return [
            'code'         => $this->getCode(),
            'info'         => $this->getCodeInfo(),
            'result'       => $this->getContent(),
            'current_time' => $this->getCurrentTime(),
        ];
    }
}