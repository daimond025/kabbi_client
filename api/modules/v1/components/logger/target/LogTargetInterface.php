<?php

namespace api\modules\v1\components\logger\target;

interface LogTargetInterface
{
    public function export($message);
}