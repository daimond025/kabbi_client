<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class OrderIsNotCompletedYetException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::ORDER_IS_NOT_COMPLETED_YET;
        parent::__construct('', $code, null);
    }
}
