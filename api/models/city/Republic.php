<?php

namespace api\models\city;

use Yii;

/**
 * This is the model class for table "{{%tbl_republic}}".
 *
 * @property string $republic_id
 * @property integer $country_id
 * @property string $name
 * @property string $timezone
 *
 * @property TblCity[] $tblCities
 * @property TblCountry $country
 */
class Republic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%republic}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'republic_id' => t('city', 'Republic ID'),
            'country_id' => t('city', 'Country ID'),
            'name' => t('city', 'Name'),
            'shortname' => 'shortname',
            'timezone' => t('city', 'Timezone'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    /**
     * Возвращает разницу в секундах местного времени от по гринвичу
     * @param int $time
     * @return int
     */
    public function getTimeOffset($time = null)
    {
        $time = is_null($time) ? time() : $time;

        $offset = $this->timezone * 60 * 60;

        return $offset;
    }
}
