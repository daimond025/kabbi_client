<?php

namespace api\modules\v1\repositories;

use api\models\order\Order;
use api\models\order\OrderDetailCost;
use api\models\worker\WorkerReviewRating;
use api\modules\v1\repositories\dto\OrderAddressData;
use api\modules\v1\repositories\dto\OrderCarData;
use api\modules\v1\repositories\dto\OrderClientData;
use api\modules\v1\repositories\dto\OrderInfoData;
use api\modules\v1\repositories\dto\OrderPaymentData;
use api\modules\v1\repositories\dto\OrderStatusData;
use api\modules\v1\repositories\dto\OrderWorkerData;
use yii\helpers\ArrayHelper;

class OrderRepository
{

    /**
     * @param $tenantId
     * @param $orderId
     *
     * @return OrderInfoData|mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function getOrderData($tenantId, $orderId)
    {
        $orders = $this->getOrdersData($tenantId, (array)$orderId);

        if (empty($orders)) {
            throw new NotFoundException();
        }

        return reset($orders);
    }
    /**
     * @param       $tenantId
     * @param array $orderIds
     *
     * @return OrderInfoData[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getOrdersData($tenantId, array $orderIds)
    {
        $notFoundOrderIds = $orderIds;
        $ordersFromRedis  =  $this->getOrdersDataFromRedis($tenantId, $notFoundOrderIds);
        $ordersFromMysql  = [];

        $notFoundOrderIds = array_diff($orderIds, array_keys($ordersFromRedis));

        if ($notFoundOrderIds) {
            $ordersFromMysql = $this->getOrdersDataFromMysql($tenantId, $notFoundOrderIds);

            if (count($ordersFromMysql) !== count($notFoundOrderIds)) {
                throw new \DomainException('Not found orderIds: ' . implode(', ', $notFoundOrderIds));
            }
        }

        return ArrayHelper::merge($ordersFromRedis, $ordersFromMysql);
    }

    /**
     * @param       $tenantId
     * @param array $orderIds
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    private function getOrdersDataFromRedis($tenantId, array $orderIds)
    {
        $orders = (array)\Yii::$app->get('redis_orders_active')->executeCommand('hvals', [$tenantId]);

        $orders = array_map(function ($order) {
            return unserialize($order);
        }, $orders);

        $ordersData = [];
        foreach ($orders as $order) {
            $orderId = ArrayHelper::getValue($order, 'order_id');

            if ($orderId === null || !ArrayHelper::isIn($orderId, $orderIds)) {
                continue;
            }

            // если есть водитель на заказе
           /* $orders = (array)\Yii::$app->get('redis_workers')->executeCommand('hget', [$tenantId, $workerCallsign]);*/


            $statusData = new OrderStatusData(
                ArrayHelper::getValue($order, 'status.status_id'),
                ArrayHelper::getValue($order, 'status.status_group'),
                ArrayHelper::getValue($order, 'status.name'),
                OrderStatusData::redis
            );



            $paymentData = new OrderPaymentData(
                ArrayHelper::getValue($order, 'payment')
            );

            $clientData = new OrderClientData(
                ArrayHelper::getValue($order, 'client_id'),
                ArrayHelper::getValue($order, 'phone')
            );

            $tariff['tariff_id']= ArrayHelper::getValue($order, 'tariff.tariff_id');
            $tariff['class_id']= ArrayHelper::getValue($order, 'tariff.class_id');

            $workerData = $this->getWorkerDataFromRedis($tenantId, $order);

            $address     = unserialize(ArrayHelper::getValue($order, 'address'));
            $addressData = [];
            if (ArrayHelper::isTraversable($address)) {
                foreach ($address as $item) {
                    $addressData[] = new OrderAddressData(
                        ArrayHelper::getValue($item, 'city'),
                        ArrayHelper::getValue($item, 'street'),
                        ArrayHelper::getValue($item, 'house'),
                        ArrayHelper::getValue($item, 'lat'),
                        ArrayHelper::getValue($item, 'lon'),
                        ArrayHelper::getValue($item, 'phone'),
                        ArrayHelper::getValue($item, 'comment'),
                        ArrayHelper::getValue($item, 'confirmation_code')
                    );
                }
            }

            $timeToClient         = ArrayHelper::getValue($order, 'update_time')
                + ArrayHelper::getValue($order, 'time_to_client') * 60 - time();

            $ordersData[$orderId] = new OrderInfoData(
                ArrayHelper::getValue($order, 'order_id'),
                ArrayHelper::getValue($order, 'order_number'),
                ArrayHelper::getValue($order, 'tenant_id'),
                ArrayHelper::getValue($order, 'city_id'),
                $statusData,
                $paymentData,
                $clientData,
                ArrayHelper::getValue($order, 'predv_price'),
                ArrayHelper::getValue($order, 'order_time'),
                ArrayHelper::getValue($order, 'time_offset'),
                $timeToClient,
                $workerData,
                [],
                $addressData,
                $tariff
            );
        }

        return $ordersData;
    }


    private function getOrdersDataFromMysql($tenantId, array $orderIds)
    {
        /** @var Order[] $orders */
        $orders = Order::find()
            ->alias('order')
            ->joinWith([
                'status',
                'worker',
                'tariff',
            ])
            ->where([
                'order.tenant_id' => $tenantId,
                'order.order_id'  => $orderIds,
            ])
            ->all();


        $ordersData = [];
        foreach ($orders as $order) {
            $statusData = new OrderStatusData(
                $order->status_id,
                ArrayHelper::getValue($order, 'status.status_group'),
                ArrayHelper::getValue($order, 'status.name')
            );

            $paymentData = new OrderPaymentData(
                $order->payment
            );

            $clientData = new OrderClientData(
                $order->client_id,
                $order->phone
            );

            $tariff['tariff_id']= ArrayHelper::getValue($order, 'tariff.tariff_id');
            $tariff['class_id']= ArrayHelper::getValue($order, 'tariff.class_id');


            $workerData = $this->getWorkerFromMysql($order);

            $address    = unserialize($order->address);

            $addressData = [];
            if (ArrayHelper::isTraversable($address)) {
                foreach ($address as $item) {
                    $addressData[] = new OrderAddressData(
                        ArrayHelper::getValue($item, 'city'),
                        ArrayHelper::getValue($item, 'street'),
                        ArrayHelper::getValue($item, 'house'),
                        ArrayHelper::getValue($item, 'lat'),
                        ArrayHelper::getValue($item, 'lon'),
                        ArrayHelper::getValue($item, 'phone'),
                        ArrayHelper::getValue($item, 'comment'),
                        ArrayHelper::getValue($item, 'confirmation_code')
                    );
                }
            }

            $ordersData[] = new OrderInfoData(
                $order->order_id,
                $order->order_number,
                $order->tenant_id,
                $order->city_id,
                $statusData,
                $paymentData,
                $clientData,
                $order->predv_price,
                $order->order_time,
                $order->time_offset,
                0,
                $workerData,
                $this->getOrderDetailCostInfo($order->order_id),
                $addressData,
                $tariff
            );
        }

        return $ordersData;
    }

    /**
     * @param $tenantId
     * @param $order
     *
     * @return OrderWorkerData|null
     * @throws \yii\base\InvalidConfigException
     */
    private function getWorkerDataFromRedis($tenantId, $order)
    {
        $carData = $this->getCarDataFromRedis($order);

        $workerData = null;
        if ($worker = ArrayHelper::getValue($order, 'worker')) {
            $workerFromRedis = \Yii::$app->get('redis_workers')->executeCommand('HGET', [
                $tenantId,
                ArrayHelper::getValue($worker, 'callsign'),
            ]);
            $workerFromRedis = unserialize($workerFromRedis);

            $rating = ArrayHelper::getValue($workerFromRedis, 'position.rating', 0)
                - ArrayHelper::getValue($workerFromRedis, 'car.raiting', 0);
            $rating = round($rating / 10, 2);


            //TODO  данные компании
            $workerData = new OrderWorkerData(
                ArrayHelper::getValue($worker, 'worker_id'),
                ArrayHelper::getValue($worker, 'last_name'),
                ArrayHelper::getValue($worker, 'name'),
                ArrayHelper::getValue($worker, 'second_name'),
                ArrayHelper::getValue($worker, 'callsign'),
                ArrayHelper::getValue($worker, 'phone'),
                ArrayHelper::getValue($worker, 'photo_url'),
                $rating,
                ArrayHelper::getValue($order, 'position_id'),
                ArrayHelper::getValue($workerFromRedis, 'geo.lat'),
                ArrayHelper::getValue($workerFromRedis, 'geo.lon'),
                ArrayHelper::getValue($workerFromRedis, 'geo.degree'),
                ArrayHelper::getValue($workerFromRedis, 'geo.speed'),
                $carData,
                ArrayHelper::getValue($worker, 'tenant_company.name')
            );
        }

        return $workerData;
    }

    /**
     * @param array $order
     *
     * @return OrderCarData|null
     */
    private function getCarDataFromRedis(array $order)
    {
        $carData = null;
        if ($car = ArrayHelper::getValue($order, 'car')) {

            $carData = new OrderCarData(
                ArrayHelper::getValue($car, 'name'),
                ArrayHelper::getValue($car, 'gos_number'),
                ArrayHelper::getValue($car, 'color'),
                ArrayHelper::getValue($car, 'photo_url'),
                ArrayHelper::getValue($car, 'class_id')
            );
        }

        return $carData;
    }

    private function getWorkerFromMysql(Order $order)
    {
        $carData = $this->getCarDataFromMysql($order);

        $workerData = null;
        if ($order->worker) {
            $company = '';
            if(isset($order->car->tenantCompany->name)){
                $company = $order->car->tenantCompany->name;
            }

            $workerData = new OrderWorkerData(
                $order->worker->worker_id,
                $order->worker->last_name,
                $order->worker->name,
                $order->worker->second_name,
                $order->worker->callsign,
                $order->worker->phone,
                $order->worker->photo,
                WorkerReviewRating::getWorkerRating($order->worker_id, $order->position_id),
                $order->position_id,
                0,
                0,
                0,
                0,
                $carData,
                $company
            );
        }

        return $workerData;
    }

    private function getCarDataFromMysql(Order $order)
    {
        $carData = null;

        if ($order->car) {
            $carData = new OrderCarData(
                $order->car->name,
                $order->car->gos_number,
                $order->car->carColor ? $order->car->carColor->name : '',
                $order->car->photo,
                $order->car->class_id ? $order->car->class_id : '71'
            );
        }

        return $carData;
    }

    private function getOrderDetailCostInfo($orderId)
    {
        $detailCostInfo = OrderDetailCost::find()
            ->select([
                "detail_id",
                "order_id",
                "accrual_city",
                "accrual_out",
                "summary_distance",
                "summary_cost",
                "summary_time",
                "city_time",
                "city_distance",
                "city_cost",
                "out_city_time",
                "out_city_distance",
                "out_city_cost",
                "city_time_wait",
                "out_time_wait",
                "before_time_wait",
                "additional_cost",
                "is_fix",
                "city_next_km_price",
                "out_next_km_price",
                "supply_price",
                "city_wait_time",
                "out_wait_time",
                "city_wait_driving",
                "out_wait_driving",
                "city_wait_price",
                "out_wait_price",
                "time",
                "distance_for_plant",
                "planting_price",
                "planting_include",
                "start_point_location",
                "city_wait_cost",
                "before_time_wait_cost",
                "out_wait_cost",
                'city_cost_time',
                'out_city_cost_time',
                'planting_include_time',
                'distance_for_plant_cost',
            ])
            ->where(['order_id' => $orderId])
            ->asArray()
            ->one();

        if (ArrayHelper::getValue($detailCostInfo, 'accrual_city') === 'MIXED') {
            $cityCost     = ArrayHelper::getValue($detailCostInfo, 'city_cost', 0);
            $cityCost     = $cityCost > 0 ? $cityCost : 0;
            $cityCostTime = ArrayHelper::getValue($detailCostInfo, 'city_cost_time', 0);
            $cityCostTime = $cityCostTime > 0 ? $cityCostTime : 0;

            $detailCostInfo['city_cost'] = (string)($cityCost + $cityCostTime);

            unset($detailCostInfo['city_cost_time']);
        }

        if (ArrayHelper::getValue($detailCostInfo, 'accrual_out') === 'MIXED') {
            $outCityCost     = ArrayHelper::getValue($detailCostInfo, 'out_city_cost', 0);
            $outCityCost     = $outCityCost > 0 ? $outCityCost : 0;
            $outCityCostTime = ArrayHelper::getValue($detailCostInfo, 'out_city_cost_time', 0);
            $outCityCostTime = $outCityCostTime > 0 ? $outCityCostTime : 0;

            $detail_cost_info['out_city_cost'] = (string)($outCityCost + $outCityCostTime);

            unset($detailCostInfo['out_city_cost_time']);
        }

        return $detailCostInfo;
    }
}
