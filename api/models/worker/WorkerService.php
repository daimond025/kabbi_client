<?php

namespace api\models\worker;

use api\models\transport\car\Car;
use Yii;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tbl_position}}".
 *
 * @property integer $city_id
 * @property string  $last_name
 * @property string  $name
 * @property string  $second_name
 * @property string  $phone
 * @property string  $partnership
 * @property string  $birthday
 * @property string  $card_number
 * @property string  $email
 * @property string  $password
 */
class WorkerService extends Object
{

    const KEY_PASSPORT = 'WorkerPassport';
    const KEY_OGRNIP = 'WorkerOgrnip';
    const KEY_OSAGO = 'WorkerOsago';
    const KEY_DRIVER_LICENSE = 'WorkerDriverLicense';
    const KEY_PTS = 'WorkerPts';

    /**
     * Создание профиля Исполнителя
     *
     * @param array   $params
     * @param integer $tenantId
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function create($params, $tenantId)
    {
        $positionId = post('position_id');

        $result      = [];
        $transaction = Yii::$app->db->beginTransaction();

        $worker           = $this->createWorker($params, $tenantId);
        $result['worker'] = $worker;

        // Если сохранение водителя успешно
        if (empty($worker['errors'])) {
            $workerId            = $worker['worker_id'];
            $workerHasPositionId = null;

            if (!empty($positionId)) {
                $workerHasPosition             = new WorkerHasPosition();
                $workerHasPosition->attributes = [
                    'worker_id'   => $workerId,
                    'position_id' => $positionId,
                    'active'      => 1,
                ];
                $workerHasPositionId           = $workerHasPosition->save() ? $workerHasPosition->id : null;
            }

            $car           = $this->createCar($params, $tenantId, $workerHasPositionId);
            $result['car'] = $car;

            $transaction->commit();

            $this->saveWorkerPhoto($workerId);

            if (empty($car['errors'])) {
                $this->saveCarPhoto($car['car_id']);
            }

            $this->createDocuments($params, $workerId, $tenantId, $positionId);

        } else {
            $transaction->rollBack();
        }


        return $result;

    }

    /**
     * Создание исполнителя
     *
     * @param $params
     * @param $tenantId
     *
     * @return array
     */
    public function createWorker($params, $tenantId)
    {
        $worker              = new Worker([
            'tenant_id' => $tenantId,
        ]);
        $worker->attributes  = $this->getParamsByWorker($params);
        $worker->created_via = Worker::SITE;

        $result = [];
        if ($worker->save()) {
            $result = [
                'worker_id' => $worker->worker_id,
                'callsign'  => $worker->callsign,
                'password'  => $worker->getPassword(),
            ];
        } else {
            $errors = [];
            foreach ($worker->errors as $error) {
                $errors = array_merge($errors, $error);
            }
            $result['errors'] = array_unique($errors);
        }

        return $result;
    }

    /**
     * Создание автомобиля и привязка к исполнителю
     *
     * @param array   $params
     * @param integer $tenantId
     * @param integer $workerHasPositionId
     *
     * @return array
     */
    public function createCar($params, $tenantId, $workerHasPositionId)
    {
        $car             = new Car([
            'tenant_id' => $tenantId,
        ]);
        $car->attributes = $this->getParamsByCar($params);

        $result = [];
        if ($car->save()) {
            $workerHasCar             = new WorkerHasCar();
            $workerHasCar->attributes = [
                'has_position_id' => $workerHasPositionId,
                'car_id'          => $car->car_id,
            ];
            $workerHasCar->save();

            $result = [
                'car_id' => $car->car_id,
            ];
        } else {
            $result['errors'] = $car->errors;
        }

        return $result;
    }

    // Сохранение документов
    public function createDocuments($params, $workerId, $tenantId, $positionId)
    {
        $post   = ArrayHelper::merge(
            [
                'worker_id'   => $workerId,
                'position_id' => $positionId,
            ],
            $this->getParamsByPassport($params),
            $this->getParamsByOgrnip($params),
            $this->getParamsByOsago($params),
            $this->getParamsByDriverLicense($params),
            $this->getParamsByPts($params)
        );
        $action = 'upload_worker_documents';
        $this->send($post, $action);

        if (isset($_FILES['passport_scan'])) {
            $this->sendFiles($_FILES['passport_scan'], $workerId, $tenantId, $positionId, 1);
        }
        if (isset($_FILES['ogrnip_scan'])) {
            $this->sendFiles($_FILES['ogrnip_scan'], $workerId, $tenantId, $positionId, 3);
        }
        if (isset($_FILES['osago_scan'])) {
            $this->sendFiles($_FILES['osago_scan'], $workerId, $tenantId, $positionId, 6);
        }
        if (isset($_FILES['driver_license_scan'])) {
            $this->sendFiles($_FILES['driver_license_scan'], $workerId, $tenantId, $positionId, 7);
        }
        if (isset($_FILES['pts_scan'])) {
            $this->sendFiles($_FILES['pts_scan'], $workerId, $tenantId, $positionId, 8);
        }

        return true;
    }

    public function sendFiles($files, $workerId, $tenantId, $positionId, $document_code)
    {
        if (empty($files['tmp_name'])) {
            return false;
        }
        foreach ($files['tmp_name'] as $file) {
            if (empty($file)) {
                continue;
            }
            $post   = [
                'worker_id'                    => $workerId,
                'tenant_id'                    => $tenantId,
                'position_id'                  => $positionId,
                'document_code'                => $document_code,
                'WorkerDocumentScan[filename]' => $this->getCurlFile($file),
            ];
            $action = 'upload_document_scan';
            $this->send($post, $action);

        }
    }

    public function getParamsByWorker($params)
    {
        return [
            'last_name'   => ArrayHelper::getValue($params, 'last_name', ''),
            'name'        => ArrayHelper::getValue($params, 'name', ''),
            'second_name' => ArrayHelper::getValue($params, 'second_name', ''),
            'phone'       => ArrayHelper::getValue($params, 'phone', ''),
            'partnership' => ArrayHelper::getValue($params, 'partnership', ''),
            'birthday'    => ArrayHelper::getValue($params, 'birthday', ''),
            'card_number' => ArrayHelper::getValue($params, 'card_number', ''),
            'city_id'     => ArrayHelper::getValue($params, 'city_id', ''),
            'email'       => ArrayHelper::getValue($params, 'email', ''),
            'password'    => '',
            'activate'    => ArrayHelper::getValue($params, 'activate', '0'),
            'lang'        => ArrayHelper::getValue($params, 'lang', ''),
            'description' => ArrayHelper::getValue($params, 'description', ''),
        ];
    }

    public function getParamsByCar($params)
    {
        return [
            'class_id'   => ArrayHelper::getValue($params, 'car_class', ''),
            'brand'      => ArrayHelper::getValue($params, 'car_brand', ''),
            'model'      => ArrayHelper::getValue($params, 'car_model', ''),
            'city_id'    => ArrayHelper::getValue($params, 'city_id', ''),
            'gos_number' => ArrayHelper::getValue($params, 'gos_number', ''),
            'color'      => ArrayHelper::getValue($params, 'car_color', ''),
            'year'       => ArrayHelper::getValue($params, 'car_year', ''),
            'owner'      => ArrayHelper::getValue($params, 'car_owner', ''),
        ];
    }

    public function getParamsByWorkerHasPosition($params)
    {
        return [
            'position_id' => $params['position_id'] ?: '',
        ];
    }

    public function getParamsByPassport($params)
    {
        $key    = self::KEY_PASSPORT;
        $result = [
            $key . '[series]'         => isset($params['passport_series']) ? $params['passport_series'] : '',
            $key . '[number]'         => isset($params['passport_number']) ? $params['passport_number'] : '',
            $key . '[issued]'         => isset($params['passport_issued']) ? $params['passport_issued'] : '',
            $key . '[registration]'   => isset($params['passport_registration']) ? $params['passport_registration'] : '',
            $key . '[actual_address]' => isset($params['passport_actual_address']) ? $params['passport_actual_address'] : '',
        ];

        return $result;
    }

    public function getParamsByOgrnip($params)
    {
        $key    = self::KEY_OGRNIP;
        $result = [
            $key . '[number]' => isset($params['ogrnip_number']) ? $params['ogrnip_number'] : '',
        ];

        return $result;
    }

    public function getParamsByOsago($params)
    {
        $key    = self::KEY_OSAGO;
        $result = [
            $key . '[series]'     => isset($params['osago_series']) ? $params['osago_series'] : '',
            $key . '[number]'     => isset($params['osago_number']) ? $params['osago_number'] : '',
            $key . '[start_date]' => isset($params['osago_start_date']) ? $params['osago_start_date'] : '',
            $key . '[end_date]'   => isset($params['osago_end_date']) ? $params['osago_end_date'] : '',
        ];

        return $result;
    }

    public function getParamsByDriverLicense($params)
    {
        $key    = self::KEY_DRIVER_LICENSE;
        $result = [
            $key . '[series]'      => isset($params['driver_license_series']) ? $params['driver_license_series'] : '',
            $key . '[number]'      => isset($params['driver_license_number']) ? $params['driver_license_number'] : '',
            $key . '[categoryMap]' => isset($params['driver_license_categories']) ? $params['driver_license_categories'] : '',
            $key . '[start_date]'  => isset($params['driver_license_start_date']) ? $params['driver_license_start_date'] : '',
        ];
        $result = array_merge($result, $this->getCategoryByDriverLicense($params));

        return $result;
    }

    protected function getCategoryByDriverLicense($params)
    {
        $key        = self::KEY_DRIVER_LICENSE;
        $categories = (array)(isset($params['driver_license_categories']) ? $params['driver_license_categories'] : null);
        $result     = [];
        foreach ($categories as $idx => $category) {
            $result[$key . '[categoryMap][' . $idx . ']'] = $category;
        }

        return $result;
    }

    public function getParamsByPts($params)
    {
        $key    = self::KEY_PTS;
        $result = [
            $key . '[series]' => isset($params['pts_series']) ? $params['pts_series'] : '',
            $key . '[number]' => isset($params['pts_number']) ? $params['pts_number'] : '',
        ];

        return $result;
    }

    protected function saveWorkerPhoto($workerId)
    {
        if (empty($_FILES['photo'])) {
            return false;
        }

        $post   = [
            'Worker[photo]' => $this->getCurlFile($_FILES['photo']['tmp_name']),
            'worker_id'     => $workerId,
        ];
        $action = 'upload_worker_photo';
        $this->send($post, $action);

        return true;
    }

    protected function saveCarPhoto($carId)
    {
        if (empty($_FILES['car_photo'])) {
            return false;
        }

        $post   = [
            'Car[photo]' => $this->getCurlFile($_FILES['car_photo']['tmp_name']),
            'car_id'     => $carId,
        ];
        $action = 'upload_car_photo';
        $this->send($post, $action);

        return true;
    }

    protected function getCurlFile($file)
    {
        return empty($file) ? null : new \CURLFile($file, mime_content_type($file));
    }


    protected function send($post, $action)
    {
        $url = Yii::$app->params['frontend.uploadUrl'] . $action;
        $ch  = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, app()->params['frontend.basicAuth']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $r = curl_exec($ch);
    }

    /**
     * Driver license category map.
     * @return array
     */
    public static function getDriverCategoryMap()
    {
        return [
            'A' => 'A',
            'B' => 'B',
            'C' => 'C',
            'D' => 'D',
            'E' => 'E',
        ];
    }


}
