<?php

namespace api\modules\v1\requests;

use api\models\tenant\TenantSetting;
use api\modules\v1\components\ErrorCode;

/**
 * Class BaseVersionedApiRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property int    $appVersion - Версия мобильного приложения.
 */
class BaseVersionedApiRequest extends BaseApiRequest
{
    const DEFAULT_APP_VERSION = '1.26.12.1.1';

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'appVersion' => [
                'type'    => self::TYPE_HEADER,
                'name'    => 'appversion',
                'default' => self::DEFAULT_APP_VERSION,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['appVersion', 'validatorAppVersion'],
        ]);
    }

    public function validatorAppVersion($attribute)
    {
        $typeClient = strtoupper($this->typeClient);
        switch ($typeClient) {

            case self::TYPE_CLIENT_ANDROID:
                $settingName = TenantSetting::SETTING_ANDROID_CLIENT_APP_VERSION;
                break;
            case self::TYPE_CLIENT_IOS:
                $settingName = TenantSetting::SETTING_IOS_CLIENT_APP_VERSION;
                break;
            default:
                $settingName = null;
        }

        if ($settingName) {
            $minimalSupportVersionClient = TenantSetting::getSettingValue($this->tenantId, $settingName);
            if (version_compare($this->appVersion, $minimalSupportVersionClient, '<')) {
                $this->addError($attribute, (string)ErrorCode::NEED_UPDATE_APP_VERSION);
            }
        }

    }
}