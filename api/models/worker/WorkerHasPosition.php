<?php

namespace api\models\worker;

use Yii;

/**
 * This is the model class for table "{{%worker_has_position}}".
 *
 * @property integer $id
 * @property integer $worker_id
 * @property integer $position_id
 * @property integer $active
 * @property integer $group_id
 * @property  integer $rating
 * @property string $name
 */
class WorkerHasPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_has_position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id', 'position_id', 'active'], 'required'],
            ['worker_id', 'exist', 'targetClass' => '\api\models\worker\Worker'],

            ['active', 'default', 'value' => 1],
            ['rating', 'default', 'value' => 0]
        ];
    }
}
