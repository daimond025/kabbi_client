<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class BlackListException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::BLACK_LIST;
        parent::__construct('', $code, null);
    }

}