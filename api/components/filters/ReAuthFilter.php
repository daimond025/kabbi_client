<?php

namespace api\components\filters;

use api\models\client\ClientPhone;
use api\models\client\Client;
use api\components\taxiApiRequest\TaxiApiRequest;
use api\components\taxiApiRequest\TaxiErrorCode;

/**
 * ReAuthFilter
 */
class ReAuthFilter extends \yii\base\ActionFilter
{
    /**
     * Verify that client params is valid
     * @return boolean
     */
    protected function isValidClient()
    {
        $tenantId = \Yii::$app->request->getHeaders()->get('tenantid');
        $clientId = \Yii::$app->request->get('client_id', \Yii::$app->request->post('client_id'));
        $phone = \Yii::$app->request->get('phone', \Yii::$app->request->post('phone'));


        return ClientPhone::find()
                ->joinWith('client')
                ->where([
                    'tenant_id' => $tenantId,
                    Client::tableName() . '.client_id' => $clientId,
                    'value'     => $phone,
                ])->exists();
    }

    public function beforeAction($action)
    {
        if (!$this->isValidClient()) {
           $request   = new TaxiApiRequest;
           $errorCode = new TaxiErrorCode();

           \Yii::$app->response->data = $request->getJsonAnswer([
               'code' => TaxiErrorCode::NEED_REAUTH,
               'info' => $errorCode->errorCodeData[TaxiErrorCode::NEED_REAUTH],
           ]);

            return false;
        } else {
            return parent::beforeAction($action);
        }
    }
}
