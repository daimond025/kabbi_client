<?php

namespace api\modules\v1\requests;

use api\models\tenant\TenantHasCity;
use api\models\transport\car\CarClass;
use api\modules\v1\components\validators\ArrayFilterValidator;

/**
 * Class GetCarsRequest
 * Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property integer[] $classId - Идентификаторы классов автомобилей
 * @property integer   $cityId - Идентификатор филиала
 * @property double    $lat
 * @property double    $lon
 * @property double    $radius - Радиус поиска
 * @property integer[] $exceptCarModels
 * @property integer[] $additionalOptions
 */
class GetCarsRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'classId'                   => [
                'name' => 'car_class_id',
                'type' => self::TYPE_GET,
                'default' => [],
            ],
            'cityId'                    => [
                'name' => 'city_id',
                'type' => self::TYPE_GET,
            ],
            'lat'                       => [
                'name' => 'from_lat',
                'type' => self::TYPE_GET,
            ],
            'lon'                       => [
                'name' => 'from_lon',
                'type' => self::TYPE_GET,
            ],
            'radius'                    => [
                'name' => 'radius',
                'type' => self::TYPE_GET,
            ],
            'exceptCarModels'           => [
                'name' => 'except_car_models',
                'type' => self::TYPE_GET,
            ],
            'additionalOptions' => [
                'name' => 'additional_options',
                'type' => self::TYPE_GET,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['classId', 'filter', 'filter' => [$this, 'filterClassId']],
            [
                'classId',
                'each',
                'rule' => [
                    'exist',
                    'targetClass'     => CarClass::className(),
                    'targetAttribute' => ['classId' => 'class_id'],
                ],
            ],

            ['cityId', 'required'],
            [
                'cityId',
                'exist',
                'targetClass'     => TenantHasCity::className(),
                'targetAttribute' => ['cityId' => 'city_id', 'tenantId' => 'tenant_id'],
                'when' => [$this, 'whenAllCity'],
            ],

            ['lat', 'required', 'when' => [$this, 'whenRadius']],
            ['lat', 'double', 'min' => -90, 'max' => 90],

            ['lon', 'required', 'when' => [$this, 'whenRadius']],
            ['lon', 'double', 'min' => -180, 'max' => 180],

            ['radius', 'double', 'min' => 0],

            ['exceptCarModels', ArrayFilterValidator::class, 'skipOnEmpty' => false],

            ['additionalOptions', ArrayFilterValidator::class, 'skipOnEmpty' => false],
        ]);
    }

    public function filterClassId($value)
    {
        return (array)$value;
    }

    /**
     * @param GetCarsRequest $model
     *
     * @return boolean
     */
    public function whenRadius($model)
    {
        return !empty($model->radius);
    }

    public function whenAllCity($model){
        return ((int)$model->cityId != 0);
    }

}