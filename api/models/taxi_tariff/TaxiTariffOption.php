<?php

namespace api\models\taxi_tariff;

use yii\db\ActiveRecord;

/**
 * Class TaxiTariffOption
 * @package api\models\taxi_tariff
 *
 * @property integer        $option_id
 * @property integer        $type_id
 * @property string         $accrual
 * @property string         $area
 * @property float          $planting_price
 * @property float          $planting_include
 * @property float          $next_km_price
 * @property float          $min_price
 * @property float          $second_min_price
 * @property float          $supply_price
 * @property integer        $wait_time
 * @property integer        $wait_driving_time
 * @property float          $wait_price
 * @property float          $speed_downtime
 * @property float          $rounding
 * @property string         $rounding_type
 * @property integer        $enabled_parking_ratio
 * @property integer        $calculation_fix
 * @property string         $next_cost_unit
 * @property float          $next_km_price_time
 * @property integer        $planting_include_time
 *
 * @property TaxiTariffType $tariffType
 */
class TaxiTariffOption extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_option}}';
    }

    public function getTariffType()
    {
        return $this->hasOne(TaxiTariffType::className(), ['type_id' => 'type_id']);
    }
}