<?php

namespace api\modules\v1\requests;

use api\models\client\ClientPhone;

/**
 * Class UpdateOrderResultRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property string $phone - Телефон клиента*
 * @property string $updateId - Ид запроса на обновление заказа*
 */
class UpdateOrderResultRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'phone'    => [
                'name' => 'phone',
                'type' => self::TYPE_GET,
            ],
            'updateId' => [
                'name' => 'update_id',
                'type' => self::TYPE_GET,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['phone', 'exist', 'targetClass' => ClientPhone::class, 'targetAttribute' => ['phone' => 'value']],
            ['updateId', 'safe'],
        ]);
    }
}