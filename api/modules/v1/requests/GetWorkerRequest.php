<?php

namespace api\modules\v1\requests;

use api\models\order\Order;
use api\models\worker\Worker;

/**
 * Class LikeWorkerRequest
 * Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property integer $workerCallsign- Идентификатор водителя *
 * @property integer $password  -- пароль водителя *
 */
class GetWorkerRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'workerCallsign' => [
                'name' => 'workerCallsign',
                'type' => self::TYPE_POST,
            ],
            'worker_all' => [
                'name' => 'worker_all',
                'type' => self::TYPE_POST,
            ],
        ]);

    }


    public function rules()
    {
        return array_merge(parent::rules(), [
            ['workerCallsign', 'required'],
            ['worker_all', 'required'],
            ['worker_all', 'in', 'range' => [0, 1]],
        ]);
    }
}
