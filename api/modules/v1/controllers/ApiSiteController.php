<?php

namespace api\modules\v1\controllers;

use api\exceptions\BadParamException;
use api\exceptions\EmptyInDbException;
use api\models\transport\car\CarBrand;
use api\models\transport\car\CarColor;
use api\models\transport\car\CarModel;
use api\models\worker\Position;
use api\models\worker\WorkerService;
use Yii;
use yii\filters\VerbFilter;
use api\components\taxiApiRequest\TaxiApiRequest;
use api\components\taxiApiRequest\TaxiErrorCode;

/**
 * API for integration web site  with GOOTAX
 *
 * @author Sergey Kalashnikov <forz666@yandex.ru>
 *
 * @version Gootax site API v1.0
 *
 * Апи совместимо с апи для клиентского приложения.
 * Можно вызывать все методы клиентского апи.
 *
 * Test Url: <a href="http://ca1.gootax.pro:8089/api-site/ping">http://ca1.gootax.pro:8089/api-site/ping</a>
 *
 * Production Url: <a href="https://ca2.gootax.pro:8089/api-site/ping">https://ca2.gootax.pro:8089/api-site/ping</a>
 *
 */
class ApiSiteController extends ApiController
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'create_order_site'     => ['post'],
                    'create_driver_profile' => ['post'],
                    'get_car_color_list'    => ['get'],
                    'get_car_brand_list'    => ['get'],
                    'get_car_model_list'    => ['get'],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();
    }

    /**
     * Название команды: get_car_color_list
     *
     * Описание команды: Вывести список цветов авто.
     *
     * Тип запроса: GET
     *
     * @param current_time - Текущее время *
     *
     * Ответ:
     * <code>
     * {
     *  "code": 0,
     *  "info": "OK",
     *  "result": {
     *  "color": {
     *           "1": "Черный",
     *           "2": "Голубой",
     *           "3": "Красный",
     *           "4": "Белый",
     *           "5": "Серебряный",
     *           "6": "Зеленый",
     *           "7": "Серый",
     *           "8": "Коричневый",
     *           "9": "Бежевый",
     *           "10": "Жёлтый",
     *           "11": "Золотой",
     *           "12": "Фиолетовый",
     *           "13": "Оранжевый",
     *           "14": "Тёмный",
     *           "15": "Вишневый",
     *           "16": "Жемчужный",
     *           "17": "Мокрый асфальт",
     *           "18": "Металический",
     *           "19": "Синий"
     *       }
     *   },
     *   "current_time": "123"
     * }
     * </code>
     *
     * @return string
     */
    public function actionGet_car_color_list()
    {
        $this->_params = $this->getParams($_GET);

        return $this->doAction('getCarColors');
    }

    private function getCarColors()
    {
        $lang = $this->getLang();

        return ['color' => CarColor::getColorList($lang)];
    }


    /**
     * Название команды: get_car_brand_list
     *
     * Описание команды: Вывести список брендов авто.
     *
     * Тип запроса: GET
     *
     * @param current_time - Текущее время *
     *
     * Ответ:
     * <code>
     * {
     *  "code": 0,
     *  "info": "OK",
     *  "result": {
     *  "brand": {
     *           "3": "Acura",
     *           "4": "Alfa Romeo",
     *           "5": "Audi",
     *           "6": "BMW",
     *           "7": "Brilliance",
     *           "8": "Cadillac",
     *           "9": "Chery",
     *           "10": "Chevrolet",
     *           "11": "Chrysler",
     *           "12": "Citroen",
     *           "13": "Daewoo",
     *           "14": "Datsun",
     *           "15": "FIAT"
     *       }
     *   },
     *   "current_time": "123"
     * }
     * </code>
     *
     * @return string
     */
    public function actionGet_car_brand_list()
    {
        $this->_params = $this->getParams($_GET);

        return $this->doAction('getCarBrand');
    }

    private function getCarBrand()
    {
        $lang = $this->getLang();

        return ['brand' => CarBrand::getBrandList()];
    }


    /**
     * Название команды: get_car_model_list
     *
     * Описание команды: Вывести список моделей авто.
     *
     * Тип запроса: GET
     *
     * @param current_time - Текущее время *
     * @param brand_id - Ид бренда *
     *
     * Ответ:
     * <code>
     * {
     *  "code": 0,
     *  "info": "OK",
     *  "result": {
     *  "brand": {
     *           "3": "Acura",
     *           "4": "Alfa Romeo",
     *           "5": "Audi",
     *           "6": "BMW",
     *           "7": "Brilliance",
     *           "8": "Cadillac",
     *           "9": "Chery",
     *           "10": "Chevrolet",
     *           "11": "Chrysler",
     *           "12": "Citroen",
     *           "13": "Daewoo",
     *           "14": "Datsun",
     *           "15": "FIAT"
     *       }
     *   },
     *   "current_time": "123"
     * }
     * </code>
     *
     * @return string
     */
    public function actionGet_car_model_list()
    {
        $this->_params = $this->getParams($_GET);

        return $this->doAction('getCarModel');
    }

    private function getCarModel()
    {
        return ['model' => CarModel::getModelList($this->_params['brand_id'])];
    }


    //    /**
    //     * Название команды: get_car_class_list
    //     *
    //     * Описание команды: Вывести список классов авто.
    //     *
    //     * Тип запроса: GET
    //     *
    //     * @param current_time - Текущее время *
    //     * @param position_id - Ид профессии *
    //     *
    //     * Ответ:
    //     * <code>
    //     * {
    //     *  "code": 0,
    //     *  "info": "OK",
    //     *  "result": {
    //     *  "class": {
    //     *           "1": "Эконом",
    //     *           "2": "Комфорт",
    //     *           "3": "Бизнес",
    //     *           "4": "VIP",
    //     *           "5": "Минивэн",
    //     *           "6": "Универсал",
    //     *           "7": "Микроавтобус",
    //     *           "8": "Грузовой",
    //     *           "9": "Эвакуатор",
    //     *       }
    //     *   },
    //     *   "current_time": "123"
    //     * }
    //     * </code>
    //     * @return string
    //     */
    //    public function actionGet_car_class_list()
    //    {
    //        $this->_params = $this->getParams($_GET);
    //        return $this->doAction('getCarClass');
    //    }
    //
    //    private function getCarClass()
    //    {
    //        $lang = $this->getLang();
    //        return ['class' => CarClass::getList($this->_params['position_id'], $lang)];
    //    }


    /**
     * Название команды: get_categories_by_driver_license
     *
     * Описание команды: Вывести список категорий водительского удостоверения.
     *
     * Тип запроса: GET
     *
     * @param current_time - Текущее время *
     *
     * Ответ:
     * <code>
     * {
     *  "code": 0,
     *  "info": "OK",
     *  "result": {
     *  "position": {
     *           "A": "A",
     *           "B": "B",
     *           "C": "C"
     *       }
     *   },
     *   "current_time": "123"
     * }
     * </code>
     *
     * @return string
     */
    public function actionGet_categories_by_driver_license()
    {
        $this->_params = $this->getParams($_GET);

        return $this->doAction('getCategoriesByDriverLicense');
    }

    private function getCategoriesByDriverLicense()
    {
        return ['categories' => WorkerService::getDriverCategoryMap()];
    }


    /**
     * Название команды: create_driver_profile.
     *
     * Описание команды:  Создание анкеты водителя.
     *
     * Специальные возвращаемые коды:
     *
     * Тип запроса - POST.
     *
     * @params int    city_id  - Ид города филиала*
     * @params string last_name  - Фамилия исполнителя*
     * @params string name  - Имя исполнителя*
     * @params string second_name  - Отчество исполнителя
     * @params string phone  - Телефон*
     * @params file   photo - Фото
     * @params string partnership - Тип сотрудничества PARTNER|STAFF*
     * @params string birthday  - ДР. Формат:1995-01-01
     * @params string description - Описание
     * @params string passport_serial  - Серия паспорта
     * @params string passport_number  - Номер паспорта
     * @params string passport_issued  - Когда и кем выдан паспорт
     * @params file   passport_scan - Скан паспорта
     * @params string address_registr  - Адрес прописки
     * @params string address_fact     - Адрес проживания
     * @params string driver_license_serial  -Серия водительского удостоверения
     * @params string driver_license_number  - Номер водительского удостоверения
     * @params string driver_license_start_date - Дата начала стажа. Формат:2015-01-01
     * @params file   driver_license_scan - Скан водителского удостоверения
     * @params string license_category  - Категория водитеслькго удостоверения (строка A,B,C,D,E)
     * @params string email  - Email
     * @params string inn  - ИНН
     * @params file   inn_scan  - Скан ИНН
     * @params string ogrnip  - ОГРНИП
     * @params file   ogrn_scan Скан ОГРНИП
     * @params string snils  - СНИЛС
     * @params file   snils_scan - скан СНИЛС
     * @params string med  - Мед.справка
     * @params string med_scan  - Скан медицинской справки
     *
     * @params int    position_id - Категория исполнителя(водитель,курьер,сантехник,...)*
     * @params int    class_id - Ид класса авто
     * @params string car_brand  - Бренд авто - LADA
     * @params string car_model  - Модель авто - Priora
     * @params string gos_number - Гос номер авто x000зщ
     * @params string car_color - Цвет авто
     * @params string car_year - Год авто. Формат- 2009
     * @params file   car_photo - Фото авто
     * @params string car_license - Лицензия авто на работу в такси
     * @params file   car_license_photo - Фото лицензии
     *
     * @return string JSON
     *
     * Результат создание профиля:
     * <code>
     * {
     *
     *  "code":0,
     *
     *  "info":"OK",
     *
     *  "result":
     *
     *      {
     *
     *          "driver_callsign":143,
     *
     *          "car_id":22
     *
     *      }
     *
     * }
     * </code>
     */
    public function actionCreate_worker_profile()
    {
        $this->_params = $this->getParams($_POST);

        return $this->doAction('getCreateWorkerProfile');
    }

    protected function getCreateWorkerProfile()
    {
        $model    = new WorkerService();
        $tenantId = $this->getTanantId();

        return $model->create($this->_params, $tenantId);
    }

    /**
     * Название команды: create_order_site.
     *
     * Описание команды:  Создание заказа/Получение предварительной стоимости.
     *
     * Специальные возвращаемые коды:
     *
     * code   info
     *
     * 10    NEED_REAUTH
     *
     * 11    BAD_PAY_TYPE
     *
     * 12    NO_MONEY
     *
     * 9     BLACK_LIST
     *
     * Тип запроса - POST.
     *
     * @params string address  - json строка содержащиая массив из точек поездки.
     *
     * <code>
     * {
     *
     * "address": [
     *
     *  {
     *
     *      "city_id": "26068",
     *
     *      "city": "Ижевск",
     *
     *      "street": "Советская",
     *
     *      "house": "12",
     *
     *      "housing": "2",
     *
     *      "porch": "2",
     *
     *      "lat": "54.5632",
     *
     *      "lon": "54.5123"
     *
     *   },
     *
     *   {
     *
     *      "city_id": "26068",
     *
     *      "city": "Ижевск",
     *
     *      "street": "Ленина",
     *
     *      "house": "12",
     *
     *      "housing": "2",
     *
     *      "porch": "2",
     *
     *      "lat": "54.5632",
     *
     *      "lon": "54.5123"
     *
     *    },
     *
     *   {
     *
     *      "city_id": "26068",
     *
     *      "city": "Ижевск",
     *
     *      "street": "Ленина",
     *
     *      "house": "12",
     *
     *      "housing": "2",
     *
     *      "porch": "2",
     *
     *      "lat": "54.5632",
     *
     *      "lon": "54.5123"
     *
     *   }
     *
     *  ]
     *
     * }
     * </code>
     * @params string type_request  - 1 - создание заказа, 2 - получить предв. стоимость. *
     *
     * @params string city_id  - Ид города (филилала) пользователя. *
     *
     * @params string client_phone   -  Телефон клиента - международный формат - 7999111223 без “+”. *
     *
     * @params string company_id   -  Ид компании.
     *
     * @params string client_name - Имя клиента.
     *
     * @params string order_time - Дата и время, на которое подать авто (для предварительного заказа).
     *
     * Формат: dd.mm.yyyy H:i:s. Если время отсутствует, то заказ на текущее время.
     *
     * @params string tariff_id  - Ид тарифа.*
     *
     * @params string pay_type - Способ оплаты. ('CASH','CARD','PERSONAL_ACCOUNT','CORP_BALANCE') - по умолчанию CASH
     *
     * @params string driver_callsign - Позывной водителя, выбор машины клиентом на карте. (пока не юзабельно)
     *
     * @params string additional_options - ИД пожеланий клиента в виде строки, Ид записываются через запятую
     *
     * @params string comment - комментарий к заказу
     *
     * @params string current_time  - текущее время timestamp (обязательный параметр)*
     *
     * @return string JSON
     *
     * Результат создание заказа:
     * <code>
     * {
     *
     *  "code":0,
     *
     *  "info":"OK",
     *
     *  "result":
     *
     *      {
     *
     *          "order_id":143,
     *
     *          "order_number":22
     *
     *      }
     *
     * }
     * </code>
     *
     * Результат предварительного расчета стоимости заказа:
     * <code>
     * {
     *
     *  "code":0,
     *
     *  "info":"OK",
     *
     *  "result":
     *
     *  {
     *
     *      "cost_result":
     *
     *          {
     *
     *              "summary_time":288.5,
     *
     *              "summary_distance":328.8,
     *
     *              "summary_cost":3320,
     *
     *              "city_time":4.7,
     *
     *              "city_distance":4.4,
     *
     *              "city_cost":70,
     *
     *              "out_city_time":283.8,
     *
     *              "out_city_distance":324.4,
     *
     *              "out_city_cost":3250,
     *
     *              "is_fix":0
     *
     *      }
     *
     *   }
     *
     * }
     * </code>
     */
    public function actionCreate_order_site()
    {
        return $this->createOrder();
    }

    public function doAction($function)
    {

        $validateData = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $request      = new TaxiApiRequest();
        $result       = null;

        try {
            if ($validateData['code'] == TaxiErrorCode::OK) {
                $result = call_user_func([$this, $function]);
            }

        } catch (EmptyInDbException $e) {
            $validateData['code'] = $e->getCode();
            $validateData['info'] = $e->getMessage();
        } catch (BadParamException $e) {
            $validateData['code'] = $e->getCode();
            $validateData['info'] = $e->getMessage();
        } catch (\Exception $e) {
            $validateData['code'] = TaxiErrorCode::INTERNAL_ERROR;
            $validateData['info'] = 'INTERNAL_ERROR';

            \Yii::error($e->getMessage(), $function);
        }

        return $request->getJsonAnswer($validateData, $result);
    }

}
