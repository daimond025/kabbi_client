<?php

namespace api\models\balance;

use Yii;
use yii\caching\Cache;
use yii\caching\TagDependency;

class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * Getting cached currency by id
     *
     * @param Cache $cache
     *
     * @return Currency[]
     */
    public static function getCachedCurrencies($cache = null)
    {
        $cacheKey = 'currencies';
        $cache    = $cache ?: \Yii::$app->cache;

        $result = $cache->get($cacheKey);
        if ($result === false) {
            $result = Currency::find()->indexBy('currency_id')->all();
            $cache->set($cacheKey, $result, 3600, new TagDependency(['tags' => 'currency']));
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        TagDependency::invalidate(\Yii::$app->cache);
    }
}