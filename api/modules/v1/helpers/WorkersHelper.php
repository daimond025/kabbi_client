<?php

namespace api\modules\v1\helpers;

use yii\helpers\ArrayHelper;

/**
 * Class WorkersHelper
 * @package api\modules\v1\helpers
 */
class WorkersHelper
{
    const TYPE_POINT_CITY = 'in';
    const TYPE_POINT_TRACK = 'out';

    public static function formatted($data)
    {
        $result = [
            'worker_id'   => ArrayHelper::getValue($data, 'worker.worker_id'),
            'callsign'    => ArrayHelper::getValue($data, 'worker.callsign'),
            'last_name'   => ArrayHelper::getValue($data, 'worker.last_name'),
            'name'        => ArrayHelper::getValue($data, 'worker.name'),
            'second_name' => ArrayHelper::getValue($data, 'worker.second_name'),

            'car_brand'      => ArrayHelper::getValue($data, 'car.brand'),
            'car_model'      => ArrayHelper::getValue($data, 'car.model'),
            'car_gos_number' => ArrayHelper::getValue($data, 'car.gos_number'),

            'lat'    => ArrayHelper::getValue($data, 'geo.lat'),
            'lon'    => ArrayHelper::getValue($data, 'geo.lon'),
            'degree' => ArrayHelper::getValue($data, 'geo.degree'),
        ];

        return $result;
    }
}