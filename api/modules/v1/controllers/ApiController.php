<?php

namespace api\modules\v1\controllers;

use api\components\orderApi\OrderApi;
use api\components\apiService\ApiService;
use api\components\routeAnalyzer\TaxiRouteAnalyzer;
use api\components\serviceEngine\ServiceEngine;
use api\exceptions\BadParamException;
use api\exceptions\EmptyInDbException;
use api\exceptions\NeedReauthException;
use api\models\client\ClientPhone;
use api\models\order\exceptions\QueueIsNotExistsException;
use api\models\order\OrderHasOption;
use api\models\order\OrderUpdateEvent;
use api\models\referralSystem\ReferralSystem;
use api\models\referralSystem\ReferralSystemMember;
use api\models\referralSystem\ReferralSystemSubscriber;
use api\models\tenant\mobileApp\MobileApp;
use api\modules\v1\components\ErrorCode;
use api\modules\v1\components\services\PromoCodeService;
use api\modules\v1\exceptions\CodeNotFoundException;
use api\models\tenant\TenantHasSms;
use api\modules\v1\repositories\ClientRepository;
use api\modules\v1\repositories\OrderRepository;
use api\modules\v1\services\OrderService;
use app\models\order\OrderTrackService;
use bonusSystem\BonusSystem;
use bonusSystem\exceptions\ClientIsNotRegisteredException;
use bonusSystem\exceptions\GetCustomerException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use paymentGate\ProfileService;
use Yii;
use api\models\client\ClientHasCompany;
use api\models\client\ClientReview;
use api\models\order\Order;
use api\models\order\OrderStatus;
use api\models\worker\Worker;
use app\models\balance\Account;
use yii\base\ErrorException;
use yii\base\Module;
use yii\di\Container;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use api\components\taxiApiRequest\TaxiApiRequest;
use api\components\taxiApiRequest\TaxiErrorCode;

use api\models\bankCard\Card;
use api\models\bankCard\CardPayment;
use api\components\curl\exceptions\RestException;

use api\models\client\Client;
use api\models\client\ClientTemp;
use api\models\taxi_tariff\TaxiTariff;
use api\models\worker\Position;
use api\models\city\City;
use api\models\transport\car\Car;
use api\models\tenant\TenantSetting;
use api\models\tenant\TenantHasCity;

use api\modules\v1\components\services\ProcessingErrorsApiOrder;
/**
 * API for integration client application with GOOTAX
 *
 * @author Sergey Kalashnikov <forz666@yandex.ru>
 *
 * @version Gootax client API v1.0
 *
 * Test Url: <a href="http://ca1.gootax.pro:8089/ping">http://ca1.gootax.pro:8089/ping</a>
 *
 * Production Url: <a href="https://ca2.gootax.pro:8089/ping">https://ca2.gootax.pro:8089/ping</a>
 *
 * API принимает входящие запросы по протоколу HTTP(S).
 *
 * Для получения данных используются запросы типа GET.
 *
 * Для записи данных в базу данных используются запросы типа POST.
 *
 * В запросе типа GET параметры запроса передаются в URL.
 *
 * В запросе типа POST параметры передаются в теле запроса в формате application/x-www-form-urlencoded
 *
 * Обязательные параметры помечены *.
 *
 * В любом запросе обязательно должен быть заголовок signature. В нем передается MD5 хэш,
 *
 * рассчитанный для строки, которая получается сцеплением строки параметров запроса (параметры отсортированны по
 *     алфавиту, строка параметров
 *
 * приведена к стандарту RFC3986) с секретным ключом.
 *
 * У каждого арендатора свой секретный ключ, его можно менять в настройках.
 *
 * Например:
 *
 * Секретный ключ:
 *
 *  123123123
 *
 * signature = MD5("clientId=12&orderId=12" + "123123123") = fe6d2f0aaecbd978e416e451747254b2
 *
 * В любом запросе необходимо отправлять следующие заголовки:
 *
 * 1) заголовок typeclient (тип устройства с которого отправлен запрос).
 *
 *  typeclient может принимать следующие значения:
 *
 * - ios
 *
 * - android
 *
 * - web
 *
 * - winfon
 *
 * - vk
 *
 *
 * 2) заголовок versionclient (1.2.3) - Версия устройства с которого отправлен запрос.
 *
 * versionclient типа string
 *
 * Смотрим сюда http://semver.org
 *
 *
 * 3)  id арендатора - tenantid
 *
 *
 * 4) язык  lang (ru/en/az/uk/kz/....) - Если язык не поддeрживается сервером, либо не передан,
 *   то используется по умолчанию lang=en.
 *
 * 5) Ид устройства - deviceid (для андроид - udid, для ios - device_token)
 *
 * 6) Ид мобильного приложения appid (берется из БД tbl_mobile_app)
 *
 * Формат ответа
 *
 * Результат выполнения запроса содержится в теле ответа в формате JSON. Общий вид возвращаемого результата:
 * <code>
 * {
 * "code":<Числовой код результата>,
 *
 * "info":”<Строковое описание кода>”,
 *
 * "result":{<Результат запроса>}
 *
 * "current_time":<Время запроса в формате Unix time>
 * }
 * </code>
 *
 * Общие коды  для всех запросов и их описание:
 * <code>
 * Описание (info)                      Код (code)
 *
 * OK                                   0
 *
 * INTERNAL_ERROR                       1
 *
 * INCORRECTLY_SIGNATURE                2
 *
 * UNKNOWN_REQUEST                      3
 *
 * BAD_PARAM                            4
 *
 * MISSING_INPUT_PARAMETER              5
 *
 * EMPTY_VALUE                          6
 *
 * EMPTY_DATA_IN_DATABASE               7
 *
 * EMPTY_TENANT_ID                      8
 *
 * BLACK_LIST                           9
 *
 * NEED_REAUTH                          10
 *
 * BAD_PAY_TYPE                         11
 *
 * NO_MONEY                             12
 *
 * BAD_BONUS                            13
 *
 * PAYMENT_GATE_ERROR                   14
 *
 * INVALID_PAN                          15
 * </code>
 *
 * Для некоторых запросов могут быть свои (специализированные) коды.
 *
 *
 * <strong>Логика авторизации, работы с профилем:</strong>
 *
 * <code>
 * Для входа в приложения используется метод accept_password
 *
 * если клиент не знает свой пароль то вызываеть метод  send_password
 *
 * Далее если  accept_password = ok то вызываем метод получения профиля get_client_profile
 * </code>
 *
 *
 *
 *
 * <strong>Алгоритм редактирования профиля:</strong>
 *
 * <code>
 * Если клиент изменил телефон то вызываем send_password_new_phone - получение смс пароля для подтверждения
 *
 * нового номера телефона, затем подверждаем его командой accept_password_new_phone
 *
 * Если accept_password_new_phone = ok  то вызываем команду update_client_profile
 *
 * Если пользователь не изменил телефон то сразу вызываем update_client_profile
 * </code>
 *
 */
class ApiController extends Controller
{
    const DEFAULT_VERSION_CLIENT = '1.0.0';

    public $headers;

    /**
     * @var Container
     */
    public $container;

    protected $_params = null;

    /**
     * @var ProfileService
     */
    private $profileService;

    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'index'                     => ['get'],
                    'ping'                      => ['get'],
                    'get_tariffs_list'          => ['get'],
                    'get_tariff_type'           => ['get'],
                    'send_password'             => ['post'],
                    'accept_password'           => ['post'],
                    'get_client_profile'        => ['get'],
                    'get_city_list'             => ['get'],
                    'get_tenant_city_list'      => ['get'],
                    'get_cars'                  => ['get'],
                    'send_password_new_phone'   => ['post'],
                    'accept_password_new_phone' => ['post'],
                    'get_info_page'             => ['get'],
                    'update_client_profile'     => ['post'],
                    'get_client_cards'          => ['get'],
                    'create_client_card'        => ['post'],
                    'check_client_card'         => ['post'],
                    'delete_client_card'        => ['post'],
                    'activate_bonus_system'     => ['post'],

                    'get_order_info'          => ['get'],
                    'create_order'            => ['post'],
                    'get_order_route'         => ['get'],
                    'reject_order'            => ['post'],
                    'get_reject_order_result' => ['get'],
                    'send_response'           => ['post'],
                    'get_active_orders_list'  => ['get'],

                    'get_geoobjects_list'   => ['get'],
                    'get_coords_by_address' => ['get'],
                    'get_address_by_coords' => ['get'],

                    'activate_referral_system'      => ['post'],
                    'activate_referral_system_code' => ['post'],
                    'get_referral_system_list'      => ['get'],

                    //                    'offer_order' => ['post'], // нету
                    'get_active_order'          => ['get'],
                ],
            ],
            'reAuth' => [
                'class' => '\api\components\filters\ReAuthFilter',
                'only'  => [
                    'get_client_cards',
                    'create_client_card',
                    'check_client_card',
                    'delete_client_card',
                ],
            ],
        ];
    }

    public function __construct($id, Module $module, ProfileService $profileService, array $config = [])
    {
        $this->profileService = $profileService;

        parent::__construct($id, $module, $config);
    }

    public function init()
    {
        $this->setDependency();
        parent::init();
    }

    /**
     * Setup dependency
     */
    protected function setDependency()
    {
        $this->container = new \yii\di\Container();
        $this->container->set('request', TaxiApiRequest::className());
        $this->container->set('errCode', TaxiErrorCode::className());
        $this->container->set('card', [
            'class' => Card::class,
            'lang'  => $this->getLang(),
        ]);
    }

    ///////////////////////////////МЕТОДЫ АПИ//////////////////////////////////////////////////////

    public function actionError()
    {
        $request      = new TaxiApiRequest();
        $validateData = [
            'code' => TaxiErrorCode::METHOD_NOT_FOUND,
            'info' => 'METHOD_NOT_FOUND',
        ];

        return $request->getJsonAnswer($validateData, null);
    }

    /**
     * Название команды: ping
     *
     * Описание команды: Пинг api.
     *
     * Специальные возвращаемые коды: нет.
     *
     * Тип запроса: GET.
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * <code>
     * {
     *  "code":0,
     *  "info":"OK",
     *  "result":null
     * }
     * </code>
     *
     */
    public function actionPing()
    {
        $this->_params = $this->getParams($_GET);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $request       = new TaxiApiRequest();
        if ($validateData['code'] == TaxiErrorCode::OK) {
            $tenantId = $this->getTanantId();
            if (!\api\models\tenant\Tenant::findOne($tenantId)) {
                $validateData['code'] = TaxiErrorCode::BAD_PARAM;
                $validateData['info'] = 'BAD_PARAM tenantid';
                $result               = null;

                return $request->getJsonAnswer($validateData, $result);
            }

            return $request->getJsonAnswer($validateData, [
                'time' => time(),
            ]);
        }

        return $request->getJsonAnswer($validateData);
    }


    /**
     * Название команды: get_position_list
     *
     * Описание команды: Вывести список профессий.
     *
     * Тип запроса: GET
     *
     * @param current_time - Текущее время *
     *
     * Ответ:
     * <code>
     * {
     *  "code": 0,
     *  "info": "OK",
     *  "result": {
     *  "position": {
     *           "1": "Водитель такси",
     *           "2": "Водитель грузовика",
     *           "3": "Курьер"
     *       }
     *   },
     *   "current_time": "123"
     * }
     * </code>
     *
     * @return string
     */
    public function actionGet_position_list()
    {
        $this->_params = $this->getParams($_GET);

        return $this->doAction('getPosition');
    }

    protected function getPosition()
    {
        $lang = $this->getLang();

        return ['position' => Position::getPositionsByCity($lang)];
    }


    /**
     * Название команды: get_referral_system_list
     *
     * Описание команды: Получение списка активных реферальных систем.
     *
     * Тип запроса: GET
     *
     * @param phone - Телефон клиента
     *
     * @param current_time - Текущее время *
     *
     * Ответ:
     * <code>
     * {
     *   "code": 0,
     *   "info": "OK",
     *   "result": {
     *     "referralSystem": [
     *       {
     *         "referral_id": 6,
     *         "city_id": 26068,
     *         "content": "vswd",
     *         "title": "bdsbds",
     *         "text": "bdsbds",
     *         "isActive": 1,
     *         "code": "111111"
     *       },
     *       {
     *         "referral_id": 7,
     *         "city_id": 22447,
     *         "content": "sdsb",
     *         "title": "bdsbds",
     *         "text": "bwewbw",
     *         "isActive": 0,
     *         "code": null
     *       }
     *     ]
     *   },
     *   "current_time": "123123"
     * }
     * </code>
     *
     * @return string
     */
    public function actionGet_referral_system_list()
    {
        $this->_params = $this->getParams($_GET);

        return $this->doAction('getReferralSystemList');
    }

    protected function getReferralSystemList()
    {
        $phone    = isset($this->_params['phone']) ? $this->_params['phone'] : null;
        $tenantId = $this->getTanantId();

        $client   = Client::findByPhone($tenantId, $phone);
        $clientId = !empty($client) ? $client->client_id : null;

        $referralSystem = (array)$this->findReferralSystemModels($clientId, $tenantId);

        return ['referral_system' => ReferralSystem::formatted($referralSystem, !empty($client))];
    }

    /**
     * Название команды: activate_referral_system
     *
     * Описание команды: Активация реферальной системы.
     *
     * Тип запроса: POST
     *
     * @param current_time - Текущее время *
     * @param phone - Телефон клиента *
     * @params referral_id - Ид реферальной системы *
     *
     * Ответ:
     * <code>
     * {
     *  "code": 0,
     *  "info": "OK",
     *  "result": 1,
     *  "current_time": "12"
     * }
     * </code>
     *
     * @return string
     */
    public function actionActivate_referral_system()
    {
        $this->_params = $this->getParams($_POST);

        return $this->doAction('activateReferralSystem');
    }

    protected function findReferralSystemModel($referralSystemId, $tenantId)
    {
        $model = ReferralSystem::find()
            ->where([
                'referral_id' => $referralSystemId,
                'tenant_id'   => $tenantId,
            ])
            ->one();

        return $model;
    }

    protected function findReferralSystemModels($clientId, $tenantId)
    {
        $model = ReferralSystem::find()
            ->alias('r')
            ->where([
                'r.tenant_id' => $tenantId,
                'r.active'    => ReferralSystem::ACTIVE,
            ])
            ->joinWith([
                'member m' => function ($query) use ($clientId) {
                    $query->onCondition(['client_id' => $clientId]);
                },
            ])
            ->all();

        return $model;
    }

    protected function createReferralSystemMember($id, $clientId)
    {
        $model             = new ReferralSystemMember();
        $model->attributes = [
            'referral_id' => $id,
            'client_id'   => $clientId,
        ];

        return $model->save() ? $model : null;
    }

    protected function activateReferralSystem()
    {
        $referralSystemId = $this->_params['referral_id'];
        $tenantId         = $this->getTanantId();
        $client           = Client::findByPhone($tenantId, $this->_params['phone']);

        if (!$client) {
            throw new NeedReauthException();
        }

        $model = $this->findReferralSystemModel($referralSystemId, $tenantId);

        if (!$model) {
            return [
                'is_activate' => 0,
                'code'        => '',
            ];
        }

        $member = $this->createReferralSystemMember($model->referral_id, $client->client_id);

        return [
            'is_activate' => (int)!empty($member),
            'code'        => isset($member->code) ? $member->code : '',
        ];
    }


    /**
     * Название команды: activate_referral_system_code
     *
     * Описание команды: Активация кода реферальной системы.
     *
     * Тип запроса: POST
     *
     * @param current_time - Текущее время *
     * @param phone - Телефон клиента *
     * @params code - Код *
     *
     * Ответ:
     * <code>
     * {
     *  "code": 0,
     *  "info": "OK",
     *  "result": 1,
     *  "current_time": "12"
     * }
     * </code>
     *
     * @return string
     */
    public function actionActivate_referral_system_code()
    {
        $this->_params = $this->getParams($_POST);

        return $this->doAction('activateReferralSystemCode');
    }

    protected function findReferralSystemMemberModelByCode($code, $type = null)
    {
        $model = ReferralSystemMember::find()
            ->alias('m')
            ->joinWith('referralSystem r')
            ->where([
                'm.code' => $code,
            ])
            ->andFilterWhere([
                'r.type' => $type,
            ])
            ->limit(1)
            ->one();

        return $model;
    }

    /**
     * @param $id
     * @param $clientId
     *
     * @return bool
     */
    protected function isCreateReferralSystemSubscriber($id, $clientId)
    {
        $model             = new ReferralSystemSubscriber();
        $model->attributes = [
            'client_id' => $clientId,
            'member_id' => $id,
        ];

        return $model->isCreate();
    }

    protected function createReferralSystemSubscriber($id, $clientId)
    {
        $model             = new ReferralSystemSubscriber();
        $model->attributes = [
            'client_id' => $clientId,
            'member_id' => $id,
        ];

        return $model->save() ? $model : null;
    }

    protected function activateReferralSystemCode()
    {
        /* @var $orderApi OrderApi */
        $orderApi = Yii::$app->get('orderApi');

        $code     = $this->_params['code'];
        $tenantId = $this->getTanantId();
        $client   = Client::findByPhone($tenantId, $this->_params['phone']);

        if (!$client) {
            throw new NeedReauthException();
        }

        $promoRes = $orderApi->activationPromoCode($tenantId, $client->client_id, $code);

        $model = $this->findReferralSystemMemberModelByCode($code, ReferralSystem::ON_ORDER);

        $subscriber = null;
        if (!$model && ($promoRes['code'] != ErrorCode::OK)) {
            throw new CodeNotFoundException();
        } elseif ($model && ($promoRes['code'] == ErrorCode::OK)) {
            Yii::error('Referral and promo codes match');
        } elseif ($model) {
            $subscriber = $this->isCreateReferralSystemSubscriber($model->member_id, $client->client_id)
                ? $this->createReferralSystemSubscriber($model->member_id, $client->client_id) : null;
        }

        return ($subscriber || ($promoRes['code'] == ErrorCode::OK)) ? 1 : 0;
    }

    /**
     *
     * Название команды: send_password.
     *
     * Описание команды: Отправить пароль клиенту через SMS, когда он его не знает
     *
     * Специальные возвращаемые коды:
     *
     * BLACK_LIST 9
     *
     * Тип запроса: POST.
     *
     * @params string phone - телефон килента. Формат: 7XXX... .*
     *
     * @params string city_id  - ИД города
     *
     * @params string current_time  - текущее время timestamp . *
     *
     * @return string JSON
     *
     * <code>
     * {
     *
     *  "code":0,
     *
     *  "info":"OK",
     *
     *  "result":
     *
     *      {
     *
     *          "password_result":1
     *
     *      }
     *
     *  }
     * </code>
     *
     * password_result :
     *
     * 1 - SMS удачно
     *
     * 0 - Не удачно
     *
     */
    public function actionSend_password()
    {
        $this->_params = $this->getParams($_POST);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $result        = [];
        $request       = new TaxiApiRequest();


        if ($validateData['code'] == TaxiErrorCode::OK) {

            $tenant_id = $this->getTanantId();
            $lang      = $this->getLang();
            $model     = Client::findByPhone($tenant_id, $this->_params['phone']);
            $cityId    = ArrayHelper::getValue($this->_params, 'city_id');



            // Если есть такой клиент
            if ($model) {
                // Если не в черном списке
                if (empty($model->black_list)) {
                    // если нету пароля создаем
                    if (empty($model->password)) {
                        $model->scenario = 'change-password';
                        $model->password = Client::generatePassword();
                        $model->save();
                    }
                    $password = $model->password;
                    if (!empty($model->city_id)) {
                        $cityId = $model->city_id;
                    }
                } else {
                    $validateData['code'] = TaxiErrorCode::BLACK_LIST;
                    $validateData['info'] = 'BLACK_LIST';

                    return $request->getJsonAnswer($validateData, $result);
                }
            } else {
                // Проверка во временной таблице
                $client_temp = ClientTemp::findOne([
                    'tenant_id' => $tenant_id,
                    'phone'     => $this->_params['phone'],
                ]);


                if ($client_temp) {
                    $password = $client_temp->password;
                } else {
                    $password = Client::generatePassword();

                    $client_temp_new             = new ClientTemp();
                    $client_temp_new->attributes = [
                        'tenant_id' => $tenant_id,
                        'phone'     => $this->_params['phone'],
                        'password'  => $password,
                    ];
                    $client_temp_new->save();
                }
            }


            if (!$cityId || true) {
                $cityId = TenantHasSms::find()
                    ->select('city_id')
                    ->where([
                        'tenant_id' => $tenant_id,
                        'active'    => TenantHasSms::IS_ACTIVE,
                        'default'   => TenantHasSms::IS_DEFAULT,
                    ])
                    ->scalar();
            }

            if (!$cityId) {
                $cityId = TenantHasCity::find()
                    ->select('city_id')
                    ->where([
                        'tenant_id' => $tenant_id,
                        'block'     => TenantHasCity::NOT_BLOCK,
                    ])
                    ->orderBy([
                        'sort' => SORT_ASC,
                    ])
                    ->limit(1)
                    ->scalar();

            }

            /** @var ApiService $apiService */
            $apiService                = app()->get('apiService');


            $result['password_result'] = (int)$apiService->sendPassword($tenant_id, $cityId, $this->_params['phone'],
                Position::TAXI_DRIVER_ID);
        }

        return $request->getJsonAnswer($validateData, $result);
    }

    /**
     * Название команды: accept_password.
     *
     * Описание команды: Подтверждение полученного пароля. (Авторизация)
     *
     * Специальные возвращаемые коды:
     *
     * BLACK_LIST 9
     *
     * Тип запроса: GET.
     *
     * @params string phone - телефон килента . Формат: XXX...  *.
     *
     * @params string password - полученный пароль *.
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @params int city_id - выбранный город пользователя
     *
     * @params string referral_code - активация код для реферальной сиситемы
     *
     * @return string JSON
     *
     * <code>
     * {
     *
     *  "code":0,
     *
     *  "info":"OK",
     *
     *  "result":
     *
     *      {
     *
     *          "accept_result":1,
     *          "accept_referral_code":1,
     *          "client_profile":"Идентично запроса get_client_profile"
     *
     *          "bonus_system":{
     *
     *              "id": "2",
     *              "name": "UDS Game",
     *              "active": 1
     *
     *          },
     *
     *      }
     *
     * }
     * </code>
     *
     * accept_result :
     *
     * 1 - удачно
     *
     * 0 - не удачно
     *
     */
    public function actionAccept_password()
    {
        $this->_params = $this->getParams($_POST);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $result        = [];
        $request       = new TaxiApiRequest();
        $isNewClient   = false;

        if ($validateData['code'] == TaxiErrorCode::OK) {
            $tenant_id                     = $this->getTanantId();
            $lang                          = $this->getLang();
            $type_client                   = $this->getTypeClient();
            $device_id                     = $this->getDeviceId();
            $appId                         = $this->getAppId();
            $this->_params['city_id']      = !empty($this->_params['city_id']) ? $this->_params['city_id'] : null;
            $this->_params['device_token'] = !empty($this->_params['device_token']) ? $this->_params['device_token'] : null;

            $model = Client::findByPhone($tenant_id, $this->_params['phone']);
            // Если есть такой клиент
            if ($model) {
                // если нету в черном списке
                if (empty($model->black_list)) {
                    // Если пароль совпал
                    if (!empty($model->password) && $model->password == $this->_params['password']) {
                        if (!empty($this->_params['referral_code'])) {
                            $member = $this->findReferralSystemMemberModelByCode($this->_params['referral_code'],
                                ReferralSystem::ON_ORDER);
                            if (!$member) {
                                return $request->getJsonAnswer([
                                    'code' => TaxiErrorCode::INCORRECTLY_REFERRAL_CODE,
                                    'info' => 'INCORRECTLY_REFERRAL_CODE',
                                ]);
                            }
                            if (!$this->isCreateReferralSystemSubscriber($member->member_id, $model->client_id)) {
                                return $request->getJsonAnswer([
                                    'code' => TaxiErrorCode::INCORRECTLY_REFERRAL_CODE,
                                    'info' => 'INCORRECTLY_REFERRAL_CODE',
                                ]);
                            }
                            $subscriber = $this->createReferralSystemSubscriber($member->member_id,
                                $model->client_id);

                            $result['activate_referral_system'] = $subscriber ? 1 : 0;
                        }
                        $model->updateDeviceAndCityData($type_client, $this->_params['device_token'],
                            $this->_params['city_id'], $lang,
                            $appId);

                        $surname    = ArrayHelper::getValue($this->_params, 'surname');
                        $name       = ArrayHelper::getValue($this->_params, 'name');
                        $patronymic = ArrayHelper::getValue($this->_params, 'patronymic');
                        $email      = ArrayHelper::getValue($this->_params, 'email');

                        $model->updateNameAndEmail($surname, $name, $patronymic, $email);
                        $result['accept_result']  = 1;
                        $result['client_profile'] = $model->formatted();
                        try {
                            $profile = $this->profileService->getProfile($tenant_id,
                                $result['client_profile']['city_id']);
                            $cards   = $this->container
                                ->get('card')->getCards($profile, $result['client_profile']['client_id']);
                        } catch (\Exception $ex) {
                            $cards = null;
                        }
                        $result['client_profile']['client_cards'] = empty($cards) ? [] : $cards;
                        if (gtoet('1.1.0')) {
                            $result['bonus_system'] = $this->getBonusSystemInfo($tenant_id, $model->client_id);
                        }

                    } else {
                        $result['accept_result'] = 0;
                    }
                } else {
                    $validateData['code'] = TaxiErrorCode::BLACK_LIST;
                    $validateData['info'] = 'BLACK_LIST';

                    return $request->getJsonAnswer($validateData, $result);
                }
            } else {
                $client_temp = ClientTemp::find()
                    ->where([
                        'tenant_id' => $tenant_id,
                        'phone'     => $this->_params['phone'],
                    ])
                    ->one();

                if ($client_temp) {
                    if ($client_temp->password == $this->_params['password']) {
                        $client             = new Client();
                        $client->scenario   = 'create';
                        $client->attributes = [
                            'tenant_id'    => $tenant_id,
                            'password'     => $this->_params['password'],
                            'device_token' => $this->_params['device_token'],
                            'appId'        => $appId,
                            'lang'         => $lang,
                        ];
                        if (!empty($this->_params['city_id'])) {
                            $client->city_id = $this->_params['city_id'];
                        }
                        if (in_array(strtoupper($type_client), Client::getClentTypeList())) {
                            $client->device = strtoupper($type_client);
                        }
                        if (!empty($this->_params['referral_code'])) {
                            $member = $this->findReferralSystemMemberModelByCode($this->_params['referral_code']);
                            if (!$member) {
                                return $request->getJsonAnswer([
                                    'code' => TaxiErrorCode::INCORRECTLY_REFERRAL_CODE,
                                    'info' => 'INCORRECTLY_REFERRAL_CODE',
                                ]);
                            }
                            if (!$this->isCreateReferralSystemSubscriber($member->member_id, $client->client_id)) {
                                return $request->getJsonAnswer([
                                    'code' => TaxiErrorCode::INCORRECTLY_REFERRAL_CODE,
                                    'info' => 'INCORRECTLY_REFERRAL_CODE',
                                ]);
                            }
                        }
                        if ($client->save()) {
                            $client_phone             = new ClientPhone();
                            $client_phone->attributes = [
                                'client_id' => $client->client_id,
                                'value'     => $this->_params['phone'],
                            ];
                            $client_phone->save();
                            $client_temp->delete();
                            $isNewClient = true;

                            $result = [
                                'accept_result'  => 1,
                                'client_profile' => $client->formatted(),
                            ];

                            if (!empty($this->_params['referral_code'])) {
                                $subscriber = $this->createReferralSystemSubscriber($member->member_id,
                                    $client->client_id);

                                $result['activate_referral_system'] = $subscriber ? 1 : 0;
                            }

                            if (gtoet('1.1.0')) {
                                $result['bonus_system'] = $this->getBonusSystemInfo($tenant_id, $client->client_id);
                            }
                        } else {
                            $result = [
                                'accept_result' => 0,
                            ];
                        }

                    } else {
                        $result = [
                            'accept_result' => 0,
                        ];
                    }
                } else {
                    $validateData['code'] = TaxiErrorCode::EMPTY_DATA_IN_DATABASE;
                    $validateData['info'] = 'EMPTY_DATA_IN_DATABASE';
                }
            }
        }

        return $request->getJsonAnswer($validateData, $result);
    }

    protected function getBonusSystemInfo($tenantId, $clientId)
    {
        $bonusSystem     = \Yii::createObject(BonusSystem::class, [$tenantId]);
        $bonusSystemType = $bonusSystem->getBonusSystemType();
        $active          = $bonusSystem->isUDSGameBonusSystem()
            ? (int)$bonusSystem->isRegisteredClient($clientId) : 1;



        return [
            'id'     => $bonusSystemType->getId(),
            'name'   => $bonusSystemType->getName(),
            'active' => $active,
        ];
    }

    /**
     * Получение профиля клиента
     *
     * В ПРОФИЛЕ НЕ ДОЛЖНО БЫТЬ ПОЛЯ С NULL! ЛОМАЕТСЯ IOS APP
     *
     * Описание команды: Получение профиля клиента
     *
     * Специальные возвращаемые коды:
     *
     * BLACK_LIST 9
     *
     * Тип запроса: GET.
     *
     * @params string phone  - телефон клиента.*
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * <code>
     * {
     *
     *  "code":0,
     *
     *  "info":"OK",
     *
     *  "result":{
     *
     *  "bonus_system":{
     *
     *      "id": "2",
     *
     *      "name": "UDS Game",
     *
     *      "active": 1
     *
     * },
     *
     *  "client_profile":{
     *
     *      "client_id":"202",
     *
     *      "surname":"Калашников",
     *
     *      "name":"Сергей",
     *
     *      "patronymic":"Петрович",
     *
     *      "email":"",
     *
     *      "birth":"1991-09-17",
     *
     *      "photo":"http://forztaxi.lcl/upload/68/55374361d860f.jpg",
     *
     *      "personal_balance":{
     *get_tenant_city_list
     *          "personal_balance_value":"9370",
     *
     *          "personal_balance_currency":"RUB"
     *
     *      },
     *
     *      "personal_bonus":[
     *
     *      {
     *          "bonus_value":"100",
     *
     *          "bonus_name":"B"
     *
     *      }
     *
     *      ],
     *
     *      "client_companies":[
     *
     *      {
     *
     *          "company_id": "1",
     *
     *          "company_name":"Колосок",
     *
     *          "company_logo":"https://3colors.uatgootax.ru/file/show-external-file?filename=thumb_577c1fb9b.jpg&id=68"
     *
     *          "company_balance_value":"930",
     *
     *          "company_balance_currency":"RUB"
     *
     *      }
     *
     *      ],
     *
     *      "client_phones":[
     *
     *       {
     *
     *          "phone":"79127629913"
     *
     *      }
     *
     *      ]
     *
     *      "client_cards":[
     *          "639002**000409",
     *          "522222**222227",
     *      ]
     *
     *   }
     *
     *  }
     *
     * }
     * </code>
     */
    public function actionGet_client_profile()
    {
        $this->_params = $this->getParams($_GET);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $result        = [];

        $request = new TaxiApiRequest();

        if ($validateData['code'] == TaxiErrorCode::OK) {

            $tenant_id = $this->getTanantId();
            $model     = Client::findByPhone($tenant_id, $this->_params['phone']);
            if ($model) {
                if (empty($model->black_list)) {
                    $result['client_profile'] = $model->formatted();


                    if (!gtoet('1.2.0')) {
                        if (gtoet('1.1.0')) {
                            $result['bonus_system'] = $this->getBonusSystemInfo($tenant_id, $model->client_id);
                        }
                        try {
                            $profile = $this->profileService->getProfile($tenant_id, $result['client_profile']['city_id']);
                            $cards   = $this->container
                                ->get('card')->getCards($profile, $result['client_profile']['client_id']);
                        } catch (\Exception $ex) {
                            $cards = null;
                        }
                        $result['client_profile']['client_cards'] = empty($cards) ? [] : $cards;
                    }
                }
            }
        }

        return $request->getJsonAnswer($validateData, $result);
    }


    /**
     * Получение баланса клиента
     *
     * Описание команды: Получение баланса клиента
     *
     * Тип запроса: GET.
     *
     * @params string phone  - телефон клиента.*
     *
     * @params string city_id  - Ид города.*
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * <code>
     * {
     *   "code": 0,
     *
     *   "info": "OK",
     *
     *   "result": {
     *
     *       "currency": {
     *
     *           "code": "RUB"
     *
     *       },
     *
     *       "personal_balance": {
     *
     *           "value": "5301"
     *
     *       },
     *
     *       "personal_bonus": {
     *
     *           "value": "3396.76"
     *
     *       },
     *
     *       "company_balance": [
     *
     *           {
     *
     *               "company_id": "1",
     *
     *               "value": "9060"
     *
     *           },
     *
     *           {
     *
     *               "company_id": "10",
     *
     *               "value": "-37"
     *
     *           },
     *
     *           {
     *
     *               "company_id": "19",
     *
     *               "value": "313"
     *
     *           },
     *
     *           {
     *
     *               "company_id": "26",
     *
     *               "value": "11935"
     *
     *           },
     *
     *           {
     *
     *               "company_id": "45",
     *
     *               "value": "0"
     *
     *           }
     *
     *       ]
     *
     *   },
     *
     *   "current_time": "123123"
     *
     *}
     * </code>
     */
    public function actionGet_client_balance()
    {
        $this->_params = $this->getParams($_GET);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $result        = [];

        $request = new TaxiApiRequest();

        if ($validateData['code'] == TaxiErrorCode::OK) {
            $tenantId = $this->getTanantId();
            $cityId   = $this->_params['city_id'] ?: null;
            $model    = Client::findByPhone($tenantId, $this->_params['phone']);

            if ($model) {

                $currencyId = TenantSetting::getTenantCurrencyId($tenantId, $cityId);
                $result     = $model->getBalance($currencyId);

                $result['bonus_system'] = $this->getBonusSystemInfo($tenantId, $model->client_id);

                try {
                    $profile = $this->profileService->getProfile($tenantId, $cityId);
                    $cards   = $this->container
                        ->get('card')->getCards($profile, $model->client_id);
                } catch (\Exception $ex) {
                    $cards = null;
                }
                $result['cards'] = (array)$cards;
            }
        }

        return $request->getJsonAnswer($validateData, $result);
    }


    /**
     * Название команды: get_city_list.
     *
     * Описание команды: Получение списка городов по нескольким буквам (автокомплит).
     *
     * Специальные возвращаемые коды: нет.
     *
     * Тип запроса: GET.
     *
     * @params string city_part  - часть названия города *.
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * <code>
     * {
     *
     *  "code":0,
     *
     *  "info":"OK",
     *
     *  "result":{
     *
     *      "city_list":[
     *
     *          {
     *
     *              "city_id":"210861",
     *
     *              "city_name":"Москва г"
     *
     *              "city_lat":"55.753676",
     *
     *              "city_lon":"37.619899"
     *
     *          }
     *
     *      ]
     *
     *  }
     *
     * }
     * </code>
     *
     */
    public function actionGet_city_list()
    {
        $this->_params = $this->getParams($_GET);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $request       = new TaxiApiRequest();
        $result        = [];
        if ($validateData['code'] == TaxiErrorCode::OK) {
            list($lang) = explode('-', $this->getLang());

            $lang = $lang == 'ru' || empty($lang) ? '' : '_' . $lang;

            if (mb_strlen($this->_params['city_part'], "utf-8") >= 3) {
                $result['city_list'] = City::searchCity($this->_params['city_part'], $lang);
            } else {
                $result['city_list'] = null;
            }
        }

        return $request->getJsonAnswer($validateData, $result);
    }


    /**
     * Запрос на отправку кода подтверждения при редактировании телефона в профиле
     *
     * Описание команды:  Запрос на отправку кода подтверждения при редактировании телефона в профиле
     *
     * Специальные возвращаемые коды:
     *
     * BLACK_LIST 9
     *
     * Тип запроса: POST.
     *
     * @params string old_phone  - старый телефон клиента. *
     *
     * @params string new_phone  - новый телефон клиента. *
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     * <code>
     * {
     *  "code":0,
     *  "info":"OK",
     *  "result":{
     *      "password_result":1
     *  }
     * }
     * </code>
     */
    public function actionSend_password_new_phone()
    {
        $this->_params = $this->getParams($_POST);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $result        = [];
        $request       = new TaxiApiRequest();

        if ($validateData['code'] == TaxiErrorCode::OK) {
            $tenant_id = $this->getTanantId();
            $lang      = $this->getLang();
            $model     = Client::findByPhone($tenant_id, $this->_params['old_phone']);

            // Если есть такой клиент
            if ($model) {
                // Если не в черном списке
                if (empty($model->black_list)) {
                    $model->scenario = 'change-password';
                    $model->password = Client::generatePassword();
                    $model->save();
                    $smsComonent               = new TaxiSmsComponent();
                    $result['password_result'] = (int)$smsComonent->sendSms($tenant_id, $this->_params['new_phone'],
                        t('sms', 'Your password', [], $lang) . ' ' . $model->password);
                } else {
                    $validateData['code'] = TaxiErrorCode::BLACK_LIST;
                    $validateData['info'] = 'BLACK_LIST';

                    return $request->getJsonAnswer($validateData, $result);
                }
            } else {
                $result['password_result'] = 0;
            }

        }

        return $request->getJsonAnswer($validateData, $result);
    }

    /**
     * Подтверждение нового телефона при редактировании профиля
     *
     * Описание команды:  Подтверждение нового телефона при редактировании профиля
     *
     * Специальные возвращаемые коды:
     *
     * BLACK_LIST 9
     *
     * Тип запроса: POST.
     *
     * @params string old_phone  - старый телефон клиента.*
     *
     * @params string new_phone  - новый телефон клиента.*
     *
     * @params string password  - смс пароль.*
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     * <code>
     * {
     *  "code":0,
     *  "info":"OK",
     *  "result":{
     *      "accept_result":1
     *  }
     * }
     * </code>
     */
    public function actionAccept_password_new_phone()
    {
        $this->_params = $this->getParams($_POST);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $result        = [];
        $request       = new TaxiApiRequest();

        if ($validateData['code'] == TaxiErrorCode::OK) {
            $tenant_id = $this->getTanantId();
            $model     = Client::findByPhone($tenant_id, $this->_params['old_phone']);

            // Если есть такой клиент
            if ($model) {
                // Если не в черном списке
                if (empty($model->black_list)) {
                    $result['accept_result'] = (int)($model->password == $this->_params['password']);
                } else {
                    $validateData['code'] = TaxiErrorCode::BLACK_LIST;
                    $validateData['info'] = 'BLACK_LIST';

                    return $request->getJsonAnswer($validateData, $result);
                }
            }
        }

        return $request->getJsonAnswer($validateData, $result);
    }

    /**
     * Получение html страницы о службе заказчика
     *
     * Тип запроса: GET
     *
     * @params string current_time  - текущее время timestamp*.
     *
     * @return string HTML
     */
    public function actionGet_info_page()
    {
        $params       = $this->getParams($_GET);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);
        $request      = new TaxiApiRequest();

        if ($validateData['code'] !== TaxiErrorCode::OK) {
            return $request->getJsonAnswer($validateData);
        } else {
            $appId    = $this->getAppId();
            $tenantId = $this->getTanantId();

            $pageData = MobileApp::find()
                ->select('page_info')
                ->where([
                    'tenant_id' => $tenantId,
                    'app_id'    => $appId,
                    'active'    => MobileApp::ACTIVE,
                ])
                ->limit(1)
                ->scalar();

            $validateData['code'] = TaxiErrorCode::OK;
            $validateData['info'] = 'OK';
            $result               = ['result' => (string)$pageData];

            return $request->getJsonAnswer($validateData, $result);
        }
    }


    /**
     * Получение html страницы о политике конфиденцальности заказчика
     *
     * Тип запроса: GET
     *
     * @params string current_time  - текущее время timestamp*.
     *
     * @return string HTML
     */
    public function actionGet_confidential_page()
    {
        $params       = $this->getParams($_GET);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);
        $request      = new TaxiApiRequest();

        if ($validateData['code'] !== TaxiErrorCode::OK) {
            return $request->getJsonAnswer($validateData);
        } else {
            $appId    = $this->getAppId();
            $tenantId = $this->getTanantId();

            $pageData = MobileApp::find()
                ->select('confidential')
                ->where([
                    'tenant_id' => $tenantId,
                    'app_id'    => $appId,
                    'active'    => MobileApp::ACTIVE,
                ])
                ->limit(1)
                ->scalar();

            $validateData['code'] = TaxiErrorCode::OK;
            $validateData['info'] = 'OK';
            $result               = ['result' => (string)$pageData];

            return $request->getJsonAnswer($validateData, $result);
        }
    }

    /**
     * Редактирование профиля клиента
     *
     * Описание команды: Редактирование профиля клиента
     *
     * Специальные возвращаемые коды: нет.
     *
     * Тип запроса: POST.
     *
     * @params string client_id  - id клиента.*
     *
     * @params string old_phone  - старый телефон клиента.*
     *
     * @params string new_phone  - новый телефон клиента.*
     *
     * @params string surname  - фамилия клиента.
     *
     * @params string name  - имя клиента.
     *
     * @params string patronymic  - отчество клиента.
     *
     * @params string email  - емэйл клиента.
     *
     * @params string birthday  - дата рождения клиента. (Формат: 1991-09-17)
     *
     * @params file photo  - фото клиента.
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * <code>
     * {
     *
     *  "code":0,
     *
     *  "info":"OK",
     *
     *  "result":
     *
     *      {
     *
     *          "update_result":1
     *          "client_profile":"Идентично запроса get_client_profile"
     *
     *      }
     *
     * }
     * </code>
     *
     */
    public function actionUpdate_client_profile()
    {
        $this->_params = $this->getParams($_POST);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $result        = [];
        $request       = new TaxiApiRequest();

        if ($validateData['code'] == TaxiErrorCode::OK) {
            $model           = Client::findOne($this->_params['client_id']);
            $model->scenario = 'profile';
            if (!empty($this->_params['surname'])) {
                $model->last_name = $this->_params['surname'];
            }
            if (!empty($this->_params['name'])) {
                $model->name = $this->_params['name'];
            }
            if (!empty($this->_params['patronymic'])) {
                $model->second_name = $this->_params['patronymic'];
            }
            if (!empty($this->_params['birthday'])) {
                $model->birth = $this->_params['birthday'];
            }
            if (!empty($this->_params['email'])) {
                $model->email = $this->_params['email'];
            }
            if ($model->save()) {
                if ($this->_params['old_phone'] != $this->_params['new_phone']) {
                    $model->deletePhone($this->_params['old_phone']);
                    $model->savePhone($this->_params['new_phone']);
                }
                $photo = isset($_FILES['photo']['tmp_name']) ? $_FILES['photo']['tmp_name'] : null;
                if (isset($photo)) {
                    $url  = Yii::$app->params['frontend.uploadUrl'] . 'upload_client_photo';
                    $post = [
                        'Client[photo]' => new \CURLFile($photo, mime_content_type($photo), ''),
                        'client_id'     => $this->_params['client_id'],
                        'phone'         => $this->_params['new_phone'],
                    ];
                    $ch   = curl_init($url);
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($ch, CURLOPT_USERPWD, app()->params['frontend.basicAuth']);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    $r            = curl_exec($ch);
                    $photo        = Client::find()
                        ->select('photo')
                        ->where(['client_id' => $this->_params['client_id']])
                        ->scalar();
                    $model->photo = $photo;
                }
                $result = [
                    'update_result'  => 1,
                    'client_profile' => $model->formatted(),
                ];
            } else {
                $result['update_result'] = 0;
            }
        }

        return $request->getJsonAnswer($validateData, $result);
    }

    /**
     * Название команды: Get_client_cards.
     *
     * Описание команды: Получение списка банковских карт клиента
     *
     * Специальные возвращаемые коды:
     *
     * NEED_REAUTH 10
     *
     * PAYMENT_GATE_ERROR 14 Ошибка запроса к платежному шлюзу.
     *
     * Тип запроса: GET.
     *
     * @params string client_id  - id клиента.*
     *
     * @params string phone - телефон клиента.*
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * <code>
     * {
     *      "code": 0,
     *
     *      "info": "OK",
     *
     *      "result": [
     *          "433311**124",
     *          "555555**1880"
     *      ]
     * }
     * </code>
     *
     */
    public function actionGet_client_cards()
    {
        $params = $this->getParams($_GET);

        return $this->doCardAction('getCards', $params);
    }

    /**
     * Название команды: Create_client_card.
     *
     * Описание команды: Привязать банковскую карту клиента
     *
     * Специальные возвращаемые коды:
     *
     * NEED_REAUTH 10
     *
     * PAYMENT_GATE_ERROR 14 Ошибка запроса к платежному шлюзу.
     *
     * Тип запроса: POST.
     *
     * @params string client_id  - id клиента.*
     *
     * @params string phone - телефон клиента.*
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * <code>
     * {
     *      "code": 0,
     *
     *      "info": "OK",
     *
     *      "result": {
     *          "orderId": "BC2xl_iW5l46Uj0jVkeOwo1d3uMJE6Rm",
     *          "url":
     *     "https://test.paymentgate.ru/testpayment/merchants/3colors_auto/payment_ru.html?mdOrder=42175111-fcab-4b3b-a86d-b82a896a71d4"
     *      }
     * }
     * </code>
     *
     */
    public function actionCreate_client_card()
    {
        $params = $this->getParams($_POST);

        return $this->doCardAction('create', $params);
    }

    /**
     * Название команды: Check_client_card.
     *
     * Описание команды: Проверить была ли привязана банковская карта клиента
     *
     * Специальные возвращаемые коды:
     *
     * NEED_REAUTH 10
     *
     * PAYMENT_GATE_ERROR 14 Ошибка запроса к платежному шлюзу.
     *
     * Тип запроса: POST.
     *
     * @params string client_id  - id клиента.*
     *
     * @params string phone - телефон клиента.*
     *
     * @params string order_id  - id заказа платежного шлюза.*
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * <code>
     * {
     *      "code": 0,
     *
     *      "info": "OK",
     *
     *      "result": null
     * }
     * </code>
     *
     */
    public function actionCheck_client_card()
    {
        $params = $this->getParams($_POST);

        return $this->doCardAction('check', $params);
    }

    /**
     * Название команды: Delete_client_card.
     *
     * Описание команды: Удаление банковской карты клиента
     *
     * Специальные возвращаемые коды:
     *
     * NEED_REAUTH 10
     *
     * PAYMENT_GATE_ERROR 14 Ошибка запроса к платежному шлюзу.
     *
     * Тип запроса: POST.
     *
     * @params string client_id  - id клиента.*
     *
     * @params string phone - телефон клиента.*
     *
     * @params string pan        - номер карты клиента.*
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * <code>
     * {
     *      "code": 0,
     *
     *      "info": "OK",
     *
     *      "result": true
     * }
     * </code>
     *
     */
    public function actionDelete_client_card()
    {
        $params = $this->getParams($_POST);

        return $this->doCardAction('delete', $params);
    }

    /**
     * Название команды: Get_coords_by_address.
     *
     * Описание команды: Получение координат по адресу
     *
     * Специальные возвращаемые коды:Нет
     *
     * Тип запроса - GET.
     *
     * @params string city - Название города .*
     *
     *
     * @params string street - Улица
     *
     * @params string house  - Дом
     *
     * @params string housing  - Строение
     *
     * @params string porch  - Подъезд
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     * <code>
     * {
     * "code":0,
     * "info":"OK",
     * "result":{
     *      "lat":56.844252,
     *      "lon":53.242294987316
     *  }
     * }
     * </code>
     */
    public function actionGet_coords_by_address()
    {
        $params       = $this->getParams($_GET);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);
        $requset      = new TaxiApiRequest();
        if ($validateData['code'] !== 0) {
            return $requset->getJsonAnswer($validateData);
        } else {
            $tenantId     = $this->getTanantId();
            $lang         = $this->getLang();
            $city         = $params['city'];
            $cityId       = $params['city_id'];
            $street       = isset($params['street']) ? $params['street'] : null;
            $house        = isset($params['house']) ? $params['house'] : null;
            $housing      = isset($params['housing']) ? $params['housing'] : null;
            $porch        = isset($params['porch']) ? $params['porch'] : null;
            $geocoderType = TenantSetting::getSettingValue($tenantId, TenantSetting::SETTING_GEOCODER_TYPE, $cityId);
            if (empty($geocoderType)) {
                $geocoderType = "ru";
            }
            $geocoder = Yii::$app->geocoder;
            $address  = $city . "," . $street . "," . $house . "," . $housing . "," . $porch;
            $result   = $geocoder->findCoordsByAddress($address, $lang, $geocoderType);
            if (!isset($result['lat'])) {
                $result = [
                    'lat' => "",
                    'lon' => "",
                ];
            }
            $validateData['code'] = TaxiErrorCode::OK;
            $validateData['info'] = 'OK';

            return $requset->getJsonAnswer($validateData, $result);
        }
    }

    /**
     * Название команды: Get_address_by_coords.
     *
     * Описание команды: Получение адреса по координатам
     *
     *
     * Специальные возвращаемые коды:
     *
     * Нет
     *
     * Тип запроса - GET.
     *
     * @params string lat -  *
     *
     * @params string lon - *
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * <code>
     * {
     *
     *  "code":0,
     *
     *  "info":"OK",
     *
     *  "result":{
     *
     *      "type":"house",
     *
     *      "name":"",
     *
     *      "lat":"56.8638838",
     *
     *      "lon":"53.158061",
     *
     *      "street":"Песочная Улица",
     *
     *      "city":"Ижевск",
     *
     *      "house":"27",
     *
     *      "housing":"",
     *
     *      "porch":"",
     *
     *      "summaryAddress":"Ижевск,Песочная Улица,27"
     * }
     * }
     * </code>
     *
     * <strong>type =  "house" | "public_place" </strong>
     */
    public function actionGet_address_by_coords()
    {
        $params       = $this->getParams($_GET);
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);
        $requset      = new TaxiApiRequest();
        if ($validateData['code'] !== 0) {
            return $requset->getJsonAnswer($validateData);
        } else {
            $tenantId     = $this->getTanantId();
            $lang         = $this->getLang();
            $lat          = $params['lat'];
            $lon          = $params['lon'];
            $cityId       = $params['city_id'];
            $geocoderType = TenantSetting::getSettingValue($tenantId, TenantSetting::SETTING_GEOCODER_TYPE, $cityId);
            if (empty($geocoderType)) {
                $geocoderType = "ru";
            }
            $geocoder = Yii::$app->geocoder;
            $result   = $geocoder->findAddressByCoords($lat, $lon, $lang, $geocoderType);
            if (empty($result)) {
                $validateData['code'] = TaxiErrorCode::EMPTY_DATA_IN_DATABASE;
                $validateData['info'] = 'EMPTY_DATA_IN_DATABASE';
                $result               = null;

                return $requset->getJsonAnswer($validateData, $result);
            }
            $validateData['code'] = TaxiErrorCode::OK;
            $validateData['info'] = 'OK';

            return $requset->getJsonAnswer($validateData, $result);
        }
    }

    /**
     * Название команды: get_geoobjects_list.
     *
     * Описание команды:  Получение списка улиц и публичных мест по нескольким буквам (автокомплит).
     *
     * Специальные возвращаемые коды: нет.
     *
     * Тип запроса: GET.
     *
     * @params string city_id Ид города *.
     *
     * @params string street_part  Часть названия улицы или гео-объекта *.
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * <code>
     * {
     *
     * "code":0,
     *
     * "info":"OK",
     *
     * "result":{
     *
     *  "geo_objects":[
     *
     *      {
     *
     *      "type":"street",
     *
     *      "city":"Удмуртская Респ, Ижевск г",
     *
     *      "street":"Александра Аксенова ул"
     *
     *      },
     *
     *      {
     *
     *      "type":"public_place",
     *
     *      "name":"Кинотеатр Дружба",
     *
     *      "lat":"56.8668",
     *
     *      "lon":"56.2271",
     *
     *      "street":"Новая ул",
     *
     *      "city":"Удмуртская Респ, Ижевск г",
     *
     *      "house":"3",
     *
     *      "housing":"2",
     *
     *      "specials":[
     *
     *           "вход 1",
     *
     *           "вход 2",
     *
     *           "парковка"
     *
     *       ]
     *
     *      }
     *
     *  ]
     *
     * }
     *
     * }
     *
     * </code>
     */
    public function actionGet_geoobjects_list()
    {
        $this->_params = $this->getParams($_GET);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $requset       = new TaxiApiRequest();

        if ($validateData['code'] !== 0) {
            return $requset->getJsonAnswer($validateData);
        }

        $validateData['code'] = TaxiErrorCode::EMPTY_DATA_IN_DATABASE;
        $validateData['info'] = 'EMPTY_DATA_IN_DATABASE';

        return $requset->getJsonAnswer($validateData);
    }

    /**
     * Название команды: create_order.
     *
     * Описание команды:  Создание заказа/Получение предварительной стоимости.
     *
     * Специальные возвращаемые коды:
     *
     * code   info
     *
     * 10    NEED_REAUTH
     *
     * 11    BAD_PAY_TYPE
     *
     * 12   NO_MONEY
     *
     * 13   BAD_BONUS
     *
     * 15   INVALID_PAN
     *
     * Тип запроса - POST.
     *
     * @params string JSON address  - json строка содержащиая массив из точек поездки.
     *
     * {
     *
     * "address": [
     *
     *  {
     *
     *      "city_id": "26068",
     *
     *      "city": "Ижевск",
     *
     *      "street": "Советская",
     *
     *      "house": "12",
     *
     *      "housing": "2",
     *
     *      "porch": "2",
     *
     *      "lat": "54.5632",
     *
     *      "lon": "54.5123"
     *
     *   },
     *
     *   {
     *
     *      "city_id": "26068",
     *
     *      "city": "Ижевск",
     *
     *      "street": "Ленина",
     *
     *      "house": "12",
     *
     *      "housing": "2",
     *
     *      "porch": "2",
     *
     *      "lat": "54.5632",
     *
     *      "lon": "54.5123"
     *
     *    },
     *
     *   {
     *
     *      "city_id": "26068",
     *
     *      "city": "Ижевск",
     *
     *      "street": "Ленина",
     *
     *      "house": "12",
     *
     *      "housing": "2",
     *
     *      "porch": "2",
     *
     *      "lat": "54.5632",
     *
     *      "lon": "54.5123"
     *
     *   }
     *
     *  ]
     *
     * }
     *
     * @params string type_request  - 1 - создание заказа, 2 - получить предв. стоимость.
     *
     * @params string device_token  -  Ключ  для отправки push уведомлений (для Android application token, для IOS
     *     device token)
     *
     * @params string city_id  - Ид города (филилала) пользователя.
     *
     * @params string client_phone   -  Телефон клиента - международный формат - 7999111223 без “+”. Обязателен при
     * создании заказа
     *
     * @params string company_id   -  Ид компании.
     *
     * @params string client_id   -  Ид клиента.
     *
     * @params string client_name - Имя клиента.
     *
     * @params string order_time - Дата и время, на которое подать авто (для предварительного заказа).
     *
     * Формат: dd.mm.yyyy h:i:s. Если время отсутствует, то заказ на текущее время.
     *
     * @params string tariff_id  - Ид тарифа.
     *
     * @params string pay_type - Способ оплаты. ('CASH','CARD','PERSONAL_ACCOUNT','CORP_BALANCE')
     *
     * @params string driver_callsign - Позывной водителя, выбор машины клиентом на карте. (пока не юзабельно)
     *
     * @params string additional_options - ИД пожеланий клиента в виде строки, Ид записываются через запятую
     *
     * @params string comment - комментарий к заказу
     *
     * @params int bonus_payment - использовать бонусы - 1, не использовать 0 или не передавать
     *
     * @params string promo_code - промокод для бонусной системы
     *
     * @params string $pan - номер банковской карты
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * Результат создание заказа:
     *
     * <code>
     * {
     *
     *  "code":0,
     *
     *  "info":"OK",
     *
     *  "result":
     *
     *      {
     *
     *          "type": "new",
     *
     *          "order_id":143,
     *
     *          "order_number":22
     *
     *      }
     *
     * }
     * </code>
     *
     * Результат предварительного расчета стоимости заказа:
     * <code>
     * {
     *
     *  "code":0,
     *
     *  "info":"OK",
     *
     *  "result":
     *
     *  {
     *
     *      "cost_result":
     *
     *          {
     *              "additionals_cost":"30",
     *
     *              "summary_time":288.5,
     *
     *              "summary_distance":328.8,
     *
     *              "summary_cost":3320,
     *
     *              "city_time":4.7,
     *
     *              "city_distance":4.4,
     *
     *              "city_cost":70,
     *
     *              "out_city_time":283.8,
     *
     *              "out_city_distance":324.4,
     *
     *              "out_city_cost":3250,
     *
     *              "is_fix":0,
     *
     *              "start_point_location":"in"
     *
     *      }
     *
     *   }
     *
     * }
     * </code>
     *
     */
    public function actionCreate_order()
    {
        return $this->createOrder();
    }

    protected function bonusProcessing($tenantId, $orderId, $clientId, $bonusPayment, $promoCode)
    {
        $bonusSystem = \Yii::createObject(BonusSystem::class, [$tenantId]);

        if ($bonusSystem->isUDSGameBonusSystem() && $bonusSystem->isRegisteredClient($clientId)) {
            $bonusSystem->registerOrder($orderId, $bonusPayment ? $promoCode : null);
        }
    }

    /**
     * @param Order $order
     *
     * @throws \yii\base\ErrorException
     */
    private function sendOrderToEngine($order)
    {
        //Отправляем заказ в ноду
        $serviceEngine = new ServiceEngine();

        try {
            $resultSendOrder = $serviceEngine->neworderAuto($order->order_id, $order->tenant_id);
        } catch (\Exception $e) {
            \Yii::error("Receive error neworderAuto: orderId={$order->order_id} (Error:{$e->getMessage()})");
            $resultSendOrder = false;
        }

        if (!$resultSendOrder) {
            \Yii::$app->redis_orders_active->executeCommand('hdel', [$order->tenant_id, $order->order_id]);
            $error = null;
            try {
                $isOrderDeleted = $order->delete();
            } catch (\Exception $ex) {
                $isOrderDeleted = false;
                $error          = $ex->getMessage();
            }

            if (empty($isOrderDeleted)) {
                \Yii::error("Error delete new order from MySql, after engine was failed (Error:{$error})", 'order');
                \Yii::$app->apiLogger->log("Error delete new order from MySql, after engine was failed (Error:{$error})");
            }

            throw new ErrorException("An error occurred while sending order to NodeJS");
        }
    }

    protected function createOrder()
    {
        $this->_params = $this->getParams($_POST);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);

        $result        = [];
        $request       = new TaxiApiRequest();

        if ($validateData['code'] == TaxiErrorCode::OK) {
            $tenant_id   = $this->getTanantId();
            $type_client = $this->getTypeClient();
            $lang        = $this->getLang();
            $appId       = $this->getAppId();
            $clientId    = $this->_params['client_id'] ? $this->_params['client_id'] : null;
            $payment     = isset($this->_params['pay_type']) ? $this->_params['pay_type'] : "CASH";
            $company_id  = isset($this->_params['company_id']) ? $this->_params['company_id'] : '';

            // daimond
            $temp_client_date = explode('|', $this->_params['client_name']);
            if(is_array($temp_client_date) && (count($temp_client_date) == 2)){
                $client_passenger_lastname = trim( $temp_client_date[0]);
                $client_passenger_name = trim($temp_client_date[1]);
            }else{
                if(!is_null($this->_params['client_name'])){
                    $client_passenger_lastname  = isset($this->_params['client_name']) ?  trim($this->_params['client_name']) : '';
                    $client_passenger_name = '';
                }else{
                    $client_passenger_lastname  = isset($this->_params['client_passenger_lastname']) ?  trim($this->_params['client_passenger_lastname']) : '';
                    $client_passenger_name = isset($this->_params['client_passenger_name']) ?  trim($this->_params['client_passenger_name']) : '';
                }
            }

            $versionclient  = \Yii::$app->request->getHeaders()->get('versionclient');
            \Yii::$app->apiLogger->log(json_encode($this->_params, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            $tariff = TaxiTariff::findOne($this->_params['tariff_id']);


            // Если расчет предв. стоимости
            if ($this->_params['type_request'] == 2) {

                /** @var TaxiRouteAnalyzer $routeAnalyzer */
                $routeAnalyzer = \Yii::$app->routeAnalyzer;

                $geocoder_type = TenantSetting::getSettingValue(
                    $tenant_id, TenantSetting::SETTING_GEOCODER_TYPE, $this->_params['city_id'], $tariff->position_id);

                if (empty($geocoder_type)) {
                    $geocoder_type = "ru";
                }

                $offset_time = City::getTimeOffset($this->_params['city_id']);
                if (empty($this->_params['order_time'])) {
                    $order_time = time() + $offset_time;
                } else {
                    $order_time = new \DateTime($this->_params['order_time']);
                    $order_time = $order_time->getTimestamp();
                }

                $order_date     = date('d-m-Y H:i:s', $order_time);

                $address = Order::filterAddress($this->_params['address'], $lang, $geocoder_type, $tenant_id,
                    $this->_params['city_id']);

                $additional_option = isset($this->_params['additional_options']) ? explode(',',
                    $this->_params['additional_options']) : null;

                $routeCostInfo = $routeAnalyzer->analyzeRoute($tenant_id, $this->_params['city_id'],
                    $address['address'], $additional_option, $this->_params['tariff_id'], $order_date, $clientId
                );

                if (empty($routeCostInfo)) {
                    Yii::error("Ошибка анализатора маршрута, невозможно создать заказ");
                    $validateData['code'] = TaxiErrorCode::INTERNAL_ERROR;
                    $validateData['info'] = 'INTERNAL_ERROR';

                    return $request->getJsonAnswer($validateData, null);
                }
                $route    = (array)$routeCostInfo;

                $costData = [
                    'additionals_cost'         => (string)ArrayHelper::getValue($route, 'additionalCost', 0),
                    'summary_time'             => (string)ArrayHelper::getValue($route, 'summaryTime', 0),
                    'summary_distance'         => (string)ArrayHelper::getValue($route, 'summaryDistance', 0),
                    'summary_cost'             => (string)ArrayHelper::getValue($route, 'summaryCost', 0),
                    'summary_cost_no_discount' => (string)ArrayHelper::getValue($route, 'summaryCostNoDiscount', 0),
                    'city_time'                => (string)ArrayHelper::getValue($route, 'ityTime', 0),
                    'city_distance'            => (string)ArrayHelper::getValue($route, 'cityDistance', 0),
                    'city_cost'                => (string)ArrayHelper::getValue($route, 'cityCost', 0),
                    'out_city_time'            => (string)ArrayHelper::getValue($route, 'outCityTime', 0),
                    'out_city_distance'        => (string)ArrayHelper::getValue($route, 'outCityDistance', 0),
                    'out_city_cost'            => (string)ArrayHelper::getValue($route, 'outCityCost', 0),
                    'is_fix'                   => (int)ArrayHelper::getValue($route, 'isFix', 0),
                    'start_point_location'     => (string)ArrayHelper::getValue($route, 'startPointLocation', ''),
                    'tariffInfo'               => (array)ArrayHelper::getValue($route, 'tariffInfo', []),
                ];

                $result['cost_result'] = $costData;

                //Если создание заказа
            } elseif ($tariff) {

                /** @var OrderApi $apiOrder */
                $apiOrder = Yii::$app->orderApi;
                /** @var ProcessingErrorsApiOrder $processingErrors */
                $processingErrors = Yii::createObject(ProcessingErrorsApiOrder::class);

                $this->_params['address'] = $this->correctAddres($this->_params['address'], $this->_params['city_id']);


                $order = new Order([
                    'tenant_id'                   => $tenant_id,
                    'city_id'                     => $this->_params['city_id'],
                    'tariff_id'                   => $this->_params['tariff_id'],
                    'client_device_token'         => !empty($this->_params['device_token']) ? $this->_params['device_token'] : '',
                    'client_id'                   => $clientId,
                    'phone'                       => $this->_params['client_phone'],
                    'except_car_models'           => ArrayHelper::getValue($this->_params, 'except_car_models'),
                    'client_passenger_phone'      => ArrayHelper::getValue($this->_params, 'client_passenger_phone'),
                    'client_passenger_lastname'   =>  $client_passenger_lastname,
                    'client_passenger_name'       =>  $client_passenger_name,
                    'client_passenger_secondname' => ArrayHelper::getValue($this->_params, 'client_passenger_secondname'),
                    'comment'                     => $this->_params['comment'],
                    'create_time'                 => time(),
                    'status_time'                 => time(),
                    'payment'                     => $payment,
                    'address'                     => json_decode($this->_params['address'], true)['address'],
                    'position_id'                 => $tariff->position_id,
                    'bonus_payment'               => !empty($this->_params['bonus_payment']) ? 1 : 0,
                    'device'                      => $type_client,
                    'app_id'                      => $appId,
                    'lang'                        => $lang,
                    'company_id'                  => $company_id,
                    'versionclient'               => $versionclient,
                    'bonus_promo_code'            => isset($this->_params['promo_code']) ? $this->_params['promo_code'] : null,
                    'orderAction'                 => Order::ORDER_ACTION_CLIENT,
                    'pan'                         => ArrayHelper::getValue($this->_params, 'pan'),
                    'order_time'                  => isset($this->_params['order_time']) ? $this->_params['order_time'] : null,
                    'additional_option'           => isset($this->_params['additional_options'])
                        ? explode(',', $this->_params['additional_options'])
                        : null,
                ]);

                try {

                    $res = $apiOrder->createOrder($order);

                    $order = new Order($res);

                    $validateData['code'] = TaxiErrorCode::OK;
                    $validateData['info'] = 'OK';
                    $result               = [
                        'type'         => $order->status->status_group,
                        'order_id'     => $order->order_id,
                        'order_number' => $order->order_number,
                        'order_cost' => $order->predv_price,
                        'order_distance' => $order->predv_distance,
                    ];


                } catch (ClientException $ex) {
                    $errors = json_decode($ex->getResponse()->getBody(), true);
                    $processingErrors->setResponse($errors);
                    $errorCode = $processingErrors->getCodeError();

                    if (OrderApi::isUserErrors($ex)) {
                        if (isset($errors['errors'])) {
                            $validateData['validation'] = $errors['errors'];
                        }
                    }

                    $validateData['code'] = $errorCode;
                    $validateData['info'] = $processingErrors
                        ->taxiErrorCodes
                        ->errorCodeData[$errorCode];
                    $result               = null;
                } catch (\Exception $ex) {
                    Yii::error("Ошибка сохранения в мускул заказа: " . $ex->getMessage());
                    $validateData['code'] = TaxiErrorCode::INTERNAL_ERROR;
                    $validateData['info'] = 'INTERNAL_ERROR';
                }

            } else {
                $validateData['code'] = TaxiErrorCode::INTERNAL_ERROR;
                $validateData['info'] = 'INTERNAL_ERROR';
                $result               = null;
            }
        }

        return $request->getJsonAnswer($validateData, $result);
    }

    protected function  correctAddres($address,$city_id ){
        $addresses =  json_decode($this->_params['address'], true)['address'];

        $rezult['address'] = [];
        foreach ($addresses as $address ){
            if(empty( $address['city_id'])){
                $address['city_id'] = $city_id;
            }
            $rezult['address'][] = $address;
        }

        return  json_encode($rezult);

    }

    /**
     * Название команды: activate_bonus_system
     *
     * Описание команды:  Активация бонусной системы.
     *
     * Тип запроса: POST
     *
     * Возвращаемые коды:
     * BAD_REQUEST = 16
     * BAD_PARAM = 4
     * INVALID_PROMO_CODE = 40
     * INTERNAL_ERROR = 1
     *
     * @params string client_phone   -  Телефон клиента - международный формат - 7999111223 без “+”. *
     * @params string promo_code - Промокод для активации бонусной сиситемы
     *
     * @return string JSON- информация по активации.
     *
     * <code>
     * {
     *  "code":0,
     *  "info":"OK",
     *  "result":
     *   {
     *      "activate_bonus_system": 1
     *  }
     * }
     * </code>
     */
    public function actionActivate_bonus_system()
    {
        $this->_params = $this->getParams($_POST);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $result        = [];
        $request       = new TaxiApiRequest();

        if ($validateData['code'] == TaxiErrorCode::OK) {
            $tenant_id = $this->getTanantId();
            try {
                $bonusSystem = \Yii::createObject(BonusSystem::class, [$tenant_id]);

                if (!$bonusSystem->isUDSGameBonusSystem()) {
                    $validateData['code'] = TaxiErrorCode::BAD_REQUEST;
                    $validateData['info'] = 'BAD_REQUEST';

                    return $request->getJsonAnswer($validateData, null);
                }

                $client = Client::findByPhone($tenant_id, $this->_params['client_phone']);

                if (!$client) {
                    $validateData['code'] = TaxiErrorCode::BAD_PARAM;
                    $validateData['info'] = 'BAD_PARAM';

                    return $request->getJsonAnswer($validateData, null);
                }

                try {
                    $bonusSystem->registerClient($client->client_id, $this->_params['promo_code']);
                    $result['activate_bonus_system'] = 1;
                } catch (GetCustomerException $ex) {
                    $validateData['code'] = TaxiErrorCode::INVALID_PROMO_CODE;
                    $validateData['info'] = 'INVALID_PROMO_CODE';

                    return $request->getJsonAnswer($validateData, null);
                }
            } catch (\Exception $ex) {
                $validateData['code'] = TaxiErrorCode::INTERNAL_ERROR;
                $validateData['info'] = 'INTERNAL_ERROR';
                \Yii::error($ex);

                return $request->getJsonAnswer($validateData, $result);
            }

        }

        return $request->getJsonAnswer($validateData, $result);
    }


    /**
     * Название команды: get_order_route.
     *
     * Описание команды:  Получение маршрута заказ.
     *
     * Специальные возвращаемые коды:
     * <code>
     * info                     code
     * EMPTY_DATA_IN_DATABASE   7
     * </code>
     * Тип запроса - GET.
     *
     * @params string order_id  - ИД заказа *.
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON- информация по заказу.
     * @throws \yii\base\InvalidConfigException
     *
     * <code>
     * {
     *  "code":0,
     *  "info":"OK",
     *  "result":
     *   {
     *      "order_id": 15626,
     *      "route_data":[
     *          {
     *              "lat":"56.844354",
     *              "status_id":"17",
     *              "lon":"53.242362",
     *              "speed":"0.00"
     *          },
     *          {
     *              "lat":"56.844354",
     *              "status_id":"17",
     *              "lon":"53.242362",
     *              "speed":"0.00"
     *          }
     *      ]
     *  }
     * }
     * </code>
     *
     */
    public function actionGet_order_route()
    {
        $this->_params = $this->getParams($_GET);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $result        = [];
        $request       = new TaxiApiRequest();

        if ($validateData['code'] == TaxiErrorCode::OK) {
            /* @var $service OrderTrackService */
            $service = \Yii::createObject(OrderTrackService::class);
            $route   = $service->getTrack($this->_params['order_id']);

            $result['order_id']   = $this->_params['order_id'];
            $result['route_data'] = $route;
        }

        return $request->getJsonAnswer($validateData, $result);
    }


    /**
     * API method to reject order
     * @return string JSON
     */
    public function actionReject_order()
    {
        $this->_params = $this->getParams($_POST);
        $validateData  = $this->checkParams($this->_params, \Yii::$app->params['checkSignature']);
        $result        = [];
        $request       = new TaxiApiRequest();

        if ($validateData['code'] == TaxiErrorCode::OK) {
            $tenantId = $this->getTanantId();
            $orderId  = $this->_params['order_id'];
            $clientId = null;
            $lang     = $this->getLang();

            $requestId = \Yii::$app->request->headers->get('Request-Id');

            /** @var $orderUpdateEvent OrderUpdateEvent */
            $orderUpdateEvent = new OrderUpdateEvent([
                'requestId' => $requestId,
                'tenantId'  => $tenantId,
                'orderId'   => $orderId,
                'senderId'  => $clientId,
                'lang'      => $lang,
                'sender'    => OrderUpdateEvent::SENDER_CLIENT,
            ]);

            try {
                $orderUpdateEvent->addEvent(['status_id' => OrderStatus::STATUS_REJECTED]);
                $result = ['reject_result' => 1];
            } catch (QueueIsNotExistsException $ex) {
                $result = ['reject_result' => 1];
            }
        }

        return $request->getJsonAnswer($validateData, $result);
    }

    /**
     * Getting result of rejecting order
     * @return string JSON
     */
    public function actionGet_reject_order_result()
    {
        $this->_params = $this->getParams($_GET);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $result        = [];
        $request       = new TaxiApiRequest();

        if ($validateData['code'] == TaxiErrorCode::OK) {
            $requestId = $this->_params['request_id'];

            $orderEvent = new OrderUpdateEvent();

            $response = json_decode($orderEvent->getResultResponse($requestId), true);
            if (empty($response)) {
                $validateData['code'] = TaxiErrorCode::RESULT_NOT_FOUND;
                $validateData['info'] = 'RESULT_NOT_FOUND';
                $result               = ['reject_result' => 0];
            } else {
                $validateData['code'] = isset($response['code']) ? $response['code'] : null;
                $validateData['info'] = isset($response['info']) ? $response['info'] : null;
                $result               = isset($response['result']) ? $response['result'] : null;
            }
        }

        return $request->getJsonAnswer($validateData, $result);
    }


    /**
     * Название команды: send_response.
     *
     * Описание команды: Передача отзыва клиента о поездке.
     *
     *
     * Специальные возвращаемые коды:
     *
     * code   info
     *
     * 201   ORDER_IS_REJECTED
     *
     * 202   ORDER_IS_NOT_COMPLETED_YET
     *
     *
     * Тип запроса - POST.
     *
     * @params string order_id  - ИД заказа *.
     *
     * @params string grade  - оценка 1-5 *.
     *
     * @params string text  - текст отзыва.
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string
     *
     * <code>
     * {
     *
     *  "code":0,
     *
     *  "info":"OK",
     *
     *  "result":
     *
     *  {
     *
     *      "respone_result":1
     *
     *  }
     *
     * }
     * </code>
     *
     * response_result - 1 успешно.
     *
     * response_result - 0 не успешно.
     */
    public function actionSend_response()
    {
        $this->_params = $this->getParams($_POST);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $result        = [];
        $request       = new TaxiApiRequest();

        $key_response = gtoet('1.2.0') ? 'response_result' : 'respone_result';

        if ($validateData['code'] == TaxiErrorCode::OK) {
            $tenant_id = $this->getTanantId();
            $order     = Order::find()
                ->joinWith('status')
                ->where(['order_id' => $this->_params['order_id']])
                ->andWhere(['tenant_id' => $tenant_id])
                ->one();

            // Если не нашли заказ
            if (!$order) {
                $validateData['code']  = TaxiErrorCode::EMPTY_DATA_IN_DATABASE;
                $validateData['info']  = 'EMPTY_DATA_IN_DATABASE';
                $result[$key_response] = 0;

                return $request->getJsonAnswer($validateData, $result);
            }

            // Если заказ еще не завершен
            if (!in_array($order->status->status_group, [OrderStatus::STATUS_GROUP_4, OrderStatus::STATUS_GROUP_5])) {
                $validateData['code']  = TaxiErrorCode::ORDER_IS_NOT_COMPLETED_YET;
                $validateData['info']  = 'ORDER_IS_NOT_COMPLETED_YET';
                $result[$key_response] = 0;

                return $request->getJsonAnswer($validateData, $result);
            }

            Worker::setRating($order->worker_id, $order->position_id, $this->_params['grade']);

            if (!empty($order->client_id)) {
                $review             = new ClientReview();
                $review->attributes = [
                    'client_id' => $order->client_id,
                    'order_id'  => $order->order_id,
                    'rating'    => (int)$this->_params['grade'],
                    'text'      => $this->_params['text'],
                ];
                if (!$review->save() || !$review->setReview($this->_params['grade'])) {
                    $validateData['code']  = TaxiErrorCode::INTERNAL_ERROR;
                    $validateData['info']  = 'INTERNAL_ERROR';
                    $result[$key_response] = 0;

                    return $request->getJsonAnswer($validateData, $result);
                }
                $data = [
                    'order_id' => $order->order_id,
                    'type'     => 'feedback',
                ];
                Yii::$app->gearman->doBackground(\api\components\gearman\Gearman::ORDER_STATISTIC, $data);
            }
            $result[$key_response] = 1;
        }

        return $request->getJsonAnswer($validateData, $result);
    }

    /**
     * Название команды: get_active_orders_list.
     *
     * Описание команды: Получить список активных заказов клиента
     *
     * Специальные возвращаемые коды:
     *
     * EMPTY_DATA_IN_DATABASE 7
     *
     *
     * Тип запроса - GET.
     *
     * @params string client_id  - ИД клиента *.
     *
     * @params string client_phone  - Телефон клиента *.
     *
     * @params string current_time  - текущее время timestamp *.
     *
     * @return string JSON
     *
     * <code>
     *  {
     *      "code":0,
     *
     *      "info":"OK",
     *
     *      "result":[
     *
     *          {
     *
     *              "order_id":"21836",
     *
     *              "order_number":"17838",
     *
     *              "status_group":"new",
     *
     *              "status_name":"Идет поиск авто...",
     *
     *              "point_from":{
     *
     *                  "city":"Глазов",
     *
     *                  "street":"Кирова",
     *
     *                  "house":"12",
     *
     *                  "housing":"",
     *
     *                  "porch":"",
     *
     *                  "apt":"",
     *
     *                  "lat":"58.1406553",
     *
     *                  "lon":"52.666656066587"
     *              }
     *          }
     *      ]
     * }
     * </code>
     *
     */
    public function actionGet_active_orders_list()
    {
        $this->_params = $this->getParams($_GET);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $result        = [];
        $request       = new TaxiApiRequest();

        if ($validateData['code'] == TaxiErrorCode::OK) {
            $tenant_id = $this->getTanantId();
            $lang      = $this->getLang();

            $result = Order::getActiveOrderList($tenant_id, $this->_params['client_phone'], $lang);
        }

        return $request->getJsonAnswer($validateData, $result);
    }


    ///////////////////////////////Рабочие функции//////////////////////////////////////////////////

    /**
     * Проверка запроса на коорректность параметров и сигнатуры
     *
     * @param array $params
     * @param bool  $needCheckSign
     *
     * @return TaxiErrorCode
     */
    protected function checkParams($params, $needCheckSign = true)
    {
        $commandName   = $this->action->id;
        $request       = new TaxiApiRequest();
        $headersObject = Yii::$app->request->getHeaders();
        $headers       = $headersObject->toArray();
        return $request->validateParams($commandName, $headers, $params, $needCheckSign);
    }

    /**
     * Получить параметры запроса
     *
     * @param array $array
     *
     * @return array
     */
    protected function getParams($array)
    {
        $params = $array;
        unset($params['q']);
        $request = new TaxiApiRequest();

        return $request->filterParams($params);
    }

    /*
     * Получить ид тенанта
     * @return int
     */

    protected function getTanantId()
    {
        $headersObject = Yii::$app->request->getHeaders();
        $headers       = $headersObject->toArray();
        $tenantId      = isset($headers['tenantid']['0']) ? $headers['tenantid']['0'] : null;

        return $tenantId;
    }

    /*
     * Получить тип устройства
     * @return string
     */

    protected function getTypeClient()
    {
        $headersObject = Yii::$app->request->getHeaders();
        $headers       = $headersObject->toArray();
        $typeClient    = isset($headers['typeclient']['0']) ? $headers['typeclient']['0'] : null;
        $typeClient    = mb_strtoupper($typeClient);

        return in_array($typeClient, Client::getClentTypeList()) ? $typeClient : '';
    }

    /**
     * Получить язык
     * @return string
     */
    protected function getLang()
    {
        $supportedLangs = Yii::$app->params['supportedLanguages'];
        $headersObject  = Yii::$app->request->getHeaders();
        $headers        = $headersObject->toArray();
        $lang           = isset($headers['lang']['0']) ? $headers['lang']['0'] : null;
        if (!in_array($lang, $supportedLangs, false)) {
            $lang = "en";
        }

        return $lang;
    }

    /**
     * Получить версию клиента
     * @return string
     */
    protected function getVersionClient()
    {
        return Yii::$app->request->getHeaders()->get('versionclient', self::DEFAULT_VERSION_CLIENT);
    }

    /**
     * Получить токен устройства
     * @return string
     */
    protected function getDeviceId()
    {
        $headersObject = Yii::$app->request->getHeaders();
        $headers       = $headersObject->toArray();
        $deviceid      = isset($headers['deviceid']['0']) ? $headers['deviceid']['0'] : null;

        return $deviceid;
    }

    /**
     * Получить ид мобильного приложения
     * @return string|null
     */
    protected function getAppId()
    {
        $headersObject = Yii::$app->request->getHeaders();
        $headers       = $headersObject->toArray();
        $appId         = !empty($headers['appid']['0']) ? $headers['appid']['0'] : null;
        if (empty($appId)) {
            $appId = MobileApp::find()
                ->select('app_id')
                ->where([
                    'tenant_id' => $this->getTanantId(),
                    'active'    => MobileApp::ACTIVE,
                ])
                ->limit(1)
                ->scalar();
        }

        return $appId;
    }

    /**
     * Execute card action
     *
     * @param string $action
     * @param array  $params
     *
     * @return mixed
     */
    protected function doCardAction($action, array $params)
    {
        /* @var $request TaxiApiRequest */
        $request      = $this->container->get('request');
        $validateData = $this->checkParams($params, Yii::$app->params['checkSignature']);

        if ($validateData['code'] !== 0) {
            return $request->getJsonAnswer($validateData);
        }

        /* @var $errCode TaxiErrorCode */
        $errCode = $this->container->get('errCode');

        /** @var $card Card */
        $card    = $this->container->get('card');

        try {
            $tenantId = $this->getTanantId();
            $clientId = (int)$params['client_id'];
            $model = Client::findOne($clientId);
            $cityId = $model->city_id;


            $profile = $this->profileService->getProfile($tenantId, $cityId);
            switch ($action) {
                case 'getCards':
                    $result = $card->getCards($profile, $clientId);
                    break;
                case 'create':
                    $response = $card->create($tenantId, $profile, $clientId);
                    $result   = empty($response['result']) ? null : $response['result'];
                    break;
                case 'check':
                    $response = $card->check($tenantId, $profile, $clientId, $params['order_id']);
                    $result   = empty($response['result']) ? null : $response['result'];
                    break;
                case 'check_review':
                    $response = $card->check_review($tenantId, $profile, $clientId, $params['pan']);
                    $result   = empty($response['result']) ? false : $response['result'];
                    break;
                case 'delete':
                    $response = $card->delete($tenantId, $profile, $clientId, $params['pan']);
                    $result   = true;
                    break;
            }

            $statusCode = empty($response['statusCode']) ? null : (int)$response['statusCode'];

            if ($statusCode === 202 && gtoet('1.9.0')) {
                $validateData['code'] = TaxiErrorCode::REQUEST_PROCESSING;
            } else {
                $validateData['code'] = TaxiErrorCode::OK;
            }
        } catch (RestException $ex) {
            $result               = null;
            $validateData['code'] = TaxiErrorCode::PAYMENT_GATE_ERROR;
            Yii::error($ex->getCode() . "\n" . $ex->getMessage(), 'bank-card');
        } catch (\Exception $ex) {
            $result               = null;
            $validateData['code'] = TaxiErrorCode::INTERNAL_ERROR;
            Yii::error($ex->getCode() . "\n" . $ex->getMessage(), 'bank-card');
        }

        $validateData['info'] = $errCode->errorCodeData[$validateData['code']];

        return $request->getJsonAnswer($validateData, $result);
    }


    public function doAction($function)
    {

        $validateData = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);
        $request      = new TaxiApiRequest();
        $result       = null;

        try {
            if ($validateData['code'] == TaxiErrorCode::OK) {
                $result = call_user_func([$this, $function]);
            }

        } catch (CodeNotFoundException $e) {
            $validateData['code'] = $e->getCode();
            $validateData['info'] = $e->getMessage();
        } catch (EmptyInDbException $e) {
            $validateData['code'] = $e->getCode();
            $validateData['info'] = $e->getMessage();
        } catch (BadParamException $e) {
            $validateData['code'] = $e->getCode();
            $validateData['info'] = $e->getMessage();
        } catch (NeedReauthException $e) {
            $validateData['code'] = $e->getCode();
            $validateData['info'] = $e->getMessage();
        } catch (\Exception $e) {
            $validateData['code'] = TaxiErrorCode::INTERNAL_ERROR;
            $validateData['info'] = 'INTERNAL_ERROR';

            \Yii::error($e, 'internal-error');
        }

        return $request->getJsonAnswer($validateData, $result);
    }


    public function actionGet_active_order()
    {
        $this->_params = $this->getParams($_GET);
        $validateData  = $this->checkParams($this->_params, Yii::$app->params['checkSignature']);

        $request       = new TaxiApiRequest();
        $results = [];

        if ($validateData['code'] == TaxiErrorCode::OK){
            $tenant_id = $this->getTanantId();
            $phone      = $this->_params['phone'];
            $lang      = $this->getLang();


            $redis_list = Yii::$app->redis_client_active_orders->executeCommand('HGET', [$tenant_id, $phone]);
            if (!empty($redis_list)){
                $redis_list = unserialize($redis_list);

                if (is_array($redis_list)){

                    $order_ids = [];
                    foreach($redis_list as $result){
                        $order_ids[] = $result;
                    }

                    if(count($order_ids) > 0){
                        $orderRepository = new OrderRepository();
                        $clientRepository = new ClientRepository();
                        $order_repositoty = new OrderService($orderRepository, $clientRepository);

                        $ordersInfo = $order_repositoty->getOrdersInfo($tenant_id, $order_ids, 1, $lang, false, false);
                        $results = ArrayHelper::getValue($ordersInfo, 0, []);
                    }
                }
            }
        }

        return $request->getJsonAnswer($validateData, $results);

    }
}