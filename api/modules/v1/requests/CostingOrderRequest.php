<?php

namespace api\modules\v1\requests;

use api\models\client\ClientPhone;
use api\models\order\Order;
use api\models\taxi_tariff\TaxiTariff;
use api\models\tenant\mobileApp\MobileAppHasCity;
use api\models\tenant\TenantCityHasPosition;
use api\models\tenant\TenantHasCity;
use api\models\tenant\TenantSetting;
use api\models\transport\car\CarOption;
use api\models\worker\Position;
use api\modules\v1\components\ErrorCode;

/**
 * Class CostingOrderRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests\crm
 *
 * @property string  $additionalOptions
 * @property string  $address *
 * @property integer $cityId *
 * @property string  $orderTime
 * @property integer $tariffId *
 * @property integer $positionId
 * @property boolean $debug
 *
 */
class CostingOrderRequest extends BaseApiRequest
{
    const DEFAULT_GEOCODER_TYPE = 'ru';

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'additionalOptions' => [
                'name'    => 'additional_options',
                'type'    => self::TYPE_POST,
                'default' => '',
            ],
            'address'           => [
                'name' => 'address',
                'type' => self::TYPE_POST,
            ],
            'cityId'            => [
                'name' => 'city_id',
                'type' => self::TYPE_POST,
            ],
            'orderTime'         => [
                'name' => 'order_time',
                'type' => self::TYPE_POST,
            ],
            'tariffId'          => [
                'name' => 'tariff_id',
                'type' => self::TYPE_POST,
            ],
            'positionId'        => [
                'name'    => 'position_id',
                'type'    => self::TYPE_POST,
                'default' => Position::TAXI_DRIVER_ID,
            ],
            'phone'             => [
                'name'  => 'phone',
                'type' => self::TYPE_POST,
            ],
            'debug'             => [
                'name'  => 'debug',
                'type' => self::TYPE_POST,
                'default' => 0,
            ],

        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [

            ['additionalOptions', 'filter', 'filter' => [$this, 'filterAdditionalOptions']],
            [
                'additionalOptions',
                'each',
                'rule' => ['exist', 'targetClass' => CarOption::className(), 'targetAttribute' => 'option_id'],
            ],

            ['address', 'required', 'message' => ErrorCode::MISSING_INPUT_PARAMETER],
            ['address', 'filter', 'filter' => [$this, 'filterAddress']],

            ['cityId', 'required', 'message' => ErrorCode::MISSING_INPUT_PARAMETER],
            [
                'cityId',
                'exist',
                'targetClass'     => TenantHasCity::className(),
                'targetAttribute' => ['cityId' => 'city_id', 'tenantId' => 'tenant_id'],
                'filter'          => ['block' => TenantHasCity::NOT_BLOCK],
            ],
            [
                'cityId',
                'exist',
                'targetClass'     => MobileAppHasCity::className(),
                'targetAttribute' => ['cityId' => 'city_id', 'appId' => 'app_id'],
            ],

            ['orderTime', 'safe'],
            ['orderTime', 'filter', 'filter' => [$this, 'filterOrderTime']],

            ['tariffId', 'required', 'message' => ErrorCode::MISSING_INPUT_PARAMETER],
            [
                'tariffId',
                'each',
                'rule' => [
                    'exist',
                    'targetClass'     => TaxiTariff::className(),
                    'targetAttribute' => ['tariffId' => 'tariff_id', 'tenantId' => 'tenant_id'],
                    'filter'          => ['block' => TaxiTariff::NOT_BLOCK],
                ],
            ],

            ['positionId', 'required', 'message' => ErrorCode::MISSING_INPUT_PARAMETER],
            [
                'positionId',
                'exist',
                'targetClass'     => TenantCityHasPosition::className(),
                'targetAttribute' => [
                    'tenantId'   => 'tenant_id',
                    'cityId'     => 'city_id',
                    'positionId' => 'position_id',
                ],
            ],

            ['phone', 'exist', 'targetClass' => ClientPhone::className(), 'targetAttribute' => ['phone' => 'value']],

            ['debug', 'boolean'],
        ]);
    }

    public function filterAdditionalOptions($value)
    {
        if (empty($value) || !is_array($value)) {
            return [];
        }

        $value = array_filter($value, function ($item) {
            $itemToInt = (int)$item;
            if ($itemToInt < 1) {
                return false;
            }

            return $itemToInt == $item;
        });

        $value = array_unique($value, SORT_NUMERIC);

        return $value;
    }

    public function filterAddress($value)
    {

        $geoCoderType = TenantSetting::getSettingValue($this->tenantId, TenantSetting::SETTING_GEOCODER_TYPE,
            $this->cityId, $this->positionId);
        $geoCoderType = !empty($geoCoderType) ? $geoCoderType : self::DEFAULT_GEOCODER_TYPE;
        $address      = Order::filterAddress($value, $this->lang, $geoCoderType, $this->tenantId, $this->cityId);

        return $address['address'];
    }

    public function filterOrderTime($value)
    {
        if ($value === null) {
            return null;
        }

        return (int)app()->formatter->asTimestamp($value);
    }

    public function getOrderTimeAsDate()
    {
        return date("d-m-Y H:i:s", $this->orderTime);
    }

}