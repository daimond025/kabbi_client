<?php

namespace api\models\referralSystem\formatter;

abstract class BaseFormatter
{
    abstract public function formatter($value);
}