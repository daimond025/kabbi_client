<?php

namespace api\models\transport\car;

use api\models\taxi_tariff\AdditionalOption;
use Yii;
use api\models\transport\TransportType;


class CarOption extends \yii\db\ActiveRecord
{

    const SOURCE_CATEGORY = 'car-options';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id' => 'ID',
            'name' => 'Name',
            'type_id' => 'Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransportType::className(), ['type_id' => 'type_id']);
    }

    public function getAdditionalOptions()
    {
        return $this->hasMany(AdditionalOption::className(), ['additional_option_id' => 'option_id']);
    }

}
