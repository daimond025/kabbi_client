<?php

namespace api\modules\v1\exceptions;


use api\components\taxiApiRequest\TaxiErrorCode;

class CodeNotFoundException extends \Exception
{

    protected $code = TaxiErrorCode::CODE_NOT_FOUND;
    protected $message = 'CODE_NOT_FOUND';

}