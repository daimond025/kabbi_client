<?php

namespace api\modules\v1\actions\crm;

use api\components\serviceEngine\ServiceEngine;
use api\modules\v1\actions\BaseApiAction;
use api\modules\v1\components\WorkerService;
use api\modules\v1\helpers\WorkersHelper;
use api\modules\v1\requests\crm\GetRelevantWorkersRequest;

/**
 * Получение списка доступных исполнителей
 *
 * Class GetRelevantWorkersAction
 *
 * @package api\modules\v1\actions\crm
 * @property GetRelevantWorkersRequest $request
 * @property int                       $_client
 */
class GetRelevantWorkersAction extends BaseApiAction
{
    protected $_clientId;

    protected function getRequest()
    {
        return new GetRelevantWorkersRequest();
    }


    protected function execute()
    {
        $serviceEngine = new ServiceEngine();
        $callsigns     = $serviceEngine->getRelevantWorkers($this->request->tenantId, $this->request->orderId);

        /** @var WorkerService $workerService */
        $workerService = app()->get('workerService');
        $workers       = $workerService->findAllInRedis($this->request->tenantId, $callsigns);

        $result = [];
        foreach ($workers as $worker) {
            $result[] = WorkersHelper::formatted($worker);
        }

        $this->response->setContent($result);
    }

}