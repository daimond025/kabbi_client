<?php

namespace api\models\order;


class OrderStatus extends \yii\db\ActiveRecord
{

    const FILTER_STATUS_1 = 'executing';
    const FILTER_STATUS_2 = 'completed';
    const FILTER_STATUS_3 = 'rejected';
    const STATUS_GROUP_0 = 'new';
    const STATUS_GROUP_1 = 'car_assigned';
    const STATUS_GROUP_2 = 'car_at_place';
    const STATUS_GROUP_3 = 'executing';
    const STATUS_GROUP_4 = 'completed';
    const STATUS_GROUP_5 = 'rejected';
    const STATUS_GROUP_6 = 'pre_order';
    const STATUS_GROUP_7 = 'warning';
    const STATUS_GROUP_8 = 'works';
    const STATUS_NEW = 1;
    const STATUS_OFFER_ORDER = 2;
    const STATUS_FREE = 4;
    const STATUS_NOPARKING = 5;
    const STATUS_PRE = 6;
    const STATUS_PRE_NOPARKING = 16;
    const STATUS_GET_DRIVER = 17;
    const STATUS_DRIVER_WAITING = 26;
    const STATUS_REJECTED = 39;
    const STATUS_COMPLETED_PAID = 37;
    const STATUS_COMPLETED_NOT_PAID = 38;
    const STATUS_EXECUTION_PRE = 55;
    const STATUS_MANUAL_MODE = 108;
    const WAITING_FOR_PAYMENT = 110;
    const WORKER_ASSIGNED_SOFT = 113;
    const WORKER_ASSIGNED_HARD = 114;

    const SOURCE_CATEGORY = 'status_event';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    public function getName($lang = 'en')
    {
        return t(self::SOURCE_CATEGORY, $this->name, [], $lang);
    }

    // Группы, в которых можно отменить заказ.
    public static function getListInRejected()
    {
        return [
            self::STATUS_GROUP_0,
            self::STATUS_GROUP_1,
            self::STATUS_GROUP_6,
        ];
    }

    /**
     * @param string $label
     * @param int    $positionId
     * @param string $language
     *
     * @return string
     */
    private static function translateStatusLabel($label, $positionId, $language)
    {
        return t(
            'status_label',
            $label, // implode(':', array_filter([$label, $positionId])),
            null,
            $language);
    }

    public static function getFilterStatusLabel($status_id, $statusGroup, $positionId, $isTaxiOrder, $lang)
    {
        $statusLabel = null;
        switch ($statusGroup) {
            case "new":
                if ($isTaxiOrder) {
                    $statusLabel = self::translateStatusLabel('Search...', $positionId, $lang);
                } else {
                    $statusLabel = self::translateStatusLabel('Search courier...', null, $lang);
                }

                break;
            case "pre_order":
                if ($status_id == 6 || $status_id == 16) {
                    $statusLabel = self::translateStatusLabel('Order is accepted', null, $lang);
                }
                if ($status_id == 7 || $status_id == 111 || $status_id == 112  || $status_id == 116  || $status_id == 119) {
                    if ($isTaxiOrder) {
                        $statusLabel = self::translateStatusLabel('Worker has accepted an order', $positionId, $lang);
                    } else {
                        $statusLabel = self::translateStatusLabel('Courier has accepted an order', null, $lang);
                    }
                }
                if($status_id == 116){
                    $statusLabel = self::translateStatusLabel('Waiting for preorder confirmation', $positionId, $lang);
                }
                if($status_id == 119){
                    $statusLabel = self::translateStatusLabel('Worker accepted preorder confirmation', $positionId, $lang);
                }

                if ($status_id == 10) {
                    if ($isTaxiOrder) {
                        $statusLabel = self::translateStatusLabel('Workers order refusing', $positionId, $lang);
                    } else {
                        $statusLabel = self::translateStatusLabel('Courier order refusing', null, $lang);
                    }
                }
                break;
            case "car_assigned":
                if ($isTaxiOrder) {
                    $statusLabel = self::translateStatusLabel('The worker left', $positionId, $lang);
                } else {
                    $statusLabel = self::translateStatusLabel('The courier left', null, $lang);
                }
                break;
            case "car_at_place":
                if ($isTaxiOrder) {
                    $statusLabel = self::translateStatusLabel('The worker arrived', $positionId, $lang);
                } else {
                    $statusLabel = self::translateStatusLabel('The courier arrived', null, $lang);
                }
                break;
            case "executing":
                $statusLabel = self::translateStatusLabel('Execution of an order', null, $lang);
                break;
            case "completed":
                $statusLabel = self::translateStatusLabel('Completed', null, $lang);
                break;
            case "rejected":
                $statusLabel = self::translateStatusLabel('Rejected', null, $lang);
                break;
        }

        return $statusLabel;
    }
}
