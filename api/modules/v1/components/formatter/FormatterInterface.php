<?php

namespace api\modules\v1\components\formatter;

interface FormatterInterface
{
    public static function formatter($data, $options);
}