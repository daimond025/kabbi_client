<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class MissingInputParamsException extends BaseApiException
{
    public function __construct($message = '')
    {
        $code = ErrorCode::MISSING_INPUT_PARAMETER;
        parent::__construct($message, $code, null);
    }

}