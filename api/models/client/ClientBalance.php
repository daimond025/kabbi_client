<?php

namespace app\models\client;

use Yii;

/**
 * This is the model class for table "tbl_client_balance".
 *
 * @property integer $balance_id
 * @property integer $client_id
 * @property string $value
 * @property string $currency
 *
 * @property Client $client
 * @property ClientTransaction[] $clientTransactions
 */
class ClientBalance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_client_balance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'required'],
            [['client_id'], 'integer'],
            [['value'], 'number'],
            [['currency'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'balance_id' => 'Balance ID',
            'client_id' => 'Client ID',
            'value' => 'Value',
            'currency' => 'Currency',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientTransactions()
    {
        return $this->hasMany(ClientTransaction::className(), ['balance_id' => 'balance_id']);
    }
}
