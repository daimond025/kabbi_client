<?php

return [
    'loggerTarget' => [
        //        'class'    => \api\modules\v1\components\logger\target\SyslogTarget::className(),
        'class'    => \api\modules\v1\components\logger\target\FakeTarget::className(),
        'identity' => getenv('SYSLOG_IDENTITY') . '-' . app()->params['version'],
    ],

    'logger' => [
        'class'  => \api\modules\v1\components\logger\Logger::className(),
        'target' => 'loggerTarget',
    ],

    'tenantService' => [
        'class' => \api\modules\v1\components\TenantService::className(),
    ],

    'taxiTariffService' => [
        'class' => \api\modules\v1\components\TaxiTariffService::className(),
    ],

    'workerService' => [
        'class' => \api\modules\v1\components\WorkerService::className(),
    ],

    'orderService' => [
        'class' => \api\modules\v1\components\OrderService::className(),
    ],

    'clientService' => [
        'class' => \api\modules\v1\components\ClientService::className(),
    ],

    'cityService' => [
        'class' => \api\modules\v1\components\CityService::className(),
    ],
];


