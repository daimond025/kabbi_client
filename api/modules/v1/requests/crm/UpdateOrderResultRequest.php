<?php

namespace api\modules\v1\requests\crm;

use api\modules\v1\requests\BaseVersionedApiRequest;

/**
 * Class UpdateOrderResultRequest
 *
 *  Обязательные поля помечены звездочкой (*)
 *
 * @package api\modules\v1\requests\crm
 *
 * @property string $phone - Телефон клиента*
 * @property string $updateId - Ид запроса на обновление заказа*
 */
class UpdateOrderResultRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'updateId' => [
                'name' => 'update_id',
                'type' => self::TYPE_GET,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['updateId', 'safe'],
        ]);
    }
}