<?php

namespace api\modules\v1\helpers;

use yii\helpers\ArrayHelper;

/**
 * Class RouteAnalyzerHelper
 * @package api\modules\v1\helpers
 */
class RouteAnalyzerHelper
{
    const TYPE_POINT_CITY = 'in';
    const TYPE_POINT_TRACK = 'out';

    public static function formatted($data)
    {
        $result = [
            'additional_cost' => ArrayHelper::getValue($data, 'additionalCost'),

            'min_price' => ArrayHelper::getValue($data, 'minPrice'),

            'summary_time'     => ArrayHelper::getValue($data, 'summaryTime'),
            'summary_distance' => ArrayHelper::getValue($data, 'summaryDistance'),
            'summary_cost'     => ArrayHelper::getValue($data, 'summaryCost'),

            'city_time'     => ArrayHelper::getValue($data, 'cityTime'),
            'city_distance' => ArrayHelper::getValue($data, 'cityDistance'),
            'city_cost'     => ArrayHelper::getValue($data, 'cityCost'),

            'out_city_time'     => ArrayHelper::getValue($data, 'outCityTime'),
            'out_city_distance' => ArrayHelper::getValue($data, 'outCityDistance'),
            'out_city_cost'     => ArrayHelper::getValue($data, 'outCityCost'),

            'is_fix'     => (int)ArrayHelper::getValue($data, 'isFix'),
            'is_airport' => (int)ArrayHelper::getValue($data, 'isAirport'),
            'is_station' => (int)ArrayHelper::getValue($data, 'isStation'),

            'tariffInfo' => ArrayHelper::getValue($data, 'tariffInfo', []),
        ];

        $result['start_point_location']  = ArrayHelper::getValue($data,
            'startPointLocation') === self::TYPE_POINT_CITY ? self::TYPE_POINT_CITY : self::TYPE_POINT_TRACK;
        $result['finish_point_location'] = ArrayHelper::getValue($data,
            'finishPointLocation') === self::TYPE_POINT_CITY ? self::TYPE_POINT_CITY : self::TYPE_POINT_TRACK;

        return $result;
    }

    public static function formattedManyTariff($data, $debug)
    {
        $resultTariffs = [];
        $tariffs       = ArrayHelper::getValue($data, 'tariffs', []);
        foreach ($tariffs as $tariff) {
            $currentResultTariff = [
                'tariff_id'                => ArrayHelper::getValue($tariff, 'tariffId'),
                'summary_cost'             => ArrayHelper::getValue($tariff, 'summaryCost'),
                'summary_cost_no_discount' => ArrayHelper::getValue($tariff, 'summaryCostNoDiscount'),
                'is_fix'                   => ArrayHelper::getValue($tariff, 'isFix'),
            ];

            if ($debug) {
                $currentResultTariff['min_price']       = ArrayHelper::getValue($tariff, 'minPrice');
                $currentResultTariff['is_airport']      = ArrayHelper::getValue($tariff, 'isAirport');
                $currentResultTariff['is_station']      = ArrayHelper::getValue($tariff, 'isStation');
                $currentResultTariff['city_cost']       = ArrayHelper::getValue($tariff, 'cityCost');
                $currentResultTariff['out_city_cost']   = ArrayHelper::getValue($tariff, 'outCityCost');
                $currentResultTariff['additional_cost'] = ArrayHelper::getValue($tariff, 'additionalCost');
                $currentResultTariff['discount']        = ArrayHelper::getValue($tariff, 'discount');
            }

            $resultTariffs[] = $currentResultTariff;
        }

        $result = [
            'tariffs' => $resultTariffs,
            'general' => [
                'summary_time'     => ArrayHelper::getValue($data, 'summaryTime'),
                'summary_distance' => ArrayHelper::getValue($data, 'summaryDistance'),
                'routeLine'        => ArrayHelper::getValue($data, 'routeLine'),
            ],
        ];

        if ($debug) {
            $result['general']['city_time']             = ArrayHelper::getValue($data, 'cityTime');
            $result['general']['city_distance']         = ArrayHelper::getValue($data, 'cityDistance');
            $result['general']['out_city_time']         = ArrayHelper::getValue($data, 'outCityTime');
            $result['general']['out_city_distance']     = ArrayHelper::getValue($data, 'outCityDistance');
            $result['general']['summary_time']          = ArrayHelper::getValue($data, 'summaryTime');
            $result['general']['summary_distance']      = ArrayHelper::getValue($data, 'summaryDistance');
            $result['general']['start_point_location']  = ArrayHelper::getValue($data, 'startPointLocation');
            $result['general']['finish_point_location'] = ArrayHelper::getValue($data, 'finishPointLocation');
        }

        return $result;
    }
}