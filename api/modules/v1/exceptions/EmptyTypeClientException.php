<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class EmptyTypeClientException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::EMPTY_TYPE_CLIENT;
        parent::__construct('', $code, null);
    }

}