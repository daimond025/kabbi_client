<?php

namespace api\models\parking;

use Yii;
use api\models\tenant\Tenant;
use api\models\city\City;

class Parking extends \yii\db\ActiveRecord
{

    const TYPE_BASE_POLYGON = 'basePolygon';
    const TYPE_CITY = 'city';
    const TYPE_OUTCITY = 'outCity';
    const TYPE_AIRPORT = 'airport';
    const TYPE_STATION = 'station';
    const TYPE_RESEPTION_AREA = 'reseptionArea';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%parking}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

}
