<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class EmptyTenantIdException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::EMPTY_TENANT_ID;
        parent::__construct('', $code, null);
    }

}