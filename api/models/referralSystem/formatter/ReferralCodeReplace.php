<?php

namespace api\models\referralSystem\formatter;


class ReferralCodeReplace extends BaseReplace
{
    protected $code = '';

    public function __construct(BaseFormatter $object, $code = '')
    {
        $this->object =& $object;
        $this->code = $code;
    }

    public function formatter($value)
    {
        if (strpos($value, "#CODE#") !== false) {
            return str_replace("#CODE#", $this->code, $this->object->formatter($value));
        }

        return $this->object->formatter($value);
    }
}