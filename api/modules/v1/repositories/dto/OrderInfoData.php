<?php

namespace api\modules\v1\repositories\dto;

class OrderInfoData
{
    public $id;
    public $number;
    public $tenantId;
    public $cityId;
    public $status;
    public $payment;
    public $client;
    public $prePrice;
    public $time;
    public $timeOffset;
    public $timeToClient;
    public $worker;
    public $detailCostInfo;
    public $address;
    public $client_tariff;

    public function __construct(
        $id,
        $number,
        $tenantId,
        $cityId,
        OrderStatusData $status,
        OrderPaymentData $payment,
        OrderClientData $client,
        $prePrice,
        $time,
        $timeOffset,
        $timeToClient,
        $worker,
        $detailCostInfo,
        $address,
        $tariff
    ) {
        $this->id             = (integer)$id;
        $this->number         = (integer)$number;
        $this->tenantId       = (integer)$tenantId;
        $this->cityId         = (integer)$cityId;
        $this->status         = $status;
        $this->payment        = $payment;
        $this->client         = $client;
        $this->prePrice       = $prePrice;
        $this->time           = (integer)$time;
        $this->timeOffset     = (integer)$timeOffset;
        $this->timeToClient   = (integer)$timeToClient;
        $this->worker         = $worker;
        $this->detailCostInfo = (array)$detailCostInfo;
        $this->address        = (array)$address;
        $this->client_tariff        = (array)$tariff;
    }
}
