<?php

$db = require __DIR__ . '/db.php';

$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/logVarsFilter.php'
);


$config = [
    'id'                  => 'api_client',
    'basePath'            => dirname(__DIR__),
    'vendorPath'          => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'api\controllers',
    'aliases'             => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components'          => [
        // Yii2 components
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl'  => '@web/assets',
        ],

        'cache' => [
            'class'     => 'yii\redis\Cache',
            'redis'     => $db['redisCache'],
            'keyPrefix' => getenv('REDIS_CACHE_KEY_PREFIX'),
        ],

        'errorHandler' => [
            'errorAction' => 'v1/api/error',
        ],

        'i18n' => [
            'translations' => [
                '*'    => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                ],
                'app*' => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                ],
            ],
        ],

        'log' => [
            'traceLevel' => 3,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'except'  => ['yii\debug\Module::checkAccess'],
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                ],
                [
                    'class'   => 'yii\log\EmailTarget',
                    'mailer'  => 'mailer',
                    'except'  => ['yii\debug\Module::checkAccess'],
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'message' => [
                        'from'    => [getenv('MAIL_SUPPORT_USERNAME')],
                        'to'      => [getenv('MAIL_SUPPORT_USERNAME')],
                        'subject' => getenv('MAIL_SUPPORT_SUBJECT'),
                    ],
                ],
                [
                    'class'   => 'notamedia\sentry\SentryTarget',
                    'dsn'     => getenv('SENTRY_DSN'),
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'except'  => [
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                    ],
                    'clientOptions' => [
                        'environment' => getenv('ENVIRONMENT_NAME'),
                    ],
                ],
            ],
        ],

        'mailer' => [
            'class'            => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport'        => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => getenv('MAIL_SUPPORT_HOST'),
                'port'       => getenv('MAIL_SUPPORT_PORT'),
                'username'   => getenv('MAIL_SUPPORT_USERNAME'),
                'password'   => getenv('MAIL_SUPPORT_PASSWORD'),
                'encryption' => getenv('MAIL_SUPPORT_ENCRYPTION'),
            ],
        ],

        'request' => [
            'baseUrl'              => '',
            'cookieValidationKey'  => getenv('COOKIE_VALIDATION_KEY'),
            'enableCsrfValidation' => false,
        ],

        'response' => [
            'class'        => '\yii\web\Response',
            'on afterSend' => function () {
                /** @var $apiLogger \logger\ApiLogger */
                $apiLogger = \Yii::$app->apiLogger;
                $apiLogger->setRequest(\Yii::$app->request);
                $apiLogger->setResponse(\Yii::$app->response);
                $apiLogger->export();
            },
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => require __DIR__ . '/routes.php',
        ],

        'user'                       => [
            'identityClass'   => '',
            'enableAutoLogin' => true,
        ],

        // Databases
        'db'                         => $db['dbMain'],
        'mongodb'                    => $db['mongodbMain'],
        'redis_pub_sub'              => $db['redisMainPubSub'],
        'redis_workers'              => $db['redisMainWorkers'],
        'redis_orders_active'        => $db['redisMainOrdersActive'],
        'redis_client_active_orders' => $db['redisMainClientActiveOrders'],
        'redis_order_event'          => $db['redisMainOrderEvent'],
        'redis_main_check_status'    => $db['redisMainCheckStatus'],
        'redis_cache_check_status'   => $db['redisCacheCheckStatus'],

        // Other components and services
        'amqp'                       => [
            'class'    => 'app\components\rabbitmq\Amqp',
            'host'     => getenv('RABBITMQ_MAIN_HOST'),
            'port'     => getenv('RABBITMQ_MAIN_PORT'),
            'user'     => getenv('RABBITMQ_MAIN_USER'),
            'password' => getenv('RABBITMQ_MAIN_PASSWORD'),
        ],

        'apiLogger' => logger\ApiLogger::class,

        'compare' => [
            'class'          => 'api\components\versionCompare\VersionCompareComponent',
            'defaultVersion' => '1.0.0',
        ],

        'gearman' => [
            'class' => 'api\components\gearman\Gearman',
            'host'  => getenv('GEARMAN_MAIN_HOST'),
            'port'  => getenv('GEARMAN_MAIN_PORT'),
        ],

        'geocoder' => [
            'class' => 'api\components\taxiGeocoder\TaxiGeocoder',
            'url'   => getenv('API_SERVICE_URL'),
        ],

        'restCurl' => 'api\components\curl\RestCurl',

        'routeAnalyzer' => [
            'class' => 'api\components\routeAnalyzer\TaxiRouteAnalyzer',
            'url'   => getenv('API_SERVICE_URL'),
        ],

        'consulService' => [
            'class'           => 'api\components\consul\ConsulService',
            'consulAgentHost' => getenv('CONSUL_MAIN_HOST'),
            'consulAgentPort' => getenv('CONSUL_MAIN_PORT'),
        ],

        'orderApi' => [
            'class'   => '\api\components\orderApi\OrderApi',
            'baseUrl' => getenv('API_ORDER_URL'),
        ],

        'apiService' => [
            'class'   => 'api\components\apiService\ApiService',
            'baseUrl' => getenv('API_SERVICE_URL'),
            'timeout' => 5,
        ],
    ],

    'modules' => [
        'v1' => 'api\modules\v1\Module',
    ],

    'params' => require __DIR__ . '/params.php',
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['192.168.1.*'],
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'allowedIPs' => ['192.168.1.*'],
    ];
}

return $config;