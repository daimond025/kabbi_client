<?php

namespace api\modules\v1\components;

use api\models\city\City;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class CityService extends Object
{
    protected $timeOffset = [];

    /**
     * Возвращает разницу в секундах местного времени от GMT
     * @param integer $cityId
     *
     * @return mixed
     */
    public function getTimeOffset($cityId)
    {
        if(!ArrayHelper::keyExists($cityId, $this->timeOffset)) {
            $this->timeOffset[$cityId] = City::getTimeOffset($cityId);
        }

        return $this->timeOffset[$cityId];
    }

}