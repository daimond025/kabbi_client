<?php

namespace api\modules\v1\requests\crm;

use api\models\order\Order;
use api\models\order\OrderStatus;
use api\models\worker\Worker;
use api\modules\v1\components\OrderService;
use api\modules\v1\requests\BaseVersionedApiRequest;

/**
 * Class UpdateOrderRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests\crm
 *
 * @property int $orderId - Ид заказа*
 * @property int $statusId - Идентификатор статуса*
 * @property int $workerCallsign - Позывной исполнителя
 */
class UpdateOrderRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'orderId'        => [
                'name' => 'order_id',
                'type' => self::TYPE_POST,
            ],
            'statusId'       => [
                'name' => 'status_id',
                'type' => self::TYPE_POST,
            ],
            'workerCallsign' => [
                'name' => 'worker_callsign',
                'type' => self::TYPE_POST,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['orderId', 'required'],
            [
                'orderId',
                'exist',
                'targetClass'     => Order::className(),
                'targetAttribute' => ['orderId' => 'order_id', 'tenantId' => 'tenant_id'],
            ],

            ['statusId', 'required'],
            ['statusId', 'filter', 'filter' => [$this, 'filterToInteger']],
            ['statusId', 'in', 'range' => self::getStatusList()],

            ['workerCallsign', 'required', 'when' => [$this, 'whenRequiredWorker']],
            [
                'workerCallsign',
                'exist',
                'targetClass'     => Worker::className(),
                'targetAttribute' => ['tenantId' => 'tenant_id', 'workerCallsign' => 'callsign'],
                'filter'          => ['block' => 0],
                'when'            => [$this, 'whenRequiredWorker'],
            ],
        ]);
    }

    public static function getStatusList()
    {
        return [
            OrderStatus::STATUS_NEW,
            OrderStatus::STATUS_MANUAL_MODE,
            OrderStatus::WORKER_ASSIGNED_SOFT,
            OrderStatus::WORKER_ASSIGNED_HARD,
        ];
    }

    /**
     * @param self $model
     *
     * @return bool
     */
    public function whenRequiredWorker($model)
    {
        /** @var OrderService $orderService */
        $orderService = app()->get('orderService');

        return $orderService->isRequiredWorker($model->statusId);
    }

    public function filterToInteger($value)
    {
        return (int)$value;
    }
}