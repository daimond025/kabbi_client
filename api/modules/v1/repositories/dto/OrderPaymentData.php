<?php

namespace api\modules\v1\repositories\dto;

class OrderPaymentData
{
    public $type;

    public function __construct($type)
    {
        $this->type = $type;
    }
}