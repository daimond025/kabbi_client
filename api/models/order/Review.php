<?php

namespace api\models\order;

use Yii;


class Review extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_review}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id','value'], 'integer']
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }
}
