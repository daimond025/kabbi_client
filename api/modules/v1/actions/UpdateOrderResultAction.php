<?php

namespace api\modules\v1\actions;

use api\exceptions\NeedReauthException;
use api\models\client\Client;
use api\models\order\Order;
use api\modules\v1\exceptions\BadParamsException;
use api\modules\v1\exceptions\ResultNotFoundException;
use api\modules\v1\requests\UpdateOrderResultRequest;

/**
 * Проверка статуса редактирования заказа
 *
 * Class UpdateOrderResultAction
 *
 * @package api\modules\v1\actions
 * @property UpdateOrderResultRequest $request
 */
class UpdateOrderResultAction extends BaseApiAction
{
    protected $_clientId;

    protected function getRequest()
    {
        return new UpdateOrderResultRequest();
    }

    protected function beforeExecute()
    {
        if (parent::beforeExecute()) {

            // Получить информацию о клиенте
            if ($this->request->phone) {
                $client = Client::findByPhone($this->request->tenantId, $this->request->phone);
                if (!$client || $client->getIsBlackList()) {
                    throw new NeedReauthException();
                }
                $this->_clientId = $client->client_id;
            }

            return true;
        }

        return false;
    }

    protected function execute()
    {
        $response = app()->orderService->updateOrderStatus($this->request->updateId);
        $result = [];
        if (empty($response)) {
            throw new ResultNotFoundException();
        } else {

            if($response['code'] !== 0) {
               return $this->response->setContent(0);
            }

            $result['code']    = isset($response['code']) ? $response['code'] : null;
            $result['info']    = isset($response['info']) ? $response['info'] : null;
            $result['updated'] = isset($response['result']) ? $response['result'] : null;
        }

        $orderId = $response['order_id'];

        // Проверка привязки клиента к заказу
        $existOrder = Order::find()
            ->where([
                'order_id'  => $orderId,
                'tenant_id' => $this->request->tenantId,
                'client_id' => $this->_clientId,
            ])
            ->exists();

        if (!$existOrder) {
            throw new BadParamsException();
        }

        return $this->response->setContent(1);
    }
}