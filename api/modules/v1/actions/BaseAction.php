<?php

namespace api\modules\v1\actions;

use api\modules\v1\components\ErrorCode;
use api\modules\v1\components\Response;
use api\modules\v1\exceptions\BaseApiException;
use api\modules\v1\requests\BaseRequest;
use yii\base\Action;
use yii\base\InvalidConfigException;

/**
 * Class BaseAction
 * @property BaseRequest $request
 * @property Response    $response
 *
 * @package api\modules\v1\actions
 */
abstract class BaseAction extends Action
{
    protected $request;
    protected $response;

    public function init()
    {
        parent::init();

        $this->request  = $this->getRequest();
        $this->response = new Response();
    }

    protected function beforeExecute()
    {
        return true;
    }

    protected function execute()
    {
        throw new InvalidConfigException();
    }

    protected function setRequest($request)
    {
        $this->request = $request;
    }

    protected function getRequest()
    {
        throw new InvalidConfigException();
    }

    public function run()
    {
        app()->logger->log('Action: {action}', ['action' => $this->id]);

        $get = app()->request->getQueryParams();
        if ($get) {
            app()->logger->log('Request.get: {get}', ['get' => json_encode($get)]);
        }
        $post = app()->request->getBodyParams();
        if ($post) {
            app()->logger->log('Request.post: {post}', ['post' => json_encode($post)]);
        }

        try {

            $this->request->validate();


            app()->logger->log('Request.model: {request}', ['request' => (string)$this->request]);

            if ($this->beforeExecute()) {
                $this->execute();
            }
        } catch (BaseApiException $ex) {
            $this->response->setCode($ex->getCode());
            $this->response->setContent($ex->getMessage());

        } catch (\Exception $ex) {
            \Yii::error($ex);
            app()->logger->error('Внутренняя ошибка {ex}', ['ex' => get_class($ex)]);
            $this->response->setCode(ErrorCode::INTERNAL_ERROR);

        } finally {
            $response = $this->response->getResponse();
            app()->logger->log('Response.info: {info}',
                ['info' => app()->response->statusCode . ' ' . app()->response->statusText]);
            app()->logger->log('Response.body: {body}', ['body' => json_encode($response)]);
            app()->logger->statistic('Statistic: ');

            return $response;
        }
    }


}