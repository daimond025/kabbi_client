<?php

namespace api\modules\v1\actions;

use api\modules\v1\requests\GetOrdersInfoRequest;
use api\modules\v1\services\OrderService;
use yii\rest\Controller;

/**
 * Получить информацию по заказам.
 *
 * Class GetOrderInfoAction
 *
 * @package api\modules\v1\actions
 * @property GetOrdersInfoRequest $request
 */
class GetOrdersInfoAction extends BaseApiAction
{
    public $orderService;

    public function __construct($id, Controller $controller, OrderService $orderService, array $config = [])
    {
        parent::__construct($id, $controller, $config);

        $this->orderService = $orderService;
    }

    protected function getRequest()
    {
        return new GetOrdersInfoRequest();
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    protected function execute()
    {
        $orderInfo = $this->orderService->getOrdersInfo(
            $this->request->tenantId,
            $this->request->ordersId,
            $this->request->positionId,
            $this->request->lang,
            $this->request->needCarPhoto,
            $this->request->needDriverPhoto
        );

        $this->response->setContent([
            'order_info2' => $orderInfo,
        ]);
    }
}
