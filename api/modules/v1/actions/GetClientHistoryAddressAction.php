<?php

namespace api\modules\v1\actions;

use api\exceptions\NeedReauthException;
use api\models\client\Client;
use api\models\client\ClientAddressHistory;
use api\modules\v1\requests\GetClientHistoryAddressRequest;

/**
 * Получение списка городов службы такси.
 *
 * Class GetClientHistoryAddressAction
 *
 * @package api\modules\v1\actions
 * @property GetClientHistoryAddressRequest $request
 */
class GetClientHistoryAddressAction extends BaseApiAction
{
    protected $_clientId;

    protected function getRequest()
    {
        return new GetClientHistoryAddressRequest();
    }

    protected function beforeExecute()
    {
        if (parent::beforeExecute()) {

            if ($this->request->phone) {
                $client = Client::findByPhone($this->request->tenantId, $this->request->phone);
                if (!$client || $client->getIsBlackList()) {
                    throw new NeedReauthException();
                }
                $this->_clientId = $client->client_id;
            }

            return true;
        }

        return false;
    }

    protected function execute()
    {
        $models = $this->findModel();

        $result = [];

        foreach ($models as $model) {
            /** @var $model ClientAddressHistory */
            $result[] = [
                'address'  => [
                    'city'    => $model->city,
                    'comment' => '',
                    'house'   => $model->house,
                    'housing' => '',
                    'label'   => $model->street,
                    'lat'     => $model->lat,
                    'lon'     => $model->lon,
                    'porch'   => '',
                    'street'  => '',
                ],
                'label'    => $model->getLabel(),
            ];
        }

        $this->response->setContent($result);
    }

    protected function findModel()
    {
        return ClientAddressHistory::findLast($this->_clientId, $this->request->cityId);
    }


}