<?php

namespace api\modules\v1\actions;

use api\modules\v1\requests\GetOrderRouteRequest;
use app\models\order\OrderTrackService;

/**
 * Получение маршрута заказа.
 *
 * Class GetOrderRouteAction
 *
 * @package api\modules\v1\actions
 * @property GetOrderRouteRequest $request
 */
class GetOrderRouteAction extends BaseApiAction
{
    protected function getRequest()
    {
        return new GetOrderRouteRequest();
    }


    protected function execute()
    {
        /* @var $service OrderTrackService */
        $service = \Yii::createObject(OrderTrackService::class);
        $route   = $service->getTrack($this->request->orderId);

        $result['order_id']   = $this->request->orderId;
        $result['route_data'] = $route;
        $this->response->setContent($result);
    }

}