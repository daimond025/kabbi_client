<?php

namespace api\models\tenant\mobileApp;

use api\models\city\City;
use yii\db\ActiveRecord;

/**
 * Class MobileApp
 *
 * @property int $id
 * @property int $app_id
 * @property int $city_id
 *
 * @package common\modules\tenant\mobileApp
 */
class MobileAppHasCity extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%mobile_app_has_city}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    public function rules()
    {
        return [
            [['app_id', 'city_id'], 'integer'],
        ];
    }
}