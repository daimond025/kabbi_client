<?php

namespace api\models\taxi_tariff;

use api\models\client\ClientHasCompany;
use api\models\tenant\TenantCityHasPosition;
use api\models\transport\car\CarOption;
use Yii;
use api\models\worker\Position;
use api\models\transport\car\CarClass;
use api\models\tenant\Tenant;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class TaxiTariff
 * @package api\models\taxi_tariff
 *
 * @property int $tariff_id
 * @property int $tenant_id
 * @property int $class_id
 * @property int $block
 * @property int $group_id
 * @property string $name
 * @property string $description
 * @property int $sort
 * @property int $enabled_site
 * @property int $enabled_app
 * @property string $logo
 * @property int $position_id
 *
 * @property AdditionalOption[] $options
 */
class TaxiTariff extends \yii\db\ActiveRecord
{

    const CURRENT_TYPE = 'CURRENT';
    const HOLIDAYS_TYPE = 'HOLIDAYS';
    const EXCEPTIONS_TYPE = 'EXCEPTIONS';

    const TYPE_BASE = 'BASE';
    const TYPE_COMPANY = 'COMPANY';
    const TYPE_COMPANY_CORP_BALANCE = 'COMPANY_CORP_BALANCE';
    const TYPE_ALL = 'ALL';

    const ENABLED = 1;
    const BLOCK = 1;
    const NOT_BLOCK = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxi_tariff}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(TaxiTariffGroup::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    public function getHasCity()
    {
        return $this->hasOne(TaxiTariffHasCity::className(), ['tariff_id' => 'tariff_id']);
    }

    public function getOptions()
    {
        return $this->hasMany(AdditionalOption::className(), ['tariff_id' => 'tariff_id']);
    }

    public function getCompanies()
    {
        return $this->hasMany(TaxiTariffHasCompany::className(), ['tariff_id' => 'tariff_id']);
    }

    public function getTariffType()
    {
        return $this->hasMany(TaxiTariffType::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * Получить тип тарифа по времени
     *
     * @param string $date 2014.11.12 11:35:00
     *
     * @return string $tariffType тип тарифа  ENUM('CURRENT', 'HOLIDAYS', 'EXCEPTIONS')
     */
    public function getTariffTypeByTime($date)
    {
        $time           = new \DateTime($date);
        $dateFull       = $time->format('d.m.Y');
        $dateShort      = $time->format('d.m');
        $dayOfWeek      = date('l', strtotime($date));
        $hoursAndMinute = $time->format('H:i');
        $toDay          = [$dayOfWeek, $dateShort, $dateFull];

        $result = self::CURRENT_TYPE;

        $records = OptionActiveDate::find()
            ->where('tariff_id = :tariff_id', [':tariff_id' => $this->tariff_id])
            ->asArray()
            ->all();
        if (!empty($records)) {

            foreach ($records as $record) {

                $date = explode('|', $record['active_date']);

                // Игнорируем если день не совпал
                if (!in_array($date[0], $toDay)) {
                    continue;
                }
                $interval = isset($date[1]) ? explode('-', $date[1]) : ['00:00', '23:59'];

                // Игнорируем если не попали в часовой промежуток
                if ($interval[0] > $hoursAndMinute || $interval[1] < $hoursAndMinute) {
                    continue;
                }

                switch ($record['tariff_type']) {
                    case self::EXCEPTIONS_TYPE :
                        if (self::checkTariffIsActive($this->tariff_id, self::EXCEPTIONS_TYPE)) {
                            return self::EXCEPTIONS_TYPE;
                        }
                        break;
                    case self::HOLIDAYS_TYPE :
                        if (self::checkTariffIsActive($this->tariff_id, self::HOLIDAYS_TYPE)) {
                            $result = self::HOLIDAYS_TYPE;
                        }
                        break;
                }
            }
        }

        return $result;
    }

    public static function checkTariffIsActive($tariffId, $tariffType)
    {
        $isActive = TaxiOption::find()
            ->where('tariff_id= :tariff_id', ['tariff_id' => $tariffId])
            ->andWhere('tariff_type= :tariff_type', ['tariff_type' => $tariffType])
            ->andWhere('active = 1 OR active IS NULL')
            ->asArray()
            ->one();
        if (!empty($isActive)) {
            return true;
        }

        return false;
    }

    public function getClientType($clientId = null)
    {
        $result = [];

        if ($this->type === self::TYPE_BASE || $this->type === self::TYPE_COMPANY || $this->type === self::TYPE_ALL) {
            $result[] = 'base';
        }
        if (($this->type === self::TYPE_COMPANY || $this->type === self::TYPE_COMPANY_CORP_BALANCE || $this->type === self::TYPE_ALL) && $clientId !== null) {
            $result[] = 'company';
        }

        return $result;
    }

    public function getCompanyIds($clientId)
    {


        if ($this->type === self::TYPE_ALL) {
            $companies = ClientHasCompany::find()
                ->alias('h')
                ->where([
                    'h.client_id' => $clientId,
                    'c.block'     => 0,
                ])
                ->joinWith('company c')
                ->all();

            return ArrayHelper::getColumn($companies, 'company_id');
        }

        if (empty($this->companies) || $clientId === null) {
            return [];
        }

        $companies = [];
        foreach ($this->companies as $item) {
            if ($item->company->block != 0) {
                continue;
            }
            $companies[] = $item->company_id;
        }

        return $companies;
    }
}
