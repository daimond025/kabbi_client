<?php

namespace api\modules\v1\repositories;

use api\models\client\ClientReview;
use api\models\client\ClientReviewRating;
use api\modules\v1\repositories\dto\OrderInfoData;
use app\models\client\ClientWorkerLike;
use yii\helpers\ArrayHelper;

class ClientRepository
{
    public function addWorkerLike($clientId, $workerId, $positionId)
    {
        $model = $this->findOrCreateWorkerLike($clientId, $workerId, $positionId);
        $model->like_count++;

        if (!$model->save()) {
            throw new \RuntimeException('Saving ClientWorkerLike error');
        }
    }

    public function addWorkerDislike($clientId, $workerId, $positionId)
    {
        $model = $this->findOrCreateWorkerLike($clientId, $workerId, $positionId);
        $model->dislike_count++;
        $model->like_count = 0;

        if (!$model->save()) {
            throw new \RuntimeException('Saving ClientWorkerLike error');
        }
    }

    private function findOrCreateWorkerLike($clientId, $workerId, $positionId)
    {
        try {
            return $this->findWorkerLike($clientId, $workerId, $positionId);

        } catch (NotFoundException $exception) {
            return new ClientWorkerLike([
                'client_id'   => $clientId,
                'worker_id'   => $workerId,
                'position_id' => $positionId,
            ]);
        }
    }

    private function findWorkerLike($clientId, $workerId, $positionId)
    {
        $model = ClientWorkerLike::findOne(
            [
                'client_id'   => $clientId,
                'worker_id'   => $workerId,
                'position_id' => $positionId,
            ]
        );

        if (!$model) {
            throw new NotFoundException();
        }

        return $model;
    }


    public function addClientReview(OrderInfoData $order, $grade, $text)
    {
        $clientReview = new ClientReview([
            'client_id' => $order->client->id,
            'order_id'  => $order->id,
            'rating'    => $grade,
            'text'      => $text,
            'active'    => ClientReview::ACTIVE,
        ]);

        if ($order->worker) {
            $clientReview->worker_id   = $order->worker->id;
            $clientReview->position_id = $order->worker->positionId;
        }

        if (!$clientReview->save()) {
            throw new \RuntimeException();
        }
    }

    public function addClientReviewRating(OrderInfoData $order, $grade)
    {
        $clientReviewRating = $this->findOrCreateClientReviewRating($order->client->id);

        $rating = (integer)$grade;

        if (!ArrayHelper::isIn($rating, range(1, 5), true)) {
            throw new \DomainException('Invalid $grade');
        }

        $clientReviewRating->addRating($rating);

        if (!$clientReviewRating->save()) {
            throw new \RuntimeException();
        }
    }

    public function findOrCreateClientReviewRating($clientId)
    {
        try {
            return $this->findClientReviewRating($clientId);
        } catch (NotFoundException $exception) {
            return ClientReviewRating::make($clientId);
        }
    }

    public function findClientReviewRating($clientId)
    {
        $model = ClientReviewRating::findOne(['client_id' => $clientId]);

        if (!$model) {
            throw new NotFoundException();
        }

        return $model;
    }
}
