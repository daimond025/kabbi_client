<?php

return [
    'version'                          => 'info/version',
    'status'                           => 'info/status',

    // изменяем методы по одному
    'get_tenant_city_list'             => 'v1/new-api/get_tenant_city_list',
    'v1/get_tenant_city_list'          => 'v1/new-api/get_tenant_city_list',
    'api-site/get_tenant_city_list'    => 'v1/new-api/get_tenant_city_list',
    'v1/api-site/get_tenant_city_list' => 'v1/new-api/get_tenant_city_list',

    'v1/api-site/get_active_city_list' => 'v1/new-api/get_active_city_list',
    'api-site/get_active_city_list'    => 'v1/new-api-site/get_active_city_list',

    'get_download_links'    => 'v1/new-api/get_download_links',
    'v1/get_download_links' => 'v1/new-api/get_download_links',

    'get_tariffs_list'             => 'v1/new-api/get_tariffs_list',
    'v1/get_tariffs_list'          => 'v1/new-api/get_tariffs_list',
    'api-site/get_tariffs_list'    => 'v1/new-api/get_tariffs_list',
    'v1/api-site/get_tariffs_list' => 'v1/new-api/get_tariffs_list',

    'get_tariffs_type'             => 'v1/new-api/get_tariffs_type',
    'v1/get_tariffs_type'          => 'v1/new-api/get_tariffs_type',
    'api-site/get_tariffs_type'    => 'v1/new-api/get_tariffs_type',
    'v1/api-site/get_tariffs_type' => 'v1/new-api/get_tariffs_type',

    'get_client_history_address'             => 'v1/new-api/get_client_history_address',
    'v1/get_client_history_address'          => 'v1/new-api/get_client_history_address',
    'api-site/get_client_history_address'    => 'v1/new-api/get_client_history_address',
    'v1/api-site/get_client_history_address' => 'v1/new-api/get_client_history_address',

    'get_order_route'    => 'v1/new-api/get_order_route',
    'v1/get_order_route' => 'v1/new-api/get_order_route',

    'update_order'             => 'v1/new-api/update_order',
    'v1/update_order'          => 'v1/new-api/update_order',
    'api-site/update_order'    => 'v1/new-api/update_order',
    'v1/api-site/update_order' => 'v1/new-api/update_order',

    'update_order_result'             => 'v1/new-api/update_order_result',
    'v1/update_order_result'          => 'v1/new-api/update_order_result',
    'api-site/update_order_result'    => 'v1/new-api/update_order_result',
    'v1/api-site/update_order_result' => 'v1/new-api/update_order_result',

    'get_order_info'    => 'v1/new-api/get_order_info',
    'v1/get_order_info' => 'v1/new-api/get_order_info',

    'get_orders_info'    => 'v1/new-api/get_orders_info',
    'v1/get_orders_info' => 'v1/new-api/get_orders_info',

    'get_cars'             => 'v1/new-api/get_cars',
    'v1/get_cars'          => 'v1/new-api/get_cars',
    'api-site/get_cars'    => 'v1/new-api/get_cars',
    'v1/api-site/get_cars' => 'v1/new-api/get_cars',

    'costing_order'    => 'v1/new-api/costing_order',
    'v1/costing_order' => 'v1/new-api/costing_order',

    'get_accessible_car_models'    => 'v1/new-api/get_accessible_car_models',
    'v1/get_accessible_car_models' => 'v1/new-api/get_accessible_car_models',

    'like_worker'    => 'v1/new-api/like_worker',
    'v1/like_worker' => 'v1/new-api/like_worker',

    'send_response'    => 'v1/new-api/send_response',
    'v1/send_response' => 'v1/new-api/send_response',

     // проверка водителя
    'check_worker_password'    => 'v1/new-api/check_worker_password',
    'get_workers'    => 'v1/new-api/get_workers',

    'get_near_workers'    => 'v1/new-api/get_near_workers',

    // для старых версий приложений оставляем старый путь
    ''                     => 'v1/api/index',
    '<action>'             => 'v1/api/<action>',
    'api-site/<action>'    => 'v1/api-site/<action>',

    'v1'          => 'v1/api/index',
    'v1/<action>' => 'v1/api/<action>',
];