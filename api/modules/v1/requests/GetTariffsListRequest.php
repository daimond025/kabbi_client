<?php

namespace api\modules\v1\requests;

use api\models\client\ClientPhone;
use api\models\tenant\TenantHasCity;

/**
 * Class GetTariffsListRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests
 *
 * @property string $phone - Телефонный номер клиента
 * @property int    $cityId - Ид филиала*
 * @property int    $date - Дата
 */
class GetTariffsListRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'cityId' => [
                'name' => 'city_id',
                'type' => self::TYPE_GET,
            ],
            'phone'  => [
                'name' => 'phone',
                'type' => self::TYPE_GET,
            ],
            'date'   => [
                'name' => 'date',
                'type' => self::TYPE_GET,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['cityId', 'required'],
            [
                'cityId',
                'exist',
                'targetClass' => TenantHasCity::class,
                'targetAttribute' => ['cityId' => 'city_id', 'tenantId' => 'tenant_id'],
                'filter' => ['block' => TenantHasCity::NOT_BLOCK],
            ],
            ['phone', 'exist', 'targetClass' => ClientPhone::class, 'targetAttribute' => ['phone' => 'value']],

            [
                'date',
                'match',
                'pattern' => '/^((0[1-9]|[12]\d)\.(0[1-9]|1[012])|30\.(0[13-9]|1[012])|31\.(0[13578]|1[02]))\.(19|20)\d\d ([01]?\d|2[0-3]):([0-5]\d)(:([0-5]\d))?$/',
            ],
        ]);
    }


}