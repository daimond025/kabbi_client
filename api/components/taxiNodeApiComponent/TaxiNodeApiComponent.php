<?php

namespace api\components\taxiNodeApiComponent;

use api\models\order\Order;
use api\models\tenant\TenantSetting;
use yii\base\Object;
use Yii;

/**
 * Компонент  для работы  апи на nodejs (распределиние заказов,выход на смену)
 * @author Сергей К.
 */
class TaxiNodeApiComponent extends Object
{

    protected $nodeApiUrl;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->nodeApiUrl = Yii::$app->params['service.engine.url'];
    }

    public function neworderAuto($orderId, $tenantId, $cityId, $positionId)
    {
        $typeOfDistribution = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_TYPE_OF_DISTRIBUTION_ORDER, $cityId, $positionId);
        $orderOfferSec      = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_ORDER_OFFER_SEC, $cityId, $positionId);
        $distanceForSearch  = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_DISTANCE_FOR_SEARCH, $cityId, $positionId);

        $checkWorkerRatingToOfferOrder       = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_CHECK_WORKER_RATING_TO_OFFER_ORDER, $cityId, $positionId);
        $circlesDistributionOrder            = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_CIRCLES_DISTRIBUTION_ORDER, $cityId, $positionId);
        $delayCirclesDistributionOrder       = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_DELAY_CIRCLES_DISTRIBUTION_ORDER, $cityId, $positionId);
        $freeStatusBetweenDistributionCycles = TenantSetting::getSettingValue(
            $tenantId, TenantSetting::SETTING_FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES, $cityId, $positionId);

        $params = [
            'order_id'                                => (string)$orderId,
            'tenant_id'                               => (string)$tenantId,
            'type_of_distribution'                    => (string)$typeOfDistribution,
            'distance_for_search'                     => (string)$distanceForSearch,
            'offer_sec'                               => (string)$orderOfferSec,
            'check_worker_rating'                     => (string)$checkWorkerRatingToOfferOrder,
            'order_distribution_cycles'               => (string)$circlesDistributionOrder,
            'order_distribution_cycles_timeout'       => (string)$delayCirclesDistributionOrder,
            'free_status_between_distribution_cycles' => (string)$freeStatusBetweenDistributionCycles,
        ];

        $result = $this->sendRequst("neworder_auto", $params);
        if ($result) {
            if (isset($result["result"])) {
                if ($result["result"] == 1) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getRelevantWorkers($tenantId, $orderId)
    {
        $params = [
            'tenant_id' => $tenantId,
            'order_id'  => $orderId,
        ];

        $response = $this->sendRequst('get_relevant_workers', $params);

        if (!$response) {
            Yii::error('Connect Error');

            return [];
        }

        if ($response['error']) {
            Yii::error($response['error']);

            return [];
        }

        return $response['result']['workers'];

    }

    private function sendRequst($method, $params)
    {
        if (is_array($params)) {
            $params = http_build_query($params);
        }
        $url = $this->getUrl($method) . "?" . $params;
        $ch  = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencode',
        ]);
        $result    = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }

    private function getUrl($method)
    {
        return "{$this->nodeApiUrl}{$method}";
    }

}
