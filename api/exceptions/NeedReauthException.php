<?php

namespace api\exceptions;

use api\components\taxiApiRequest\TaxiErrorCode;
use api\modules\v1\exceptions\BaseApiException;

class NeedReauthException extends BaseApiException
{

    protected $code = TaxiErrorCode::NEED_REAUTH;
    protected $message = 'NEED_REAUTH';

}
