<?php

namespace api\components\taxiApiRequest;

use api\components\taxiApiRequest\TaxiErrorCode;
use yii\base\Object;
use Yii;
use DateTime;
use yii\helpers\ArrayHelper;

/*
 * Для общей работы с набором методов АПИ, их параметров
 */

class TaxiApiMethods extends Object
{
    const REQUARED = '!REQ!';
    const ADDRESS_REQUARED = '!ADDRESSREQ!';

    public $validateData;

    /**
     * Карта для исправления и автозаполнения пареметров коммнад апи
     * @var array
     */
    private $_fixParamsMap = [
        'get_tariffs_list'          => [
            'city_id'      => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
            'date'         => null,
            'phone'        => null,
        ],
        'get_tariffs_type'          => [
            'current_time' => TaxiApiMethods::REQUARED,
            'city_id'      => TaxiApiMethods::REQUARED,
            'date'         => null,
        ],
        'ping'                      => [
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'send_password'             => [
            'phone'        => TaxiApiMethods::REQUARED,
            'city_id'      => null,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'accept_password'           => [
            'phone'         => TaxiApiMethods::REQUARED,
            'password'      => TaxiApiMethods::REQUARED,
            'current_time'  => TaxiApiMethods::REQUARED,
            'device_token'  => null,
            'referral_code' => null,
            'city_id'       => null,
            'surname'       => null,
            'name'          => null,
            'patronymic'    => null,
            'email'         => null,
        ],
        'get_geoobjects_list'       => [
            'city_id'      => TaxiApiMethods::REQUARED,
            'street_part'  => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_tenant_city_list'      => [
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_city_list'             => [
            'current_time' => TaxiApiMethods::REQUARED,
            'city_part'    => TaxiApiMethods::REQUARED,
        ],
        'get_cars'                  => [
            'city_id'      => TaxiApiMethods::REQUARED,
            'from_lat'     => null,
            'from_lat'     => null,
            'from_lon'     => null,
            'radius'       => null,
            'car_class_id' => null,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'create_order_site'         => [
            'address'            => TaxiApiMethods::REQUARED,
            'city_id'            => TaxiApiMethods::REQUARED,
            'company_id'         => null,
            'client_phone'       => null,
            'client_name'        => null,
            'tariff_id'          => TaxiApiMethods::REQUARED,
            'order_time'         => null,
            'pay_type'           => null,
            'driver_callsign'    => null,
            'additional_options' => null,
            'comment'            => null,
            'bonus_payment'      => null,
            'promo_code'         => null,
            'current_time'       => TaxiApiMethods::REQUARED,
            'type_request'       => TaxiApiMethods::REQUARED,
        ],
        'create_order' => [
            'address'                     => TaxiApiMethods::REQUARED,
            'device_token'                => TaxiApiMethods::REQUARED,
            'city_id'                     => TaxiApiMethods::REQUARED,
            'client_id'                   => null,
            'company_id'                  => null,
            'client_phone'                => null,
            'client_name'                 => null,
            'except_car_models'           => null,
            'client_passenger_phone'      => null,
            'client_passenger_lastname'   => null,
            'client_passenger_name'       => null,
            'client_passenger_secondname' => null,
            'tariff_id'                   => TaxiApiMethods::REQUARED,
            'order_time'                  => null,
            'pay_type'                    => null,
            'driver_callsign'             => null,
            'additional_options'          => null,
            'comment'                     => null,
            'bonus_payment'               => null,
            'promo_code'                  => null,
            'current_time'                => TaxiApiMethods::REQUARED,
            'type_request'                => TaxiApiMethods::REQUARED,
            'pan'                         => null,
        ],
        'get_order_info'            => [
            'order_id'          => TaxiApiMethods::REQUARED,
            'need_car_photo'    => null,
            'need_driver_photo' => null,
            'lang'              => null,
            'current_time'      => TaxiApiMethods::REQUARED,
        ],
        'get_orders_info'           => [
            'orders_id'         => TaxiApiMethods::REQUARED,
            'need_car_photo'    => null,
            'need_driver_photo' => null,
            'lang'              => null,
            'current_time'      => TaxiApiMethods::REQUARED,
        ],
        'reject_order'              => [
            'order_id'     => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_reject_order_result'   => [
            'request_id'   => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'offer_order'               => [
            'order_id'        => TaxiApiMethods::REQUARED,
            'driver_callsign' => TaxiApiMethods::REQUARED,
            'current_time'    => TaxiApiMethods::REQUARED,
        ],
        'send_response'             => [
            'order_id'     => TaxiApiMethods::REQUARED,
            'grade'        => TaxiApiMethods::REQUARED,
            'text'         => null,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_info_page'             => [
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_confidential_page'     => [
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_client_profile'        => [
            'phone'        => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_client_balance'        => [
            'phone'        => TaxiApiMethods::REQUARED,
            'city_id'      => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'send_password_new_phone'   => [
            'old_phone'    => TaxiApiMethods::REQUARED,
            'new_phone'    => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'accept_password_new_phone' => [
            'old_phone'    => TaxiApiMethods::REQUARED,
            'new_phone'    => TaxiApiMethods::REQUARED,
            'password'     => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'update_client_profile'     => [
            'client_id'    => TaxiApiMethods::REQUARED,
            'old_phone'    => TaxiApiMethods::REQUARED,
            'new_phone'    => TaxiApiMethods::REQUARED,
            'surname'      => null,
            'name'         => null,
            'patronymic'   => null,
            'email'        => null,
            'birthday'     => null,
            'photo'        => null,
            'current_time' => null,
        ],
        'create_worker_profile'     => [
            'city_id'     => TaxiApiMethods::REQUARED,
            'last_name'   => TaxiApiMethods::REQUARED,
            'name'        => TaxiApiMethods::REQUARED,
            'second_name' => null,
            'phone'       => TaxiApiMethods::REQUARED,
            'photo'       => null,
            'partnership' => TaxiApiMethods::REQUARED,
            'birthday'    => null,
            'card_number' => null,
            'email'       => null,
            'lang'        => null,
            'description' => null,

            'position_id' => TaxiApiMethods::REQUARED,
            'activate'    => null,

            'car_color'  => null,
            'car_model'  => null,
            'car_brand'  => null,
            'car_year'   => null,
            'car_class'  => null,
            'car_owner'  => null,
            'gos_number' => null,
            'car_photo'  => null,

            'passport_series'         => null,
            'passport_number'         => null,
            'passport_issued'         => null,
            'passport_registration'   => null,
            'passport_actual_address' => null,
            'pasport_scan'            => null,

            'ogrnip_number' => null,
            'ogrnip_scan'   => null,

            'osago_series'     => null,
            'osago_number'     => null,
            'osago_start_date' => null,
            'osago_end_date'   => null,
            'osago_scan'       => null,

            'driver_license_series'     => null,
            'driver_license_number'     => null,
            'driver_license_categories' => null,
            'driver_license_start_date' => null,
            'driver_license_scan'       => null,

            'pts_series' => null,
            'pts_number' => null,
            'pts_scan'   => null,

            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_active_orders_list'    => [
            'client_id'    => TaxiApiMethods::REQUARED,
            'client_phone' => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_coords_by_address'     => [
            'city'         => TaxiApiMethods::REQUARED,
            'city_id'      => TaxiApiMethods::REQUARED,
            'street'       => null,
            'house'        => null,
            'housing'      => null,
            'porch'        => null,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_address_by_coords'     => [
            'lat'          => TaxiApiMethods::REQUARED,
            'lon'          => TaxiApiMethods::REQUARED,
            'city_id'      => null,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_order_route'           => [
            'order_id'     => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_client_cards'          => [
            'client_id'    => TaxiApiMethods::REQUARED,
            'phone'        => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'create_client_card'        => [
            'client_id'    => TaxiApiMethods::REQUARED,
            'phone'        => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'check_client_card'         => [
            'client_id'    => TaxiApiMethods::REQUARED,
            'phone'        => TaxiApiMethods::REQUARED,
            'order_id'     => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'delete_client_card'        => [
            'client_id'    => TaxiApiMethods::REQUARED,
            'phone'        => TaxiApiMethods::REQUARED,
            'pan'          => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_domain'                => [
            'domain'       => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],

        'get_car_color_list'               => [
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_categories_by_driver_license' => [
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_car_brand_list'               => [
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_position_list'                => [
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_car_model_list'               => [
            'brand_id'     => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_car_class_list'               => [
            'position_id'  => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'activate_bonus_system'            => [
            'client_phone' => TaxiApiMethods::REQUARED,
            'promo_code'   => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'activate_referral_system'         => [
            'phone'        => TaxiApiMethods::REQUARED,
            'referral_id'  => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'activate_referral_system_code'    => [
            'phone'        => TaxiApiMethods::REQUARED,
            'code'         => TaxiApiMethods::REQUARED,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_referral_system_list'         => [
            'phone'        => null,
            'current_time' => TaxiApiMethods::REQUARED,
        ],
        'get_active_order'         => [
            'phone'        => TaxiApiMethods::REQUARED,
        ],

    ];

    public function __construct()
    {
        $this->validateData['code']  = null;
        $this->validateData['param'] = null;
        parent::__construct();
    }

    /*
     * GETTERS AND SETTERS
     */

    /*
     * METHODS
     */

    /**
     * Получить порядок по умолчанию для параметров метода, с дефолтными значениями
     *
     * @param string $commandName - метод
     *
     * @return array - array('name' => '', 'type' => null, 'orderId' => 0, ...)
     */
    private function defaultOrder($commandName)
    {
        $map = $this->_fixParamsMap[$commandName];
        return $map;
    }

    private function setValidateDataResult($code, $param = null)
    {
        $this->validateData['code']  = $code;
        $this->validateData['param'] = $param;

        return $this->validateData;
    }

    /**
     * Проверяет имена параметров на корректность
     *
     * @param type $commandName
     * @param type $params
     *
     * @return int TaxiErrorCode
     */
    private function validateParamName($commandName, $params)
    {
        $defaultOrder = $this->defaultOrder($commandName);
        foreach ($params as $param => $value) {
            if (!key_exists($param, $defaultOrder)) {
                return $this->setValidateDataResult(TaxiErrorCode::BAD_PARAM, $param);
            }
        }

        return $this->setValidateDataResult(TaxiErrorCode::OK);
    }

    /**
     * Проверка, заполнены ли обязательные параметры
     *
     * @param type $commandName
     * @param type $params
     *
     * @return int TaxiErrorCode
     */
    private function validateParamRequared($commandName, $params)
    {
        $defaultOrder = $this->defaultOrder($commandName);
        foreach ($defaultOrder as $defaultParam => $defaultValue) {
            if ($defaultValue == TaxiApiMethods::REQUARED) {
                if (!key_exists($defaultParam, $params)) {
                    return $this->setValidateDataResult(TaxiErrorCode::MISSING_INPUT_PARAMETER, $defaultParam);
                }
                if (key_exists($defaultParam, $params) && empty($params[$defaultParam])) {
                    return $this->setValidateDataResult(TaxiErrorCode::EMPTY_VALUE, $defaultParam);
                }
            }
          /*  if ($defaultValue == TaxiApiMethods::ADDRESS_REQUARED) {
                if (!key_exists($defaultParam, $params)) {
                    return $this->setValidateDataResult(TaxiErrorCode::MISSING_INPUT_PARAMETER, $defaultParam);
                }

                if (key_exists($defaultParam, $params) && empty($params[$defaultParam])) {
                    return $this->setValidateDataResult(TaxiErrorCode::EMPTY_VALUE, $defaultParam);
                }
            }*/
        }
        return TaxiErrorCode::OK;
    }

    public function validateAddress($addresses){
        $validation = true;
        $LatLon = [];

        $item = [];
        foreach($addresses as $address){
            if(!isset($address['lat']) && !isset($address['lon']) ){

            }
        }

    }

    /**
     * Проверка входных парамтеров команды на корректность,
     * Если одна из проверок не проходит то возвращается код ошибки в
     * соответствующем методе
     *
     * @param strin $commandName - команда
     * @param array $params - параметры
     *
     * @return int TaxiErrorCode
     */
    public function validateParams($commandName, $params)
    {
        if (key_exists($commandName, $this->_fixParamsMap)) {
            //Проверяем параметры запроса на корректное название
            $validateData = $this->validateParamName($commandName, $params);
            if (!empty($validateData['code'])) {
                return $validateData;
            }

            //Проверяем, заполнены ли обязательные параметры
            $validateData = $this->validateParamRequared($commandName, $params);
            if (!empty($validateData['code'])) {
                return $validateData;
            }


            if (empty($validateData['code'])) {
                return $this->setValidateDataResult(TaxiErrorCode::OK);
            }
        } else {
            //Неизвестный запрос
            return $this->setValidateDataResult(TaxiErrorCode::UNKNOWN_REQUEST);
        }
    }

}
