<?php

namespace api\components\apiService;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class ApiService extends Object
{
    public $baseUrl;
    public $timeout = 5;

    const ACTION_SEND_PASSWORD = '/v1/notification/send_password_for_client';

    public function sendPassword($tenantId, $cityId, $phone, $positionId)
    {
        $params = [
            'tenant_id' => $tenantId,
            'city_id' => $cityId,
            'phone' => $phone,
            'positionId' => $positionId,
        ];


        $response = $this->get(self::ACTION_SEND_PASSWORD, $params);

        return $response && ArrayHelper::getValue($response, 'result') === 1;
    }

    public function get($action, $params)
    {
        $options = [
            'query'   => $params,
            'timeout' => $this->timeout,
        ];

        return $this->send('GET', $action, $options);
    }

    public function send($method, $action, $options)
    {
        $client = new Client();


        try {
            $response = $client->request($method, $this->getUrl($action), $options);

            return json_decode($response->getBody(), true);

        } catch (ClientException $ex) {
            \Yii::error('Connect exception. Status code ' . $ex->getResponse()->getStatusCode());
        }

        return null;
    }


    protected function getUrl($action)
    {
        return rtrim($this->baseUrl, '/') . $action;
    }
}