<?php

namespace api\modules\v1\components\validators;

use yii\validators\Validator;

class ArrayFilterValidator extends Validator
{
    public $delimiter = ',';

    public function validateAttribute($model, $attribute)
    {
        $attr              = $model->$attribute;
        $model->$attribute = empty($attr) ? [] : explode($this->delimiter, $attr);
    }
}
