<?php

namespace api\modules\v1\actions;

use api\modules\v1\requests\BaseVersionedApiRequest;

/**
 * Получение списка городов службы такси.
 *
 * Class GetCityListAction
 *
 * @package api\modules\v1\actions
 * @property BaseVersionedApiRequest $request
 */
class GetCityListAction extends BaseApiAction
{
    protected function getRequest()
    {
        return new BaseVersionedApiRequest();
    }

    protected function execute()
    {
        $prefix = $this->getPrefix();
        $this->response->setContent([
            'city_list' => app()->tenantService->getActiveCityListByTenantId($this->request->tenantId, $prefix),
        ]);
    }

    protected function getPrefix()
    {
        return $this->request->lang === 'ru' ? '' : '_' . $this->request->lang;
    }
}