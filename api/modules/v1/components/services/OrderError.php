<?php

namespace api\modules\v1\components\services;

use yii\base\Object;

class OrderError extends Object
{
    const NO_RESULT                        = 0;
    const SUCCESS                          = 100;
    const ERROR_SAVING                     = 101;
    const INVALID_ORDER_TIME               = 102;
    const OLD_DATA                         = 103;
    const FORBIDDEN_CHANGE_ORDER_TIME      = 104;
    const INVALID_VALUE                    = 105;
    const FORBIDDEN_CHANGE_WORKER          = 106;
    const INTERNAL_ERROR                   = 107;
    const COMPLETE_ORDER_ERROR             = 108;
    const COMPANY_BLOCKED_ERROR            = 109;
    const NO_RELEVANT_EXCHANGE_CONNECTIONS = 110;
    const FORBIDDEN_CHANGE_STATUS_ID       = 111;

    const BAD_PARAM                     = 4;
    const MISSING_PARAMETER             = 5;
    const BLACK_LIST                    = 8;
    const BAD_PAY_TYPE                  = 11;
    const NO_MONEY                      = 12;
    const INVALID_PAN                   = 15;
    const INVALID_PROMO_CODE            = 40;
    const BONUS_SYSTEM_IS_NOT_ACTIVATED = 41;
}
