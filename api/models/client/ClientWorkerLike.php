<?php

namespace app\models\client;

use api\models\client\Client;
use api\models\worker\Worker;
use api\models\worker\WorkerHasPosition;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%client_worker_like}}".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $worker_id
 * @property integer $position_id
 * @property integer $like_count
 * @property integer $dislike_count
 */
class ClientWorkerLike extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_worker_like}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['client_id', 'required'],
            ['client_id', 'exist', 'targetClass' => Client::class],

            ['worker_id', 'required'],
            ['worker_id', 'exist', 'targetClass' => Worker::class],

            ['position_id', 'required'],
            [
                'position_id',
                'exist',
                'targetClass'     => WorkerHasPosition::class,
                'targetAttribute' => ['worker_id', 'position_id'],
            ],

            ['like_count', 'integer', 'max' => 9999999999],
            ['dislike_count', 'integer', 'max' => 9999999999],
        ];
    }
}
