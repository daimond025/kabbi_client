<?php

namespace api\components\orderApi;

use api\components\orderApi\interfaces\createOrder\ApiOrderInterface;
use GuzzleHttp\Client;
use api\components\orderApi\exceptions\OrderApiException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * Class OrderApi
 * @package frontend\components
 */
class OrderApi extends Object
{

    const STATUS_USER_ERROR_400 = 400;
    const STATUS_USER_ERROR_422 = 422;

    /* @var string */
    public $baseUrl;

    /* @var int */
    public $timeout = 15;

    /* @var int */
    public $connectionTimeout = 15;

    /* @var Client */
    private $httpClient;

    /**
     * @inherited
     */
    public function init()
    {
        parent::init();

        $this->httpClient = new Client([
            'base_uri'          => $this->baseUrl,
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);
    }

    public function activationPromoCode($tenantId, $clientId, $codeId)
    {

        try {
            $response = $this->httpClient->request('POST', "promocode/promo/activate_promo_code", [
                'form_params' => [
                    'tenant_id'  => $tenantId,
                    'client_id'  => $clientId,
                    'promo_code' => $codeId,
                ],
            ]);

            return json_decode($response->getBody(), true);
        } catch (\Exception $ex) {
            throw new OrderApiException('Error activated code', 0, $ex);
        }
    }


    public function findSuitablePromoCode($tenant_id, $client_id, $order_time, $city_id, $position_id, $carClassId)
    {
        try {
            $response = $this->httpClient->get('promocode/promo/find_suitable_promo_code', [
                'query' => [
                    'tenant_id'    => $tenant_id,
                    'client_id'    => $client_id,
                    'order_time'   => $order_time,
                    'city_id'      => $city_id,
                    'position_id'  => $position_id,
                    'car_class_id' => $carClassId,
                ],
            ]);

            return json_decode($response->getBody(), true);
        } catch (\Exception $ex) {
            throw new OrderApiException('Error find code', 0, $ex);
        }
    }

    /**
     * @param ApiOrderInterface $order
     *
     * @return mixed
     * @throws OrderApiException
     */
    public function createOrder(ApiOrderInterface $order)
    {
        try {

            $response = $this->httpClient->request('POST', 'v1/order/create', [
                'form_params' => [
                    'city_id'                     => $order->getCityId(),
                    'order_now'                   => $order->getOrderNow(),
                    'order_date'                  => $order->getOrderDate(),
                    'order_hours'                 => $order->getOrderHours(),
                    'order_minutes'               => $order->getOrderMinutes(),
                    'phone'                       => $order->getPhone(),
                    'client_id'                   => $order->getClientId(),
                    'except_car_models'           => $order->getExceptCarModels(),
                    'client_lastname'             => $order->getClientPassengerLastName(),
                    'client_secondname'             => $order->getClientPassengerSecondName(),

                    'client_passenger_phone'      => $order->getClientPassengerPhone(),
                    'client_passenger_lastname'   => $order->getClientPassengerLastName(),
                    'client_passenger_name'       => $order->getClientPassengerName(),
                    'client_passenger_secondname' => $order->getClientPassengerSecondName(),

                    'comment'                     => $order->getComment(),
                    'bonus_payment'               => $order->getBonusPayment(),
                    'payment'                     => $order->getPayment(),
                    'position_id'                 => $order->getPositionId(),
                    'tariff_id'                   => $order->getTariffId(),
                    'predv_price'                 => $order->getPredvPrice(),
                    'predv_distance'              => $order->getPredvDistance(),
                    'predv_time'                  => $order->getPredvTime(),
                    'predv_price_no_discount'     => $order->getPredvPriceNoDiscount(),
                    'is_fix'                      => $order->getIsFix(),
                    'orderAction'                 => $order->getOrderAction(),
                    'parking_id'                  => $order->getParkingId(),
                    'company_id'                  => $order->getCompanyId(),
                    'device'                      => $order->getDevice(),
                    'tenant_id'                   => $order->getTenantId(),
                    'user_modifed'                => $order->getUserModifedId(),
                    'user_create'                 => $order->getUserCreatedId(),
                    'call_id'                     => $order->getCallId(),
                    'worker_id'                   => $order->getWorkerId(),
                    'car_id'                      => $order->getCarId(),
                    'versionclient'               => $order->getVersionClient(),
                    'lang'                        => $order->getLang(),
                    'app_id'                      => $order->getAppId(),
                    'pan'                         => $order->getPan(),
                    'order_time'                  => $order->getOrderTime(),
                    'additional_option'           => $order->getAdditionalOption(),
                    'client_device_token'         => $order->getClientDeviceToken(),

                    'bonus_promo_code' =>$order->getBonusPromoCode(),

                    'address' => $order->getAddressOriginal(),
                ],
                'headers'     => [
                    'lang' => $order->getLang(),
                ],
            ]);

            // TODO daimond025
          /*  var_dump( (string) $response->getBody()->getContents());
            exit();*/

            return json_decode($response->getBody(), true);
        } catch (GuzzleException $ex) {
            $this->throwValidateErrors($ex);
            throw new OrderApiException('Error create order', 0, $ex);
        }
    }

    protected function throwValidateErrors($ex)
    {
        if (self::isUserErrors($ex)) {
            throw $ex;
        }
    }

    public static function isUserErrors(GuzzleException $exception)
    {
        return $exception instanceof ClientException && ArrayHelper::isIn($exception->getResponse()->getStatusCode(), [
                self::STATUS_USER_ERROR_400,
                self::STATUS_USER_ERROR_422,
            ]);
    }
}
