<?php

namespace logger;

use logger\handlers\BasicHandler;
use logger\handlers\LogHandlerInterface;

/**
 * Class LogHandlerFactory
 * @package logger
 */
class LogHandlerFactory
{
    /**
     * Getting log handler
     *
     * @param string $method
     *
     * @return LogHandlerInterface
     */
    public function getHandler($method)
    {
        switch ($method) {
            case '/create_order':
            case '/v1/create_order':
            case '/v1/api/create_order':
                return new BasicHandler([
                    'type'     => ['name' => 'type_request'],
                    'client'   => ['name' => 'client_phone'],
                    'clientId' => ['name' => 'client_id'],
                    'city'     => ['name' => 'city_id'],
                    'tariff'   => ['name' => 'tariff_id'],
                    'payment'  => ['name' => 'pay_type'],
                ], [
                    'orderId' => 'result.order_id',
                    'order#'  => 'result.order_number',
                    'type'    => 'result.type',
                ]);
            case '/reject_order':
            case '/v1/reject_order':
            case '/v1/api/reject_order':
                return new BasicHandler([
                    'orderId' => ['name' => 'order_id'],
                ], [
                    'rejectResult' => 'result.reject_result',
                ]);
            default:
                return new BasicHandler();
        }
    }

}