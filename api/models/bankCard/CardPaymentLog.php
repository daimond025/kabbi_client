<?php

namespace api\models\bankCard;

use Yii;
use console\components\billing\models\Order;

/**
 * This is the model class for table "{{%card_payment_log}}".
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property string  $profile
 * @property integer $client_id
 * @property integer $order_id
 * @property string $type
 * @property string $params
 * @property string $response
 * @property integer $result
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Order $order
 */
class CardPaymentLog extends \yii\db\ActiveRecord
{

    const TYPE_PAY         = 'PAY';
    const TYPE_REFUND      = 'REFUND';
    const TYPE_ADD_CARD    = 'ADD CARD';
    const TYPE_CHECK_CARD  = 'CHECK CARD';
    const TYPE_REMOVE_CARD = 'REMOVE CARD';
    const RESULT_OK        = 1;
    const RESULT_ERROR     = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%card_payment_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'client_id', 'type', 'result'], 'required'],
            [['tenant_id', 'client_id', 'order_id', 'result'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['profile', 'type', 'params', 'response'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'tenant_id'  => 'Tenant ID',
            'profile'    => 'Profile',
            'client_id'  => 'Client ID',
            'order_id'   => 'Order ID',
            'type'       => 'Type',
            'params'     => 'Params',
            'response'   => 'Response',
            'result'     => 'Result',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * Added result of the payment gate operation to log
     * @param integer $tenantId
     * @param string  $profile
     * @param integer $orderId
     * @param integer $clientId
     * @param string $type
     * @param string $params
     * @param string $response
     * @param integer $result
     * @throws \yii\base\Exception
     */
    public static function log($tenantId, $profile, $orderId, $clientId, $type, $params,
        $response, $result)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $params = [
                'tenant_id' => $tenantId,
                'profile'   => $profile,
                'order_id'  => $orderId,
                'client_id' => $clientId,
                'type'      => $type,
                'params'    => json_encode($params, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
                'response'  => json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
                'result'    => $result,
            ];

            $model = new static($params);

            if ($model->save()) {
                $transaction->commit();
                return;
            } else {
                $transaction->rollback();
                $message = implode("\n", $model->getFirstErrors());
            }
        } catch (\Exception $ex) {
            $transaction->rollback();
            $message = $ex->getMessage();
        }

        Yii::error(implode("\n", [
            "Error occurred saving payment information to log.",
            print_r($params, true),
            $message,
        ]), "bank-card");
    }
}
