<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class UnsupportedVersionClientException extends BaseApiException
{
    public function __construct()
    {
        $code = ErrorCode::UNSUPPORTED_VERSION_CLIENT;
        parent::__construct('', $code, null);
    }

}