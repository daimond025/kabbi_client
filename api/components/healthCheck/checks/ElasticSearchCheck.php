<?php

namespace healthCheck\checks;

use healthCheck\exceptions\HealthCheckException;
use yii\elasticsearch\Connection;

/**
 * Class ElasticSearchCheck
 * @package healthCheck\checks
 */
class ElasticSearchCheck extends BaseCheck
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var string
     */
    private $url;

    /**
     * ElasticSearchCheck constructor.
     *
     * @param string     $name
     * @param Connection $connection
     * @param string     $url
     */
    public function __construct($name, Connection $connection, $url)
    {
        $this->connection = $connection;
        $this->url        = $url;

        parent::__construct($name);
    }

    public function run()
    {
        try {
            $data = $this->connection->get($this->url);
            if (empty($data)) {
                throw new HealthCheckException("No data received: url={$this->url}");
            }
        } catch (\Exception $ex) {
            throw new HealthCheckException(
                'Check elasticsearch exception: error=' . $ex->getMessage() . ', code=' . $ex->getCode(), 0, $ex);
        }
    }
}