<?php

namespace api\modules\v1\actions;

use api\modules\v1\components\ErrorCode;
use api\modules\v1\exceptions\BadParamsException;
use api\modules\v1\exceptions\EmptyDeviceIdException;
use api\modules\v1\exceptions\EmptyTenantIdException;
use api\modules\v1\exceptions\EmptyTypeClientException;
use api\modules\v1\exceptions\IncorrectlyAppIdException;
use api\modules\v1\exceptions\IncorrectlySignatureException;
use api\modules\v1\exceptions\MissingInputParamsException;
use api\modules\v1\exceptions\NeedUpdateAppVersionException;
use api\modules\v1\exceptions\UnsupportedTypeClientException;
use api\modules\v1\exceptions\UnsupportedVersionClientException;
use api\modules\v1\requests\BaseVersionedApiRequest;

/**
 * Class BaseApiAction
 * @package api\modules\v1\actions
 *
 * @property BaseVersionedApiRequest $request
 */
class BaseApiAction extends BaseAction
{
    protected function getRequest()
    {
        return new BaseVersionedApiRequest();
    }

    /**
     * @return bool
     * @throws BadParamsException
     * @throws EmptyDeviceIdException
     * @throws EmptyTenantIdException
     * @throws EmptyTypeClientException
     * @throws IncorrectlyAppIdException
     * @throws IncorrectlySignatureException
     * @throws MissingInputParamsException
     * @throws NeedUpdateAppVersionException
     * @throws UnsupportedTypeClientException
     * @throws UnsupportedVersionClientException
     */
    protected function beforeExecute()
    {
        if (parent::beforeExecute()) {


            $this->response->setCurrentTime($this->request->currentTime);


            if ($this->request->requestId) {
                app()->logger->setId($this->request->requestId);
            }

            if ($this->request->getFirstError('tenantId') === (string)ErrorCode::EMPTY_TENANT_ID) {
                app()->logger->error('Не найден арендатор {tenantId}', ['tenantId' => $this->request->tenantId]);

                throw new EmptyTenantIdException();
            }

            if ($this->request->getFirstError('deviceId') === (string)ErrorCode::EMPTY_DEVICE_ID) {
                app()->logger->error('Не указан deviceid для мобильных устройств');
                throw new EmptyDeviceIdException();
            }

            if ($this->request->getFirstError('typeClient') === (string)ErrorCode::EMPTY_TYPE_CLIENT) {
                app()->logger->error('Не указан тип устройства typeClient');
                throw new EmptyTypeClientException();
            }

            if ($this->request->getFirstError('typeClient') === (string)ErrorCode::UNSUPPORTED_TYPE_CLIENT) {
                app()->logger->error('Неподдерживаемый сервером тип устройства {typeClient}',
                    ['typeClient' => $this->request->typeClient]);
                throw new UnsupportedTypeClientException();
            }

            if ($this->request->getFirstError('versionClient') === (string)ErrorCode::UNSUPPORTED_VERSION_CLIENT) {
                app()->logger->error('Неподдерживаемая сервером версия устройства {versionClient}',
                    ['versionClient' => $this->request->versionClient]);
                throw new UnsupportedVersionClientException();
            }

            if ($this->request->getFirstError('appVersion') === (string)ErrorCode::NEED_UPDATE_APP_VERSION) {
                app()->logger->error('Неподдерживаемая сервером версия приложения {appVersion}',
                    ['appVersion' => $this->request->appVersion]);
                throw new NeedUpdateAppVersionException();
            }

            if ($this->request->getFirstError('appId') === (string)ErrorCode::INCORRECTLY_APP_ID) {
                app()->logger->error('Указан неправильный Id мобильного приложения {appId}',
                    ['appId' => $this->request->appId]);
                throw new IncorrectlyAppIdException();
            }


            //TODO проверка сигнатуры
            if ($this->request->getFirstError('signature') === (string)ErrorCode::INCORRECTLY_SIGNATURE) {

                app()->logger->error('Ошибка сигнатуры:');
                app()->logger->error('Моя сигнатура: {sign}', ['sign' => $this->request->getHash()]);
                app()->logger->error('Сигнатура приложения: {sign}', ['sign' => $this->request->signature]);



                \Yii::error('Ошибка сигнатуры:');
                \Yii::error('Моя сигнатура: ' . $this->request->getHash());
                \Yii::error('Сигнатура приложения: ' . $this->request->signature);

                throw new IncorrectlySignatureException();
            }

            if (!empty($this->request->getFirstErrors())) {
                $errors     = $this->request->getFirstErrors();
                $firstError = reset($errors);
                $key        = key($errors);

                if ($firstError === (string)ErrorCode::MISSING_INPUT_PARAMETER) {
                    app()->logger->error('Пустой парамнтр {param}',
                        ['param' => key($errors)]);
                    throw new MissingInputParamsException($key);
                }

                app()->logger->error('Ошибка входящих данных: {key} = {value}',
                    ['key' => $key, 'value' => $this->request->$key]);
                throw new BadParamsException($this->request->getFirstError($key));
            }


            return true;
        }

        return false;
    }
}