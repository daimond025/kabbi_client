<?php

namespace api\modules\v1\components;

use api\models\client\Client;
use api\models\client\ClientHasCompany;
use api\models\client\ClientPhone;
use yii\base\Object;

class ClientService extends Object
{


    /**
     * Создание клиента по номеру телефона
     *
     * @param integer $tenantId
     * @param integer $cityId
     * @param string  $phone
     *
     * @return Client|null
     */
    public function createClientByPhone($tenantId, $cityId, $phone)
    {
        $client = new Client([
            'tenant_id'  => $tenantId,
            'city_id'    => $cityId,
            'black_list' => 0,
        ]);

        $transaction = app()->db->beginTransaction();

        if (!$client->save()) {
            $transaction->rollBack();

            return null;
        }

        $phone = $this->createClientPhone($client->client_id, $phone);

        if (!$phone) {
            $transaction->rollBack();

            return null;
        }

        $transaction->commit();

        return $client;
    }


    /**
     * Создание телефона клиента
     *
     * @param integer $clientId
     * @param string  $phone
     *
     * @return ClientPhone|null
     */
    public function createClientPhone($clientId, $phone)
    {
        $phone = new ClientPhone([
            'client_id' => $clientId,
            'value'     => $phone,
        ]);

        return $phone->save() ? $phone : null;
    }


    /**
     * Вернуть клиента по его телефонному номеру
     *
     * @param $tenantId
     * @param $phone
     *
     * @return Client|null
     */
    public function findByPhone($tenantId, $phone)
    {
        $model = ClientPhone::find()
            ->alias('ph')
            ->joinWith('client cl')
            ->where([
                'cl.tenant_id' => $tenantId,
                'ph.value'     => $phone,
            ])
            ->one();

        return $model ? $model->client : null;
    }


    /**
     * Проверяет находится ли клиент в огранизации
     *
     * @param integer $clientId
     * @param integer $companyId
     *
     * @return bool
     */
    public function isClientInCompany($clientId, $companyId)
    {
        return ClientHasCompany::find()
            ->where([
                'client_id'  => $clientId,
                'company_id' => $companyId,
            ])
            ->exists();
    }

}