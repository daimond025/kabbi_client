<?php

namespace api\modules\v1\actions\crm;

use api\models\client\Client;
use api\models\tenant\TenantSetting;
use api\modules\v1\actions\BaseApiAction;
use api\modules\v1\components\CityService;
use api\modules\v1\components\OrderService;
use api\modules\v1\requests\crm\CostingOrderRequest;

/**
 * Получение предварительной стоимости заказа
 *
 * Class CostingOrderAction
 *
 * @package api\modules\v1\actions\crm
 * @property CostingOrderRequest $request
 */
class CostingOrderAction extends BaseApiAction
{
    protected $_clientId;

    protected function getRequest()
    {
        return new CostingOrderRequest();
    }

    protected function beforeExecute()
    {
        if (parent::beforeExecute()) {

            // Если время заказа не задано, то указываем текущее время
            if ($this->request->orderTime === null) {
                $this->request->orderTime = time();

                /** @var CityService $cityService */
                $cityService = app()->get('cityService');

                $offsetTime               = $cityService->getTimeOffset($this->request->cityId);
                $this->request->orderTime += $offsetTime;
            }

            if ($this->request->phone) {
                /** @var Client|null $client */
                $client = Client::findByPhone($this->request->tenantId, $this->request->phone);
                if($client) {
                    $this->_clientId = $client->client_id;
                }
            }

            return true;
        }

        return false;
    }


    protected function execute()
    {
        /** @var OrderService $orderService */
        $orderService = app()->get('orderService');

        $geoCoderType = TenantSetting::getSettingValue($this->request->tenantId, TenantSetting::SETTING_GEOCODER_TYPE,
            $this->request->cityId, $this->request->positionId);
        $geoCoderType = !empty($geoCoderType) ? $geoCoderType : CostingOrderRequest::DEFAULT_GEOCODER_TYPE;

        $this->request->orderTime = $orderService->formattedOrderTime($this->request->orderTime,
            $this->request->tenantId, $this->request->cityId, $this->request->positionId);
        $params                   = [
            'additionalOptions' => $this->request->additionalOptions,
            'address'           => $this->request->address,
            'cityId'            => $this->request->cityId,
            'geoCoderType'      => $geoCoderType,
            'orderTime'         => $this->request->getOrderTimeAsDate(),
            'tariffId'          => $this->request->tariffId,
            'clientId'          => $this->_clientId,
            'tenantId'          => $this->request->tenantId,
            'lang' => $this->request->lang,
        ];
        $result                   = $orderService->costingOrder($params);
        $this->response->setContent($result);
    }

}