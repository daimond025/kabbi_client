<?php

namespace api\modules\v1\components;

use api\components\routeAnalyzer\TaxiRouteAnalyzer;
use api\components\serviceEngine\ServiceEngine;
use api\models\order\exceptions\QueueIsNotExistsException;
use api\models\order\Order;
use api\models\order\OrderHasOption;
use api\models\order\OrderStatus;
use api\models\tenant\TenantSetting;
use api\modules\v1\helpers\RouteAnalyzerHelper;
use api\modules\v1\requests\crm\CreateOrderRequest;
use yii\base\ErrorException;
use api\models\order\OrderUpdateEvent;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class OrderService extends Object
{
    /**
     * Создание заказа
     */
    public function createOrder($params)
    {
        $paramsFromCosting = [
            'additionalOptions' => ArrayHelper::getValue($params, 'additionalOptions'),
            'address'           => ArrayHelper::getValue($params, 'address'),
            'cityId'            => ArrayHelper::getValue($params, 'cityId'),
            'geoCoderType'      => ArrayHelper::getValue($params, 'geoCoderType'),
            'orderTime'         => ArrayHelper::getValue($params, 'orderTime'),
            'tariffId'          => ArrayHelper::getValue($params, 'tariffId'),
            'tenantId'          => ArrayHelper::getValue($params, 'tenantId'),
            'lang'              => ArrayHelper::getValue($params, 'lang'),
        ];

        $costingOrder = $this->costingOrder($paramsFromCosting);

        $cost  = ArrayHelper::getValue($params, 'cost');
        $isFix = $cost !== null;

        $order             = new Order();
        $order->attributes = [
            'tenant_id' => ArrayHelper::getValue($params, 'tenantId'),
            'city_id'   => ArrayHelper::getValue($params, 'cityId'),
            'tariff_id' => ArrayHelper::getValue($params, 'tariffId'),

            'predv_price'    => $cost === null ? ArrayHelper::getValue($costingOrder, 'summary_cost') : $cost,
            'predv_distance' => ArrayHelper::getValue($costingOrder, 'summary_distance'),
            'predv_time'     => ArrayHelper::getValue($costingOrder, 'summary_time'),

            'parking_id'  => ArrayHelper::getValue($params['address']['A'], 'parking_id', null),
            'client_id'   => ArrayHelper::getValue($params, 'clientId'),
            'phone'       => ArrayHelper::getValue($params, 'phone'),
            'comment'     => ArrayHelper::getValue($params, 'comment'),
            'create_time' => time(),
            'status_time' => time(),
            'payment'     => ArrayHelper::getValue($params, 'payment'),
            'company_id'  => ArrayHelper::getValue($params, 'companyId'),
            'address'     => serialize($params['address']),

            'currency_id'   => ArrayHelper::getValue($params, 'currencyId'),
            'position_id'   => ArrayHelper::getValue($params, 'positionId'),
            'time_offset'   => ArrayHelper::getValue($params, 'timeOffset'),
            'is_fix'        => (int)$isFix,
            'bonus_payment' => ArrayHelper::getValue($params, 'bonusPayment'),

            'status_id'  => $params['statusId'],
            'order_time' => ArrayHelper::getValue($params, 'orderTime'),
        ];

        $transaction = app()->db->beginTransaction();

        if (!$order->save()) {
            $transaction->rollBack();

            return null;
        }

        try {
            $additionalOptions = ArrayHelper::getValue($params, 'additionalOptions');

            if (!empty($additionalOptions)) {
                if (!OrderHasOption::manySave($additionalOptions, $order->order_id)) {
                    $transaction->rollBack();

                    return null;
                }
            }

            $redisData = [
                'additionals_cost'     => ArrayHelper::getValue($costingOrder, 'additional_cost', 0),
                'summary_time'         => ArrayHelper::getValue($costingOrder, 'summary_time', 0),
                'summary_distance'     => ArrayHelper::getValue($costingOrder, 'summary_distance', 0),
                'summary_cost'         => ArrayHelper::getValue($costingOrder, 'summary_cost', 0),
                'city_time'            => ArrayHelper::getValue($costingOrder, 'city_time', 0),
                'city_distance'        => ArrayHelper::getValue($costingOrder, 'city_distance', 0),
                'city_cost'            => ArrayHelper::getValue($costingOrder, 'city_cost', 0),
                'out_city_time'        => ArrayHelper::getValue($costingOrder, 'out_city_time', 0),
                'out_city_distance'    => ArrayHelper::getValue($costingOrder, 'out_city_distance', 0),
                'out_city_cost'        => ArrayHelper::getValue($costingOrder, 'out_city_cost', 0),
                'is_fix'               => ArrayHelper::getValue($costingOrder, 'is_fix', 0),
                'start_point_location' => ArrayHelper::getValue($costingOrder, 'start_point_location', 0),
                'tariffInfo'           => ArrayHelper::getValue($costingOrder, 'tariffInfo', []),
            ];

            $this->saveToRedis($order->order_id, $redisData);
            $this->sendOrderToEngine($order);

            $transaction->commit();

        } catch (\ErrorException $exception) {
            $transaction->rollBack();

            $message = "Error save order: message=\"{$exception->getMessage()}\", orderId={$order->order_id}";
            \Yii::error($message);
            \Yii::$app->apiLogger->log($message);
        }

        return $order->order_id;
    }


    protected function saveToRedis($orderId, $costData)
    {
        Order::saveOrderToRedis($orderId, $costData);
    }

    protected function sendOrderToEngine($order)
    {
        $serviceEngine = new ServiceEngine();

        try {
            $resultSendOrder = $serviceEngine->neworderAuto($order->order_id, $order->tenant_id);
        } catch (\Exception $e) {
            \Yii::error("Receive error neworderAuto: orderId={$order->order_id} (Error:{$e->getMessage()})");
            $resultSendOrder = false;
        }

        if (!$resultSendOrder) {
            \Yii::$app->redis_orders_active->executeCommand('hdel', [$order->tenant_id, $order->order_id]);
            $error = null;
            try {
                $isOrderDeleted = $order->delete();
            } catch (\Exception $ex) {
                $isOrderDeleted = false;
                $error          = $ex->getMessage();
            }

            if (empty($isOrderDeleted)) {
                \Yii::error("Error delete new order from MySql, after engine was failed (Error:{$error})", 'order');
                \Yii::$app->apiLogger->log("Error delete new order from MySql, after engine was failed (Error:{$error})");
            }

            throw new ErrorException("An error occurred while sending order to NodeJS");
        }
    }

    /**
     * Предрасчет заказа
     *
     * @param array $params
     *
     * @return array
     */
    public function costingOrder($params)
    {
        /** @var TaxiRouteAnalyzer $routeAnalyzerService */
        $routeAnalyzerService = app()->get('routeAnalyzer');
        $response             = $routeAnalyzerService->analyzeRoute($params['tenantId'], $params['cityId'],
            $params['address'], $params['additionalOptions'], $params['tariffId'], $params['orderTime'], $params['clientId'],
            $params['lang'], $params['geoCoderType']);

        return RouteAnalyzerHelper::formatted($response);
    }

    /**
     * @param $params
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function costingOrderManyTariffs($params)
    {
        /** @var TaxiRouteAnalyzer $routeAnalyzerService */
        $routeAnalyzerService = app()->get('routeAnalyzer');

        $response = $routeAnalyzerService->calculate(
            ArrayHelper::getValue($params,'tenantId'),
            ArrayHelper::getValue($params,'cityId'),
            ArrayHelper::getValue($params,'address'),
            ArrayHelper::getValue($params,'additionalOptions', []),
            ArrayHelper::getValue($params,'tariffId', []),
            ArrayHelper::getValue($params,'orderTime'),
            ArrayHelper::getValue($params,'clientId'),
            ArrayHelper::getValue($params,'lang'),
            ArrayHelper::getValue($params,'geoCoderType')
        );


        return $response;
        return RouteAnalyzerHelper::formatted($response);
    }


    /**
     * Выводит время заказа с учетом подачи авто
     *
     * @param integer $orderTime
     * @param integer $tenantId
     * @param integer $cityId
     * @param integer $positionId
     *
     * @return int
     */
    public function formattedOrderTime($orderTime, $tenantId, $cityId, $positionId)
    {
        /** @var CityService $cityService */
        $cityService = app()->get('cityService');

        $offsetTime = $cityService->getTimeOffset($cityId);

        $pickUp = TenantSetting::getSettingValue($tenantId, TenantSetting::SETTING_PICK_UP, $cityId, $positionId);
        $pickUp = $pickUp ? $pickUp : Order::PICK_UP_TIME;

        $minimalCreatedOrderTime = time() + $pickUp + $offsetTime;

        if ($orderTime < $minimalCreatedOrderTime) {
            return $minimalCreatedOrderTime;
        }

        return $orderTime;
    }

    /**
     * Проверяет является ли заказ предварительным
     *
     * @param integer $orderTime
     * @param integer $tenantId
     * @param integer $cityId
     * @param integer $positionId
     *
     * @return bool
     */
    public function isPreOrder($orderTime, $tenantId, $cityId, $positionId)
    {
        /** @var CityService $cityService */
        $cityService = app()->get('cityService');

        $offsetTime = $cityService->getTimeOffset($cityId);

        $preOrderTimeAsMinute = TenantSetting::getSettingValue($tenantId, TenantSetting::SETTING_PRE_ORDER, $cityId,
            $positionId);
        $preOrderTime         = $preOrderTimeAsMinute ? $preOrderTimeAsMinute * 60 : Order::PRE_ORDER_TIME;
        $minimalPreOrderTime  = time() + $preOrderTime + $offsetTime;

        return $orderTime > $minimalPreOrderTime;

    }


    /**
     * Проверяет зависим ли вид оплаты от организации
     *
     * @param string $payment
     *
     * @return bool
     */
    public function isCompanyByPayment($payment)
    {
        return ArrayHelper::isIn($payment, $this->getCompanyByPaymentList());
    }

    /**
     * Возвращает массив вид оплаты с зависимостью от организации
     * @return array
     */
    public function getCompanyByPaymentList()
    {
        return [
            Order::PAYMENT_CORP_BALANCE,
        ];
    }

    public function getPaymentListByClient()
    {
        return ArrayHelper::merge($this->getPaymentList(), [Order::PAYMENT_CARD]);
    }

    /**
     * Возвращает массив всех видов оплаты
     *
     * @return array
     */
    public function getPaymentList()
    {
        return [
            Order::PAYMENT_CORP_BALANCE,
            Order::PAYMENT_PERSONAL_ACCOUNT,
            Order::PAYMENT_CASH,
        ];
    }

    public function getBonusPaymentList()
    {
        return [0, 1];
    }

    /**
     *
     *
     * @param integer $orderTime
     * @param integer $tenantId
     * @param integer $cityId
     * @param integer $positionId
     * @param string  $state
     *
     * @return int
     */
    public function getStatus($orderTime, $tenantId, $cityId, $positionId, $state)
    {
        switch ($state) {

            case CreateOrderRequest::STATE_FREE:
                return OrderStatus::STATUS_FREE;

            case CreateOrderRequest::STATE_FREEZE:
                return OrderStatus::STATUS_MANUAL_MODE;

            case CreateOrderRequest::STATE_NEW:
            default:
                return $this->isPreOrder($orderTime, $tenantId, $cityId,
                    $positionId) ? OrderStatus::STATUS_PRE : OrderStatus::STATUS_NEW;
        }
    }

    public function updateOrder($clientId, $tenantId, $orderId, $requestId, $lang, $params)
    {

        //Достаем инфу в редисе
        $orderData = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenantId, $orderId]);

        if (!$orderData) {
            return false;
        }

        $orderData = unserialize($orderData);

        if (!isset($orderData['status']['status_id']) || (int)$orderData['status']['status_id'] === OrderStatus::WAITING_FOR_PAYMENT) {
            return false;
        }

        if (!isset($orderData['status']['status_group']) || $orderData['status']['status_group'] === OrderStatus::STATUS_GROUP_4
            || $orderData['status']['status_group'] === OrderStatus::STATUS_GROUP_5) {
            return false;
        }

        $order = Order::findOne($orderId);

        if (!$order) {
            return false;
        }

        $newAddress        = Order::filterAddress($params['address'], $lang, $lang, $tenantId,
            $order->city_id);
        $params['address'] = $newAddress['address'];

        $address = unserialize($order->address);

        $params['address']['A'] = $address['A'];
        //        $params['address']      = serialize($params['address']);

        $addOptions  = ArrayHelper::getColumn($order->options, 'option_id');
        $costingData = [
            'tenantId'          => $order->tenant_id,
            'cityId'            => $order->city_id,
            'address'           => $params['address'],
            'additionalOptions' => implode(',', $addOptions),
            'clientId'          => $clientId,
            'tariffId'          => $order->tariff_id,
            'orderTime'         => app()->formatter->asDatetime($order->order_time, 'dd.MM.YYYY HH:mm:ss'),
            'lang'              => 'ru',
            'geoCoderType'      => 'ru',

        ];
        $response    = $this->costingOrder($costingData);

        $params['predv_price']    = $response['summary_cost'];
        $params['predv_distance'] = $response['summary_distance'];
        $params['predv_time']     = $response['summary_time'];

        $orderUpdateEvent = new OrderUpdateEvent([
            'requestId'      => $requestId,
            'tenantId'       => $tenantId,
            'orderId'        => $orderId,
            'lang'           => $lang,
            'lastUpdateTime' => $order->update_time,
            'senderId'       => $clientId,
            'sender'         => OrderUpdateEvent::SENDER_CLIENT,
            //            'sender'         => OrderUpdateEvent::SENDER_OPERATOR,
        ]);

        try {
            $orderUpdateEvent->addEvent($params);

            return true;
        } catch (QueueIsNotExistsException $ignore) {
        }

        return false;
    }

    public function updateOrderStatus($updateId)
    {
        $orderEvent = new OrderUpdateEvent();
        $response   = json_decode($orderEvent->getResultResponse($updateId), true);

        return $response;
    }

    public function isRequiredWorker($statusId)
    {
        return (int)$statusId === OrderStatus::WORKER_ASSIGNED_SOFT || (int)$statusId === OrderStatus::WORKER_ASSIGNED_HARD;
    }

    public function updateOrderAsOperator($tenantId, $orderId, $requestId, $lang, $params)
    {

        $order = Order::findOne($orderId);
        if (!$order) {
            return false;
        }

        $orderUpdateEvent = new OrderUpdateEvent([
            'requestId'      => $requestId,
            'tenantId'       => $tenantId,
            'orderId'        => $orderId,
            'lang'           => $lang,
            'lastUpdateTime' => $order->update_time,
            'sender'         => OrderUpdateEvent::SENDER_OPERATOR,
        ]);

        try {
            $orderUpdateEvent->addEvent($params);

            return true;
        } catch (QueueIsNotExistsException $ignore) {
        }

        return false;
    }
}