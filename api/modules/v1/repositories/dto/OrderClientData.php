<?php

namespace api\modules\v1\repositories\dto;

class OrderClientData
{
    public $id;
    public $phone;

    public function __construct($id, $phone)
    {
        $this->id    = (integer)$id;
        $this->phone = $phone;
    }
}
