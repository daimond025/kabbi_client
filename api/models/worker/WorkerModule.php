<?php

namespace api\models\worker;

use Yii;

/**
 * This is the model class for table "{{%tbl_module}}".
 *
 * @property integer $id
 * @property string $name
 */
class WorkerModule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%module}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
