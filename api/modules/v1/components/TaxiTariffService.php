<?php

namespace api\modules\v1\components;

use api\models\client\ClientHasCompany;
use api\models\taxi_tariff\TaxiTariff;
use api\models\taxi_tariff\TaxiTariffActiveDate;
use api\models\taxi_tariff\TaxiTariffType;
use api\models\tenant\Tenant;
use api\models\tenant\TenantCityHasPosition;
use api\models\transport\car\CarOption;
use yii\base\Object;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class TaxiTariffService extends Object
{

    public function getTaxiTariffList($clientId, $date, $lang, $tenantId, $cityId, $typeClient, $positionId, $simple)
    {
        $tariffs = $this->findTaxiTariffModel($clientId, $tenantId, $cityId, $typeClient, $positionId);
        $domain  = $this->getDomain($tenantId);

        $bonusTariffList = $this->getBonusTariffList($tenantId, $cityId);

        $result = array_map(function ($item) use (
            $date,
            $lang,
            $domain,
            $cityId,
            $bonusTariffList,
            $simple,
            $clientId
        ) {
            $tariffTypeModel = $this->getTariffTypeByTime($item->tariff_id, $date);


            /** @var TaxiTariff $item */
            $el = [
                'tariff_id'    => (string)$item->tariff_id,
                'position_id'  => (int)$item->position_id,
                'car_class_id' => $item->class_id,
                'type'         => $tariffTypeModel->type,
            ];

            if ($simple) {
                return $el;
            }
            $el = array_merge($el, [
                'city_id'            => $cityId,
                'logo_thumb'         => !empty($item->logo) ? app()->params['frontend.protocol'] . '://' . $domain . '.'
                    . app()->params['frontend.domain'] . '/file/show-external-file?filename=thumb_' . $item->logo
                    . '&id=' . $item->tenant_id : '',
                'logo'               => !empty($item->logo) ? app()->params['frontend.protocol'] . '://' . $domain . '.'
                    . app()->params['frontend.domain'] . '/file/show-external-file?filename=' . $item->logo
                    . '&id=' . $item->tenant_id : '',
                'tariff_name'        => empty($item->name) ? t('app', 'Untitled', [], $lang) : (string)$item->name,
                'description'        => (string)$item->description,
                'is_bonus'           => (int)in_array($item->tariff_id, $bonusTariffList),
                'client_type'        => $item->getClientType($clientId),
                'companies'          => $item->getCompanyIds($clientId),
                'additional_options' => array_map(function ($item) use ($lang) {
                    $item->option->name = t(CarOption::SOURCE_CATEGORY, $item->option->name, [], $lang);
                    unset($item->option->type_id);
                    $el = [
                        'id'                   => $item->id,
                        'tariff_id'            => $item->tariffType->tariff_id,
                        'additional_option_id' => $item->additional_option_id,
                        'price'                => $item->price,
                        'tariff_type'          => $item->tariffType->type,
                        'option'               => [
                            'option_id' => $item->option->option_id,
                            'name'      => $item->option->name,
                        ],
                    ];

                    return $el;
                }, $tariffTypeModel->hasAdditional),
            ]);



            return $el;
        }, $tariffs);



        return $result;
    }

    protected function findTaxiTariffModel($clientId, $tenantId, $cityId, $typeClient, $positionId = null)
    {
        $companies = [];
        if ($clientId) {
            $companies = $this->getCompanyIdListByClient($clientId);
        }

        $availablePositionIds = $this->getAvailablePositionIdList($tenantId, $cityId);

        $query = TaxiTariff::find()
            ->alias('t')
            ->andFilterWhere(['position_id' => $positionId])
            ->andWhere(['position_id' => $availablePositionIds])
            ->joinWith([
                'hasCity hc'  => function ($query) use ($cityId) {
                    /** @var ActiveQuery $query */
                    $query->where([
                        'hc.city_id' => $cityId,
                    ]);
                },
                'options.option o',
                'companies c' => function ($query) use ($companies) {
                    /** @var ActiveQuery $query */
                    $query->onCondition(['c.company_id' => $companies]);
                },
                'options'     => function ($query) {
                    /** @var ActiveQuery $query */
                    $query->joinWith('option');
                },
            ])
            ->orderBy([
                'sort' => SORT_ASC,
                'name' => SORT_ASC,
            ]);

        $query->andWhere([
            'type' => [TaxiTariff::TYPE_BASE, TaxiTariff::TYPE_ALL],
        ]);

        if ($clientId) {
            $query->orWhere([
                'c.company_id' => $companies,
            ]);
        }

        $query->andWhere([
            't.tenant_id' => $tenantId,
            't.block'     => TaxiTariff::NOT_BLOCK,
        ]);

        switch ($typeClient) {
            case 'WEB':
                $query->andWhere([
                    'enabled_site' => TaxiTariff::ENABLED,
                ]);
                break;
            case 'HOSPITAL':
                $query->andWhere([
                    'enabled_hospital' => TaxiTariff::ENABLED,
                ]);
                break;
            default:
                $query->andWhere([
                    'enabled_app' => TaxiTariff::ENABLED,
                ]);
        }

        return $query->all();
    }

    protected function getCompanyIdListByClient($clientId)
    {
        if (!$clientId) {
            return [];
        }

        $companies = ClientHasCompany::findAll(['client_id' => $clientId]);
        $companies = ArrayHelper::getColumn($companies, 'company_id');

        return $companies;
    }

    protected function getAvailablePositionIdList($tenantId, $cityId)
    {
        return TenantCityHasPosition::find()
            ->select('position_id')
            ->where([
                'tenant_id' => $tenantId,
                'city_id'   => $cityId,
            ])
            ->column();
    }

    protected function getDomain($tenantId)
    {
        return Tenant::getDomain($tenantId);
    }

    protected function getBonusTariffList($tenantId, $cityId)
    {
        $bonusSystem     = \Yii::createObject(\bonusSystem\BonusSystem::class, [$tenantId]);
        $bonusSystemType = $bonusSystem->getBonusSystemType();

        $tariffs = (new Query())
            ->select('t.tariff_id')
            ->distinct()
            ->from('{{%client_bonus}} c')
            ->innerJoin('{{%client_bonus_has_tariff}} t', 'c.bonus_id = t.bonus_id')
            ->where([
                'c.tenant_id'       => $tenantId,
                'c.city_id'         => $cityId,
                'c.bonus_system_id' => $bonusSystemType->getId(),
            ])
            ->all();

        return is_array($tariffs) ? ArrayHelper::getColumn($tariffs, 'tariff_id') : [];
    }

    protected function getTariffTypeByTime($tariffId, $date)
    {
        $time = new \DateTime($date);

        $dateFull       = $time->format('d.m.Y');
        $dateShort      = $time->format('d.m');
        $dayOfWeek      = date('l', strtotime($date));
        $hoursAndMinute = $time->format('H:i');
        $toDay          = [$dayOfWeek, $dateShort, $dateFull];

        $records = $this->findOptionActiveDateModel($tariffId);



        /** @var TaxiTariffActiveDate $record */
        foreach ($records as $record) {

            $arr           = explode('|', $record->active_date, 2);
            $date          = ArrayHelper::getValue($arr, 0);
            $interval      = ArrayHelper::getValue($arr, 1);
            $isInterval    = (boolean)$interval;
            $arr           = $isInterval ? explode('-', $interval, 2) : [];
            $startInterval = ArrayHelper::getValue($arr, 0, '00:00');
            $endInterval   = ArrayHelper::getValue($arr, 1, '23:59');

            if (empty($date)) {
                if ($startInterval <= $endInterval) {
                    if ($startInterval <= $hoursAndMinute && $hoursAndMinute < $endInterval) {

                        return $record->tariffType;
                    }
                } else {
                    if ($startInterval <= $hoursAndMinute || $hoursAndMinute < $endInterval) {

                        return $record->tariffType;
                    }
                }

                continue;
            }

            if (ArrayHelper::isIn($date, $toDay)) {
                if ($startInterval <= $hoursAndMinute && $hoursAndMinute < $endInterval) {

                    return $record->tariffType;
                }
            }
        }

        return TaxiTariffType::findOne(['tariff_id' => $tariffId, 'type' => TaxiTariffType::TYPE_CURRENT]);
    }

    protected function findOptionActiveDateModel($tariffId)
    {
        $model = TaxiTariffActiveDate::find()
            ->alias('date')
            ->joinWith('tariffType type', false)
            ->where([
                'type.tariff_id' => $tariffId,
            ])
            ->orderBy([
                'type.sort' => SORT_ASC,
            ])
            ->all();

        return $model;
    }
}