<?php

namespace bonusSystem\exceptions;

use yii\base\Exception;

/**
 * Class PaymentStrategyNotFoundException
 * @package bonusSystem\exceptions
 */
class PaymentStrategyNotFoundException extends Exception
{

}