<?php


namespace api\modules\v1\controllers;

use api\modules\v1\actions\CheckWorkerPassword;
use api\modules\v1\actions\CostingOrderAction;
use api\modules\v1\actions\GetAccessibleCarModelsAction;
use api\modules\v1\actions\GetCarsAction;
use api\modules\v1\actions\GetClientHistoryAddressAction;
use api\modules\v1\actions\GetDownloadLinksAction;
use api\modules\v1\actions\GetNearWorkers;
use api\modules\v1\actions\GetOrderInfoAction;
use api\modules\v1\actions\GetOrderRouteAction;
use api\modules\v1\actions\GetOrdersInfoAction;
use api\modules\v1\actions\GetTariffsListAction;
use api\modules\v1\actions\GetTariffsTypeAction;
use api\modules\v1\actions\GetTenantCityListAction;
use api\modules\v1\actions\GetWorkers;
use api\modules\v1\actions\LikeWorkerAction;
use api\modules\v1\actions\SendResponseAction;
use api\modules\v1\actions\UpdateOrderAction;
use api\modules\v1\actions\UpdateOrderResultAction;
use yii\rest\Controller;
use yii\web\Response;

class NewApiController extends Controller
{
    public function actions()
    {
        return [
            'get_tenant_city_list'       => GetTenantCityListAction::class,
            'get_tariffs_list'           => GetTariffsListAction::class,
            'get_tariffs_type'           => GetTariffsTypeAction::class,
            'get_download_links'         => GetDownloadLinksAction::class,
            'get_client_history_address' => GetClientHistoryAddressAction::class,
            'get_order_route'            => GetOrderRouteAction::class,
            'update_order'               => UpdateOrderAction::class,
            'update_order_result'        => UpdateOrderResultAction::class,
            'get_order_info'             => GetOrderInfoAction::class,
            'get_orders_info'            => GetOrdersInfoAction::class,
            'get_cars'                   => GetCarsAction::class,
            'costing_order'              => CostingOrderAction::class,
            'get_accessible_car_models'  => GetAccessibleCarModelsAction::class,
            'like_worker'                => LikeWorkerAction::class,
            'send_response'              => SendResponseAction::class,

            'check_worker_password'      => CheckWorkerPassword::class,
            'get_workers'      => GetWorkers::class,
            // distance from order to worker
            'get_near_workers'      => GetNearWorkers::class
        ];

    }

    public function actionError()
    {
        return 'error';
    }

    public function actionVersion()
    {
        app()->response->format = Response::FORMAT_RAW;

        return 'v' . app()->params['version'];
    }

    public function actionTest()
    {
        return 'test';
    }
}