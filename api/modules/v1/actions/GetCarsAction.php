<?php

namespace api\modules\v1\actions;

use api\modules\v1\components\WorkerService;
use api\modules\v1\requests\GetCarsRequest;

/**
 * Получение списка автомобилей на линии (для карты).
 *
 *
 *
 * Class GetCarsAction
 *
 * @package api\modules\v1\actions
 * @property GetCarsRequest $request
 */
class GetCarsAction extends BaseApiAction
{
    protected function getRequest()
    {
        return new GetCarsRequest();
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    protected function execute()
    {
        /** @var WorkerService $workerService */
        $workerService = app()->get('workerService');

        $this->response->setContent([
            'cars' => $workerService->getCars(
                $this->request->tenantId,
                $this->request->classId,
                $this->request->cityId,
                $this->request->lang,
                $this->request->lat,
                $this->request->lon,
                $this->request->radius,
                $this->request->exceptCarModels,
                $this->request->additionalOptions
            ),
        ]);
    }

}