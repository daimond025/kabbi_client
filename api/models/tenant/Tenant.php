<?php

namespace api\models\tenant;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tenant}}".
 *
 * @property string $tenant_id
 * @property string $company_name
 * @property string $full_company_name
 * @property string $legal_address
 * @property string $post_address
 * @property string $contact_name
 * @property string $contact_last_name
 * @property string $contact_second_name
 * @property string $contact_phone
 * @property string $contact_email
 * @property string $company_phone
 * @property string $domain
 * @property string $email
 * @property string $inn
 * @property string $create_time
 * @property string $bookkeeper
 * @property string $kpp
 * @property string $ogrn
 * @property string $site
 * @property string $archive
 * @property string $logo
 * @property string $director
 * @property string $director_position
 * @property string $status
 */
class Tenant extends \yii\db\ActiveRecord
{

    public static $_domain = [];
    /**
     * Const for tenant status
     */
    const ACTIVE = 'ACTIVE';
    const BLOCKED = 'BLOCKED';
    const REMOVED = 'REMOVED';

    /**
     * @var bool For lock organization
     */
    public $blocked = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tenant_id' => 'Tenant ID',
            'company_name' => t('tenant', 'Company Name'),
            'full_company_name' => t('tenant', 'Full Company Name'),
            'legal_address' => t('tenant', 'Legal Address'),
            'post_address' => t('tenant', 'Post Address'),
            'contact_name' => t('tenant', 'Contact name'),
            'contact_second_name' => t('app', 'Second name'),
            'contact_last_name' => t('app', 'Last name'),
            'contact_phone' => t('tenant', 'Contact phone'),
            'contact_email' => t('tenant', 'Contact email'),
            'company_phone' => t('tenant', 'Company phone'),
            'domain' => t('tenant', 'Domain'),
            'email' => t('tenant', 'Company email'),
            'inn' => t('tenant', 'Inn'),
            'create_time' => t('tenant', 'Create Time'),
            'bookkeeper' => t('tenant', 'Bookkeeper name'),
            'kpp' => t('tenant', 'Kpp'),
            'ogrn' => t('tenant', 'Ogrn'),
            'site' => t('tenant', 'Site'),
            'archive' => 'Archive',
            'director' => t('tenant', 'Director name'),
            'director_position' => t('tenant', 'Director position'),
            'logo' => t('tenant', 'Company logo'),
        ];
    }

    public static function getCityList($tenant_id, $lang = '')
    {
        $model = TenantHasCity::find()
            ->where([
                'tenant_id' => $tenant_id,
                'block' => 0
            ])
            ->joinWith('city')
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        return ArrayHelper::map($model, 'city_id', function ($model) use ($lang) {
            return $model->city->getName($lang);
        });
    }

    public static function getDomain($tenant_id)
    {
        if (empty(self::$_domain[$tenant_id])) {

            self::$_domain[$tenant_id] = self::find()
                ->select('domain')
                ->where([
                    'tenant_id' => $tenant_id
                ])
                ->scalar();
        }
        return self::$_domain[$tenant_id];
    }

}
