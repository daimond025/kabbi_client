<?php

namespace api\components\taxiGeocoder;

use yii\base\Object;
use Yii;
use DateTime;

/**
 * Middleware for taxi-service-api geocoder methods
 *
 * @author Sergey K.
 */
class TaxiGeocoder extends Object
{
    /**
     * @var string
     */
    public $url;

    /**
     * Timeout of requst to taxi-service-api
     * @var int
     */
    public $timeout = 15;

    public function init()
    {
        parent::init();
    }

    /**
     * Get coordinates by address
     *
     * @param string $address Ижевск,улица советская,22,2,2
     * @param string $lang
     * @param string $geocoderType
     *
     * @return array
     * [
     *  "lat"=>56.213213,
     *  "lon"=>54.213123
     * ]
     */
    public function findCoordsByAddress($address, $lang = "ru", $geocoderType = "ru")
    {
        $address = urlencode($address);
        $params  = [
            'address'       => $address,
            'lang'          => $lang,
            'geocoder_type' => $geocoderType,
        ];
        $result  = $this->sendGetRequest('find_coords_by_address', 1, $params);
        if (isset($result['code'])) {
            if ($result['code'] == 0) {
                return $result["result"];
            }
        }
    }

    /**
     * Get address by coordinates
     *
     * @param float  $lat
     * @param float  $lon
     * @param string $lang
     * @param string $geocoderType
     *
     * @return mixed
     */
    public function findAddressByCoords($lat, $lon, $lang = "ru", $geocoderType = "ru")
    {
        $params = [
            'lat'           => $lat,
            'lon'           => $lon,
            'lang'          => $lang,
            'geocoder_type' => $geocoderType,
        ];
        $result = $this->sendGetRequest('find_address_by_coords', 1, $params);
        if (isset($result['code'])) {
            if ($result['code'] == 0) {
                return $result["result"];
            }
        }
    }

    /**
     * Send GET request to taxi-service-api
     *
     * @param string $method
     * @param string $version
     * @param array  $params
     *
     * @return bool|mixed
     */
    private function sendGetRequest($method, $version, $params)
    {
        if (is_array($params)) {
            $params = http_build_query($params);
        }

        if (strlen($params) > 0) {
            $url = $this->getUrl($method, $version) . "?" . $params;
        } else {
            $url = $this->getUrl($method, $version);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        $result    = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }

    /**
     * Send POST request  taxi-service-api
     *
     * @param string $method
     * @param string $version
     * @param array  $params
     *
     * @return bool|mixed
     */
    private function sendPostRequest($method, $version, $params)
    {
        $url = $this->getUrl($method, $version);
        $ch  = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $result    = curl_exec($ch);
        $errorCode = curl_errno($ch);
        curl_close($ch);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }

    /**
     * Get url of taxi-service-api geocoder method
     *
     * @param string $method
     * @param string $version
     *
     * @return string
     */
    private function getUrl($method, $version)
    {
        return "{$this->url}v{$version}/geocoder/{$method}";
    }

}
