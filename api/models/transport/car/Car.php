<?php

namespace api\models\transport\car;

use api\models\tenant\Tenant;
use api\models\worker\Position;
use api\models\worker\TenantCompany;
use api\models\worker\Worker;
use api\models\worker\WorkerReviewRating;

/**
 * Class Car
 * @package api\models\transport\car
 *
 * @property integer  $car_id
 * @property integer  $tenant_id
 * @property integer  $city_id
 * @property string   $name
 * @property string   $gos_number
 * @property integer  $color
 * @property integer  $year
 * @property string   $photo
 * @property string   $owner
 *
 * @property CarColor $carColor
 * @property CarModel $carModel
 */
class Car extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['tenant_id', 'class_id', 'city_id', 'gos_number', 'color', 'year', 'owner', 'brand', 'model'],
                'required',
            ],
            ['class_id', 'exist', 'targetClass' => '\api\models\transport\car\CarClass'],
            ['color', 'exist', 'targetClass' => '\api\models\transport\car\CarColor', 'targetAttribute' => 'color_id'],
            ['brand', 'exist', 'targetClass' => '\api\models\transport\car\CarBrand', 'targetAttribute' => 'brand_id'],
            ['class_id', 'exist', 'targetClass' => '\api\models\transport\car\CarClass'],
            [
                ['brand', 'model'],
                'exist',
                'targetClass'     => '\api\models\transport\car\CarModel',
                'targetAttribute' => ['model' => 'model_id', 'brand' => 'brand_id'],
            ],
            [
                'city_id',
                'exist',
                'targetClass'     => '\api\models\tenant\TenantHasCity',
                'targetAttribute' => ['city_id', 'tenant_id'],
            ],
            ['owner', 'in', 'range' => $this->getOwnerList()],
            [['name'], 'safe'],
        ];
    }

    protected function getOwnerList()
    {
        return ['WORKER', 'COMPANY'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarColor()
    {
        return $this->hasOne(CarColor::className(), ['color_id' => 'color']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    public function getCarModel()
    {
        return $this->hasOne(CarModel::class, ['model_id' => 'model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }


    public function getTenantCompany()
    {
        return $this->hasOne(TenantCompany::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    public static function getCarInfo(
        $car_id,
        $callsign,
        $tenant_id,
        $positionId,
        $lang,
        $need_car_photo,
        $need_worker_photo,
        $isTaxi
    ) {
        $car = self::find()
            ->where([
                'car_id' => $car_id,
            ])
            ->asArray()
            ->one();

        $worker = Worker::find()
            ->select(['worker_id', 'last_name', 'name', 'second_name', 'photo', 'phone'])
            ->where([
                'tenant_id' => $tenant_id,
                'callsign'  => $callsign,
            ])
            ->asArray()
            ->one();

        $position = Position::findOne($positionId);

        if ($isTaxi) {
            $description = empty($car)
                ? t('employee', $position->name, [], $lang)
                : implode(' ', [
                    $worker['car']['name'],
                    t('car', $worker['car']['color'], [], $lang),
                    $worker['car']['gos_number'],
                ]);
        } else {
            $description = t('car', 'courier', [], $lang);
        }

        return [
            "car_description" => $description,
            "car_lat"         => 0,
            "car_lon"         => 0,
            "speed"           => 0.0,
            "car_time"        => 0,
            "degree"          => 0,
            "driver_fio"      => implode(' ', [$worker['name'], $worker['second_name']]),
            "driver_phone"    => (string)$worker['phone'],
            'driver_photo'    => empty($worker['photo']) || !$need_worker_photo ? ''
                : self::getUrl($worker['photo'], $tenant_id),
            'car_photo'       => empty($car['photo']) || !$need_car_photo ? ''
                : self::getUrl($car['photo'], $tenant_id),
            "raiting"         => WorkerReviewRating::getWorkerRating($worker['worker_id'], $positionId),
        ];
    }

    public static function getUrl($photo, $tenant_id = null)
    {
        $url = app()->params['frontend.protocol'] . '://';
        if ($tenant_id) {
            $url .= Tenant::getDomain($tenant_id) . '.';
        }
        $url .= app()->params['frontend.domain'] . '/file/show-external-file?filename=' . $photo;
        if ($tenant_id) {
            $url .= '&id=' . $tenant_id;
        }

        return $url;
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->brand = CarBrand::getBrandName($this->brand);
            $this->model = CarModel::getModelName($this->model);
            $this->name  = $this->brand . ' ' . $this->model;
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

}
