<?php

namespace api\models\client;

use yii\db\ActiveRecord;

/**
 * Class ClientReviewRating
 * @package api\models\client
 *
 * @property integer $client_id
 * @property integer $one
 * @property integer $two
 * @property integer $three
 * @property integer $four
 * @property integer $five
 * @property integer $count
 */
class ClientReviewRating extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_review_raiting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'one', 'two', 'three', 'four', 'five', 'count'], 'integer']
        ];
    }

    public static function make($clientId)
    {
        return new ClientReviewRating([
            'client_id' => $clientId,
            'one'       => 0,
            'two'       => 0,
            'three'     => 0,
            'four'      => 0,
            'five'      => 0,
            'count'     => 0,
        ]);
    }

    public function addRating($rating)
    {
        switch ($rating) {
            case 1:
                $this->one++;
                $this->count++;
                break;
            case 2:
                $this->two++;
                $this->count++;
                break;
            case 3:
                $this->three++;
                $this->count++;
                break;
            case 4:
                $this->four++;
                $this->count++;
                break;
            case 5:
                $this->five++;
                $this->count++;
                break;
        }
    }
}
