<?php

namespace api\models\transport;

use Yii;

/**
 * This is the model class for table "{{%tbl_transport_type}}".
 *
 * @property integer $id
 * @property string $name
 */
class TransportType extends \yii\db\ActiveRecord
{

    const ID_PASSENGER = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transport_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'ID',
            'name' => 'Name',
        ];
    }
}
