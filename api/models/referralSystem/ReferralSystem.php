<?php

namespace api\models\referralSystem;

use api\models\referralSystem\formatter\CompanyNameReplace;
use api\models\referralSystem\formatter\MinPriceReplace;
use api\models\referralSystem\formatter\ReferralBonusReplace;
use api\models\referralSystem\formatter\ReferralCodeReplace;
use api\models\referralSystem\formatter\ReferralSystemFormatter;
use api\models\referralSystem\formatter\ReferrerBonusReplace;
use api\models\tenant\Tenant;


/**
 * This is the model class for table "{{%referral}}".
 * @property integer              $referral_id
 * @property integer              $tenant_id
 * @property integer              $active
 * @property integer              $sort
 * @property string               $name
 * @property integer              $city_id
 * @property string               $type
 * @property integer              $is_active_subscriber_limit
 * @property integer              $subscriber_limit
 * @property integer              $referral_bonus
 * @property string               $referral_bonus_type
 * @property integer              $referrer_bonus
 * @property string               $referrer_bonus_type
 * @property integer              $order_count
 * @property float                $min_price
 * @property string               $text_in_client_api
 * @property string               $title
 * @property string               $text
 *
 * @property integer              $block
 * @property boolean              $isActive
 *
 * @property ReferralSystemMember $member
 * @property Tenant               $tenant
 */
class ReferralSystem extends \yii\db\ActiveRecord
{

    const ON_ORDER = 'ON_ORDER';
    const ON_REGISTER = 'ON_REGISTER';

    const PERCENT = 'PERCENT';
    const BONUS = 'BONUS';

    const ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%referral}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasMany(ReferralSystemMember::className(), ['referral_id' => 'referral_id']);
    }

    /**
     * @param $array ReferralSystem[]
     *
     * @return array
     */
    public static function formatted($array, $isClient = false)
    {
        return array_map(function ($item) use ($isClient) {
            /** @var self $item */
            $element = [
                'referral_id' => $item->referral_id,
                'name'        => $item->name,
                'city_id'     => $item->city_id,
                'content'     => $item->formatter('text_in_client_api'),
                'title'       => $item->formatter('title'),
                'text'        => $item->formatter('text'),
            ];
            if ($isClient) {
                $element['is_activate'] = !empty($item->member) ? 1 : 0;
                $element['code']        = !empty($item->member[0]->code) ? $item->member[0]->code : null;
            }

            return $element;
        }, $array);
    }

    public function formatter($attribute)
    {
        $model = new ReferralSystemFormatter();
        $model = new CompanyNameReplace($model, $this->tenant->company_name);
        $model = new MinPriceReplace($model, $this->min_price);
        $model = new ReferralCodeReplace($model, !empty($this->member[0]->code) ? $this->member[0]->code : '???');
        $model = new ReferralBonusReplace($model, $this->referral_bonus);
        $model = new ReferrerBonusReplace($model, $this->referrer_bonus);

        return $model->formatter($this->$attribute);
    }

}
