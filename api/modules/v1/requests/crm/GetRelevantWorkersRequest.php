<?php

namespace api\modules\v1\requests\crm;

use api\models\order\Order;
use api\modules\v1\requests\BaseVersionedApiRequest;

/**
 * Class GetRelevantWorkersRequest
 *  Обязательные поля помечены звездочкой (*)
 * @package api\modules\v1\requests\crm
 *
 * @property int $orderId - Ид заказа*
 */
class GetRelevantWorkersRequest extends BaseVersionedApiRequest
{

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'orderId' => [
                'name' => 'order_id',
                'type' => self::TYPE_GET,
            ],
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['orderId', 'required'],
            [
                'orderId',
                'exist',
                'targetClass'     => Order::className(),
                'targetAttribute' => ['orderId' => 'order_id', 'tenantId' => 'tenant_id'],
            ],
        ]);
    }


}