<?php

namespace api\modules\v1\actions;

use api\models\worker\Worker;
use api\modules\v1\requests\CheckWorkerPasswordRequest;
use api\modules\v1\requests\GetWorkerRequest;
use api\modules\v1\requests\LikeWorkerRequest;
use api\modules\v1\services\ClientService;
use yii\rest\Controller;

/**
 * Понравился исполнитель или нет.
 *
 * Class LikeWorkerAction
 *
 * @package api\modules\v1\actions
 * @property LikeWorkerRequest $request
 */
class GetWorkers extends BaseApiAction
{


    protected function getRequest()
    {
        return new GetWorkerRequest();
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    protected function execute()
    {
        $workers = null;
        if( (int)$this->request->worker_all  == 1){
            $workers =  Worker::find()
                ->where([
                    'tenant_id' => $this->request->tenantId,
                    'block'  => 0,
                ])->all();
        }elseif (((int)$this->request->worker_all  == 0) && ($this->request->workerCallsign)){
            $workers =  Worker::find()
                ->where([
                    'tenant_id' => $this->request->tenantId
                ])->andWhere(['like', 'callsign', $this->request->workerCallsign])->all();

        }

        // формирование вывода
        $worker_outpur['worker'] = [];
        foreach ($workers as $worker){
            $worker_add['callsign'] = $worker->callsign;
            $worker_add['last_name'] = $worker->last_name;
            $worker_add['name'] = $worker->name;
            $worker_add['second_name'] = $worker->second_name;
            $worker_add['tenant_company_id'] = $worker->tenant_company_id;
            $worker_add['email'] = $worker->email;
            $worker_outpur['worker'][] = $worker_add;
        }

        $this->response->setContent($worker_outpur);
    }
}
