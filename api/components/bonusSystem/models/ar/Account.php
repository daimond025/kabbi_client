<?php

namespace bonusSystem\models\ar;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%account}}".
 *
 * @property integer       $account_id
 * @property integer       $acc_kind_id
 * @property integer       $acc_type_id
 * @property integer       $owner_id
 * @property integer       $currency_id
 * @property integer       $tenant_id
 * @property double        $balance
 *
 * @property AccountKind   $accKind
 * @property AccountType   $accType
 * @property Currency      $currency
 * @property Tenant        $tenant
 * @property Operation[]   $operations
 * @property Transaction[] $transactions
 * @property Transaction[] $transactions0
 */
class Account extends ActiveRecord
{
    const ACCOUNT_TYPE_ID_ACTIVE = 1;
    const ACCOUNT_TYPE_ID_PASSIVE = 2;

    const ACCOUNT_KIND_ID_CLIENT_BONUS = 7;
    const ACCOUNT_KIND_ID_CLIENT_BONUS_UDS_GAME = 9;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acc_kind_id', 'acc_type_id', 'currency_id'], 'required'],
            [['acc_kind_id', 'acc_type_id', 'owner_id', 'currency_id', 'tenant_id'], 'integer'],
            [['balance'], 'number'],
            [
                ['owner_id', 'acc_kind_id', 'acc_type_id', 'tenant_id', 'currency_id'],
                'unique',
                'targetAttribute' => ['owner_id', 'acc_kind_id', 'acc_type_id', 'tenant_id', 'currency_id'],
                'message'         => 'The combination of Acc Kind ID, Acc Type ID, Owner ID, Currency ID and Tenant ID has already been taken.',
            ],
            [
                ['acc_kind_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => AccountKind::className(),
                'targetAttribute' => ['acc_kind_id' => 'kind_id'],
            ],
            [
                ['acc_type_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => AccountType::className(),
                'targetAttribute' => ['acc_type_id' => 'type_id'],
            ],
            [
                ['currency_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Currency::className(),
                'targetAttribute' => ['currency_id' => 'currency_id'],
            ],
            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id'  => 'Account ID',
            'acc_kind_id' => 'Acc Kind ID',
            'acc_type_id' => 'Acc Type ID',
            'owner_id'    => 'Owner ID',
            'currency_id' => 'Currency ID',
            'tenant_id'   => 'Tenant ID',
            'balance'     => 'Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccKind()
    {
        return $this->hasOne(AccountKind::className(), ['kind_id' => 'acc_kind_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccType()
    {
        return $this->hasOne(AccountType::className(), ['type_id' => 'acc_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperations()
    {
        return $this->hasMany(Operation::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['receiver_acc_id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions0()
    {
        return $this->hasMany(Transaction::className(), ['sender_acc_id' => 'account_id']);
    }
}
