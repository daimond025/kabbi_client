<?php

namespace bonusSystem\models\ar;

use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%tenant_has_bonus_system}}".
 *
 * @property integer     $id
 * @property integer     $tenant_id
 * @property integer     $bonus_system_id
 * @property string      $api_key
 *
 * @property BonusSystem $bonusSystem
 */
class TenantHasBonusSystem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_has_bonus_system}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'bonus_system_id'], 'required'],
            [['tenant_id', 'bonus_system_id'], 'integer'],
            [['api_key'], 'string', 'max' => 255],
            [['tenant_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'tenant_id'       => 'Tenant ID',
            'bonus_system_id' => 'Bonus System ID',
            'api_key'         => 'Api Key',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonusSystem()
    {
        return $this->hasOne(BonusSystem::className(), ['id' => 'bonus_system_id']);
    }

}
