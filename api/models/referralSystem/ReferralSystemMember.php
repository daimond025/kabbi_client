<?php

namespace api\models\referralSystem;

use api\models\client\Client;


/**
 * This is the model class for table "{{%referral_system_members}}".
 * @property integer        $member_id
 * @property integer        $client_id
 * @property integer        $referral_id
 * @property string         $code
 *
 * @property ReferralSystem $referralSystem
 * @property Client         $client
 */
class ReferralSystemMember extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%referral_system_members}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'referral_id'], 'required'],
            ['client_id', 'exist', 'targetClass' => Client::className()],
            ['referral_id', 'exist', 'targetClass' => ReferralSystem::className()],
            ['client_id', 'unique', 'targetAttribute' => ['client_id', 'referral_id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferralSystem()
    {
        return $this->hasOne(ReferralSystem::className(), ['referral_id' => 'referral_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * Генерация случайной строки
     *
     * @return string
     * */
    protected function getRandomString($length = 6)
    {
        $chars    = 'ABDEFGHKNQRSTYZ23456789';
        $numChars = strlen($chars);
        $string   = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, mt_rand(1, $numChars) - 1, 1);
        }

        return $string;

    }

    /**
     * Генерация уникального кода
     *
     * @return string
     */
    protected function generateCode()
    {
        $codes = self::find()
            ->select('code')
            ->all();

        $codes = (array)$codes;

        do {
            $code = $this->getRandomString(6);
        } while (in_array($code, $codes, true));

        return $code;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (empty($this->code)) {
                $this->code = $this->generateCode();
            }

            return true;
        }

        return false;
    }
}
