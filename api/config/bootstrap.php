<?php

\Yii::setAlias('api', dirname(dirname(__DIR__)) . '/api');
\Yii::setAlias('app', '@api');
\Yii::setAlias('bonusSystem', '@api/components/bonusSystem');
\Yii::setAlias('logger', '@api/components/logger');
\Yii::setAlias('healthCheck', '@api/components/healthCheck');
\Yii::setAlias('paymentGate', '@api/components/paymentGate');

\logger\ApiLogger::setupStartTimeForStatistic();