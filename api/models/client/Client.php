<?php

namespace api\models\client;

use api\models\balance\Currency;
use bonusSystem\BonusSystem;
use Yii;
use api\models\tenant\TenantSetting;
use api\models\tenant\Tenant;
use app\models\balance\Account;
use yii\db\ActiveQuery;

/**
 * Class Client
 *
 * @property int $city_id
 *
 * @package api\models\client
 */
class Client extends \yii\db\ActiveRecord
{

    const PASSWORD_SIZE = 6;

    const TYPE_WINFON = 'WINFON';
    const TYPE_IOS = 'IOS';
    const TYPE_ANDROID = 'ANDROID';
    const TYPE_WEB = 'WEB';

    const TYPE_HOSPITAL = 'HOSPITAL';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password'], 'integer', 'on' => ['change-password', 'create']],

            [['tenant_id', 'city_id', 'app_id'], 'integer', 'on' => ['create']],

            [['city_id'], 'in', 'range' => array_keys(Tenant::getCityList($this->tenant_id)), 'on' => ['update']],
            [['device'], 'in', 'range' => self::getClentTypeList(), 'on' => ['update', 'create']],
            [['device_token'], 'safe', 'on' => ['update', 'create']],
            [['lang'], 'in', 'range' => Yii::$app->params['supportedLanguages'], 'on' => ['update', 'create']],

            [['photo'], 'string', 'max' => 255, 'on' => 'profile'],
            [['last_name', 'name', 'second_name', 'email'], 'string', 'max' => 45],
            [['email'], 'email'],
            [['photo'], 'file', 'extensions' => ['png', 'jpeg', 'jpg'], 'on' => 'profile'],
            [['birth'], 'match', 'pattern' => '/^[0-9]{4}+\-[0-9]{2}+\-[0-9]{2}$/', 'on' => 'profile'],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public function getIsBlackList()
    {
        return (int)$this->black_list === 1;
    }


    public function getFullName()
    {
        return implode(' ', [$this->last_name, $this->name, $this->second_name]);
    }

    public static function generatePassword()
    {
        $numerals = [];
        while (count($numerals) < 4) {
            $rand            = rand(1, 9);
            $numerals[$rand] = $rand;
        }
        $str_result = '';
        for ($i = 0; $i < self::PASSWORD_SIZE; $i++) {
            $str_result .= array_rand($numerals);
        }

        return $str_result;
    }

    public static function getClentTypeList()
    {
        return [self::TYPE_ANDROID, self::TYPE_IOS, self::TYPE_WINFON, self::TYPE_WEB, self::TYPE_HOSPITAL];
    }

    public static function findByPhone($tenant_id, $phone)
    {
        $model = ClientPhone::find()
            ->where([
                'value' => $phone,
            ])
            ->joinWith([
                'client' => function ($query) use ($tenant_id) {
                    $query->where([
                        'tenant_id' => $tenant_id,
                    ]);
                },
            ])
            ->one();

        return !is_null($model) ? $model->client : null;
    }

    public function savePhone($phone)
    {
        $model = ClientPhone::find()
            ->where([
                'value'     => $phone,
                'client_id' => $this->client_id,
            ])
            ->one();
        if (!$model) {
            $client_phone            = new ClientPhone();
            $client_phone->client_id = $this->client_id;
            $client_phone->value     = $phone;
            $client_phone->save();
        }
    }

    public function deletePhone($phone)
    {
        return ClientPhone::deleteAll([
            'value'     => $phone,
            'client_id' => $this->client_id,
        ]);
    }


    public function updateNameAndEmail($lastName, $name, $secondName, $email)
    {
        $isNeedSave = false;

        if ($lastName !== null) {
            $this->last_name = $lastName;

            if ($this->validate('last_name')) {
                $isNeedSave = true;
            } else {
                $this->last_name = $this->oldAttributes['last_name'];
            }
        }

        if ($name !== null) {
            $this->name = $name;
            if ($this->validate('name')) {
                $isNeedSave = true;
            } else {
                $this->name = $this->oldAttributes['name'];
            }
        }

        if ($secondName !== null) {
            $this->second_name = $secondName;

            if ($this->validate('second_name')) {
                $isNeedSave = true;
            } else {
                $this->second_name = $this->oldAttributes['second_name'];
            }
        }

        if ($email !== null) {
            $this->email = $email;

            if ($this->validate('email')) {
                $isNeedSave = true;
            } else {
                $this->email = $this->oldAttributes['email'];
            }
        }

        return $isNeedSave ? $this->save(false, ['last_name', 'name', 'second_name', 'email']) : true;
    }


    public function updateDeviceAndCityData($client_type, $device_id, $city_id, $lang, $appId)
    {
        $this->scenario = 'update';
        $client_type    = strtoupper($client_type);

        if (in_array($client_type, self::getClentTypeList())) {
            $this->device = $client_type;
        }

        if (!empty($city_id)) {
            $this->city_id = $city_id;
        }

        $this->app_id       = $appId;
        $this->device_token = $device_id;
        $this->lang         = $lang;

        $this->clearDeviceToken();

        return $this->save();
    }

    public function clearDeviceToken()
    {
        if ($this->device_token !== null) {
            $clients = self::find()
                ->where(['device_token' => $this->device_token])
                ->andWhere(['not', ['client_id' => $this->client_id]])
                ->all();
            foreach ($clients as $client) {
                $client->device_token = null;
                $client->update();
            }
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub

        if ($insert) {
            $this->clearDeviceToken();
        }
    }

    public static function staticUpdateDeviceAndCityData($client_id, $type_client, $device_id, $city_id, $lang, $appId)
    {
        $client = self::findOne($client_id);
        if (isset($client)) {
            return $client->updateDeviceAndCityData($type_client, $device_id, $city_id, $lang, $appId);
        }

        return false;
    }


    public function getPhotoUrl()
    {
        $domain = Tenant::getDomain($this->tenant_id);

        return !empty($this->photo) ? app()->params['frontend.protocol'] . '://' . $domain . '.'
            . app()->params['frontend.domain'] . '/file/show-external-file?filename=' . $this->photo
            . '&id=' . $this->tenant_id : '';
    }

    public function getMoneyBalance($currency_id)
    {
        $moneyData = Account::findOne([
            'owner_id'    => $this->client_id,
            'tenant_id'   => $this->tenant_id,
            'acc_kind_id' => Account::CLIENT_KIND,
            'currency_id' => $currency_id,
        ]);

        $balance = isset($moneyData->balance) ? $moneyData->balance : 0;

        return [
            "personal_balance_value"    => $this->getFormattedSum($balance, $currency_id),
            "personal_balance_currency" => (string)TenantSetting::getTenantCurrencyCode($this->tenant_id,
                $this->city_id),
        ];
    }

    public function getBonusBalance($currency_id)
    {
        try {
            $bonusSystem = \Yii::createObject(BonusSystem::class, [$this->tenant_id]);

            $bonusValue = $bonusSystem->getBalance($this->client_id, $currency_id);
        } catch (\yii\base\Exception $ex) {
            $bonusValue = 0;
        }

        return [
            "bonus_value" => $this->getFormattedSum($bonusValue, $currency_id),
            "bonus_name"  => 'B',
        ];
    }

    /**
     * Getting formatted sum
     *
     * @param float $sum
     * @param int   $currencyId
     *
     * @return float
     */
    public function getFormattedSum($sum, $currencyId)
    {
        $currencies = Currency::getCachedCurrencies();
        $minorUnit  = empty($currencies[$currencyId]->minor_unit) ? 0 : $currencies[$currencyId]->minor_unit;

        return floor(round((empty($sum) ? 0 : $sum) * pow(10, $minorUnit), 1))
            / pow(10, $minorUnit);
    }

    public function formatted()
    {
        $currency_id      = TenantSetting::getTenantCurrencyId($this->tenant_id, $this->city_id);
        $personal_balance = $this->getMoneyBalance($currency_id);
        $personal_bonus   = [$this->getBonusBalance($currency_id)];
        $domain           = Tenant::getDomain($this->tenant_id);

        $client_companies = Company::find()
            ->alias('t1')
            ->select([
                'client.company_id',
                'company_name'                => 't1.name',
                'company_logo'                => 't1.logo',
                'company_balance_value'       => 'account.balance',
                'company_balance_currency'    => 'currency.code',
                'company_balance_currency_id' => 'currency.currency_id',
            ])
            ->where([
                't1.tenant_id'     => $this->tenant_id,
                'client.client_id' => $this->client_id,
            ])
            ->joinWith([
                'hasClient client',
                'account account',
                'account.currency currency',
            ], false)
            ->asArray()
            ->all();

        $client_companies = array_map(function ($item) use ($domain, $personal_balance) {
            $item['company_logo']          = !empty($item['company_logo']) ? app()->params['frontend.protocol'] . '://' . $domain . '.'
                . app()->params['frontend.domain'] . '/file/show-external-file?filename=thumb_' . $item['company_logo']
                . '&id=' . $this->tenant_id : '';
            $item['company_balance_value'] = $this->getFormattedSum(
                $item['company_balance_value'], $item['company_balance_currency_id']);

            if ($item['company_balance_currency'] === null) {
                $item['company_balance_currency'] = $personal_balance['personal_balance_currency'];
            }

            return $item;
        }, $client_companies);

        $client_phones = ClientPhone::find()
            ->select(['phone' => 'value'])
            ->where([
                'client_id' => $this->client_id,
            ])
            ->asArray()
            ->all();

        $client_cards = [];

        $client_profile = [
            'client_id'        => $this->client_id,
            'city_id'          => $this->city_id,
            'surname'          => (string)$this->last_name,
            'name'             => (string)$this->name,
            'patronymic'       => (string)$this->second_name,
            'email'            => (string)$this->email,
            'birth'            => (string)$this->birth,
            'photo'            => $this->getPhotoUrl(),
            'personal_balance' => $personal_balance,
            'personal_bonus'   => $personal_bonus,
            'client_companies' => $client_companies,
            'client_phones'    => $client_phones,
            'client_cards'     => $client_cards,
        ];

        if (gtoet('1.2.0')) {
            unset($client_profile['personal_balance'], $client_profile['personal_bonus'],
                $client_profile['client_companies'], $client_profile['client_cards']);
        }

        return $client_profile;
    }

    /**
     * Получение баланса бонусов клиента с учетом валюты
     *
     * @param int $clientId
     * @param int $tenantId
     * @param int $currencyId
     *
     * @return int
     */
    public static function getClientBonusBalanceByCurrency($clientId, $tenantId, $currencyId)
    {
        $bonusData = Account::findOne([
            'owner_id'    => $clientId,
            'tenant_id'   => $tenantId,
            'acc_kind_id' => Account::CLIENT_BONUS,
            'currency_id' => $currencyId,
        ]);
        if (isset($bonusData->balance)) {
            return $bonusData->balance;
        }

        return 0;
    }

    public function isBlocked()
    {
        return $this->black_list == 1;
    }


    public function getBalance($currencyId)
    {
        $currency = Currency::findOne($currencyId);

        if (!$currency) {
            return [];
        }

        $code  = $currency->code;
        $minor = (int)$currency->minor_unit;

        $personalBalance = [
            'value' => (string)round($this->getPersonalBalance($currencyId), $minor),
        ];

        $personalBonus = [
            'value' => (string)round($this->getPersonalBonus($currencyId), $minor),
        ];

        $balanceCompany = $this->getBalanceCompany($currencyId);
        $balanceCompany = array_map(function ($company) use ($minor) {
            $value = round((float)$company['balance'], $minor);

            return [
                'company_id'   => (string)$company['company_id'],
                'company_name' => (string)$company['name'],
                'value'        => (string)$value,
            ];
        }, $balanceCompany);

        return [
            'currency'         => [
                'code' => $currency->code,
            ],
            'personal_balance' => $personalBalance,
            'personal_bonus'   => $personalBonus,
            'company_balance'  => $balanceCompany,
        ];
    }

    public function getPersonalBalance($currencyId)
    {
        $account = Account::find()
            ->select('balance')
            ->where([
                'acc_kind_id' => Account::CLIENT_KIND,
                'owner_id'    => $this->client_id,
                'currency_id' => $currencyId,
                'tenant_id'   => $this->tenant_id,
            ])
            ->scalar();

        return (float)$account;
    }

    public function getPersonalBonus($currencyId)
    {
        try {
            $bonusSystem = \Yii::createObject(BonusSystem::class, [$this->tenant_id]);

            $value = $bonusSystem->getBalance($this->client_id, $currencyId);
        } catch (\yii\base\Exception $ex) {
            $value = 0;
        }

        return (float)$value;
    }

    public function getBalanceCompany($currencyId)
    {
        $companies = Company::find()
            ->select([
                'c.company_id',
                'c.name',
                'a.balance',
            ])
            ->alias('c')
            ->where([
                'c.block'      => 0,
                'hc.client_id' => $this->client_id,
                //                'a.currency_id' => $currencyId,
            ])
            ->joinWith([
                'hasClient hc',
                'accounts a' => function (ActiveQuery $query) use ($currencyId) {
                    $query->onCondition([
                        'a.currency_id' => $currencyId,
                        'acc_kind_id'   => Account::COMPANY_KIND,
                    ]);
                },
            ], false)
            //            ->createCommand()->rawSql;
            ->asArray()
            ->all();

        return (array)$companies;
    }

}
