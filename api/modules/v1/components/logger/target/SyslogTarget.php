<?php

namespace api\modules\v1\components\logger\target;

use yii\base\Component;

class SyslogTarget extends Component implements LogTargetInterface
{
    /**
     * @var string
     */
    public $identity;

    public function export($message)
    {
        openlog($this->identity, LOG_ODELAY | LOG_PID, LOG_USER);
        syslog(LOG_INFO, $message);
        closelog();
    }
}