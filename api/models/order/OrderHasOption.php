<?php

namespace api\models\order;

class OrderHasOption extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%order_has_option}}';
    }

    public function rules()
    {
        return [
            [['order_id'], 'exist', 'targetClass' => '\api\models\order\Order'],
            [['option_id'], 'integer'],
        ];
    }

    public static function manySave($options, $order_id)
    {
        if (!empty($options) && is_array($options)) {
            $insertValue = [];

            foreach ($options as $option_id) {
                if ($option_id > 0) {
                    $insertValue[] = [$order_id, $option_id];
                }
            }

            if (!empty($insertValue)) {
                $connection = app()->db;
                $connection->createCommand()
                    ->batchInsert(self::tableName(), ['order_id', 'option_id'], $insertValue)
                    ->execute();
                return true;
            }
        }
        return false;
    }
}