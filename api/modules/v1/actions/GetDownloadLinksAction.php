<?php

namespace api\modules\v1\actions;

use api\models\tenant\mobileApp\MobileApp;
use api\modules\v1\requests\BaseApiRequest;

/**
 * Получение ссылки на клиентское приложение
 *
 * Class GetDownloadLinksAction
 *
 * @package api\modules\v1\actions
 * @property BaseApiRequest $request
 */
class GetDownloadLinksAction extends BaseApiAction
{
    protected function getRequest()
    {
        return new BaseApiRequest();
    }

    protected function execute()
    {
        $mobileApp = $this->findModel($this->request->appId);

        if (!$mobileApp) {
            $linkAndroid = $linkIos = '';
        } else {
            $linkAndroid = $mobileApp->link_android;
            $linkIos     = $mobileApp->link_ios;
        }

        $this->response->setContent([
            'android' => $linkAndroid,
            'ios'     => $linkIos,
        ]);
    }

    protected function findModel($appId)
    {
        return MobileApp::findOne($appId);
    }
}