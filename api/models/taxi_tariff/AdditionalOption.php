<?php

namespace api\models\taxi_tariff;

use api\models\transport\car\CarOption;
use Yii;

/**
 * This is the model class for table "{{%tbl_additional_option}}".
 *
 * @property string $id
 * @property string $tariff_id
 * @property integer $additional_option_id
 * @property string $price
 * @property string $tariff_type
 */
class AdditionalOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%additional_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(CarOption::className(), ['option_id' => 'additional_option_id']);
    }
}
