<?php

namespace api\modules\v1\exceptions;

use api\modules\v1\components\ErrorCode;

class BadParamsException extends BaseApiException
{
    public function __construct($message = '')
    {
        $code = ErrorCode::BAD_PARAM;
        parent::__construct($message, $code, null);
    }

}