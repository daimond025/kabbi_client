<?php

namespace api\modules\v1\repositories\dto;

use api\models\order\OrderStatus;

class OrderStatusData
{
    public $id;
    public $group;
    public $name;

    const redis = 1;

    public function __construct($id, $group, $name, $redis = 0)
    {
        // фильтрация статуса -
        if($redis === 1 && ($group === OrderStatus::FILTER_STATUS_2  || $group === OrderStatus::FILTER_STATUS_3 ) ){
            $this->id    = 110;
            $this->group = OrderStatus::FILTER_STATUS_1 ;
            $this->name  = "Execution of an order"  ;
        }else{
            $this->id    = (integer)$id;
            $this->group = $group;
            $this->name  = $name;
        }
    }
}
