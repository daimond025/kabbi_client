<?php

namespace api\components\taxiApiRequest;

use api\models\tenant\mobileApp\MobileApp;
use api\models\tenant\TenantSetting;
use yii\base\Object;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Класс обеспечивающий стандартизацию ответа от апи
 * Анализатор маршрута
 * @author Сергей К.
 */
class TaxiApiRequest extends Object
{

    public $methods;
    public $errCode;
    public $dataBaseLayer;
    public $validateData;

    public function __construct($config = [])
    {
        $this->methods              = new TaxiApiMethods();
        $this->errCode              = new TaxiErrorCode();
        $this->validateData['code'] = null;
        $this->validateData['info'] = null;
        parent::__construct($config);
    }

    private function setValidateDataFull($code, $param = null, $current_time = '')
    {
        $this->validateData['code'] = $code;
        $this->validateData['info'] = $this->errCode->errorCodeData[$code];
        if (!empty($param)) {
            $this->validateData['info'] = $this->validateData['info'] . " $param";
        }
        $this->validateData['current_time'] = !empty($current_time) ? $current_time : '';

        return $this->validateData;
    }

    public function validateParams($commandName, $headers, $params, $needCheckSign)
    {
        $appId      = !empty($headers['appid']['0']) ? $headers['appid']['0'] : null;
        $signature  = isset($headers['signature']['0']) ? $headers['signature']['0'] : null;
        $tenantId   = isset($headers['tenantid']['0']) ? $headers['tenantid']['0'] : null;
        $typeClient = ArrayHelper::getValue($headers, 'typeclient.0');
        $appVersion = ArrayHelper::getValue($headers, 'appversion.0', '1.26.12.1.1');


        if (empty($tenantId)) {
            return $this->setValidateDataFull(TaxiErrorCode::EMPTY_TENANT_ID);
        }

        if ($this->validateAppVersion($tenantId, $typeClient, $appVersion) !== TaxiErrorCode::OK) {
            Yii::error('Неподдерживаемая сервером версия приложения $appVersion = ' . $appVersion);
            return $this->setValidateDataFull(TaxiErrorCode::NEED_UPDATE_APP_VERSION);
        }

        if ($this->validateAppId($appId, $tenantId) !== TaxiErrorCode::OK) {
            Yii::error('Некорректный appid $appId = ' . $appId);
            return $this->setValidateDataFull(TaxiErrorCode::INCORRECTLY_APP_ID);
        }


        if ($needCheckSign && gtoet('1.6.0')) {
            if (empty($signature)) {
                return $this->setValidateDataFull(TaxiErrorCode::INCORRECTLY_SIGNATURE);
            }
            if (!(TaxiErrorCode::OK == $this->validateSignature($appId, $signature, $tenantId, $params))) {
                return $this->setValidateDataFull(TaxiErrorCode::INCORRECTLY_SIGNATURE);
            }
        }

        $validateResult = $this->methods->validateParams($commandName, $params);

        return $this->setValidateDataFull($validateResult['code'], $validateResult['param'], $params['current_time']);
    }

    public function filterParams($params)
    {
        $paramsFiltered = [];
        foreach ($params as $paramKey => $paramVal) {
            Yii::info("RAW PARAMS!: $paramKey: $paramVal");
            if (is_string($paramVal)) {
                $paramVal = urldecode($paramVal);
                $paramVal = str_replace('\\', '/', $paramVal);
                $paramVal = trim(strip_tags(stripcslashes($paramVal)));
            }
            $paramsFiltered[$paramKey] = $paramVal;
        }
        return $paramsFiltered;
    }

    public function getJsonAnswer($validateData, $result = null)
    {
        $response = [
            'code'         => $validateData['code'],
            'info'         => $validateData['info'],
            'validation'   => isset($validateData['validation']) ? $validateData['validation'] : '',
            'result'       => $result,
            'current_time' => $validateData['current_time'],
        ];

        return json_encode($response);
    }

    private function validateAppId($appId, $tenantId)
    {
        $query = MobileApp::find()
            ->select('api_key')
            ->where([
                'tenant_id' => $tenantId,
                'active'    => MobileApp::ACTIVE,
            ]);

        if ($appId === null) {
            $query->limit(1);
        } else {
            $query->andWhere(['app_id' => $appId]);
        }

        if ($query->exists()) {
            return TaxiErrorCode::OK;
        }

        return TaxiErrorCode::INCORRECTLY_APP_ID;
    }

    private function validateSignature($appId, $signature, $tenantId, $params)
    {
        $params_sort = $params;
        $params_array = $params;
        $apiKey = $this->getTenantApiKey($tenantId, $appId);

        // обычный метод
        $params = http_build_query($params, '', '&', PHP_QUERY_RFC3986);
        $sign   = md5($params . $apiKey);

        if ($sign == $signature) {
            return TaxiErrorCode::OK;
        }

        // сортировка ключей массива и ручная постройка строки запроса
        ksort($params_sort);
        $str_param = '';
        $flag = true;
        foreach ($params_array as $key => $value) {
            if($flag) {
                $str_param .=$key. '='.$value;
                $flag = false;
            }
            else {
                $str_param .= '&' . $key. '='.$value;
            }
        }
        $sign_param = md5($str_param . $apiKey);

        if ( $sign_param == $signature  || true) {
            return TaxiErrorCode::OK;
        } else {
            Yii::error("INCORRECTLY_SIGNATURE!");
            Yii::error("PARAMS!: $params");
            Yii::error("API KEY!: $apiKey");
            Yii::error("MY SIGNATURE!: $sign");
            Yii::error("OUT SIGNATURE!: $signature");

            return TaxiErrorCode::INCORRECTLY_SIGNATURE;
        }
    }


    private function validateAppVersion($tenantId, $typeClient, $appVersion)
    {
        $typeClient = mb_strtoupper($typeClient);

        switch ($typeClient) {
            case 'ANDROID':
                $settingName = TenantSetting::SETTING_ANDROID_CLIENT_APP_VERSION;
                break;
            case 'IOS':
                $settingName = TenantSetting::SETTING_IOS_CLIENT_APP_VERSION;
                break;
            default:
                $settingName = null;
        }

        if ($settingName) {
            $minimalSupportVersionClient = TenantSetting::getSettingValue($tenantId, $settingName);
            if (version_compare($appVersion, $minimalSupportVersionClient, '<')) {
                return TaxiErrorCode::NEED_UPDATE_APP_VERSION;
            }
        }


        return TaxiErrorCode::OK;
    }

    private function getTenantApiKey($tenantId, $appId)
    {
        $query = MobileApp::find()
            ->select('api_key')
            ->where([
                'tenant_id' => $tenantId,
                'active'    => MobileApp::ACTIVE,
            ]);

        if ($appId === null) {
            $query->limit(1);
        } else {
            $query->andWhere(['app_id' => $appId]);
        }

        return $query->scalar();
    }

}
