<?php

namespace api\models\tenant\mobileApp;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class MobileApp
 *
 * @property int $app_id
 * @property int $tenant_id
 * @property string $name
 * @property int $sort
 * @property int $active
 * @property string $api_key
 * @property string $push_key_android
 * @property string $push_key_ios
 * @property string $link_android
 * @property string $link_ios
 * @property string $page_info
 * @property string $confidential
 *
 * @property bool $isActive
 *
 * @package common\modules\tenant\mobileApp
 */
class MobileApp extends ActiveRecord
{

    const ACTIVE = '1';

    public static function tableName()
    {
        return '{{%mobile_app}}';
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->active === 1;
    }

    public function setIsActive($value)
    {
        $this->active = (int)$value;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasCity()
    {
        return $this->hasMany(MobileAppHasCity::className(), ['app_id' => 'app_id']);
    }

    public function getCityList()
    {
        return ArrayHelper::getColumn($this->hasCity, 'city_id');
    }
}