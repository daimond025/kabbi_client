<?php

namespace api\exceptions;

use api\components\taxiApiRequest\TaxiErrorCode;

class EmptyInDbException extends \Exception
{
    
    protected $code = TaxiErrorCode::EMPTY_DATA_IN_DATABASE;
    protected $message = 'EMPTY_DATA_IN_DATABASE';
    
}
