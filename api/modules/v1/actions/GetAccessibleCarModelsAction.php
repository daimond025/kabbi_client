<?php

namespace api\modules\v1\actions;

use api\modules\v1\requests\GetAccessibleCarModelsRequest;
use api\modules\v1\services\CarService;
use yii\rest\Controller;

/**
 * Class GetAccessibleCarModelsAction
 *
 * @package api\modules\v1\actions
 * @property GetAccessibleCarModelsRequest $request
 */
class GetAccessibleCarModelsAction extends BaseApiAction
{
    public $carService;

    public function __construct($id, Controller $controller, CarService $carService, array $config = [])
    {
        parent::__construct($id, $controller, $config);

        $this->carService = $carService;
    }

    protected function getRequest()
    {
        return new GetAccessibleCarModelsRequest();
    }

    protected function execute()
    {
        $this->response->setContent(
            $this->carService->getAccessibleCarModelsByTariff($this->request->tenantId, $this->request->tariffId)
        );
    }

}